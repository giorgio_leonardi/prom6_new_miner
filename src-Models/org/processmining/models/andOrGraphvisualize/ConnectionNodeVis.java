/**non serve pi� rappresentava connettivi fra eventi (nodi struttura )*/

package org.processmining.models.andOrGraphvisualize;

import java.awt.Dimension;
import java.awt.Graphics2D;

import javax.swing.SwingConstants;

import org.processmining.models.andOrGraph.ConnectionNode;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.shapes.Decorated;
import org.processmining.models.shapes.Diamond;

public class ConnectionNodeVis extends ConnectionNode implements Decorated {

	private AttributeMap att;
	
	public ConnectionNodeVis(ConnectionNode d){
		super(d.getLabel());
		att=new AttributeMap();
		att.put(AttributeMap.LABEL, d.getLabel());
		att.put(AttributeMap.SHOWLABEL, true);
		att.put(AttributeMap.LABELVERTICALALIGNMENT, SwingConstants.CENTER);
		att.put(AttributeMap.LABELHORIZONTALALIGNMENT, SwingConstants.CENTER);
		att.put(AttributeMap.SIZE, new Dimension(80, 40));
		att.put(AttributeMap.SHAPE, new Diamond());
		att.put(AttributeMap.SQUAREBB, false);
		att.put(AttributeMap.RESIZABLE, false);
	}
	
	public AttributeMap getAttributeMap() {
		// TODO Auto-generated method stub
		return att;
	}

	public void decorate(Graphics2D g2d, double x, double y, double width, double height) {
		
	/*	
		final int labelX = (int) Math.round(x + 15 + (2 * 3) + 10);
		final int labelY = (int) Math.round(y);
		final int labelW = (int) Math.round(width - 2 * (15 + (2 * 3) + 10));
		final int labelH = (int) Math.round(height-20);

		JLabel label = new JLabel(getLabel());
		label.setPreferredSize(new Dimension(labelW, labelH));
		label.setSize(new Dimension(labelW, labelH));

		label.setFont(new Font(label.getFont().getFamily(), label.getFont().getStyle(), 8));
		label.validate();
		label.paint(g2d.create(labelX, labelY, labelW, labelH));
	*/
	}
}
