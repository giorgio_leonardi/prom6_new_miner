package org.processmining.models.andOrGraphvisualize;

import java.awt.Graphics2D;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.SwingConstants;



import org.processmining.models.andOrGraph.Edge;
import org.processmining.models.andOrGraph.EventsNode;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.NodeID;
import org.processmining.models.graphbased.directed.DirectedGraphElement;
import org.processmining.models.shapes.Decorated;

public class JGraphAndOr extends GraphAndOr implements Decorated {
	
	private AttributeMap att;

	/**crea un grafo adatto alla visualizzazione a partire da graph*/
	public JGraphAndOr(GraphAndOr graph){
		this(graph.getLabel(),graph.getAnnotated());
		Set<Node> nodes=graph.getNodes();
		Map<NodeID,EventNodeVis> map=new TreeMap<NodeID,EventNodeVis>();
		//aggiunge i nodi presenti in graph
		for (Node d: nodes){
			EventNodeVis n=new EventNodeVis((EventsNode)d);
			addNode(n);
			graphElementAdded(n);
			map.put(d.getId(), n);
		}
		//aggiunge gli archi
		Set<Edge> edges=graph.getEdges();
		for (Edge d:edges){
			EventNodeVis source=map.get(d.getSource().getId());
			EventNodeVis target=map.get(d.getTarget().getId());
			String label=d.getLabel();
			
			if(source==null)
				System.out.println("JGraphAndOr null -> "+target.toString());
			if(target==null)
				System.out.println("JGraphAndOr "+source.toString() +" -> null");
			
			if(source == null || target ==null) 
				continue;
			
			Edge e;
			if (!getAnnotated())
				e=new EdgeVis(source,target,d.isAddedNode());
			else e=new EdgeVis(source,target,Double.parseDouble(label),d.isAddedNode());
			graphElementAdded(e);
			addEdge(e);
		}
	} 
	
	

	/**costruttore privato*/
	private JGraphAndOr(String label,boolean b){
		super(label,b);
		att=new AttributeMap();
		att.put(AttributeMap.PREF_ORIENTATION, SwingConstants.NORTH);
		att.put(AttributeMap.AUTOSIZE, true);
		att.put(AttributeMap.LABEL, label);
	}
	
	
	/**non implementato*/
	public void decorate(Graphics2D g2d, double x, double y, double width, double height) {
		// TODO Auto-generated method stub
		
	}
	
	/**non implementato*/
	protected Map<? extends DirectedGraphElement, ? extends DirectedGraphElement> cloneFrom(
			JGraphAndOr graph) {
		// TODO Auto-generated method stub
		return super.cloneFrom(graph);
	}
	
	/**mappa degli attributi*/
	public AttributeMap getAttributeMap() {
		// TODO Auto-generated method stub
		return att;
	}


}
