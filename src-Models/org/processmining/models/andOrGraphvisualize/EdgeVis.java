package org.processmining.models.andOrGraphvisualize;

import java.awt.Color;
import java.awt.Graphics2D;

import org.processmining.models.andOrGraph.Edge;
import org.processmining.models.andOrGraph.EventsNode;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.AttributeMap.ArrowType;
import org.processmining.models.shapes.Decorated;

public class EdgeVis extends Edge implements Decorated {
	
	private AttributeMap map;
	/**per ora non implementato*/
	public void decorate(Graphics2D g2d, double x, double y, double width, double height) {
		// TODO Auto-generated method stub
	}


	
	/**senza visualizzazione della label*/
	public EdgeVis(EventNodeVis source, EventNodeVis target,boolean b){
		super(source,target);
		map=new AttributeMap();
		map.put(AttributeMap.EDGEEND, ArrowType.ARROWTYPE_CLASSIC);
		map.put(AttributeMap.EDGEENDFILLED, true);		
		map.put(AttributeMap.SHOWLABEL, false);
		if (b)
				map.put(AttributeMap.EDGECOLOR, Color.ORANGE);
	}

	
	public EdgeVis(EventNodeVis source, EventNodeVis target,
			double d, boolean b) {
		super(source,target,d);
		map=new AttributeMap();
		map.put(AttributeMap.EDGEEND, ArrowType.ARROWTYPE_CLASSIC);
		map.put(AttributeMap.EDGEENDFILLED, true);
		map.put(AttributeMap.LABEL,getLabel());
		map.put(AttributeMap.SHOWLABEL, true);
		if (b)
			map.put(AttributeMap.EDGECOLOR, Color.ORANGE);
	}

	@Override
	public AttributeMap getAttributeMap() {
		// TODO Auto-generated method stub
		return map;
	}
}
