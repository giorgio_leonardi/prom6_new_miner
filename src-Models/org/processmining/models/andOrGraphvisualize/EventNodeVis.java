package org.processmining.models.andOrGraphvisualize;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

import javax.swing.SwingConstants;

import org.processmining.models.andOrGraph.EventsNode;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.shapes.Decorated;
import org.processmining.models.shapes.Rectangle;

public class EventNodeVis extends EventsNode implements Decorated {


	private AttributeMap att;
	
	public EventNodeVis(EventsNode e){
		super (e.getEvent(),e.isJolly(),e.getOccNumber());
		setRetrieve(e.isRetrieve());
		att=new AttributeMap();
	//	att.put(AttributeMap.LABEL, e.getLabel()+" \n ("+e.getOccNumber()+")");
		att.put(AttributeMap.LABEL, e.getLabel()+" ("+e.getOccNumber()+")");
		att.put(AttributeMap.SHOWLABEL, true);
		att.put(AttributeMap.LABELVERTICALALIGNMENT, SwingConstants.TOP);
		att.put(AttributeMap.LABELHORIZONTALALIGNMENT, SwingConstants.CENTER);
		att.put(AttributeMap.SIZE, new Dimension(120, 50));
		att.put(AttributeMap.SHAPE, new Rectangle(false));
		att.put(AttributeMap.SQUAREBB, false);
		att.put(AttributeMap.RESIZABLE, false);
		//att.put(AttributeMap.AUTOSIZE, true);
		switch (isRetrieve()) {
		case NORMAL:
			att.put(AttributeMap.FILLCOLOR,Color.white);
			break;
		case QUERY_RETRIEVED: 
			att.put(AttributeMap.FILLCOLOR,Color.red);
			break;
		case DUMMY_RETRIEVED:
			att.put(AttributeMap.FILLCOLOR,Color.YELLOW);
			break;
		case MIXED_RETRIEVED:
			att.put(AttributeMap.FILLCOLOR,Color.green);
			break;
		case NEW_NODE:
			att.put(AttributeMap.FILLCOLOR,Color.ORANGE);
		}
	}
	
	public AttributeMap getAttributeMap() {
		// TODO Auto-generated method stub
		return att;
	}
	
	public void decorate(Graphics2D g2d, double x, double y, double width, double height) {
		
		/*
		final int labelX = (int) Math.round(x + 15 + (2 * 3) + 10);
		final int labelY = (int) Math.round(y);
		final int labelW = (int) Math.round(width - 2 * (15 + (2 * 3) + 10));
		final int labelH = (int) Math.round(height-20);

		JLabel label = new JLabel(getLabel());
		label.setPreferredSize(new Dimension(labelW, labelH));
		label.setSize(new Dimension(labelW, labelH));

		label.setFont(new Font(label.getFont().getFamily(), label.getFont().getStyle(), 8));
		label.validate();
		label.paint(g2d.create(labelX, labelY, labelW, labelH));
		*/
	}
}
