package org.processmining.models.patterntree;

import java.awt.Dimension;
import java.util.HashSet;
import java.util.Set;

import javax.swing.SwingConstants;

import org.processmining.models.connections.GraphLayoutConnection;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.AbstractDirectedGraph;
import org.processmining.models.graphbased.directed.AbstractDirectedGraphNode;
import org.processmining.models.shapes.Ellipse;
import org.processmining.models.shapes.Rectangle;

/**
 * @author Jiafei Li
 * @email jiafei@jlu.edu.cn
 * @version Mar 8, 2010
 */

public abstract class PatternTreeNode extends AbstractDirectedGraphNode {
	private final Set<String> labelAlphabet;
	private final AbstractDirectedGraph<PatternTreeNode, PatternTreeEdge<? extends PatternTreeNode, ? extends PatternTreeNode>> graph;
	private String nameToEdit;
	Double alphabetTotalCount;
	Double alphabetStandaloneCount;
	Double alphabetConservednessCount;

	public PatternTreeNode(
			AbstractDirectedGraph<PatternTreeNode, PatternTreeEdge<? extends PatternTreeNode, ? extends PatternTreeNode>> graph,
			Set<String> label) {
		super();

		// to be deleted
		this.graph = graph;
		labelAlphabet = label;
		if (label.size() == 1) {
			getAttributeMap().put(AttributeMap.SHAPE, new Ellipse());
			//getAttributeMap().put(AttributeMap.SIZE, new Dimension(label.toString().length() * 3, 60));
			//getAttributeMap().put(AttributeMap.SIZE, new Dimension(150, 60));

			getAttributeMap().put(AttributeMap.LABEL, label.toString());
		} else {
			getAttributeMap().put(AttributeMap.SHAPE, new Rectangle(true));
			//getAttributeMap().put(AttributeMap.SIZE, new Dimension(label.toString().length() * 3, 60));
			//getAttributeMap().put(AttributeMap.SIZE, new Dimension(150, 60));
			getAttributeMap().put(AttributeMap.LABEL, label.toString());
		}

		getAttributeMap().put(AttributeMap.SHOWLABEL, true);
		getAttributeMap().put(AttributeMap.LABELHORIZONTALALIGNMENT, SwingConstants.CENTER);
		getAttributeMap().put(AttributeMap.SQUAREBB, false);
		getAttributeMap().put(AttributeMap.RESIZABLE, true);
		//getAttributeMap().put(AttributeMap.SIZE, new Dimension(label.toString().length()*20, 60));
		//getAttributeMap().put(AttributeMap.POSITION, new Point2D.Double(10, 10));
	}

	public AbstractDirectedGraph<PatternTreeNode, PatternTreeEdge<? extends PatternTreeNode, ? extends PatternTreeNode>> getGraph() {
		return graph;
	}

	public Set<String> getLabelAlphabet() {
		return labelAlphabet;
	}

	/**
	 * get Label of the node
	 */
	public String getLabel() {
		return getAttributeMap().get(AttributeMap.LABEL).toString();
	}

	/**
	 * set Label of the node
	 * 
	 * @param newLabel
	 */
	public void setLabel(String newLabel) {
		getAttributeMap().remove(AttributeMap.LABEL);
		getAttributeMap().put(AttributeMap.LABEL, newLabel);
		getAttributeMap().put(AttributeMap.SIZE, new Dimension(newLabel.length() * 5, 60));
	}

	/**
	 * Get the pattern name for editing.
	 * 
	 * @return name to display
	 */
	public String getNameToEdit() {
		return nameToEdit;
	}

	/**
	 * Set the name of pattern for editing. the format is
	 * "event name-event type" if the pattern contains a single alphabet.
	 * otherwise, the value is the pattern name only.
	 * 
	 * @param nameToDisplay
	 */
	public void setNameToEdit(String newNameToEdit) {
		nameToEdit = newNameToEdit;
	}

	public Set<PatternTreeNode> getPredecessors() {
		Set<PatternTreeNode> predecessor = new HashSet<PatternTreeNode>();
		Set<PatternTreeEdge<? extends PatternTreeNode, ? extends PatternTreeNode>> edges = graph.getEdges();
		for (PatternTreeEdge<? extends PatternTreeNode, ? extends PatternTreeNode> edge : edges) {
			if (edge.getTarget().equals(this)) {
				predecessor.add(edge.getSource());
			}
		}
		return predecessor;
	}

	public Set<PatternTreeNode> getSuccessors() {
		Set<PatternTreeNode> predecessor = new HashSet<PatternTreeNode>();
		Set<PatternTreeEdge<? extends PatternTreeNode, ? extends PatternTreeNode>> edges = graph.getEdges();
		for (PatternTreeEdge<? extends PatternTreeNode, ? extends PatternTreeNode> edge : edges) {
			if (edge.getSource().equals(this)) {
				predecessor.add(edge.getTarget());
			}
		}
		return predecessor;
	}

	public Double getAlphabetTotalCount() {
		return alphabetTotalCount;
	}

	public void setAlphabetTotalCount(Double alphabetTotalCount) {
		this.alphabetTotalCount = alphabetTotalCount;
	}

	public Double getAlphabetStandaloneCount() {
		return alphabetStandaloneCount;
	}

	public void setAlphabetStandaloneCount(Double alphabetStandaloneCount) {
		this.alphabetStandaloneCount = alphabetStandaloneCount;
	}

	public Double getAlphabetConservednessCount() {
		return alphabetConservednessCount;
	}

	public void setAlphabetConservednessCount(Double alphabetConservednessCount) {
		this.alphabetConservednessCount = alphabetConservednessCount;
	}

	//public void set
}