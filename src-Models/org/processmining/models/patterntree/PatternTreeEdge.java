package org.processmining.models.patterntree;

import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.AttributeMap.ArrowType;
import org.processmining.models.graphbased.directed.AbstractDirectedGraphEdge;

/**
 * @author Jiafei Li
 * @email jiafei@jlu.edu.cn
 * @version Mar 8, 2010
 */
public abstract class PatternTreeEdge<S extends PatternTreeNode, T extends PatternTreeNode> extends
		AbstractDirectedGraphEdge<S, T> {

	public PatternTreeEdge(S source, T target) {
		super(source, target);
		//	getAttributeMap().put(AttributeMap.SHOWLABEL, false);
		getAttributeMap().put(AttributeMap.EDGEEND, ArrowType.ARROWTYPE_TECHNICAL);
		getAttributeMap().put(AttributeMap.EDGEENDFILLED, true);
		//	getAttributeMap().put(AttributeMap.LABEL, source.toString() + " -->>" + target.toString());
	}

}