package org.processmining.models.andOrGraph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class EventPermutation {
	
	//insieme delle occorrenze di un evento (traccia, posizione nella traccia)
	private ArrayList<Pair<Integer,Integer>> occurs;
	
	private String name;

	public EventPermutation(Collection<String> perm) {
		name="";
		for (String s:perm)
			name+=s+"&&";
		name=name.substring(0,name.length()-2);
		occurs=new ArrayList<Pair<Integer,Integer>>();
	}

	public void addOccurrence(int traccia, int pos) {
		Pair<Integer,Integer> p= new Pair<Integer, Integer>(traccia,pos );
		occurs.add(p);
	}

	public ArrayList<Pair<Integer, Integer>> getOccurs() {
		return occurs;
	}

	public Set<Integer> getTraces() {
		Set<Integer> result=new TreeSet<Integer>();
		for (Pair<Integer, Integer> o:occurs)
			if (o.isEnabled())
				result.add(o.getFirst());
		return result;
	}

	public void addOccurrences(ArrayList<Pair<Integer, Integer>> occ) {
		occurs.addAll(occ);
	}


	public String toString(){
		String result="["+name+"]/"+"{";
		for (Pair<Integer, Integer> o:occurs)
			if (o.isEnabled())
				result+=o.getFirst()+"-"+o.getSecond()+",";
		return result.substring(0, result.length()-1)+"}";
	
	}

	public String getName() {
		
		return name;
	}




	public void removeTrace(Set<Integer> logRed) {
		for (Pair<Integer, Integer> p:occurs){
			if (logRed.contains(p.getFirst()))
				p.toggle();
		}
	}




	public void resetTrace() {
		for (Pair<Integer, Integer> p:occurs){
			p.toggle();
		}
	}




	public void resetTrace(Set<Integer> second) {
		for (Pair<Integer, Integer> p:occurs){
			if (second.contains(p.getFirst()) && !p.isEnabled())
				p.toggle();
		}
	}
	
}
