package org.processmining.models.andOrGraph;

import org.processmining.models.graphbased.directed.DirectedGraphEdge;

public class Edge extends DirectedGraphEdgeImpl<EventsNode, EventsNode> implements DirectedGraphEdge<EventsNode, EventsNode>{

	public Edge(EventsNode source, EventsNode target) {
		super(source, target);
	}

	public Edge(EventsNode source, EventsNode target,double d){
		
			super (source,target, d);
			}

	public Edge(EventsNode source, EventsNode target, int d, boolean b) {
		super (source,target, d,b);
	}
}
