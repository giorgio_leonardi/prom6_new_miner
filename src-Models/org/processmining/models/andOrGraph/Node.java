package org.processmining.models.andOrGraph;

import java.util.Set;

import org.processmining.models.graphbased.NodeID;
import org.processmining.models.graphbased.directed.DirectedGraphNode;

public interface Node extends DirectedGraphNode{
	public static final int NORMAL=0;
	public static final int QUERY_RETRIEVED=1;
	public static final int DUMMY_RETRIEVED=2;
	public static final int MIXED_RETRIEVED =3;
	public static final int NEW_NODE = 4;
	
	public Event getEvent();
	public Set<DirectedGraphEdgeImpl> getOutEdge();
	public void addOutEdge(DirectedGraphEdgeImpl<?, ?> outEdge);
	public int getOccNumber();
	public Set<DirectedGraphEdgeImpl> getInEdge();
	public void addInEdge(DirectedGraphEdgeImpl<?, ?> inEdge);
	public Set<Integer> getTraces();
	public void resetEdge();
	public boolean isLeaf();
	public int isRetrieve();
	public void setRetrieve(int retrieve);

}
