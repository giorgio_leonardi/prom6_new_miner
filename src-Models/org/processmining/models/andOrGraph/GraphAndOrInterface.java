package org.processmining.models.andOrGraph;

import java.util.Collection;
import java.util.Set;

import org.processmining.models.graphbased.directed.DirectedGraph;
import org.processmining.models.graphbased.directed.DirectedGraphNode;

public interface GraphAndOrInterface extends  DirectedGraph<Node, Edge>{

	public void addNode(Node n);
	public boolean addEdge(Edge e);
	public Set<Node> getNodes() ;
	public void removeNode(DirectedGraphNode node);
	public String getLabel();
	public Set<Edge> getEdges() ;
	public Set<Node> getLeafs();
	public Set<Node> getNodesByLabel(String label);
	public Set<Node> getNodesLoop();
	public int nodeCount();
	public Collection<Node> getUniqueNode(boolean a);
	public Node getRoot() ;
}
