package org.processmining.models.andOrGraph;

import java.util.Set;

import org.processmining.models.graphbased.NodeID;
import org.processmining.models.graphbased.directed.AbstractDirectedGraph;
import org.processmining.models.graphbased.directed.AbstractDirectedGraphNode;
import org.processmining.models.graphbased.directed.DirectedGraphNode;

public class ConnectionNode extends AbstractDirectedGraphNode {

	private String label;
	private NodeID ID;
	private Set<DirectedGraphEdgeImpl> outEdge;
	private DirectedGraphEdgeImpl inEdge;

	
	public ConnectionNode(String label){
		this.label=label;
		ID=new NodeID();
	}
	
	public String getLabel() {
		// TODO Auto-generated method stub
		return label;
	}

	public AbstractDirectedGraph<?, ?> getGraph() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public int compareTo(DirectedGraphNode o) {
		// TODO Auto-generated method stub
		return ID.compareTo(o.getId());
	}

	public NodeID getId() {
		// TODO Auto-generated method stub
		return ID;
	}

	public Set<DirectedGraphEdgeImpl> getOutEdge() {
		return outEdge;
	}

	public void setOutEdge(Set<DirectedGraphEdgeImpl> outEdge) {
		this.outEdge = outEdge;
	}

	public DirectedGraphEdgeImpl getInEdge() {
		return inEdge;
	}

	public void setInEdge(DirectedGraphEdgeImpl inEdge) {
		this.inEdge = inEdge;
	}

	public String toString(){
		return label;
	}

	
		
}
