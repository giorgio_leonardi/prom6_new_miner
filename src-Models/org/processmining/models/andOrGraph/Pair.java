package org.processmining.models.andOrGraph;

import java.util.Iterator;
import java.util.TreeSet;

/**implementazione di una coppia generica*/
public class Pair<A,B> implements Cloneable,Comparable<Pair<A, B>>{

	private A first;
    private B second;
    private boolean enable;
    
    public Pair(A first, B second) {
    	super();
    	this.first = first;
    	this.second = second;
    	enable=true;
    }

    public int hashCode() {
    	int hashFirst = first != null ? first.hashCode() : 0;
    	int hashSecond = second != null ? second.hashCode() : 0;

    	return (hashFirst + hashSecond) * hashSecond + hashFirst;
    }

    public boolean equals(Object other) {
    	if (other instanceof Pair) {
    		@SuppressWarnings("unchecked")
			Pair<A,B> otherPair = (Pair<A, B>) other;
    		return 
    		((  this.first == otherPair.first ||
    			( this.first != null && otherPair.first != null &&
    			  this.first.equals(otherPair.first))) &&
    		 (	this.second == otherPair.second ||
    			( this.second != null && otherPair.second != null &&
    			  this.second.equals(otherPair.second))) );
    	}

    	return false;
    }

    public String toString()
    { 
           return "(" + first + ", " + second + ")"; 
    }

    public A getFirst() {
    	return first;
    }

    public void setFirst(A first) {
    	this.first = first;
    }

    public B getSecond() {
    	return second;
    }

    public void setSecond(B second) {
    	this.second = second;
    }
    
    public Object clone() throws CloneNotSupportedException{
    	return super.clone();
    }

	@Override
	public int compareTo(Pair<A, B> o) {
		int c=((Comparable<A>)first).compareTo(o.getFirst());
		if (c==0) {
			if (((TreeSet<B>)(second)).isEmpty()) c= -1; 
			else if (((TreeSet<B>)(o.getSecond())).isEmpty()) c= 1; 
			else {
				TreeSet ts1 = (TreeSet<B>)(second);
				TreeSet ts2= (TreeSet<B>)(o.getSecond());
				Iterator it1= ts1.iterator();
				Iterator it2= ts2.iterator();
				System.out.println ("this.second : " + ts1.toString());
				System.out.println ("o.second : " + ts2.toString());
				while (it1.hasNext() && it2.hasNext()) {
					Integer i1= (Integer)(it1.next());
					Integer i2= (Integer)(it2.next());
					c= i1.compareTo (i2);
					if (c != 0) return c;
				}
				if (it1.hasNext()) return 1;
				else if (it2.hasNext()) return -1;
				else return 0;
//				c= ((Comparable<B>)second).compareTo(o.getSecond());
			}
		}
		return c;
	}
	
	public void toggle(){
		enable=!enable;
	}

	public boolean isEnabled() {
		
		return enable;
	}
}
