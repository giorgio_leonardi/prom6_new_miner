package org.processmining.models.andOrGraph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;

import permutation.Permutation;

public class Event implements Comparable<Event>, Cloneable {
	private String name; // nome dell'evento pu� essere un evento singolo oppure
							// un anyorder
	private int count; // numero di occorrenze dell'evento
	private Vector<EventPermutation> eventPermutation; // lista delle
														// permutazioni se � un
														// evento anyorder. nel
														// caso in cui sia un
														// evento singolo ci
														// sar� solo un elemento
	private int id;
	private static int cont = 0;
	private static int dim;
	private Set<String> event = new TreeSet<String>();

	/* costruttore */
	public Event(String name) {
		id = cont++;
		// inserisco tutte le permutazioni
		Collection<String> perm = Permutation.Initialize(name);
		eventPermutation = new Vector<EventPermutation>();
		// johnson trotter x ogni permutazione
		
		EventPermutation ep = new EventPermutation(perm);
		eventPermutation.add(ep);
		this.name=ep.getName();
		while ((perm=Permutation.getPermutation())!=null){
			ep=new EventPermutation(perm);
			eventPermutation.add(ep);
		}
		Permutation.close();
		event = new TreeSet<String>();
		StringTokenizer st = new StringTokenizer(name, "&&");
		while (st.hasMoreTokens())
			event.add(st.nextToken());
		dim = event.size();
	}

	/** aggiunge una nuova occorrenza (traccia,pos) della permutazione perm **/
	public void inc(int traccia, int pos, String perm) {
		EventPermutation ep = null;
		if (eventPermutation.size() > 1) {

			ep = searchPermutation(perm);
		} else
			ep = eventPermutation.get(0);

		ep.addOccurrence(traccia, pos);
		count++;
	}

	private EventPermutation searchPermutation(String name2) {
		for (EventPermutation e : eventPermutation)
			if (e.getName().equals(name2))
				return e;
		return null;
	}

	/** ritorna il numero di occorrenze */
	public int getCount() {
		return count;
	}

	/** ritorna il nome dell'evento */
	public String getName() {
		return name;
	}
	
	public void setName(String new_name) {
		this.name = new_name;
	}

	/** compara l'evento in base al nome */
	public int compareTo(Event arg0) {
		// TODO da rivedere pu� darsi che debba vedere tutte le permutazioni.
		// forse � meglio salvare come nome la prima permutazione
		return name.toLowerCase().compareTo(arg0.getName().toLowerCase());
	}

	public String toString() {
		String result="";
		for (EventPermutation e:eventPermutation)
			result+=e.toString()+"|";
		return result.substring(0,result.length()-1);
	}

	/** ritorna la lista delle occorrenze **/
	public ArrayList<Pair<Integer, Integer>> getOccurs() {
		ArrayList<Pair<Integer, Integer>> result = eventPermutation.get(0)
				.getOccurs();
		for (int i = 1; i < eventPermutation.size(); i++)
			result.addAll(eventPermutation.get(i).getOccurs());
		return result;
	}

	/** torna l'id */
	public int getID() {
		return id;
	}

	/** insieme delle tracce dove compare l'evento */
	public Collection<Integer> getTrace() {
		Set<Integer> result = eventPermutation.get(0).getTraces();
		for (int i = 0; i < eventPermutation.size(); i++)
			result.addAll(eventPermutation.get(i).getTraces());
		return result;
	}

	/**
	 * aggiungo un insieme di occorrenze
	 * 
	 * @param log
	 */
	public void setOccur(ArrayList<Pair<Integer, Integer>> occ, XLog log) {
		EventPermutation ep = null;
		int trace;
		int index, prevIndex;
		String name;
		if (dim > 1) {
			Collections.sort(occ);
			for (Pair<Integer, Integer> p : occ) {
				trace = p.getFirst();
				index = p.getSecond();
				prevIndex = index;
				String ev;
				int c=0;
				name = "";
				boolean esci = false;
				while (index < log.get(trace).size() && !esci) {

					ev = XLogInfoImpl.NAME_CLASSIFIER.getClassIdentity(log.get(
							trace).get(index));
					if (event.contains(ev) && c<dim) {
						name += ev + "&&";
						index++;
						c++;
					} else
						esci = true;
				}
				EventPermutation evPerm = searchPermutation(name.substring(0,
						name.length() - 2));
				if (evPerm != null){
					evPerm.addOccurrence(trace, prevIndex);
					count++;
				}
			}
		} else {
			ep = eventPermutation.get(0);
			ep.addOccurrences(occ);
			count+=occ.size();
		}
	}

	public Object clone() throws CloneNotSupportedException {
		Event e= (Event) super.clone();
		e.event=event;
		e.id=id;
		e.eventPermutation=(Vector<EventPermutation>) eventPermutation.clone();
		e.count=count;
		e.name=name;
		return e ;
	}

	public void setCount(int c) {
		count = c;
	}

	public boolean isSubset(Event set) {
		Set<String> copy = new TreeSet<String>(event);
		copy.removeAll(set.getEvents());
		return copy.isEmpty();
	}

	public Collection<String> getEvents() {
		return event;
	}

	public boolean isAnd() {
		return dim >= 2;
	}

	public int getDimension() {

		return event.size();
	}

	public double numberOccInTraces(Set<Integer> traces) {
		Set<Integer> copy = new TreeSet<Integer>(traces);
		copy.retainAll(this.getTrace());
		return copy.size();
	}

	public ArrayList<String> getPermutations() {
		ArrayList<String> perms=new ArrayList<String>();
		for(EventPermutation s:eventPermutation){
			perms.add(s.getName());
		}
		return perms;
	}

	public ArrayList<Pair<Integer, Integer>> getOccurs(String s) {
		for (EventPermutation perm:eventPermutation)
			if (perm.getName().equals(s))
				return perm.getOccurs(); 
		return null;
	}

	public void addOccurs(ArrayList<Pair<Integer, Integer>> occ, String s) {
		for (EventPermutation perm:eventPermutation)
			if (perm.getName().equals(s)){
				count+=occ.size();
				perm.addOccurrences(occ); 
				return;
			}
	}

	public void removeTrace(Set<Integer> logRed) {
		for (int i = 0; i < eventPermutation.size(); i++)
			eventPermutation.get(i).removeTrace(logRed);
		
	}

	public void resetTrace() {
		for (int i = 0; i < eventPermutation.size(); i++)
			eventPermutation.get(i).resetTrace();
		
	}

	public void resetTrace(Set<Integer> second) {
		for (int i = 0; i < eventPermutation.size(); i++)
			eventPermutation.get(i).resetTrace(second);
	}


}
