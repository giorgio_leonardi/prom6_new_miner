package org.processmining.models.andOrGraph;



import ioUtility.LabelUtility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

import org.deckfour.xes.model.XLog;
import org.processmining.models.graphbased.NodeID;
import org.processmining.models.graphbased.directed.AbstractDirectedGraph;
import org.processmining.models.graphbased.directed.DirectedGraph;
import org.processmining.models.graphbased.directed.DirectedGraphEdge;
import org.processmining.models.graphbased.directed.DirectedGraphElement;
import org.processmining.models.graphbased.directed.DirectedGraphNode;


public class GraphAndOr extends AbstractDirectedGraph<Node, Edge> implements GraphAndOrInterface{

	// Il log serve per le tracce verdi, inoltre potrebbe servire in generale per ottimizzare processi per le tracce
	private XLog log;
	// Hash dove sono presenti i grafi sequenziali riferiti alla singole traccem n_traccia -> GraphAndOr
	private Hashtable <Integer,GraphAndOr> traceGraphTable;	
	// ArrayList contiene il nome dei nodi astratti
	ArrayList<String> tableAbstraction;
	
	
	private LinkedHashSet<Node> nodes;

	private Set<Edge> edges;
	
	/*tabella label->lista di nodi dove compaiono solo eventi con quella label*/
	private Hashtable <String,Set<Node>> nodeTable;
	
	private String label;
	
	private boolean annotated;
	
	private Set<Node> nodeLoop;
	
	private Map<NodeID,Node> map;
	
	public GraphAndOr(String logName,boolean ann){
		super();
		int index;
		nodes=new LinkedHashSet<Node>();
		edges=new LinkedHashSet<Edge>();
		nodeTable=new Hashtable<String, Set<Node>>();
		nodeLoop=new TreeSet<Node>();
		if ((index=logName.lastIndexOf("."))!=-1)
			logName=logName.substring(0,index);
		this.label="graph mined from "+logName;
		annotated=ann;
		
		traceGraphTable = new Hashtable<Integer, GraphAndOr>();
		tableAbstraction = new ArrayList<String>();
		
	}
	public Set<Node> getNodes() {
		return nodes;
	}

	

	public void removeNode(DirectedGraphNode node) {
		
		Set<Node> t = nodeTable.get(node.getLabel());
		boolean b=t.remove(node);
		if (t.isEmpty())
			nodeTable.remove(node.getLabel());
		else 
			nodeTable.replace(node.getLabel(),t);
		b=nodes.remove(node);
	}


	public GraphAndOr getEmptyClone() {
		return new GraphAndOr(getLabel(),annotated);
	}

	
	public String toString(){
		
		return "Nodi: "+nodes.toString()+"\n Archi: "+edges.toString();
	}
	

	public String getLabel() {
		return label;
	}
	
	
	@SuppressWarnings("rawtypes")
	public void removeEdge(DirectedGraphEdge edge) {
		if (edge instanceof Edge) {
			edges.remove(edge);
		} else {
			assert (false);
		}
		((EventsNode)edge.getSource()).removeOutEdge(edge);
		((EventsNode)edge.getTarget()).removeInEdge(edge);
	}
	
	
	public Set<Edge> getEdges() {
		return edges;
	}
	
	@Override
	protected Map<? extends DirectedGraphElement, ? extends DirectedGraphElement> cloneFrom(
			DirectedGraph<Node, Edge> graph) {
		return null;
	}
	
	
	public void addNode(Node n) {
		nodes.add(n);
		//if (isLoop(n))
		//	nodeLoop.add(n);
		addTable(n);
	}
	
	private boolean isLoop(Node n) {
		Set<Node> nodesLabel = getNodesByLabel(n.getLabel());
		if (nodesLabel==null) return false;
		for (Node node:nodesLabel){
			Set<Integer> traces = node.getTraces();
			traces.retainAll(n.getTraces());
			if (!traces.isEmpty())
				return true;
		}
		return false;
	}
	private void addTable(Node n) {
		Set<Node> setNode=nodeTable.get(n.getLabel());
		if (setNode==null){
			setNode=new TreeSet<Node>();
			setNode.add(n);
			nodeTable.put(n.getLabel(), setNode);			
			}
		else {
			setNode.add(n);
			nodeTable.put(n.getLabel(), setNode);
		}
		
	}

	
	public boolean addEdge(Edge e) {
		
		if(e==null || e.getSource()==null || e.getTarget()==null) {
			System.out.println("Error handling: AddEdge null (GraphAndOr)");
			return false;
		}
		
		if(e != null && e.getSource()!=null && e.getTarget()!=null)
			if (!contains(e.getSource().getId(), e.getTarget().getId())){
				edges.add(e);
				return true;
			}		
		return false;
	}
	
	
	public Set<Node> getLeafs() {
		Iterator<Node> nodeIt=nodes.iterator();
		Set<Node> result=new TreeSet<Node>();
		while (nodeIt.hasNext()){
			Node node=nodeIt.next();
			if (node.getOutEdge().isEmpty())
				result.add((EventsNode) node);
			}
		return result;
	}
	public  Set<Node> getNodesByLabel(String label) {
		
		return nodeTable.get(label);
	}
	public Node getRoot() {
		for(Node n:nodes){
			if (n.getInEdge().isEmpty())
				return n;
		}
		return null;
	}
	public Set<String> getLabels() {
		return nodeTable.keySet();
	}
	
	public Set<Node> getNodesLoop(){
		return nodeLoop;
	}
	
	public Node addSubgraph(Node start,Node end, GraphAndOr tempGraph,Node root) {
		Edge e=new Edge((EventsNode)start, (EventsNode)root,Math.round(root.getOccNumber()/(double)start.getOccNumber()*100)/100.0);
		Iterator<DirectedGraphEdgeImpl> edgeIt = start.getOutEdge().iterator();
		root.addInEdge(e);
		edges.add(e);
		edges.addAll(tempGraph.getEdges());
		EventsNode endNode= (EventsNode) tempGraph.getNodesByLabel(end.getLabel()).iterator().next();
		for(DirectedGraphEdgeImpl<?, ?> edge:end.getOutEdge()){
			e=new Edge(endNode,edge.getTarget(),Double.parseDouble(edge.getLabel()));
			endNode.addOutEdge(e);

			edge.getTarget().removeInEdge(edge);
			edge.getTarget().addInEdge(e);
			edges.add(e);
		}
		edges.removeAll(end.getOutEdge());
		nodes.addAll(tempGraph.getNodes());
		return endNode;
	}
	
	public void removeEdges(Set<DirectedGraphEdgeImpl> removeEdge) {
		
		edges.removeAll(removeEdge);
	}
	public Node getNodeByID(NodeID id) {
		for(Node node:nodes){
			if (node.getId().equals(id))
				return node;
		}
		return null;
	}
	@Override
	public int nodeCount() {
		return nodes.size();
	}
	
	public boolean contains(NodeID sourceID, NodeID targetID) {
		EventsNode source=(EventsNode) getNodeByID(sourceID);
		EventsNode target=(EventsNode) getNodeByID(targetID);
		Edge edge=new Edge(source, target);
		return edges.contains(edge);
	}
	
	@SuppressWarnings("unchecked")
	public GraphAndOr clone(){
		GraphAndOr copy=new GraphAndOr(getLabel(),annotated);
		copyElements(nodes,edges,nodeTable,copy);
		//clone del log
		copy.setLog(this.log);
		copy.traceGraphTable = (Hashtable<Integer, GraphAndOr>) this.traceGraphTable.clone();
		copy.tableAbstraction = (ArrayList<String>) this.tableAbstraction.clone();
		return copy;
	}
	
	private void copyElements(LinkedHashSet<Node> nodes, Set<Edge> edges, Hashtable<String, Set<Node>> nodeTable, GraphAndOr copy) {
		
		Map<NodeID,Node> map=new HashMap<NodeID, Node>();
		for (Node oldNode:nodes){
			Node currNode=new EventsNode(oldNode.getEvent(),false,0);
			copy.nodes.add(currNode);
			map.put(oldNode.getId(), currNode);
		}
		
		for(Edge oldEdge:edges){
			Node source=map.get(oldEdge.getSource().getId());
			Node target=map.get(oldEdge.getTarget().getId());
			Edge currEdge=new Edge((EventsNode)source,(EventsNode) target,0);
			copy.edges.add(currEdge);
			source.addOutEdge(currEdge);
			target.addInEdge(currEdge);
		}
		
		for (String t:nodeTable.keySet()){
			Set<Node> set=new TreeSet<Node>();
			for (Node n:nodeTable.get(t)){
				Node currNode =map.get(n.getId());
				set.add(currNode);
			}
			copy.nodeTable.put(t, set);
		}
		copy.map=map;
	}
	public Collection<Node> getUniqueNode(boolean hard) {
		ArrayList<Node> temp= new ArrayList<Node>(nodes);
		ArrayList<Node> result=new ArrayList<Node>();
		Collections.sort(temp,new Comparator<Node>(){
			public int compare(Node n, Node n2){
				return n.getEvent().compareTo(n2.getEvent());
			}
		});
		System.out.println(temp);
		int i =0;
		ArrayList<Set<String>> p=new ArrayList<Set<String>>();
		while (i<temp.size()-1){
			Node n=temp.get(i);
			LabelUtility.intersection(n.getLabel(),p,hard);
			i++;
		}
		for (Set<String> label:p){
			Event e=new Event(LabelUtility.makeLabel(label));
			Node newNode=new EventsNode(e,false,0);
			result.add(newNode);
		}
		return result;
	}
	
	public void setAnnotated(boolean b) {
		annotated=b;
		
	}
	public boolean getAnnotated() {
		
		return annotated;
	}
	public void reset() {
		for (Node n:nodes)
			n.setRetrieve(0);
		for (Edge e:edges)
			e.setAddedNode(false);
	}
	
	
	public void simplifies(){
		//map per la ricerca di duplicati
		Map<String,Node> map=new HashMap<String,Node>();
		//nodi da eliminare
		Set<Node> nodeToDelete=new TreeSet<Node>();
		//archi da eliminare
		Set<Edge> edgeToDelete=new TreeSet<Edge>();
		//archi da inserire
		HashMap<Node,ArrayList<Edge>> edgeToAdd=new HashMap<Node,ArrayList<Edge>>();
		
		Queue<Node> toCheck=new LinkedList<Node>();
		Set<Node> alreadyChecked=new TreeSet<Node>();
		toCheck.add(getRoot());
		//per ogni nodo controllo gli archi uscenti
		while (!toCheck.isEmpty()){
			Node n=toCheck.poll();
			
			alreadyChecked.add(n);
						
			for (DirectedGraphEdgeImpl e:n.getOutEdge()){
				
				Node nodeTarget=map.get(e.getTarget().getLabel());
				//se il nodo target � presente pi� volte lo avr� gi� inserito nella mappa e devo eliminarlo
				if (nodeTarget!=null){
					nodeToDelete.add(e.getTarget());
					edgeToDelete.add((Edge) e);
					//tutti gli archi di questo nodo devo copiarli nell'altro
					for (DirectedGraphEdgeImpl edgeTarget:e.getTarget().getOutEdge())
					{
						Edge newEdge=new Edge((EventsNode) nodeTarget,edgeTarget.getTarget(),0);
						if (addEdge(newEdge)){
							//System.out.println("Aggiungo arco "+newEdge+" "+nodeTarget.getId()+"->"+edgeTarget.getTarget().getId());
							
							if( edgeToAdd.containsKey(nodeTarget) ) {
								ArrayList<Edge> tmp_ed = edgeToAdd.get(nodeTarget);
								tmp_ed.add(newEdge);
								edgeToAdd.put(nodeTarget, tmp_ed);
							}
							else {
								ArrayList<Edge> tmp_ed = new ArrayList<Edge>();
								tmp_ed.add(newEdge);
								edgeToAdd.put(nodeTarget, tmp_ed);
							}
							
							edgeTarget.getTarget().addInEdge(newEdge);
						};
						edgeToDelete.add((Edge) edgeTarget);
					}
					for (DirectedGraphEdgeImpl edgeTarget:e.getTarget().getInEdge())
					{
						if (!edgeTarget.getSource().equals(n))
						{
							Edge newEdge=new Edge(edgeTarget.getSource(),(EventsNode) nodeTarget,0);
							if (addEdge(newEdge)){
								//System.out.println("Aggiungo arco "+newEdge+" "+edgeTarget.getSource().getId()+"->"+nodeTarget.getId());					
								nodeTarget.addInEdge(newEdge);
								edgeTarget.getSource().addOutEdge(newEdge);
							}
						}
						edgeToDelete.add((Edge) edgeTarget);
					}
				}
				//se non � nella mappa ce lo inserisco
				else {
					map.put(e.getTarget().getLabel(), e.getTarget());
					if (!alreadyChecked.contains(e.getTarget()) && !toCheck.contains(e.getTarget()))
						toCheck.add(e.getTarget());
				}
			}
			//resetto la map per gli archi uscenti dal prossimo nodo
			map.clear();
			
			for(Map.Entry<Node,ArrayList<Edge>> entry : edgeToAdd.entrySet()) {				
				for(Edge e: entry.getValue())
					entry.getKey().addOutEdge(e);				
			}
			
			//e gli archi
			for (Edge e:edgeToDelete){
				//System.out.println("Rimuovo arco "+e+" "+e.getSource().getId()+"->"+e.getTarget().getId());
				e.getSource().removeOutEdge(e);
				e.getTarget().removeInEdge(e);
				removeEdge(e);
			}
			
			//elimino i nodi da eliminare
			for (Node n0:nodeToDelete){
				//System.out.println("Rimuovo il nodo "+n0+" "+n0.getId());
				removeNode(n0);
			}
			
			toCheck.removeAll(nodeToDelete);
			nodeToDelete.clear();
			edgeToDelete.clear();
			edgeToAdd.clear();
		}	
	
	}
	
	public void stampaDettagliato() {
		for (Node n:nodes){
			System.out.println(n.toString()+":");
			for (DirectedGraphEdgeImpl e:n.getOutEdge()){
				System.out.println("\t"+e.toString());
			}
			System.out.println();
		}
//		System.out.println("Archi:");
//		for (Edge e:edges){
//			System.out.println("\t"+e.toString());
//		}
	}
	
	public Set<Node> getNodesByEvents(String node) {
		String[] events = node.split("&&");
		Set<String> copy;
		Set<Node> result=new HashSet<Node>();
		for(Node n:nodes){
			copy=new HashSet<String>(Arrays.asList(events));
			copy.removeAll(n.getEvent().getEvents());
			if (copy.size()!= events.length)
				result.add(n);
		}
		return result;
	}
	public void printNodesID() {
		for (Node n:nodes)
			System.out.println(n.getLabel()+"==>"+n.getId().toString());
	}
	public Node getCopy(NodeID id) {
		return map.get(id);
	}
	
	
	
	/* Set and Get Log	 */
	
	public void setLog(XLog l) {
		this.log = l;
	}
	
	public XLog getLog() {
		return log;
	}
		
	
	/** InsertTraceGraph: inserisce il grafo riferito alla sola traccia  
	 * @param number_trace: numero della traccia
	 * @param graph: grafo riferito al numero della traccia da inserire
	 */
	
	public void insertTraceGraph(Integer number_trace, GraphAndOr graph) {
		traceGraphTable.put(number_trace, graph);
	}
	
	/** getTraceGraph: restituisce il grafo riferito alla traccia number_trace 
	 * @param number_trace: numero della traccia
	 * @return GraphAndOr: grafo riferito al numero della traccia
	 */
	public GraphAndOr getTraceGraph(Integer number_trace) {
		return traceGraphTable.get(number_trace);
	}
	
	/** getTraceGraphKeys: restituisce le tracce
	 * @return Set<Integer>: set delle chiave del traceGraphTable
	 */
	public Set<Integer> getTraceGraphKeys() {
		return traceGraphTable.keySet();
	}
		
	public void cloneTraceGraphTable(GraphAndOr another) {
		this.traceGraphTable = another.traceGraphTable;
	}
	
	
	/* 
	 * Gestione delle astrazioni
	 */
	
	//restituisco la tabella astrazioni
	public ArrayList<String> getAbstractionTable(){
		return tableAbstraction;
	}
	
	//Aggiunge un nodo alla tabella delle astrazioni
	public void markedAbstraction(String nomeNodo) {
		tableAbstraction.add(nomeNodo);
	}
	//Verifica se un nodo � presente nella tabella delle astrazioni
	public boolean isMarked(String nomeNodo) {
		if(tableAbstraction.contains(nomeNodo))
			return true;
		else
			return false;
	}
		
	
}
