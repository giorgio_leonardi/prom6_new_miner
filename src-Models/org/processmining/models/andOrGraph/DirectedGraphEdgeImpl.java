package org.processmining.models.andOrGraph;



import org.processmining.models.graphbased.directed.AbstractDirectedGraph;
import org.processmining.models.graphbased.directed.AbstractDirectedGraphEdge;


public class DirectedGraphEdgeImpl<S extends EventsNode, T extends EventsNode> extends AbstractDirectedGraphEdge<S,T>{

	private String label;//valore di dipendenza dell'arco
	private Integer ID;
	private static int cont=0;
	private boolean addedNode=false;
	//private AttributeMap att;
	
	/**torna l'ID*/
	public Integer getID() {
		return ID;
	}

	/**
	 * costruttore con anche la dipendenza 
	 * */
	public DirectedGraphEdgeImpl(S source, T target,double dep) {
		super(source, target);
		label=Double.toString(dep);
		this.ID=cont++;
		//att=new AttributeMap();
	}
	
	/**
	 * costruttore senza dipendenza
	 * 
	 * */
	public DirectedGraphEdgeImpl(S source, T target) {
		super(source, target);
		label=null;
		this.ID=cont++;
		//att=new AttributeMap();
	}

	public DirectedGraphEdgeImpl(S source, T target,double dep,boolean f){
		super(source, target);
		label=Double.toString(dep);
		this.ID=cont++;
		setAddedNode(f);
	}
	
	/**ritorna la label*/
	public String getLabel() {
	
		return label;
	}

	/**non implementato*/
	public AbstractDirectedGraph<?, ?> getGraph() {
		// TODO Auto-generated method stub
		return null;
	}

	/**ritorna la mappa degli attributi*/
/*	public AttributeMap getAttributeMap() {
		// TODO Auto-generated method stub
		return att;
	}
*/

	/**ritorna la sorgente dell'arco*/
	public S getSource() {
		return source;
	}

	/**ritorna la destinazione dell'arco*/
	public T getTarget() {
		return target;
	}

	public String toString (){
		return source.toString()+"->"+target.toString();
	}

	public boolean isAddedNode() {
		return addedNode;
	}

	public void setAddedNode(boolean addedNode) {
		this.addedNode = addedNode;
	}

	
}
