package org.processmining.models.andOrGraph;

import java.util.Set;
import java.util.TreeSet;

import org.processmining.models.graphbased.directed.AbstractDirectedGraph;
import org.processmining.models.graphbased.directed.AbstractDirectedGraphNode;
import org.processmining.models.graphbased.directed.DirectedGraphEdge;

public class EventsNode extends AbstractDirectedGraphNode implements Node{

	
	
	private Event events;
	@SuppressWarnings("rawtypes")
	protected Set<DirectedGraphEdgeImpl> outEdge;
	@SuppressWarnings("rawtypes")
	protected Set<DirectedGraphEdgeImpl> inEdge;
	boolean jolly;
	int occNumber;
	private int retrieve; 
	
	public boolean isJolly(){
		return jolly;
	}
	
	@SuppressWarnings("rawtypes")
	public Set<DirectedGraphEdgeImpl> getOutEdge() {
		return outEdge;
	}

	public Event getEvent() {
		return events;
	}
	
	public void addOutEdge(DirectedGraphEdgeImpl<?, ?> outEdge) {
		this.outEdge.add(outEdge);
	}

	@SuppressWarnings("rawtypes")
	public EventsNode(Event set,boolean j,int occ) {
		super();
		events=set;
		jolly=j;
		occNumber=occ;
		outEdge=new TreeSet<DirectedGraphEdgeImpl>();
		inEdge=new TreeSet<DirectedGraphEdgeImpl>();
		resetRetrieve();
	}
	
	private void resetRetrieve() {
		retrieve=NORMAL;
	}

	@SuppressWarnings("rawtypes")
	public EventsNode(EventsNode e){
		super();
		events=e.events;
		jolly=e.jolly;
		occNumber=e.occNumber;
		outEdge=new TreeSet<DirectedGraphEdgeImpl>(e.outEdge);
		inEdge=new TreeSet<DirectedGraphEdgeImpl>(e.inEdge);
		setRetrieve(NORMAL);
	}
	
	public String getLabel() {
		return events.getName();
	}

	public int getOccNumber(){
		return occNumber;
	}
	
	public AbstractDirectedGraph<?, ?> getGraph() {
		return null;
	}


	@SuppressWarnings("rawtypes")
	public Set<DirectedGraphEdgeImpl> getInEdge() {
		return inEdge;
	}

	public void addInEdge(DirectedGraphEdgeImpl<?, ?> inEdge) {
		this.inEdge.add(inEdge);
	}

	public String toString(){
		return "Evento "+getLabel()+" "+occNumber;
	}

	public void removeOutEdge(DirectedGraphEdge<?, ?> edge) {
		outEdge.remove(edge);
	}

	public void removeInEdge(DirectedGraphEdge<?, ?> edge) {
		inEdge.remove(edge);
	}

	

	public void printOcc() {
		System.out.println("Occorrenze del nodo "+this.getId()+"--"+getLabel());
			System.out.println(events.getOccurs());
	}

	

	@Override
	public Set<Integer> getTraces() {
		
		return (Set<Integer>) events.getTrace();
	}

	@Override
	public void resetEdge() {
		outEdge.clear();
		inEdge.clear();
	}

	@Override
	public boolean isLeaf() {
		return outEdge.isEmpty();
	}

	public int isRetrieve() {
		return retrieve;
	}

	public void setRetrieve(int retrieve) {
		
		if (retrieve!=this.retrieve){
			if (retrieve>MIXED_RETRIEVED)
				this.retrieve=retrieve;
			else {
				int total = retrieve+this.retrieve;
				if (total>MIXED_RETRIEVED)
					this.retrieve=MIXED_RETRIEVED;
				else this.retrieve=total;
			}
		}
	}
	
	
	
}
