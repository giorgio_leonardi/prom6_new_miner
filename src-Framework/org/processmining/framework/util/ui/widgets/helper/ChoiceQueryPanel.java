package org.processmining.framework.util.ui.widgets.helper;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.framework.util.ui.widgets.ProMComboBox;

class ChoiceQueryPanel<T> extends JPanel {

	private static final long serialVersionUID = -6547392010448275699L;

	private final ProMComboBox<T> choiceField;

	public ChoiceQueryPanel(Iterable<T> choices) {
		super();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		choiceField = new ProMComboBox<>(choices);
		choiceField.setPreferredSize(null);
		add(choiceField);
	}

	public InteractionResult getUserChoice(UIPluginContext context, String query) {
		return context.showConfiguration(query, this);
	}

	@SuppressWarnings("unchecked")
	public T getResult() {
		return (T) choiceField.getModel().getSelectedItem();
	}

}