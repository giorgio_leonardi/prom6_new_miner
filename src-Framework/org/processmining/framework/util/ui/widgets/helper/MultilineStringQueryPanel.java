package org.processmining.framework.util.ui.widgets.helper;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.framework.util.ui.widgets.ProMTextArea;

public class MultilineStringQueryPanel extends JPanel {

	private static final long serialVersionUID = -8701706429341608153L;
	
	private final ProMTextArea textArea;
	
	public MultilineStringQueryPanel(String text) {
		super();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		if (text != null) {
			add(new JLabel(text));
		}
		textArea = new ProMTextArea();
		textArea.setPreferredSize(null);
		add(textArea);
	}

	public MultilineStringQueryPanel() {
		this(null);
	}

	public InteractionResult getUserChoice(UIPluginContext context, String query) {
		return context.showConfiguration(query, this);
	}

	public String getResult() {
		return textArea.getText();
	}

	
}
