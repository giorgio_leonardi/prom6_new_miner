package org.processmining.framework.util.ui.widgets.helper;

import java.awt.Component;
import java.awt.Dimension;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.processmining.contexts.uitopia.UIPluginContext;

/**
 * Commonly used functionality for the ProM UI as static helper methods.
 * 
 * @author F. Mannhardt
 * 
 */
public class ProMUIHelper {
	
	/******************* ERROR / WARNING / INFO MESSAGES *****************/
	
	/**
	 * Displays an error message in a 'modal' pop-up using the global UI as
	 * parent JFrame.
	 * 
	 * @param context
	 * @param errorMessage
	 * @param errorTitle
	 */
	public static void showErrorMessage(UIPluginContext context, String errorMessage, String errorTitle) {
		Object[] options = { "OK" };

		JOptionPane.showOptionDialog(context.getGlobalContext().getUI(), createMessageBody(errorMessage), errorTitle,
				JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
	}

	/**
	 * Displays an error message in a 'modal' pop-up using a default JFrame.
	 * 
	 * @param errorMessage
	 * @param errorTitle
	 */
	public static void showErrorMessage(String errorMessage, String errorTitle) {
		Object[] options = { "OK" };
		JOptionPane.showOptionDialog(null, createMessageBody(errorMessage), errorTitle, JOptionPane.PLAIN_MESSAGE,
				JOptionPane.ERROR_MESSAGE, null, options, options[0]);
	}

	/**
	 * Displays an error message in a 'modal' pop-up using the specified
	 * Component as parent.
	 * 
	 * @param component
	 * @param errorMessage
	 * @param errorTitle
	 */
	public static void showErrorMessage(Component component, String errorMessage, String errorTitle) {
		Object[] options = { "OK" };
		JOptionPane.showOptionDialog(component, createMessageBody(errorMessage), errorTitle, JOptionPane.PLAIN_MESSAGE,
				JOptionPane.ERROR_MESSAGE, null, options, options[0]);
	}

	/**
	 * Displays an warning message in a 'modal' pop-up using the global UI as
	 * parent JFrame.
	 * 
	 * @param context
	 * @param warnMessage
	 * @param warnTitle
	 */
	public static void showWarningMessage(UIPluginContext context, String warnMessage, String warnTitle) {
		Object[] options = { "OK" };
		JOptionPane.showOptionDialog(context.getGlobalContext().getUI(), createMessageBody(warnMessage), warnTitle,
				JOptionPane.PLAIN_MESSAGE, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
	}

	/**
	 * Displays an warning message in a 'modal' pop-up using a default JFrame.
	 * 
	 * @param warnMessage
	 * @param warnTitle
	 */
	public static void showWarningMessage(String warnMessage, String warnTitle) {
		Object[] options = { "OK" };
		JOptionPane.showOptionDialog(null, createMessageBody(warnMessage), warnTitle, JOptionPane.PLAIN_MESSAGE,
				JOptionPane.WARNING_MESSAGE, null, options, options[0]);
	}

	/**
	 * Displays an warning message in a 'modal' pop-up using using the specified
	 * Component as parent.
	 * 
	 * @param component
	 * @param warnMessage
	 * @param warnTitle
	 */
	public static void showWarningMessage(Component component, String warnMessage, String warnTitle) {
		Object[] options = { "OK" };
		JOptionPane.showOptionDialog(component, createMessageBody(warnMessage), warnTitle, JOptionPane.PLAIN_MESSAGE,
				JOptionPane.WARNING_MESSAGE, null, options, options[0]);
	}
	
	private static JComponent createMessageBody(String message) {
		JTextArea errorBody = new JTextArea(message);
		errorBody.setWrapStyleWord(true);
		errorBody.setLineWrap(true);
		return new JScrollPane(errorBody) {

			private static final long serialVersionUID = 1L;

			public Dimension getPreferredSize() {
				return new Dimension(480, 320);
			}

		};
	}
	
	/******************* QUERY DIALOGS *****************/

	/**
	 * Displays a configuration dialog asking the user to enter a String
	 * literal.
	 * 
	 * @param context
	 * @param queryCaption
	 * @return
	 * @throws UserCancelledException
	 */
	public static String queryForString(UIPluginContext context, String queryCaption) throws UserCancelledException {
		return queryForString(context, queryCaption, null, null);
	}

	/**
	 * Displays a configuration dialog asking the user to enter a String literal
	 * 
	 * @param context
	 * @param queryCaption
	 * @param queryText
	 * @return
	 * @throws UserCancelledException
	 */
	public static String queryForString(UIPluginContext context, String queryCaption, String queryText)
			throws UserCancelledException {
		return queryForString(context, queryCaption, queryText, null);
	}

	/**
	 * Displays a configuration dialog asking the user to enter a String literal
	 * 
	 * @param context
	 * @param queryCaption
	 * @param queryText
	 * @param defaultValue
	 * @return
	 * @throws UserCancelledException
	 */
	public static String queryForString(UIPluginContext context, String queryCaption, String queryText,
			String defaultValue) throws UserCancelledException {
		StringQueryPanel queryPanel = new StringQueryPanel(queryText, defaultValue);
		InteractionResult choice = queryPanel.getUserChoice(context, queryCaption);
		if (choice == InteractionResult.FINISHED || choice == InteractionResult.CONTINUE) {
			return queryPanel.getResult();
		} else {
			throw new UserCancelledException();
		}
	}

	/**
	 * Displays a configuration dialog asking the user to enter a Integer, uses
	 * {@link Integer#parseInt(String)} to convert the user input.
	 * 
	 * @param context
	 * @param query
	 * @return
	 * @throws NumberFormatException
	 * @throws UserCancelledException
	 */
	public static int queryForInteger(UIPluginContext context, String query) throws NumberFormatException,
			UserCancelledException {
		return Integer.parseInt(queryForString(context, query));
	}

	/**
	 * Displays a configuration dialog asking the user to enter a Float, uses
	 * {@link Float#parseFloat(String)} to convert the user input.
	 * 
	 * @param context
	 * @param query
	 * @return
	 * @throws NumberFormatException
	 * @throws UserCancelledException
	 */
	public static float queryForFloat(UIPluginContext context, String query) throws NumberFormatException,
			UserCancelledException {
		return Float.parseFloat(queryForString(context, query));
	}

	/**
	 * Displays a configuration dialog asking the user to enter a Float, uses
	 * {@link Float#parseFloat(String)} to convert the user input.
	 * 
	 * @param context
	 * @param query
	 * @return
	 * @throws NumberFormatException
	 * @throws UserCancelledException
	 */
	public static double queryForDouble(UIPluginContext context, String query) throws NumberFormatException,
			UserCancelledException {
		return Double.parseDouble(queryForString(context, query));
	}

	public static int[] queryForIntArray(UIPluginContext context, String query) throws NumberFormatException,
			UserCancelledException {
		String result = queryForString(context, query);
		if (result != null && result.length() > 0) {
			String[] traceIndexArray = result.split(",");
			int[] traceIndexSet = new int[traceIndexArray.length];
			for (int i = 0; i < traceIndexArray.length; i++) {
				String singleTraceIndex = traceIndexArray[i];
				traceIndexSet[i] = Integer.parseInt(singleTraceIndex);
			}
			return traceIndexSet;
		} else {
			throw new NumberFormatException("Invalid format, should be comma separated integers!");
		}
	}

	public static <T> T queryForObject(UIPluginContext context, String query, T[] choices)
			throws UserCancelledException {
		return queryForObject(context, query, Arrays.asList(choices));
	}

	public static <T> T queryForObject(UIPluginContext context, String query, Iterable<T> choices)
			throws UserCancelledException {
		ChoiceQueryPanel<T> queryPanel = new ChoiceQueryPanel<>(choices);
		InteractionResult choice = queryPanel.getUserChoice(context, query);
		if (choice == InteractionResult.FINISHED || choice == InteractionResult.CONTINUE) {
			return queryPanel.getResult();
		} else {
			throw new UserCancelledException();
		}
	}

	public static String queryForString(UIPluginContext context, String query, String[] choices)
			throws UserCancelledException {
		return queryForString(context, query, Arrays.asList(choices));
	}

	public static String queryForString(UIPluginContext context, String query, Iterable<String> choices)
			throws UserCancelledException {
		ChoiceQueryPanel<String> queryPanel = new ChoiceQueryPanel<>(choices);
		InteractionResult choice = queryPanel.getUserChoice(context, query);
		if (choice == InteractionResult.FINISHED || choice == InteractionResult.CONTINUE) {
			return queryPanel.getResult();
		} else {
			throw new UserCancelledException();
		}
	}

	public static String queryForMultilineString(UIPluginContext context, String queryCaption)
			throws UserCancelledException {
		MultilineStringQueryPanel queryPanel = new MultilineStringQueryPanel();
		InteractionResult choice = queryPanel.getUserChoice(context, queryCaption);
		if (choice == InteractionResult.FINISHED || choice == InteractionResult.CONTINUE) {
			return queryPanel.getResult();
		} else {
			throw new UserCancelledException();
		}
	}

	public static String queryForMultilineString(UIPluginContext context, String queryCaption, String queryText)
			throws UserCancelledException {
		MultilineStringQueryPanel queryPanel = new MultilineStringQueryPanel(queryText);
		InteractionResult choice = queryPanel.getUserChoice(context, queryCaption);
		if (choice == InteractionResult.FINISHED || choice == InteractionResult.CONTINUE) {
			return queryPanel.getResult();
		} else {
			throw new UserCancelledException();
		}
	}

	public static <T> List<T> queryForObjects(UIPluginContext context, String query, T[] choices)
			throws UserCancelledException {
		return queryForObjects(context, query, Arrays.asList(choices));
	}

	public static <T> List<T> queryForObjects(UIPluginContext context, String query, Iterable<T> choices)
			throws UserCancelledException {
		MultipleChoiceQueryPanel<T> queryPanel = new MultipleChoiceQueryPanel<>(query, choices);
		InteractionResult choice = queryPanel.getUserChoice(context, query);
		if (choice == InteractionResult.FINISHED || choice == InteractionResult.CONTINUE) {
			return queryPanel.getResult();
		} else {
			throw new UserCancelledException();
		}
	}

	public static List<String> queryForStrings(UIPluginContext context, String query, Iterable<String> choices)
			throws UserCancelledException {
		MultipleChoiceQueryPanel<String> queryPanel = new MultipleChoiceQueryPanel<>(query, choices);
		InteractionResult choice = queryPanel.getUserChoice(context, query);
		if (choice == InteractionResult.FINISHED || choice == InteractionResult.CONTINUE) {
			return queryPanel.getResult();
		} else {
			throw new UserCancelledException();
		}
	}

	public static List<String> queryForStrings(UIPluginContext context, String query, String[] choices)
			throws UserCancelledException {
		return queryForStrings(context, query, Arrays.asList(choices));
	}

	public static <T> Map<T, String> queryMapToString(UIPluginContext context, String query, Iterable<T> sources)
			throws UserCancelledException {
		FreeMappingQueryPanel<T> queryPanel = new FreeMappingQueryPanel<>(query, sources);
		InteractionResult choice = queryPanel.getUserChoice(context, query);
		if (choice == InteractionResult.FINISHED || choice == InteractionResult.CONTINUE) {
			return queryPanel.getResult();
		} else {
			throw new UserCancelledException();
		}
	}

	public static <T> Map<T, String> queryMapToString(UIPluginContext context, String query, T[] sources)
			throws UserCancelledException {
		return queryMapToString(context, query, Arrays.asList(sources));
	}

}