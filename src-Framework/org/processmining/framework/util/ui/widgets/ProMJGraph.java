package org.processmining.framework.util.ui.widgets;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.ToolTipManager;

import org.jgraph.JGraph;
import org.jgraph.event.GraphLayoutCacheEvent;
import org.jgraph.event.GraphLayoutCacheListener;
import org.jgraph.event.GraphModelEvent;
import org.jgraph.event.GraphModelListener;
import org.jgraph.event.GraphSelectionEvent;
import org.jgraph.event.GraphSelectionListener;
import org.jgraph.graph.CellView;
import org.jgraph.graph.ConnectionSet;
import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.GraphLayoutCache;
import org.jgraph.graph.ParentMap;
import org.processmining.framework.util.Cast;
import org.processmining.framework.util.Cleanable;
import org.processmining.framework.util.ui.scalableview.ScalableComponent;
import org.processmining.models.connections.GraphLayoutConnection;
import org.processmining.models.connections.GraphLayoutConnection.Listener;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.AttributeMapOwner;
import org.processmining.models.graphbased.Expandable;
import org.processmining.models.graphbased.ExpansionListener;
import org.processmining.models.graphbased.ViewSpecificAttributeMap;
import org.processmining.models.graphbased.directed.BoundaryDirectedGraphNode;
import org.processmining.models.graphbased.directed.ContainableDirectedGraphElement;
import org.processmining.models.graphbased.directed.ContainingDirectedGraphNode;
import org.processmining.models.graphbased.directed.DirectedGraph;
import org.processmining.models.graphbased.directed.DirectedGraphEdge;
import org.processmining.models.graphbased.directed.DirectedGraphNode;
import org.processmining.models.jgraph.JGraphFoldingManager;
import org.processmining.models.jgraph.ProMGraphModel;
import org.processmining.models.jgraph.elements.ProMGraphCell;
import org.processmining.models.jgraph.elements.ProMGraphEdge;
import org.processmining.models.jgraph.elements.ProMGraphElement;
import org.processmining.models.jgraph.elements.ProMGraphPort;
import org.processmining.models.jgraph.factory.ProMCellViewFactory;

import com.jgraph.layout.JGraphFacade;
import com.jgraph.layout.JGraphLayout;

/**
 * Modifies for the Widgets package by michael
 */
public class ProMJGraph extends JGraph implements GraphModelListener, GraphLayoutCacheListener, GraphSelectionListener,
		Cleanable, ExpansionListener, ScalableComponent, Listener {

	/**
	 * 
	 */
	public static final String PIPVIEWATTRIBUTE = "signalPIPView";

	private static final long serialVersionUID = -8477633603192312230L;

	private final Map<BoundaryDirectedGraphNode, ProMGraphPort> boundaryNodeMap = new HashMap<BoundaryDirectedGraphNode, ProMGraphPort>();
	private final Map<DirectedGraphEdge<?, ?>, ProMGraphEdge> edgeMap = new HashMap<DirectedGraphEdge<?, ?>, ProMGraphEdge>();
	private final boolean isPIP;
	private JGraphLayout layout;

	private final GraphLayoutConnection layoutConnection;

	private final ProMGraphModel model;

	private final Map<DirectedGraphNode, ProMGraphCell> nodeMap = new HashMap<DirectedGraphNode, ProMGraphCell>();

	private final ViewSpecificAttributeMap viewSpecificAttributes;

	Set<UpdateListener> updateListeners = new HashSet<UpdateListener>();

	/**
	 * @param model
	 * @param isPIP
	 * @param viewSpecificAttributes
	 * @param layoutConnection
	 */
	public ProMJGraph(final ProMGraphModel model, final boolean isPIP,
			final ViewSpecificAttributeMap viewSpecificAttributes, final GraphLayoutConnection layoutConnection) {
		super(model, new GraphLayoutCache(model, new ProMCellViewFactory(isPIP, viewSpecificAttributes), true));
		this.layoutConnection = layoutConnection;
		layoutConnection.addListener(this);
		getGraphLayoutCache().setShowsInvisibleEditedCells(false);
		this.isPIP = isPIP;
		this.viewSpecificAttributes = viewSpecificAttributes;

		getGraphLayoutCache().setMovesChildrenOnExpand(true);
		// Strange: setResizesParentsOnCollapse has to be set to FALSE!
		getGraphLayoutCache().setResizesParentsOnCollapse(false);
		getGraphLayoutCache().setMovesParentsOnCollapse(true);

		this.model = model;

		setAntiAliased(true);
		setDisconnectable(false);
		setConnectable(false);
		setGridEnabled(false);
		setDoubleBuffered(true);
		setSelectionEnabled(!isPIP);
		setMoveBelowZero(false);
		setPortsVisible(true);
		setPortsScaled(true);

		final DirectedGraph<?, ?> net = model.getGraph();

		final List<DirectedGraphNode> todo = new ArrayList<DirectedGraphNode>(net.getNodes());
		final List<Object> toInsert = new ArrayList<Object>();
		while (!todo.isEmpty()) {
			final Iterator<DirectedGraphNode> it = todo.iterator();
			while (it.hasNext()) {
				final DirectedGraphNode n = it.next();
				if (n instanceof BoundaryDirectedGraphNode) {
					final DirectedGraphNode m = ((BoundaryDirectedGraphNode) n).getBoundingNode();
					if (m != null && !nodeMap.containsKey(m)) {
						// first make sure the bounding node is added
						continue;
					} else if (m != null) {
						// add as port
						addPort((BoundaryDirectedGraphNode) n, m);
						it.remove();
						continue;
					}
				}
				if (n instanceof ContainableDirectedGraphElement) {
					final ContainingDirectedGraphNode c = Cast.<ContainableDirectedGraphElement>cast(n).getParent();
					if (c != null && !nodeMap.containsKey(c)) {
						// if parent is not added yet, then continue
						continue;
					} else if (c == null) {
						toInsert.add(addCell(n));
					} else {
						addCell(n);
					}
				} else {
					toInsert.add(addCell(n));
				}

				it.remove();
			}
		}

		//		getGraphLayoutCache().insert(toInsert.toArray());

		//		getGraphLayoutCache().insert(boundaryNodeMap.values().toArray());
		for (final DirectedGraphEdge<?, ?> e : net.getEdges()) {
			if (e instanceof ContainableDirectedGraphElement) {
				final ContainingDirectedGraphNode m = Cast.<ContainableDirectedGraphElement>cast(e).getParent();
				if (m == null) {
					toInsert.add(addEdge(e));
				} else {
					addEdge(e);
				}
			} else {
				toInsert.add(addEdge(e));
			}
		}
		getGraphLayoutCache().insert(toInsert.toArray());
		// Add the listeners, only AFTER copying the graph.

		registerAsListener();
		layoutConnection.getExpansionListeners().add(this);

		if (!isPIP) {
			addMouseListener(new JGraphFoldingManager(layoutConnection));
		}

		ToolTipManager.sharedInstance().registerComponent(this);
	}

	/**
	 * @param model
	 * @param viewSpecificAttributes
	 * @param layoutConnection
	 */
	public ProMJGraph(final ProMGraphModel model, final ViewSpecificAttributeMap viewSpecificAttributes,
			final GraphLayoutConnection layoutConnection) {
		this(model, false, viewSpecificAttributes, layoutConnection);
	}

	/**
	 * @see org.processmining.framework.util.ui.scalableview.ScalableComponent#addUpdateListener(org.processmining.framework.util.ui.scalableview.ScalableComponent.UpdateListener)
	 */
	@Override
	public void addUpdateListener(final UpdateListener listener) {
		updateListeners.add(listener);
	}

	/**
	 * @see org.processmining.framework.util.Cleanable#cleanUp()
	 */
	@Override
	public void cleanUp() {

		final List<Cleanable> cells = new ArrayList<Cleanable>(nodeMap.values());
		cells.addAll(boundaryNodeMap.values());
		cells.addAll(edgeMap.values());
		getGraphLayoutCache().removeCells(cells.toArray());

		for (final Cleanable cell : cells) {
			cell.cleanUp();
		}

		model.removeGraphModelListener(this);
		removeGraphSelectionListener(this);
		getGraphLayoutCache().removeGraphLayoutCacheListener(this);
		ToolTipManager.sharedInstance().unregisterComponent(this);

		removeAll();
		setVisible(false);
		setEnabled(false);
		setLayout(null);
		setGraphLayoutCache(null);

	}

	/**
	 * @see org.processmining.framework.util.ui.scalableview.ScalableComponent#getComponent()
	 */
	@Override
	public JComponent getComponent() {
		// for interface Scalable
		return this;
	}

	/**
	 * Returns the <code>GraphModel</code> that is providing the data.
	 * 
	 * @return the model that is providing the data
	 */
	@Override
	public ProMGraphModel getModel() {
		return (ProMGraphModel) graphModel;
	}

	/**
	 * @return
	 */
	public DirectedGraph<? extends DirectedGraphNode, ? extends DirectedGraphEdge<? extends DirectedGraphNode, ? extends DirectedGraphNode>> getProMGraph() {
		return model.getGraph();
	}

	/**
	 * @see org.jgraph.JGraph#getToolTipText(java.awt.event.MouseEvent)
	 */
	@Override
	public String getToolTipText(final MouseEvent event) {
		// get first cell under the mouse pointer's position
		final Object cell = getFirstCellForLocation(event.getX(), event.getY());

		final ViewSpecificAttributeMap map = getViewSpecificAttributes();

		// determine what is being pointed to by the mouse pointer
		if (cell instanceof ProMGraphCell) {
			// mouse is pointing to a node or a port on that node
			final ProMGraphCell c = (ProMGraphCell) cell;
			return map.get(c.getNode(), AttributeMap.TOOLTIP, c.getLabel());
		} else if (cell instanceof ProMGraphEdge) {
			final ProMGraphEdge e = (ProMGraphEdge) cell;
			return map.get(e.getEdge(), AttributeMap.TOOLTIP, e.getLabel());
		}

		return null;
	}

	/**
	 * @return
	 */
	public JGraphLayout getUpdateLayout() {
		return layout;
	}

	/**
	 * @return
	 */
	public ViewSpecificAttributeMap getViewSpecificAttributes() {
		return viewSpecificAttributes;
	}

	/**
	 * @see org.jgraph.event.GraphModelListener#graphChanged(org.jgraph.event.GraphModelEvent)
	 */
	@Override
	public void graphChanged(final GraphModelEvent e) {
		handleChange(e.getChange());
		changeHandled();
		for (final UpdateListener l : updateListeners) {
			l.updated();
		}
	}

	/**
	 * @see org.jgraph.event.GraphLayoutCacheListener#graphLayoutCacheChanged(org.jgraph.event.GraphLayoutCacheEvent)
	 */
	@Override
	public void graphLayoutCacheChanged(final GraphLayoutCacheEvent e) {
		handleChange(e.getChange());
		changeHandled();
		for (final UpdateListener l : updateListeners) {
			l.updated();
		}
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return model.getGraph().hashCode();
	}

	/**
	 * @see org.processmining.models.connections.GraphLayoutConnection.Listener#layoutConnectionUpdated(org.processmining.models.graphbased.AttributeMapOwner[])
	 */
	@Override
	public void layoutConnectionUpdated(final AttributeMapOwner... owners) {
		update((Object[]) owners);
	}

	/**
	 * @see org.processmining.models.graphbased.ExpansionListener#nodeCollapsed(org.processmining.models.graphbased.Expandable)
	 */
	@Override
	public void nodeCollapsed(final Expandable source) {
		final ProMGraphCell cell = nodeMap.get(source);
		// Before calling collapse, set the size of cell to the collapsed size

		Point2D pos = layoutConnection.getPosition(source);
		if (pos == null) {
			pos = new Point2D.Double(10, 10);
		}

		final Dimension size = source.getCollapsedSize();

		final Rectangle2D bounds = GraphConstants.getBounds(cell.getAttributes());
		bounds.setFrame(pos.getX(), pos.getY(), size.getWidth(), size.getHeight());
		getGraphLayoutCache().collapse(DefaultGraphModel.getDescendants(model, new Object[] { cell }).toArray());
	}

	/**
	 * @see org.processmining.models.graphbased.ExpansionListener#nodeExpanded(org.processmining.models.graphbased.Expandable)
	 */
	@Override
	public void nodeExpanded(final Expandable source) {
		final ProMGraphCell cell = nodeMap.get(source);
		getGraphLayoutCache().expand(DefaultGraphModel.getDescendants(model, new Object[] { cell }).toArray());
	}

	/**
	 * @see org.processmining.framework.util.ui.scalableview.ScalableComponent#removeUpdateListener(org.processmining.framework.util.ui.scalableview.ScalableComponent.UpdateListener)
	 */
	@Override
	public void removeUpdateListener(final UpdateListener listener) {
		updateListeners.remove(listener);
	}

	// returns the original origin
	/**
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void repositionToOrigin() {

		//		facade.translateCells(facade.getVertices(), 100.0, 100.0);
		//		facade.translateCells(facade.getEdges(), 100.0, 100.0);
		//		getGraphLayoutCache().edit(facade.createNestedMap(true, false));
		/*
		 * Second, pull everything back to (2,2). Works like a charm, even when
		 * a hack...
		 */
		//TODO Doesn't correctly handle collapsed nodes.

		final JGraphFacade facade = new JGraphFacade(this);
		facade.setIgnoresHiddenCells(true);
		facade.setIgnoresCellsInGroups(false);
		facade.setIgnoresUnconnectedCells(false);

		final double x = facade.getGraphOrigin().getX();
		final double y = facade.getGraphOrigin().getY();

		final ArrayList cells = new ArrayList();
		cells.addAll(facade.getVertices());
		cells.addAll(facade.getEdges());
		facade.translateCells(cells, 2.0 - x, 2.0 - y);
		final Map map = facade.createNestedMap(true, false);
		getGraphLayoutCache().edit(map);

	}

	/**
	 * @param layout
	 */
	public void setUpdateLayout(final JGraphLayout layout) {
		this.layout = layout;
	}

	/**
	 * @see java.awt.Component#toString()
	 */
	@Override
	public String toString() {
		return model.toString();
	}

	/**
	 * @param elements
	 */
	public void update(final Collection<?> elements) {
		updateElements(elements);
	}

	/**
	 * @param elements
	 */
	public void update(final Object... elements) {
		updateElements(Arrays.asList(elements));
	}

	/**
	 * @see org.jgraph.event.GraphSelectionListener#valueChanged(org.jgraph.event.GraphSelectionEvent)
	 */
	@Override
	public void valueChanged(final GraphSelectionEvent e) {
		// Ignore for now
	}

	private ProMGraphCell addCell(final DirectedGraphNode node) {
		final ProMGraphCell cell = new ProMGraphCell(node, model, layoutConnection);

		// TODO: This is probably wrong.
		// cell.addPort(new Point2D.Double(0,0));
		cell.addPort();
		((DefaultPort) cell.getChildAt(0)).setUserObject("default port");

		// getting the size
		nodeMap.put(node, cell);

		// if the node is contained in another node, its cell must be contained in the cell of that node
		if (node instanceof ContainableDirectedGraphElement) {
			final ContainingDirectedGraphNode parent = Cast.<ContainableDirectedGraphElement>cast(node).getParent();
			if (parent != null) {
				final ProMGraphCell parentNode = nodeMap.get(parent);
				parentNode.add(cell);
				cell.setParent(parentNode);
			}
		}

		return cell;
	}

	private ProMGraphEdge addEdge(final DirectedGraphEdge<?, ?> e) {
		final ProMGraphEdge edge = new ProMGraphEdge(e, model, layoutConnection);
		// For now, assume a single port.
		ProMGraphPort srcPort;
		if (e.getSource() instanceof BoundaryDirectedGraphNode
				&& ((BoundaryDirectedGraphNode) e.getSource()).getBoundingNode() != null) {
			srcPort = boundaryNodeMap.get(e.getSource());
		} else {
			srcPort = (ProMGraphPort) nodeMap.get(e.getSource()).getChildAt(0);
		}
		ProMGraphPort tgtPort;
		if (e.getTarget() instanceof BoundaryDirectedGraphNode
				&& ((BoundaryDirectedGraphNode) e.getTarget()).getBoundingNode() != null) {
			tgtPort = boundaryNodeMap.get(e.getTarget());
		} else {
			tgtPort = (ProMGraphPort) nodeMap.get(e.getTarget()).getChildAt(0);
		}

		edge.setSource(srcPort);
		edge.setTarget(tgtPort);

		srcPort.addEdge(edge);
		tgtPort.addEdge(edge);

		edgeMap.put(e, edge);

		// if the edge is contained in a node, its cell must be contained in the cell of that node
		if (e instanceof ContainableDirectedGraphElement) {
			final ContainingDirectedGraphNode parent = Cast.<ContainableDirectedGraphElement>cast(e).getParent();
			if (parent != null) {
				nodeMap.get(parent).add(edge);
				assert edge.getParent() == nodeMap.get(parent);
			}
		}

		return edge;
	}

	private ProMGraphPort addPort(final BoundaryDirectedGraphNode node, final DirectedGraphNode boundingNode) {
		final ProMGraphCell cell = nodeMap.get(boundingNode);
		final ProMGraphPort port = cell.addPort(new Point2D.Float(10, 10), node);
		assert port.getParent() == cell;

		boundaryNodeMap.put(node, port);

		return port;
	}

	private void handleChange(final GraphLayoutCacheEvent.GraphLayoutCacheChange change) {
		// A change originated in from the graph. This needs to be reflected in
		// the layoutConnection (if applicable)
		synchronized (model) {
			boolean signalChange = false;
			final Object[] changed = change.getChanged();

			final Set<AttributeMapOwner> changedOwners = new HashSet<AttributeMapOwner>();
			final Set<ProMGraphEdge> edges = new HashSet<ProMGraphEdge>();
			for (final Object o : changed) {
				if (o instanceof ProMGraphCell) {
					// handle a change for a cell
					final ProMGraphCell cell = (ProMGraphCell) o;

					final DirectedGraphNode node = cell.getNode();

					Rectangle2D rect;
					if (change.getSource() instanceof ProMGraphModel) {
						rect = GraphConstants.getBounds(cell.getAttributes());
					} else {
						rect = cell.getView().getBounds();
					}

					if (handleNodeChange(cell, node, rect)) {
						changedOwners.add(node);
						signalChange = true;
					}
				}
				if (o instanceof ProMGraphEdge) {
					edges.add((ProMGraphEdge) o);
				}
			}
			for (final ProMGraphEdge cell : edges) {
				// handle a change for a cell
				final DirectedGraphEdge<?, ?> edge = cell.getEdge();

				List<?> points;
				if (change.getSource() instanceof ProMGraphModel) {
					points = GraphConstants.getPoints(cell.getAttributes());
				} else {
					points = cell.getView().getPoints();
				}

				if (handleEdgeChange(cell, edge, points)) {
					changedOwners.add(edge);
					signalChange = true;
				}
			}
			if (signalChange && !isPIP) {
				layoutConnection.updatedAttributes(changedOwners.toArray(new AttributeMapOwner[0]));
			}
		}
	}

	private boolean handleEdgeChange(final ProMGraphEdge cell, final DirectedGraphEdge<?, ?> edge, final List<?> points) {
		boolean changed = false;

		final List<Point2D> list = new ArrayList<Point2D>(3);
		if (points != null) {
			for (int i = 1; i < points.size() - 1; i++) {
				final Point2D point = (Point2D) points.get(i);
				list.add(new Point2D.Double(point.getX(), point.getY()));
			}
		}
		changed |= layoutConnection.setEdgePoints(edge, list);

		return changed;
	}

	private boolean handleNodeChange(final ProMGraphCell cell, final DirectedGraphNode node, final Rectangle2D rect) {
		boolean changed = false;

		//		// get the view's bounds and put them in the attributemap
		//		Rectangle2D rect = cell.getView().getBounds();
		//		rect = GraphConstants.getBounds(cell.getAttributes());

		if (rect != null) {
			// SIZE
			final Dimension2D size = new Dimension((int) rect.getWidth(), (int) rect.getHeight());
			changed |= layoutConnection.setSize(node, size);

			// POSITION
			final Point2D pos = new Point2D.Double(rect.getX(), rect.getY());
			changed |= layoutConnection.setPosition(node, pos);

		}

		return changed;
	}

	private void registerAsListener() {
		model.addGraphModelListener(this);
		addGraphSelectionListener(this);
		getGraphLayoutCache().addGraphLayoutCacheListener(this);
	}

	private void updateElements(final Collection<?> elements) {

		// For each updated element, find the corresponding view of the corresponding cell and copy the
		// attributes that matter, i.e. size/position/points.

		//The order in which cells, ports and edges are added matters:
		//Cells first, ports second and edges third (because ports are attached to cells and edges to ports.)
		final Vector<ProMGraphElement> cellsToAdd = new Vector<ProMGraphElement>();
		final Vector<ProMGraphElement> portsToAdd = new Vector<ProMGraphElement>();
		final Vector<ProMGraphElement> edgesToAdd = new Vector<ProMGraphElement>();
		final Vector<CellView> cellViewsToAdd = new Vector<CellView>();
		final Vector<CellView> portViewsToAdd = new Vector<CellView>();
		final Vector<CellView> edgeViewsToAdd = new Vector<CellView>();

		final List<DirectedGraphNode> todo = new ArrayList<DirectedGraphNode>();
		final List<Object> toInsert = new ArrayList<Object>();
		final List<Object> toRemove = new ArrayList<Object>();
		final Set<Object> changed = new HashSet<Object>();
		changed.addAll(elements);
		for (final Object element : elements) {
			if (element instanceof DirectedGraphNode) {
				if (!nodeMap.containsKey(element) && !boundaryNodeMap.containsKey(element)) {
					todo.add((DirectedGraphNode) element);
				}
			}
		}
		changed.remove(todo);
		while (!todo.isEmpty()) {
			final Iterator<DirectedGraphNode> it = todo.iterator();
			while (it.hasNext()) {
				final DirectedGraphNode n = it.next();
				if (n instanceof BoundaryDirectedGraphNode) {
					final DirectedGraphNode m = ((BoundaryDirectedGraphNode) n).getBoundingNode();
					if (m != null && !nodeMap.containsKey(m)) {
						// first make sure the bounding node is added
						continue;
					} else if (m != null) {
						// add as port
						addPort((BoundaryDirectedGraphNode) n, m);
						it.remove();
						continue;
					}
				}
				if (n instanceof ContainableDirectedGraphElement) {
					final ContainingDirectedGraphNode c = Cast.<ContainableDirectedGraphElement>cast(n).getParent();
					if (c != null && !nodeMap.containsKey(c)) {
						// if parent is not added yet, then continue
						continue;
					} else if (c == null) {
						toInsert.add(addCell(n));
					} else {
						addCell(n);
					}
				} else {
					toInsert.add(addCell(n));
				}

				it.remove();
			}
		}

		//		getGraphLayoutCache().insert(toInsert.toArray());

		//		getGraphLayoutCache().insert(boundaryNodeMap.values().toArray());
		for (final Object element : elements) {
			if (element instanceof DirectedGraphEdge<?, ?>) {
				if (!edgeMap.containsKey(element)) {
					changed.remove(element);
					final DirectedGraphEdge<?, ?> e = (DirectedGraphEdge<?, ?>) element;
					if (e instanceof ContainableDirectedGraphElement) {
						final ContainingDirectedGraphNode m = Cast.<ContainableDirectedGraphElement>cast(e).getParent();
						if (m == null) {
							toInsert.add(addEdge(e));
						} else {
							addEdge(e);
						}
					} else {
						toInsert.add(addEdge(e));
					}
				}
			}
		}
		getGraphLayoutCache().insert(toInsert.toArray());
		toInsert.clear();

		final Set<Object> graphElements = new HashSet<Object>();
		graphElements.addAll(model.getGraph().getNodes());
		graphElements.addAll(model.getGraph().getEdges());

		for (final Object element : elements) {
			if (element instanceof BoundaryDirectedGraphNode ? ((BoundaryDirectedGraphNode) element).getBoundingNode() != null
					: false) {
				final ProMGraphPort cell = boundaryNodeMap.get(element);
				if (graphElements.contains(element)) {
					portsToAdd.add(cell);
					portViewsToAdd.add(cell.getView());
				} else {
					changed.remove(element);
					boundaryNodeMap.remove(element);
					toInsert.add(cell);
					nodeMap.get(((BoundaryDirectedGraphNode) element).getBoundingNode()).remove(cell);
				}
			} else if (element instanceof DirectedGraphNode) {
				final ProMGraphCell cell = nodeMap.get(element);
				if (graphElements.contains(element)) {
					cellsToAdd.add(cell);
					cellViewsToAdd.add(cell.getView());
				} else {
					changed.remove(element);
					toRemove.add(element);
					toInsert.add(cell);
				}
			} else if (element instanceof DirectedGraphEdge<?, ?>) {
				final ProMGraphEdge cell = edgeMap.get(element);
				if (graphElements.contains(element)) {
					edgesToAdd.add(cell);
					edgeViewsToAdd.add(cell.getView());
				} else {
					changed.remove(element);
					edgeMap.remove(element);
					toInsert.add(cell);
					((ProMGraphPort) nodeMap.get(((DirectedGraphEdge<?, ?>) element).getSource()).getChildAt(0))
							.removeEdge(cell);
					((ProMGraphPort) nodeMap.get(((DirectedGraphEdge<?, ?>) element).getTarget()).getChildAt(0))
							.removeEdge(cell);
				}
			} else if (element instanceof DirectedGraph<?, ?>) {
				// graph has changed
			} else {
				assert false;
			}
		}
		for (final Object element : toRemove) {
			nodeMap.remove(element);
		}
		getGraphLayoutCache().remove(toInsert.toArray());

		final Vector<CellView> views = cellViewsToAdd;
		views.addAll(portViewsToAdd);
		views.addAll(edgeViewsToAdd);
		final Vector<ProMGraphElement> cells = cellsToAdd;
		cells.addAll(portsToAdd);
		cells.addAll(edgesToAdd);
		final Rectangle2D oldBound = GraphLayoutCache.getBounds(views.toArray(new CellView[0]));
		for (final ProMGraphElement cell : cells) {
			if (cell.getView() != null) {
				cell.updateViewsFromMap();
			}
		}
		if (oldBound != null) {
			Rectangle2D.union(oldBound, GraphLayoutCache.getBounds(views.toArray(new CellView[0])), oldBound);
		}
		//		repaint(oldBound.getBounds());
		getGraphLayoutCache().cellViewsChanged(views.toArray(new CellView[0]));
		// HV: Refresh the graph to show the changes.
		refresh();
	}

	/**
	 * Might be overridden to signal that a change was handled
	 */
	protected void changeHandled() {
		layoutConnection.updated();
	}

}

class Change implements GraphModelEvent.GraphModelChange {

	private final Collection<Object> added;
	private final Collection<Object> changed;
	private final Rectangle2D dirtyRegion;
	private final Collection<Object> removed;
	private final ProMGraphModel source;

	public Change(final ProMGraphModel source, final Collection<Object> added, final Collection<Object> removed,
			final Collection<Object> changed, final Rectangle2D dirtyRegion) {
		this.source = source;
		this.added = added;
		this.removed = removed;
		this.changed = changed;
		this.dirtyRegion = dirtyRegion;

	}

	@Override
	public Map<?, ?> getAttributes() {
		return null;
	}

	@Override
	public Object[] getChanged() {
		return changed.toArray();
	}

	@Override
	public ConnectionSet getConnectionSet() {
		return null;
	}

	@Override
	public Object[] getContext() {
		return null;
	}

	@Override
	public Rectangle2D getDirtyRegion() {
		return dirtyRegion;
	}

	@Override
	public Object[] getInserted() {
		return added.toArray();
	}

	@Override
	public ParentMap getParentMap() {
		return null;
	}

	@Override
	public Map<?, ?> getPreviousAttributes() {
		return null;
	}

	@Override
	public ConnectionSet getPreviousConnectionSet() {
		return null;
	}

	@Override
	public ParentMap getPreviousParentMap() {
		return null;
	}

	@Override
	public Object[] getRemoved() {
		return removed.toArray();
	}

	@Override
	public Object getSource() {
		return source;
	}

	@Override
	public CellView[] getViews(final GraphLayoutCache view) {
		return null;
	}

	@Override
	public void putViews(final GraphLayoutCache view, final CellView[] cellViews) {

	}

	@Override
	public void setDirtyRegion(final Rectangle2D dirty) {

	}

}