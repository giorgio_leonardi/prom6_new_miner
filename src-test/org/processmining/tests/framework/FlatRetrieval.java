package org.processmining.tests.framework;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.factory.XFactoryNaiveImpl;
import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;

import com.sun.management.OperatingSystemMXBean;

public class FlatRetrieval {

	public static ArrayList<XTrace> retrieveTraces(XLog L, XTrace T) {
		ArrayList<XTrace> result= new ArrayList();
		XConceptExtension conceptE = XConceptExtension.instance();
		
		for (XTrace TR: L) {
			Iterator<XEvent> TRAct = TR.iterator();
			Iterator<XEvent> TAct = T.iterator();
			
			boolean trovato= true;
			while (TRAct.hasNext() && TAct.hasNext() && trovato) {
				XEvent ev1= TRAct.next();
				XEvent ev2= TAct.next();
				String name1= conceptE.extractName(ev1);
				String name2= conceptE.extractName(ev2);
				if (name1.compareTo(name2) != 0) trovato= false;
			}
			if (trovato) result.add(TR);
		}
		
		return result;
	}
	
	public static XLog readMXMLLog (String fileName) throws Exception {
		XLog result= null;
		
		XMxmlParser parser= new XMxmlParser();
		File fl= new File(fileName);
		List<XLog> logs= parser.parse(fl);
		if (logs != null) {
			if (!logs.isEmpty()) result= logs.get(0);
		}
		
		return result;
	}
	
	public static XTrace createQuery() {
		XFactoryNaiveImpl factory= new XFactoryNaiveImpl(); 
		XTrace query= factory.createTrace();
		XEvent event = factory.createEvent();

		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "START");
		query.add(event);
		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "arrivo_dip_emergenza");
		query.add(event);
		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "valutazione_neurologica");
		query.add(event);
		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "em_neuro_TAC senza mdc");
		query.add(event);
		
		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "em_ecgperformed");
		query.add(event);
////		
		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "em_hematocperformed");
		query.add(event);
		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "ricovero");
		query.add(event);

		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "ric_testecg");
		query.add(event);

				
		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "ric_testcoagulscreening");
		query.add(event);	
		
		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "ric_clinantiaggregating");
		query.add(event);
		
		event = factory.createEvent();
		XConceptExtension.instance().assignName(event, "ric_rehabtherapy");
		query.add(event);
//		event = factory.createEvent();
//		XConceptExtension.instance().assignName(event, "ric_testchestrx");
//		query.add(event);
//		event = factory.createEvent();
//		XConceptExtension.instance().assignName(event, "ric_testdopplersovratrunk");
//		query.add(event);
		return query;
	}
	
	
	
	public static void main(String[] args) {
		OperatingSystemMXBean osmx = (com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();

        // String fileName= args[0];
		String fileName= "voghera.mxml";
		int numRuns=1000;
		int n_iterDaElim=500;
		double [] sperim=new double[numRuns];	
		ArrayList<XTrace> resultList= null;	
		XTrace query= createQuery();
		
					for (int i= 0; i < numRuns; i++) {
		try {
				XLog log= readMXMLLog (fileName);
				if (log == null) System.out.println("LOG NULLO!");
				else {
					long timeStart = System.nanoTime();
					//long timeStart =  osmx.getProcessCpuTime();
					resultList = retrieveTraces (log, query);
					long timeEnd = System.nanoTime();
					//long timeEnd = osmx.getProcessCpuTime();
					sperim[i]=(timeEnd-timeStart)/1000000.0;
					if (resultList == null) System.out.println("RESULTLIST NULLO!");
					else {
						System.out.println("Numero di tracce trovate:" + resultList.size() + " in " + sperim[i] + " millisecondi");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}}
		System.out.println("Numero di tracce recuperate: " + resultList.size());
		double sum=0;
		for (int i=n_iterDaElim;i<numRuns;i++)
			sum+=sperim[i];
		double average=sum/(numRuns-n_iterDaElim);
		double devStd=0;
		for (int i=n_iterDaElim;i<numRuns;i++)
			devStd=Math.pow(average-sperim[i],2);
		devStd=devStd/(numRuns-n_iterDaElim);
		devStd=Math.sqrt(devStd);
		System.out.println("Tempo impiegato in media: " + average + " millisecondi in media");
		System.out.println("deviazione Standard: "+devStd +"millisecondi");
		
	}

}
