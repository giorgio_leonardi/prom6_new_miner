package org.processmining.tests.framework;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;

public class RegExprRetrieval {
	private static boolean DEBUG= true;

	public static XLog readMXMLLog(String fileName) throws Exception {
		XLog result = null;

		XMxmlParser parser = new XMxmlParser();
//		File fl = new File(fileName);
		InputStream fl = new FileInputStream(fileName);
		List<XLog> logs = parser.parse(fl);
		if (logs != null) {
			if (!logs.isEmpty())
				result = logs.get(0);
		}

		return result;
	}

	public static String translateTrace(XTrace T) {
		String trace = "";
		XConceptExtension conceptE = XConceptExtension.instance();

		Iterator<XEvent> TIter = T.iterator();
		while (TIter.hasNext()) {
			XEvent ev1 = TIter.next();
			trace = trace + conceptE.extractName(ev1) + ",";
		}
		trace = trace.substring(0, (trace.length() - 1));

		return trace;
	}

	public static ArrayList<String> createArchive(XLog log) {
		String trace = new String();
		ArrayList<String> output = null;

		// CREO UN ARRAYLIST DI STRINGHE E CREO UNA STRINGA PER OGNI TRACCIA
		// COME: "AZIONE1","AZIONE2",...,"AZIONEn"
		output = new ArrayList<String>();
		for (XTrace T : log) {
			trace = translateTrace(T);
			output.add(trace);
		}

		return output;
	}

	// SECONDA SERIE DI QUERY PER RIVISTA - V2.0

	public static String createQuery1V2() {
		String output = new String();

		output = "^(\\w*,){0,5}Assess_eligibility,Reject_application,Loan_application_rejected$";

		return output;
	}

	public static String createQuery2V2() {
		String output = new String();

		output = "^Loan_application_received,Check_application_form_completeness,Appraise_property,(\\w*,){0,5}$";

		return output;
	}

	public static String createQuery3V2(int i) {
		String output = new String();

		output = "^Loan_application_received,Check_application_form_completeness,(\\w*,){" + i
				+ ",5}Reject_application,Loan_application_rejected$";

		return output;
	}

	public static String createQuery4V2() {
		String output = new String();

		output = "^(\\w*,){0,2}Appraise_property,(\\w*,){1,2}Assess_eligibility,Reject_application,Loan_application_rejected$";

		return output;
	}

	public static String createQuery5V2() {
		String output = new String();

		output = "^(\\w*,){0,1}Check_application_form_completeness,(\\w*,){2,3}Assess_loan_risk,Assess_eligibility,(\\w*,){1,2}Loan_application_rejected$";

		return output;
	}

	// PRIMA SERIE DI QUERY PER RIVISTA - V 1.0

	public static String createQuery1() {
		String output = new String();

		output = "^Loan_application_received,Check_application_form_completeness,Appraise_property,Check_credit_history,";
		output = output + "Assess_loan_risk,Assess_eligibility,Reject_application,Loan_application_rejected$";

		return output;
	}

	public static String createQuery2() {
		String output = new String();

		output = "^Loan_application_received,Check_application_form_completeness,(\\w*,){0,6}";
		output = output + "Loan_application_rejected$";

		return output;
	}

	public static String createQuery3() {
		String output = new String();

		output = "^Loan_application_received,Check_application_form_completeness,Return_application_back_to_applicant,";
		output = output + "Receive_updated_application,Check_application_form_completeness,Appraise_property,";
		output = output + "Check_credit_history,Assess_loan_risk,Assess_eligibility,Send_home_insurance_quote,";
		output = output + "Verify_repayment_agreement,Approve_application,Loan_application_approved$";

		return output;
	}

	public static String createQuery4() {
		String output = new String();

		output = "^(\\w*,){0,3}Receive_updated_application,(\\w*,){0,8}Loan_application_canceled$";

		return output;
	}

	public static String createQuery5() {
		String output = new String();

		output = "^Loan_application_received,Check_application_form_completeness,Return_application_back_to_applicant,";
		output = output
				+ "Receive_updated_application,Check_application_form_completeness,Return_application_back_to_applicant,";
		output = output
				+ "Receive_updated_application,Check_application_form_completeness,Return_application_back_to_applicant,";
		output = output
				+ "Receive_updated_application,Check_application_form_completeness,Return_application_back_to_applicant,";
		output = output
				+ "Receive_updated_application,Check_application_form_completeness,Return_application_back_to_applicant,";
		output = output + "Receive_updated_application,Check_application_form_completeness,Check_credit_history,";
		output = output + "Appraise_property,Assess_loan_risk,Assess_eligibility,Prepare_acceptance_pack,";
		output = output + "Check_if_home_insurance_quote_is_requested,Send_acceptance_pack,Verify_repayment_agreement,";
		output = output + "Approve_application,Loan_application_approved$";

		return output;
	}

	public static String createQuery6() {
		String output = new String();

		output = output
				+ "^(\\w*,){6,6}Receive_updated_application,(\\w*,){3,8}Check_credit_history,(\\w*,|\\w*){5,10}$";

		return output;
	}

	public static int executeQuery(ArrayList<String> archive, String regexpr) {
		int output = 0;
		String element;
		Iterator<String> iter = archive.iterator();

		while (iter.hasNext()) {
			element = iter.next();

			Pattern pattern = Pattern.compile(regexpr);
			Matcher matcher = pattern.matcher(element);

			if (matcher.matches()) {
				output++;
			}
		}

		return output;
	}

	public static int executeQueryLog(XLog archive, String regexpr) {
		int output = 0;
		String element;

		for (XTrace T : archive) {
			element = translateTrace(T);

			Pattern pattern = Pattern.compile(regexpr);
			Matcher matcher = pattern.matcher(element);

			if (matcher.matches()) {
				output++;
			}
		}

		return output;
	}

	public static double executeCompleteTestFixed(ArrayList<String> archive, int numRuns, int numQuery) {
		double output = 0;
		long startTime, endTime;
		String regExp = "";
		int result = 0;

		startTime = System.currentTimeMillis();
		switch (numQuery) {
			case 1 :
				regExp = createQuery1();
				break;
			case 2 :
				regExp = createQuery2();
				break;
			case 3 :
				regExp = createQuery3();
				break;
			case 4 :
				regExp = createQuery4();
				break;
			case 5 :
				regExp = createQuery5();
				break;
			case 6 :
				regExp = createQuery6();
				break;
		}
		System.out.println("\n\n Execution of query: " + regExp);

		for (int i = 0; i < numRuns; i++) {
			result = executeQuery(archive, regExp);
		}
		endTime = System.currentTimeMillis();

		if (DEBUG) System.out.println("RESULT : " + result + " Matches");
		output = ((double) endTime - (double) startTime) / (double) numRuns;

		return output;
	}

	public static double executeCompleteTestFixedLog(XLog archive, int numRuns, int numQuery) {
		double output = 0;
		long startTime, endTime;
		String regExp = "";
		int result = 0;

		startTime = System.currentTimeMillis();
		switch (numQuery) {
			case 1 :
				regExp = createQuery1();
				break;
			case 2 :
				regExp = createQuery2();
				break;
			case 3 :
				regExp = createQuery3();
				break;
			case 4 :
				regExp = createQuery4();
				break;
			case 5 :
				regExp = createQuery5();
				break;
			case 6 :
				regExp = createQuery6();
				break;
		}
		System.out.println("\n\n Execution of query: " + regExp);

		for (int i = 0; i < numRuns; i++) {
			result = executeQueryLog(archive, regExp);
		}
		endTime = System.currentTimeMillis();

		if (DEBUG) System.out.println("RESULT : " + result + " Matches");
		output = ((double) endTime - (double) startTime) / (double) numRuns;

		return output;
	}

	public static double executeCompleteTestFixedV2(ArrayList<String> archive, int numRuns, int numQuery) {
		double output = 0;
		long startTime, endTime;
		String regExp = "";
		int result = 0;

		startTime = System.currentTimeMillis();
		switch (numQuery) {
			case 1 :
				regExp = createQuery1V2();
				break;
			case 2 :
				regExp = createQuery2V2();
				break;
			case 3 :
				regExp = createQuery3V2(0);
				break;
			case 4 :
				regExp = createQuery3V2(1);
				break;
			case 5 :
				regExp = createQuery3V2(2);
				break;
			case 6 :
				regExp = createQuery3V2(3);
				break;
			case 7 :
				regExp = createQuery3V2(4);
				break;
			case 8 :
				regExp = createQuery4V2();
				break;
			case 9 :
				regExp = createQuery5V2();
				break;
		}
		System.out.println("\n\n Execution of query: " + regExp);

		for (int i = 0; i < numRuns; i++) {
			result = executeQuery(archive, regExp);
		}
		endTime = System.currentTimeMillis();

		if (DEBUG) System.out.println("RESULT : " + result + " Matches");
		output = ((double) endTime - (double) startTime) / (double) numRuns;

		return output;
	}

	public static double executeCompleteTestFixedLogV2(XLog archive, int numRuns, int numQuery) {
		double output = 0;
		long startTime, endTime;
		String regExp = "";
		int result = 0;

		switch (numQuery) {
			case 1 :
				regExp = createQuery1V2();
				break;
			case 2 :
				regExp = createQuery2V2();
				break;
			case 3 :
				regExp = createQuery3V2(0);
				break;
			case 4 :
				regExp = createQuery3V2(1);
				break;
			case 5 :
				regExp = createQuery3V2(2);
				break;
			case 6 :
				regExp = createQuery3V2(3);
				break;
			case 7 :
				regExp = createQuery3V2(4);
				break;
			case 8 :
				regExp = createQuery4V2();
				break;
			case 9 :
				regExp = createQuery5V2();
				break;
		}
		System.out.println("\n\n Execution of query: " + regExp);

		startTime = System.currentTimeMillis();
		for (int i = 0; i < numRuns; i++) {
			result = executeQueryLog(archive, regExp);
		}
		endTime = System.currentTimeMillis();

		if (DEBUG) System.out.println("RESULT : " + result + " Matches");
		output = ((double) endTime - (double) startTime) / (double) numRuns;

		return output;
	}

	public static void main(String[] args) throws IOException {
		int numRuns = 10; // N. OF RUNS FOR EACH QUERY
		ArrayList<String> archivesOriginale; // LOGS OF DIFFERENT DIMENSIONS
		ArrayList<String> archivesDuplicato; // LOGS OF DIFFERENT DIMENSIONS

		XLog archivesLogOriginale; // LOGS OF DIFFERENT DIMENSIONS
		XLog archivesLogDuplicato; // LOGS OF DIFFERENT DIMENSIONS

		String fileNamesOriginali[] = { "log1k.mxml", "log3k.mxml", "log5k.mxml", "log7k.mxml", "log10k.mxml" };
		String fileNamesDuplicati[] = { "log1k.mxml", "log2k.mxml", "log4k.mxml", "log8k.mxml", "log16k.mxml",
				"log32k.mxml" };
		// NAMES OF FILES CONTAINING THE LOGS TO BE LOADED
		int i, q;

		try {
			FileWriter outFile;
			outFile = new FileWriter("Risultati.csv");

			BufferedWriter outBuffer;
			outBuffer = new BufferedWriter(outFile);

			outBuffer.write("RISULTATI SPERIMENTAZIONE REGEXP;;;;;\n\n");
			outBuffer.write("RISULTATI DATASETS ORIGINALI CAMPIONATI;;;;;\n\n");
			outBuffer.write("DIMENSIONE ARCHIVIO;QUERY1 OTF;QUERY1 PRE;QUERY2 OTF;QUERY2 PRE;QUERY3 OTF;QUERY3 PRE;QUERY4 OTF;QUERY4 PRE;QUERY5 OTF;QUERY5 PRE;");
			outBuffer.write("QUERY6 OTF;QUERY6 PRE;QUERY7 OTF;QUERY7 PRE;QUERY8 OTF;QUERY8 PRE;QUERY9 OTF;QUERY9 PRE\n");

			String directoryName = "C:\\Users\\Luca\\git\\prom\\log\\";
			for (i = 0; i < fileNamesOriginali.length; i++) {
				System.out.println(" Loading the archive: " + fileNamesOriginali[i]);
				archivesLogOriginale = readMXMLLog(directoryName + fileNamesOriginali[i]);
				archivesOriginale= createArchive(archivesLogOriginale);
				System.out.println(" Archive loaded. " + archivesLogOriginale.size() + " traces in memory \n");
				outBuffer.write(String.valueOf(archivesLogOriginale.size()));
				for (q = 1; q < 10; q++) {
					System.out.println("\n Benchmark 1 in execution... \n");
					System.out.println("\n\n Query of type " + q + ":");
					//        			outBuffer.write("Query " + q);

					double elapsedTime = executeCompleteTestFixedLogV2(archivesLogOriginale, numRuns, q);
					outBuffer.write(";" + String.valueOf(elapsedTime).replace(".", ","));
					elapsedTime = executeCompleteTestFixedV2(archivesOriginale, numRuns, q);
					outBuffer.write(";" + String.valueOf(elapsedTime).replace(".", ","));

					System.out.print(" Mean time to perform this query, using " + numRuns + " runs ");
					System.out.print("on: " + archivesLogOriginale.size() + " elements: ");
					System.out.println(elapsedTime + " milli-seconds.");
				}
				outBuffer.write("\n");
				archivesLogOriginale.clear();
				archivesOriginale.clear();
			}

			outBuffer.write("\n\nRISULTATI DATASETS PER DUPLICAZIONE;;;;;;\n\n");
			outBuffer.write("DIMENSIONE ARCHIVIO;QUERY1 OTF;QUERY1 PRE;QUERY2 OTF;QUERY2 PRE;QUERY3 OTF;QUERY3 PRE;QUERY4 OTF;QUERY4 PRE;QUERY5 OTF;QUERY5 PRE;");
			outBuffer.write("QUERY6 OTF;QUERY6 PRE;QUERY7 OTF;QUERY7 PRE;QUERY8 OTF;QUERY8 PRE;QUERY9 OTF;QUERY9 PRE\n");
			directoryName = "C:\\Users\\Luca\\git\\prom\\log\\";
			for (i = 0; i < fileNamesDuplicati.length; i++) {
				System.out.println(" Loading the archive: " + fileNamesDuplicati[i]);
				archivesLogDuplicato = readMXMLLog(directoryName + fileNamesDuplicati[i]);
				archivesDuplicato= createArchive(archivesLogDuplicato);
				System.out.println(" Archive loaded. " + archivesLogDuplicato.size() + " traces in memory \n");
				outBuffer.write(String.valueOf(archivesLogDuplicato.size()));
				for (q = 1; q < 10; q++) {
					System.out.println("\n Benchmark 2 in execution... \n");
					System.out.println("\n\n Query of type " + q + ":");
					//                    outBuffer.write("Query " + q);

					double elapsedTime = executeCompleteTestFixedLogV2(archivesLogDuplicato, numRuns, q);
					outBuffer.write(";" + String.valueOf(elapsedTime).replace(".", ","));
					elapsedTime = executeCompleteTestFixedV2(archivesDuplicato, numRuns, q);
					outBuffer.write(";" + String.valueOf(elapsedTime).replace(".", ","));

					System.out.print(" Mean time to perform this query, using " + numRuns + " runs ");
					System.out.print("on: " + archivesLogDuplicato.size() + " elements: ");
					System.out.println(elapsedTime + " milli-seconds.");
				}
				outBuffer.write("\n");
				archivesLogDuplicato.clear();
				archivesDuplicato.clear();
			}
			System.out.println("\n\n All Tests done. ");
			outBuffer.flush();
			outBuffer.close();
			outFile.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("\n\n All tests performed. Please see the log for the results");
	}
}
