package org.processmining.tests.framework;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;

public class RegExpTkde {
	private static boolean DEBUG= true;

	public static XLog readMXMLLog(String fileName) throws Exception {
		XLog result = null;

		XMxmlParser parser = new XMxmlParser();
//		File fl = new File(fileName);
		InputStream fl = new FileInputStream(fileName);
		List<XLog> logs = parser.parse(fl);
		if (logs != null) {
			if (!logs.isEmpty())
				result = logs.get(0);
		}

		return result;
	}

	public static String translateTrace(XTrace T) {
		String trace = "";
		XConceptExtension conceptE = XConceptExtension.instance();

		Iterator<XEvent> TIter = T.iterator();
		while (TIter.hasNext()) {
			XEvent ev1 = TIter.next();
			trace = trace + conceptE.extractName(ev1) + ",";
		}
		trace = trace.substring(0, (trace.length() - 1));

		return trace;
	}

	public static ArrayList<String> createArchive(XLog log) {
		String trace = new String();
		ArrayList<String> output = null;

		// CREO UN ARRAYLIST DI STRINGHE E CREO UNA STRINGA PER OGNI TRACCIA
		// COME: "AZIONE1","AZIONE2",...,"AZIONEn"
		output = new ArrayList<String>();
		for (XTrace T : log) {
			trace = translateTrace(T);
			output.add(trace);
		}
//		System.out.println(output);
		return output;
	}

	// SERIE DI QUERY PER RIVISTA - 

	public static ArrayList<String> createQueryDensity() {
		ArrayList<String> output = new ArrayList<String>();
		output.add("^P,I,C,W(\\w*,){3,8}END$");
		
		return output;
	}
	
	public static ArrayList<String> createQuery1() {
		ArrayList<String> output = new ArrayList<String>();
		
		output.add("^M,H,F(\\w*,){0,8}END$");
		
		return output;
	}
	//F&T
	public static ArrayList<String> createQuery2() {
		ArrayList<String> output = new ArrayList<String>();
		output.add("^M,H,F,T(\\w*,){0,8}END$");
		output.add("^M,H,T,F(\\w*,){0,8}END$");
		return output;
	}
	//F&T&I
	public static ArrayList<String> createQuery3() {
		ArrayList<String> output = new ArrayList<String>();
		//PERMUTAZIONI CON F PRIMO ELEMENTO
		output.add("^M,H,F,T,I(\\w*,){0,8}END$");
		output.add("^M,H,F,I,T(\\w*,){0,8}END$");
		
		//PERMUTAZIONI CON T PRIMO ELEMENTO
		output.add("^M,H,T,F,I(\\w*,){0,8}END$");
		output.add("^M,H,T,I,F(\\w*,){0,8}END$");
		
		//PERMUTAZIONI CON I PRIMO ELEMENTO
		output.add("^M,H,I,T,F(\\w*,){0,8}END$");
		output.add("^M,H,I,F,T(\\w*,){0,8}END$");
		return output;
	}
	//F&T&I&W
	public static ArrayList<String> createQuery4() {
		ArrayList<String> output = new ArrayList<String>();
		//PERMUTAZIONI CON F PRIMO ELEMENTO
		output.add("^M,H,F,T,I,W(\\w*,){0,8}END$");
		output.add("^M,H,F,I,T,W(\\w*,){0,8}END$");
		output.add("^M,H,F,W,I,T(\\w*,){0,8}END$");
		output.add("^M,H,F,W,T,I(\\w*,){0,8}END$");
		output.add("^M,H,F,T,W,I(\\w*,){0,8}END$");
		output.add("^M,H,F,I,W,T(\\w*,){0,8}END$");
		//PERMUTAZIONI CON T PRIMO ELEMENTO
		output.add("^M,H,T,F,I,W(\\w*,){0,8}END$");
		output.add("^M,H,T,I,F,W(\\w*,){0,8}END$");
		output.add("^M,H,T,W,I,F(\\w*,){0,8}END$");
		output.add("^M,H,T,W,F,I(\\w*,){0,8}END$");
		output.add("^M,H,T,F,W,I(\\w*,){0,8}END$");
		output.add("^M,H,T,I,W,F(\\w*,){0,8}END$");
		//PERMUTAZIONI CON I PRIMO ELEMENTO
		output.add("^M,H,I,T,F,W(\\w*,){0,8}END$");
		output.add("^M,H,I,F,T,W(\\w*,){0,8}END$");
		output.add("^M,H,I,W,F,T(\\w*,){0,8}END$");
		output.add("^M,H,I,W,T,F(\\w*,){0,8}END$");
		output.add("^M,H,I,T,W,F(\\w*,){0,8}END$");
		output.add("^M,H,I,F,W,T(\\w*,){0,8}END$");
		//PERMUTAZIONI CON W PRIMO ELEMENTO
		output.add("^M,H,W,F,T,I(\\w*,){0,8}END$");
		output.add("^M,H,W,F,I,T(\\w*,){0,8}END$");
		output.add("^M,H,W,T,F,I(\\w*,){0,8}END$");
		output.add("^M,H,W,T,I,F(\\w*,){0,8}END$");
		output.add("^M,H,W,I,T,F(\\w*,){0,8}END$");
		output.add("^M,H,W,I,F,T(\\w*,){0,8}END$");
		return output;
	}
//
//	public static String createQuery5V2() {
//		String output = new String();
//
//		output = "^(\\w*,){0,1}Check_application_form_completeness,(\\w*,){2,3}Assess_loan_risk,Assess_eligibility,(\\w*,){1,2}Loan_application_rejected$";
//
//		return output;
//	}

	// PRIMA SERIE DI QUERY PER RIVISTA - V 1.0

//	public static String createQuery1() {
//		String output = new String();
//
//		output = "^Loan_application_received,Check_application_form_completeness,Appraise_property,Check_credit_history,";
//		output = output + "Assess_loan_risk,Assess_eligibility,Reject_application,Loan_application_rejected$";
//
//		return output;
//	}
//
//	public static String createQuery2() {
//		String output = new String();
//
//		output = "^Loan_application_received,Check_application_form_completeness,(\\w*,){0,6}";
//		output = output + "Loan_application_rejected$";
//
//		return output;
//	}
//
//	public static String createQuery3() {
//		String output = new String();
//
//		output = "^Loan_application_received,Check_application_form_completeness,Return_application_back_to_applicant,";
//		output = output + "Receive_updated_application,Check_application_form_completeness,Appraise_property,";
//		output = output + "Check_credit_history,Assess_loan_risk,Assess_eligibility,Send_home_insurance_quote,";
//		output = output + "Verify_repayment_agreement,Approve_application,Loan_application_approved$";
//
//		return output;
//	}
//
//	public static String createQuery4() {
//		String output = new String();
//
//		output = "^(\\w*,){0,3}Receive_updated_application,(\\w*,){0,8}Loan_application_canceled$";
//
//		return output;
//	}
//
//	public static String createQuery5() {
//		String output = new String();
//
//		output = "^Loan_application_received,Check_application_form_completeness,Return_application_back_to_applicant,";
//		output = output
//				+ "Receive_updated_application,Check_application_form_completeness,Return_application_back_to_applicant,";
//		output = output
//				+ "Receive_updated_application,Check_application_form_completeness,Return_application_back_to_applicant,";
//		output = output
//				+ "Receive_updated_application,Check_application_form_completeness,Return_application_back_to_applicant,";
//		output = output
//				+ "Receive_updated_application,Check_application_form_completeness,Return_application_back_to_applicant,";
//		output = output + "Receive_updated_application,Check_application_form_completeness,Check_credit_history,";
//		output = output + "Appraise_property,Assess_loan_risk,Assess_eligibility,Prepare_acceptance_pack,";
//		output = output + "Check_if_home_insurance_quote_is_requested,Send_acceptance_pack,Verify_repayment_agreement,";
//		output = output + "Approve_application,Loan_application_approved$";
//
//		return output;
//	}
//
//	public static String createQuery6() {
//		String output = new String();
//
//		output = output
//				+ "^(\\w*,){6,6}Receive_updated_application,(\\w*,){3,8}Check_credit_history,(\\w*,|\\w*){5,10}$";
//
//		return output;
//	}

	public static int executeQuery(ArrayList<String> archive, ArrayList<String> regExp) {
		int output = 0;
		String element;
		

		for (String r:regExp){
			Iterator<String> iter = archive.iterator();
			while (iter.hasNext()) {
				element = iter.next();

				Pattern pattern = Pattern.compile(r);
				Matcher matcher = pattern.matcher(element);

				if (matcher.matches()) {
					output++;
				}
			}
		}
		return output;
	}

	public static int executeQueryLog(XLog archive, ArrayList<String> regExp) {
		int output = 0;
		String element;
		for (String r:regExp){
			for (XTrace T : archive) {
				element = translateTrace(T);
	
				Pattern pattern = Pattern.compile(r);
				Matcher matcher = pattern.matcher(element);
	
				if (matcher.matches()) {
					output++;
				}
			}
		}
		return output;
	}

//	public static double executeCompleteTestFixed(ArrayList<String> archive, int numRuns, int numQuery) {
//		double output = 0;
//		long startTime, endTime;
//		String regExp = "";
//		int result = 0;
//
//		startTime = System.currentTimeMillis();
//		switch (numQuery) {
//			case 1 :
//				regExp = createQuery1();
//				break;
//			case 2 :
//				regExp = createQuery2();
//				break;
//			case 3 :
//				regExp = createQuery3();
//				break;
//			case 4 :
//				regExp = createQuery4();
//				break;
//			case 5 :
//				regExp = createQuery5();
//				break;
//			case 6 :
//				regExp = createQuery6();
//				break;
//		}
//		System.out.println("\n\n Execution of query: " + regExp);
//
//		for (int i = 0; i < numRuns; i++) {
//			result = executeQuery(archive, regExp);
//		}
//		endTime = System.currentTimeMillis();
//
//		if (DEBUG) System.out.println("RESULT : " + result + " Matches");
//		output = ((double) endTime - (double) startTime) / (double) numRuns;
//
//		return output;
//	}

//	public static double executeCompleteTestFixedLog(XLog archive, int numRuns, int numQuery) {
//		double output = 0;
//		long startTime, endTime;
//		String regExp = "";
//		int result = 0;
//
//		startTime = System.currentTimeMillis();
//		switch (numQuery) {
//			case 1 :
//				regExp = createQuery1();
//				break;
//			case 2 :
//				regExp = createQuery2();
//				break;
//			case 3 :
//				regExp = createQuery3();
//				break;
//			case 4 :
//				regExp = createQuery4();
//				break;
//			case 5 :
//				regExp = createQuery5();
//				break;
//			case 6 :
//				regExp = createQuery6();
//				break;
//		}
//		System.out.println("\n\n Execution of query: " + regExp);
//
//		for (int i = 0; i < numRuns; i++) {
//			result = executeQueryLog(archive, regExp);
//		}
//		endTime = System.currentTimeMillis();
//
//		if (DEBUG) System.out.println("RESULT : " + result + " Matches");
//		output = ((double) endTime - (double) startTime) / (double) numRuns;
//
//		return output;
//	}
	
	public static double executeCompleteTestLogDensity(ArrayList<String> archive, int numRuns, int numQuery) {
		double output = 0;
		long startTime, endTime;
		ArrayList<String> regExp = null ;
		int result = 0;

		startTime = System.currentTimeMillis();
		switch (numQuery) {
			case 1 :
				regExp = createQueryDensity();
				break;
		}
		System.out.println("\n\n Execution of query: " + numQuery);

		for (int i = 0; i < numRuns; i++) {
			result = executeQuery(archive, regExp);
		}
		endTime = System.currentTimeMillis();

		if (DEBUG) System.out.println("RESULT : " + result + " Matches");
		output = ((double) endTime - (double) startTime) / (double) numRuns;

		return output;
	}

	public static double executeCompleteTestAnyOrder(ArrayList<String> archive, int numRuns, int numQuery) {
		double output = 0;
		long startTime, endTime;
		ArrayList<String> regExp = null ;
		int result = 0;

		startTime = System.currentTimeMillis();
		switch (numQuery) {
			case 1 :
				regExp = createQuery1();
				break;
			case 2 :
				regExp = createQuery2();
				break;
			case 3 :
				regExp = createQuery3();
				break;
			case 4 :
				regExp = createQuery4();
				break;
//			case 5 :
//				regExp = createQuery3V2(2);
//				break;
//			case 6 :
//				regExp = createQuery3V2(3);
//				break;
//			case 7 :
//				regExp = createQuery3V2(4);
//				break;
//			case 8 :
//				regExp = createQuery4V2();
//				break;
//			case 9 :
//				regExp = createQuery5V2();
//				break;
		}
		System.out.println("\n\n Execution of query: " + numQuery);

		for (int i = 0; i < numRuns; i++) {
			result = executeQuery(archive, regExp);
		}
		endTime = System.currentTimeMillis();

		if (DEBUG) System.out.println("RESULT : " + result + " Matches");
		output = ((double) endTime - (double) startTime) / (double) numRuns;

		return output;
	}

	public static double executeCompleteTestFixedLogV2(XLog archive, int numRuns, int numQuery) {
		double output = 0;
		long startTime, endTime;
		ArrayList<String> regExp = null;
		int result = 0;

		switch (numQuery) {
			case 1 :
				regExp = createQuery1();
				break;
			case 2 :
				regExp = createQuery2();
				break;
			case 3 :
				regExp = createQuery3();
				break;
//			case 4 :
//				regExp = createQuery3V2(1);
//				break;
//			case 5 :
//				regExp = createQuery3V2(2);
//				break;
//			case 6 :
//				regExp = createQuery3V2(3);
//				break;
//			case 7 :
//				regExp = createQuery3V2(4);
//				break;
//			case 8 :
//				regExp = createQuery4V2();
//				break;
//			case 9 :
//				regExp = createQuery5V2();
//				break;
		}
		System.out.println("\n\n Execution of query: " + regExp);

		startTime = System.currentTimeMillis();
		for (int i = 0; i < numRuns; i++) {
			result = executeQueryLog(archive, regExp);
		}
		endTime = System.currentTimeMillis();

		if (DEBUG) System.out.println("RESULT : " + result + " Matches");
		output = ((double) endTime - (double) startTime) / (double) numRuns;

		return output;
	}

	public static void main(String[] args) throws IOException {
		int numRuns = 1000; // N. OF RUNS FOR EACH QUERY
		ArrayList<String> archives; // LOGS OF DIFFERENT DIMENSIONS

		XLog archivesLog; // LOGS OF DIFFERENT DIMENSIONS
		// NAMES OF FILES CONTAINING THE LOGS TO BE LOADED
		String fileNamesReplicati[] = { "logSample.mxml", "logSample2k.mxml", "logSample4k.mxml", "logSample8k.mxml", "logSample16k.mxml" };
		String fileName="ch2012A.mxml";
		
		int i, q;

		try {
			FileWriter outFile;
			outFile = new FileWriter("Risultati.csv");

			BufferedWriter outBuffer;
			outBuffer = new BufferedWriter(outFile);

			outBuffer.write("RISULTATI SPERIMENTAZIONE REGEXP;;;;;\n\n");
			outBuffer.write("RISULTATI DATASETS CON CRESCENTE DENSITA';;;;;\n\n");
			outBuffer.write("DIMENSIONE ARCHIVIO;QUERY1\n");

			String directoryName = "C:\\Users\\Luca\\git\\prom\\log\\";
			for (i = 0; i < fileNamesReplicati.length; i++) {
				System.out.println(" Loading the archive: " + fileNamesReplicati[i]);
				archivesLog = readMXMLLog(directoryName + fileNamesReplicati[i]);
				archives= createArchive(archivesLog);
				System.out.println(" Archive loaded. " + archivesLog.size() + " traces in memory \n");
				outBuffer.write(String.valueOf(archivesLog.size()));
				for (q = 1; q < 2; q++) {
					System.out.println("\n Benchmark 1 in execution... \n");
					System.out.println("\n\n Query of type " + q + ":");
//					outBuffer.write("Query " + q);

//					double elapsedTime = executeCompleteTestLogDensity(archivesLogOriginale, numRuns, q);
//					outBuffer.write(";" + String.valueOf(elapsedTime).replace(".", ","));
					double elapsedTime = executeCompleteTestLogDensity(archives, numRuns, q);
					outBuffer.write(";" + String.valueOf(elapsedTime).replace(".", ","));

					System.out.print(" Mean time to perform this query, using " + numRuns + " runs ");
					System.out.print("on: " + archivesLog.size() + " elements: ");
					System.out.println(elapsedTime + " milli-seconds.");
				}
				outBuffer.write("\n");
				archivesLog.clear();
				archives.clear();
			}

			outBuffer.write("\n\nRISULTATI DATASETS PER AND CRESCENTE;;;;;;\n\n");
			outBuffer.write("DIMENSIONE ARCHIVIO;QUERY1;QUERY2;QUERY3;QUERY4;\n");

			System.out.println(" Loading the archive: " + fileName);
			archivesLog = readMXMLLog(directoryName + fileName);
			archives= createArchive(archivesLog);
			System.out.println(" Archive loaded. " + archivesLog.size() + " traces in memory \n");
			outBuffer.write(String.valueOf(archivesLog.size()));
			for (q = 1; q < 5; q++) {
				System.out.println("\n Benchmark 2 in execution... \n");
				System.out.println("\n\n Query of type " + q + ":");
                //outBuffer.write("Query " + q);

//				double elapsedTime = executeCompleteTestFixedLogV2(archivesLogDuplicato, numRuns, q);
//					outBuffer.write(";" + String.valueOf(elapsedTime).replace(".", ","));
                double elapsedTime = executeCompleteTestAnyOrder(archives, numRuns, q);
				outBuffer.write(";" + String.valueOf(elapsedTime).replace(".", ","));
//
				System.out.print(" Mean time to perform this query, using " + numRuns + " runs ");
				System.out.print("on: " + archivesLog.size() + " elements: ");
				System.out.println(elapsedTime + " milli-seconds.");
			}
			outBuffer.write("\n");
			archivesLog.clear();
			archives.clear();
//		}
		System.out.println("\n\n All Tests done. ");
		outBuffer.flush();
		outBuffer.close();
		outFile.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("\n\n All tests performed. Please see the log for the results");
	}
}
