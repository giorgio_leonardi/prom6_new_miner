package thomson;


public class Delay extends Pattern {

	private int min;
	private int max;
	
	public Delay(String s){
	//	System.out.println("Delay "+s);
		String[] n = s.split(",");
		min=Integer.parseInt(n[0]);
		max=Integer.parseInt(n[1]);
	}
	public String toString(){
		return "Delay " + min +"-"+max +"\n";
	}
	public int getMin() {
		
		return min;
	}
	public int getMax() {
		
		return max;
	}
}
