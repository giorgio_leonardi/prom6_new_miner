package thomson;




public class Sequence extends Pattern{
	
	public Sequence(String s){
		//System.out.println("Sequence "+s);
		element = s.split(",");
	}
	public String toString()
	{
		String res="";
		for (String s:element){
			res+=s+"-";
		}
		return res.substring(0,res.length()-1)+"\n";
	}
}
