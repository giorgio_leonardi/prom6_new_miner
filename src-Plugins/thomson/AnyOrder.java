package thomson;

public class AnyOrder extends Pattern {

	public AnyOrder(String s){
		//System.out.println("Any Order: "+s);
		element=s.split("&");
	}
	public String toString()
	{
		String res="";
		for (String s:element){
			res+=s+"&";
		}
		return res.substring(0,res.length()-1)+"\n";
	}
}
