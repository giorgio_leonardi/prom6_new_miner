package permutation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

/**
 
 *   @author luca
 *
 */
public class Permutation {
	
	private static List<DirectionalEntity> entityList = new ArrayList<DirectionalEntity>();
	
	private static Integer numPermutations = new Integer("0");
	
	private static ArrayList<DirectionalEntity> sortedList = new ArrayList<DirectionalEntity>();
	
	public static Collection<String> Initialize(String str) {
		
		//string tokenizer che divide la stringa quando trova &
		StringTokenizer st=new StringTokenizer(str,"&");
		//costruisco le entit� in base ai diversi eventi
		while (st.hasMoreTokens()){
			DirectionalEntity entity = new DirectionalEntity(st.nextToken().trim(), -1);
			entityList.add(entity);
		}
		//li ordino
		Collections.sort(entityList);
		//inizializzo la lista che poi utilizzer� per le permutazioni
		sortedList.addAll(entityList);
		//faccio il reverse
		Collections.reverse(sortedList);
		numPermutations++;
		Collection<String> result=new ArrayList<String>(entityList.size());
		for(DirectionalEntity e : entityList) {
			result.add(e.getEntity());
		}
		return result;
	}
	
	public static Collection<String>  getPermutation() {
		
		Collection<String> result=new ArrayList<String>(entityList.size());
		DirectionalEntity mobileDirectionalEntity = getMaxMobileDirectionalEntity();
		if(mobileDirectionalEntity == null) 
				// sono state trovate tutte le permutazioni
				return null;			
		//controllo la direzione dell'elemento con il max mobile
		if(mobileDirectionalEntity.getDirection() == 1) {
			// cambia l'elemento con max mobile con l'elemento a destra nella lista
			int positionInOriginalList = entityList.indexOf(mobileDirectionalEntity);
			entityList.set(positionInOriginalList, entityList.get(positionInOriginalList+1));
			entityList.set(positionInOriginalList+1, mobileDirectionalEntity);
		}
		else {
			// cambia l'elemento con max mobile con l'elemento a sinistra nella lista
			int positionInOriginalList = entityList.indexOf(mobileDirectionalEntity);
			entityList.set(positionInOriginalList, entityList.get(positionInOriginalList-1));
			entityList.set(positionInOriginalList-1, mobileDirectionalEntity);
		}
		
		// ora cambiamo la direzione di tutti gli elementi maggiori di mobileDirectionalNumber
		for(DirectionalEntity e : entityList) {
			if(mobileDirectionalEntity.compareTo(e) < 0)
				e.reverseDir();
		}
		//abbiamo una nuova permutazione
		for(DirectionalEntity e : entityList) {
			result.add(e.getEntity());
		}
			
		numPermutations++;
			
		return result;
		}
	
	
	/**
	 * ricerca l'elemento con il numero mobile massimo controllando i numeri in ordine di grandezza decrescente 
	 * 
	 * @return mobileDirectionalNumber, se esiste altrimenti null
	 */
	private static DirectionalEntity getMaxMobileDirectionalEntity() {
		for(DirectionalEntity dirEntity : sortedList) {
			DirectionalEntity maxDirEntity = dirEntity;
			// adesso controlla per la posizione di maxDirNum nella lista originale per vedere se 
			// questo maxDirNum � attualmente un mobile directional number
			int positionInOriginalList = entityList.indexOf(maxDirEntity);
			
			if((maxDirEntity.getDirection() == -1 && (positionInOriginalList != 0 && entityList.get(positionInOriginalList-1).compareTo(maxDirEntity) < 0)
					|| (maxDirEntity.getDirection() == 1 && (positionInOriginalList != entityList.size()-1 && entityList.get(positionInOriginalList+1).compareTo(maxDirEntity) < 0)))) {
				// questo � il max directional number
				return maxDirEntity;
			}
		}
		return null; //sono state trovate tutte le permutazioni
	}

	public static void close() {
		entityList.clear();
		numPermutations=0;
		sortedList.clear();
		
	}

	public static Set<String> getElement() {
		TreeSet<String> res=new TreeSet<String>();
		for (DirectionalEntity s: entityList)
			res.add(s.getEntity());
		return res;
	}
	
	
}
