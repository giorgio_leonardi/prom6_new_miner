package permutation;

/**
 *   @author luca
 *
 */
public class DirectionalEntity implements Comparable<DirectionalEntity> {
	
	private String entity;
	
	private int direction; // can be -1 or 1 depending on the position
	
	public DirectionalEntity(String entity, int dir) {
		this.entity = entity;
		this.direction = dir;
	}
	

	@Override
	public int compareTo(DirectionalEntity directionalEntity) {
		return (this.entity.compareTo(directionalEntity.entity));
	}
	
	public void reverseDir() {
		direction = -direction ;
	}

	public String getEntity() {
		return entity;
	}

	public int getDirection() {
		return direction;
	}

}
