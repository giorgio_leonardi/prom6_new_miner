package ioUtility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class LabelUtility {
	public static  int intersection(String event, ArrayList<Set<String>> p,boolean hard) {
		String[] token=event.split("&&");
		boolean trovato=false;
		int j =0;
	//	System.out.println(event+" "+token.length);
		while (j<p.size() && !trovato){
			int nMatch=0;
			Set<String> t= p.get(j);
			int i=0;
			Iterator<String> it=t.iterator();
			String next=it.next();
			while (i<token.length ) {
				//System.out.println("Compara "+token[i]+"e "+next);
				int comp=token[i].compareTo(next);
				if (comp==0){
					if (hard)
						trovato=true;
					else nMatch++;
					break;
				}else if (comp>0) 
						if (it.hasNext())
							next=it.next();
						else {
							i++;
							it=t.iterator();
							next=it.next();
						}
					else i++;
			}
			if (nMatch==token.length) 
				trovato=true;
			j++;
		}
		if (trovato)
		{
			Set<String> s=p.get(j-1);
			for (int i=0;i<token.length;i++)
				s.add(token[i]);
			int size;
			while (j<p.size()){
				size=p.size();
				Set<String> t= p.get(j);
				int i=0;
				Iterator<String> it=t.iterator();
				String next=it.next();
				while (i<token.length ) {
					//System.out.println("Compara "+token[i]+"e "+next);
					int comp=token[i].compareTo(next);
					if (comp==0){
						s.addAll(t);
						p.remove(j);
						break;
					}else if (comp>0) 
							if (it.hasNext())
								next=it.next();
							else {
								i++;
								it=t.iterator();
								next=it.next();
							}
						else i++;
				} 	
				if (size==p.size())
					j++;
			}
			
		}else 
		{
			//System.out.println("Non l'ho trovato inserisco event nell'insieme");
			Set<String> s=new TreeSet<String>();
			for (int i=0;i<token.length;i++)
				s.add(token[i]);
			String primo=s.iterator().next();
			int i =0;
			while (i<p.size() && primo.toUpperCase().compareTo(p.get(i++).iterator().next().toUpperCase())>0);
			p.add(i,s);
		}
		
		if (trovato) return 1; else return -1;
	}
	public static String makeLabel(Set<String> label) {
		String r="";
		for (String s:label){
			r+=s+"&";
		}
		return r.substring(0,r.length()-1);
	}
	
}
