package ioUtility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.processmining.models.andOrGraph.Edge;
import org.processmining.models.andOrGraph.Event;
import org.processmining.models.andOrGraph.EventsNode;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.models.andOrGraph.Pair;
import org.processmining.models.graphbased.NodeID;

public class FileManager {

	public FileManager() {
		super();

	}

	public GraphAndOr readGraph(String filename) {
		Map<Integer, NodeID> map = new HashMap<Integer, NodeID>();
		GraphViz gv = new GraphViz();
		gv.readSource(filename);
		String dot = gv.getDotSource();
		//System.out.println(dot);
		StringTokenizer st = new StringTokenizer(dot, "\n");
		GraphAndOr graph = new GraphAndOr("G",true);
		while (st.hasMoreTokens()) {
			String token = st.nextToken().trim();
			if (token.equals("}") || token.contains("digraph")) continue;
			if (token.startsWith("//")) continue;
			//se nella riga c'� -> allora � un arco altrimenti un nodo 
			if (!token.contains("->")) {
				//intero che identifica il nodo
				int node = Integer.parseInt(token.substring(0,
						token.indexOf('[')));
				String label;
				if (token.contains(","))
				//label del nodo
				label = token.substring(token.indexOf('[') + 8,
						token.indexOf(',') - 1);
				else label=token.substring(token.indexOf('[')+8,token.length()-2);
				//costruisco l'evento	
				Event event = new Event(label);
				if (token.contains(",")){
					//ricostruisco la lista delle occorrenze
					String occurrence = token.substring(token.indexOf(',') + 10,
						token.length() - 2);
				
					StringTokenizer st2 = new StringTokenizer(occurrence, "[]|");
					while (st2.hasMoreTokens()) {
						String t = st2.nextToken();
						String b = st2.nextToken();
						if (!b.contains("-")) continue;
						String o = b.substring(2,b.length()-1);
						String[] i = o.split(",");
						
						ArrayList<Pair<Integer, Integer>> occ = new ArrayList<Pair<Integer, Integer>>(
								i.length);
						for (int j = 0; j < i.length; j++) {
								String[] trace = i[j].split("-");
								occ.add(new Pair<Integer, Integer>(Integer
										.parseInt(trace[0]), Integer.parseInt(trace[1])));
						}
						event.addOccurs(occ, t);
					}
				}
				//creo il nodo
				Node n = new EventsNode(event, false, event.getCount());
				//lo aggiungo al grafo
				graph.addNode(n);
				//lo inserisco nella map 
				map.put(node, n.getId());
			}
			else {
				StringTokenizer st2=new StringTokenizer(token,"->[]=");
				//System.out.println(st2.countTokens());
				if (token.contains("[") && st2.countTokens()!=4)
					return null;
				if (!token.contains("[") && st2.countTokens()!=2) return null;
				String s=st2.nextToken();
				NodeID sourceID=map.get(Integer.parseInt(s));
				NodeID targetID=map.get(Integer.parseInt(st2.nextToken()));
				EventsNode source=(EventsNode) graph.getNodeByID(sourceID);
				EventsNode target=(EventsNode) graph.getNodeByID(targetID);
				Edge edge;
				if (token.contains("[")){
					st2.nextToken();
					edge=new Edge(source, target,Double.parseDouble(st2.nextToken()));
					}
				else edge=new Edge(source, target);
				source.addOutEdge(edge);
				target.addInEdge(edge);
				graph.addEdge(edge);
			}
		}
		return graph;
	}

	public boolean writeGraph(GraphAndOr g, File file) throws FileNotFoundException,
			UnsupportedEncodingException {

		Map<NodeID, Integer> map = new HashMap<NodeID, Integer>(g.getNodes()
				.size());
		GraphViz gv = new GraphViz();

		gv.addln(gv.start_graph());

		int i = 0;

		for (Node node : g.getNodes()) {
			
			map.put(node.getId(), i);
			Event ev = node.getEvent();
			gv.addln(i + "[label=\"" + node.getLabel() + "\",comment=\""
					+ ev.toString() + "\"]");
			i++;
		}
		for (Edge edge : g.getEdges()) {
			NodeID source = edge.getSource().getId();
			NodeID target = edge.getTarget().getId();
			map.get(source);
			gv.addln(map.get(source) + "->" + map.get(target) + "[label="
					+ edge.getLabel() + "]");
		}
		//
		gv.addln(gv.end_graph());
		try {
			gv.writeDotSourceToFile(gv.getDotSource(),file);
//			System.out.println(gv.getDotSource());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

}
