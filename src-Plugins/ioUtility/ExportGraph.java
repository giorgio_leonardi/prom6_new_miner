package ioUtility;

import java.io.File;
import java.io.IOException;



import org.processmining.contexts.uitopia.annotations.UIExportPlugin;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.models.andOrGraph.GraphAndOr;

@Plugin(name = "export Graph", returnLabels = {}, returnTypes = {}, parameterLabels = {
		"grafo", "File" }, userAccessible = true)
@UIExportPlugin(description = "grafo", extension = "gv")
public class ExportGraph {
	@PluginVariant(variantLabel = "grafo", requiredParameterLabels = { 0, 1 })
	public void export(PluginContext context, GraphAndOr graph, File file) throws IOException {
		FileManager fm=new FileManager();
		fm.writeGraph(graph,file);
	}
}
