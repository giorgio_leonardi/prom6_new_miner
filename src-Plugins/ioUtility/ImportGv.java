package ioUtility;





import java.io.File;
import java.io.InputStream;

import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.processmining.contexts.uitopia.annotations.UIImportPlugin;
import org.processmining.framework.abstractplugins.AbstractImportPlugin;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.models.andOrGraph.GraphAndOr;




@Plugin(name = "Import Grafo", parameterLabels = { "file gv" }, returnLabels = { "Grafo" }, returnTypes = { GraphAndOr.class })
@UIImportPlugin(description = "file gv", extensions = { "gv" })
public class ImportGv extends AbstractImportPlugin {




	protected FileFilter getFileFilter() {
		return new FileNameExtensionFilter("gv files", "gv");
	}


	@Override
	protected Object importFromStream(PluginContext context, InputStream input,
			String filename, long fileSizeInBytes) throws Exception {
		FileManager fm=new FileManager();
		File file = getFile();
		
		GraphAndOr graph=fm.readGraph(file.getPath());
	
		return graph;
	}
}

