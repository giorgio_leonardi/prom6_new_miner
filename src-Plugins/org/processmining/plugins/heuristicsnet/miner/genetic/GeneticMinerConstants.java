package org.processmining.plugins.heuristicsnet.miner.genetic;

/**
 * Contains the default values for the input parameters of the Genetic Algorithm
 * Miner.
 * 
 * @author Ana Karla Alves de Medeiros
 * 
 */
public class GeneticMinerConstants {

	/**
	 * Index of the default population type to be used during the mining. This
	 * index is based on the array in the class
	 * <code>InitialPopulationFactory</code>, method
	 * <code>getInitialPopulationTypes()</code>.
	 */
	public static final int INITIAL_POPULATION_TYPE = 0;

	/**
	 * Default size of the population with <code>HeuristicsNet</code> objects.
	 */
	public static final int POPULATION_SIZE = 10;

	/**
	 * Default value of the elitism rate. The number of objects in a population
	 * that will be directly copied to the next population is equal to
	 * <code>ELITISM_RATE</code> times <code>POPULATION_SIZE</code>.
	 */
	public static final double ELITISM_RATE = 0.2;

	/**
	 * Default value for the maximum number of population to be created during a
	 * run of the genetic algorithm.
	 */
	public static final int MAX_GENERATION = 1000;

	/**
	 * Index of the default fitness type to be used during the mining. This
	 * index is based on the array in the class <code>FitnessFactory</code>,
	 * method <code>getAllFitnessTypes()</code>.
	 */
	public static final int FITNESS_TYPE = 4;

	/**
	 * Index of the default selection type to be used during the mining. This
	 * index is based on the array in the class
	 * <code>SelectionMethodFactory</code>, method
	 * <code>getAllSelectionMethodsTypes()</code>.
	 */
	public static final int SELECTION_TYPE = 1;

	/**
	 * Default value for the crossover rate.
	 */
	public static final double CROSSOVER_RATE = 0.8;

	/**
	 * Index of the default crossover type to be used during the mining. This
	 * index is based on the array in the class <code>CrossoverFactory</code>,
	 * method <code>getAllCrossoverTypes()</code>.
	 */
	public static final int CROSSOVER_TYPE = 0;

	/**
	 * Default value for the mutation rate.
	 */
	public static final double MUTATION_RATE = 0.2;

	/**
	 * Index of the default mutation type to be used during the mining. This
	 * index is based on the array in the class <code>MutationFactory</code>,
	 * method <code>getAllMutationTypes()</code>.
	 */
	public static final int MUTATION_TYPE = 0;

	/**
	 * Default value of the power factor to be used when computing the
	 * dependency relations.
	 */
	public static final double POWER = 1;

	/**
	 * Default values to set if statistics about executions times, fitness
	 * values etc should be kept or not.
	 */
	public static final boolean KEEP_STATISTICS = false;

	/**
	 * Default value used execute one run the genetic algorithm.
	 */
	public static final long SEED = 1;

}
