package org.processmining.plugins.heuristicsnet.miner.genetic.miner;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XLogImpl;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.framework.plugin.events.Logger.MessageLevel;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.models.heuristics.impl.ActivitiesMappingStructures;
import org.processmining.models.heuristics.impl.HNSubSet;
import org.processmining.plugins.heuristicsnet.array.visualization.HeuristicsNetArrayObject;
import org.processmining.plugins.heuristicsnet.miner.genetic.VisualizeEvolution;
import org.processmining.plugins.heuristicsnet.miner.genetic.fitness.Fitness;
import org.processmining.plugins.heuristicsnet.miner.genetic.fitness.FitnessFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.miner.settings.GeneticMinerSettings;
import org.processmining.plugins.heuristicsnet.miner.genetic.population.InitialPopulationFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.util.MethodsOverHeuristicsNets;

public class IterativeGeneticMiner {

	public static HeuristicsNetArrayObject GeneticMinerSplitTheLogInternal(UIPluginContext context, XLog log,
			GeneticMinerSettings settings, int percentage, int addInterval, boolean view) {
		// TODO Auto-generated method stub

		VisualizeEvolution visualization = null;
		if (view) {
			visualization = new VisualizeEvolution(null, "GeneticMinerEvolution", true);
			visualization.showModal();
		}

		XLog[] splitLogs = splitLog(log, percentage);

		XLogInfo fullLogInfo = XLogInfoFactory.createLogInfo(log);
		Fitness fullFitness = FitnessFactory.getFitness(settings.getFitnessType(), fullLogInfo,
				FitnessFactory.ALL_FITNESS_PARAMETERS);

		int iteration = 0;

		// build initial population
		XLog currentLog = splitLogs[iteration];

		XLogInfo logInfo = XLogInfoFactory.createLogInfo(currentLog);

		GeneticMiner miner = new GeneticMiner(context, logInfo, settings);

		Fitness fitness = FitnessFactory.getFitness(settings.getFitnessType(), miner.getLogInfo(),
				FitnessFactory.ALL_FITNESS_PARAMETERS);
		Random generator = new Random(settings.getSeed());
		HeuristicsNet[] population = new HeuristicsNet[settings.getPopulationSize()];

		population = InitialPopulationFactory.getPopulation(settings.getInitialPopulationType(), generator,
				fullLogInfo, settings.getPower()).build(population);
		population = fitness.calculate(population);

		// ////////////////////////////////////////////////

		if (population == null) {
			return null;
		}

		HNSubSet protectedActivities = createProtectedActivities(logInfo.getEventClasses(), fullLogInfo
				.getEventClasses(), population[0].getActivitiesMappingStructures());
		int step = (addInterval < settings.getMaxGeneration()) ? addInterval : settings.getMaxGeneration();
		int currentGeneration = 1;

		while (currentGeneration < settings.getMaxGeneration()) {
			if (view) {
				if (visualization.isShowing() == false) {
					break;
				}
			}

			GeneticMinerSettings settings4Miner = settings.copy();
			settings4Miner.setMaxGeneration(step);
			miner.changeSettings(settings4Miner);

			if (view) {
				population = miner.mineStepsWithOtherEventClasses(population, protectedActivities, generator,
						visualization);
			} else {
				population = miner.mineStepsWithOtherEventClasses(population, protectedActivities, generator);
			}

			currentGeneration = currentGeneration + miner.getCurrentStatistics().getCurrentGeneration();

			// System.out.println("["+currentGeneration+"] Before "+Double.toString(population[population.length
			// - 1].getFitness()));

			population = fullFitness.calculate(population);
			Arrays.sort(population);

			// System.out.println("["+currentGeneration+"] After "+Double.toString(population[population.length
			// - 1].getFitness()));

			if (view) {
				visualization.log("[ Re-compute on the entire log ]:  bestFitness="
						+ Double.toString(population[population.length - 1].getFitness()), MessageLevel.NORMAL);
			}

			if (population[population.length - 1].getFitness() > settings.getStopFitness()) {
				break;
			}

			iteration++;
			if (iteration < splitLogs.length) {
				Iterator<XTrace> traces = splitLogs[iteration].iterator();
				while (traces.hasNext()) {
					XTrace trace = traces.next();
					currentLog.add(trace);
				}
				logInfo = XLogInfoFactory.createLogInfo(currentLog);
				protectedActivities = createProtectedActivities(logInfo.getEventClasses(), fullLogInfo
						.getEventClasses(), population[0].getActivitiesMappingStructures());
				miner.changeLog(logInfo);
				if (view) {
					visualization.log("Added the log part " + iteration, MessageLevel.WARNING);
				}
			} else {
				step = settings.getMaxGeneration() - currentGeneration;
			}
		}

		population = (MethodsOverHeuristicsNets.removeUnusedElements(population, fullFitness));
		Arrays.sort(population);

		return new HeuristicsNetArrayObject(population);

	}

	/**
	 * This method removes from the set of events the events that are not for
	 * the current log
	 * 
	 * @param eventClasses
	 * @param allEventsSet2
	 * @param activitiesMapping
	 * @return
	 */
	private static HNSubSet createProtectedActivities(XEventClasses eventClasses, XEventClasses allEventsSet2,
			ActivitiesMappingStructures activitiesMapping) {
		// TODO Auto-generated method stub

		HNSubSet protectedActivities = new HNSubSet();

		for (XEventClass event : allEventsSet2.getClasses()) {

			if (!(eventClasses.getClasses().contains(event))) {
				protectedActivities.addAll(activitiesMapping.getReverseActivitiesMapping().get(event));

			}

		}

		return protectedActivities;
	}

	private static XLog[] splitLog(XLog log, int nrOfPieces) {
		// TODO Auto-generated method stub

		int size = (log.size() / nrOfPieces) + 1;

		XLog[] logs = new XLog[nrOfPieces];

		// System.out.println(log.size()+" "+nrOfIslands+" "+maxDuplicateTraces+" "+numberOfTracesPerIsland);

		// check that all traces get on at least one island and that
		// no trace needs to be duplicated more than maxDuplicateTRaces

		for (int i = 0; i < nrOfPieces; i++) {
			logs[i] = new XLogImpl(log.getAttributes());

		}

		// int[] duplicates = new int[log.size()];

		Random random = new Random(System.currentTimeMillis() / 100000);

		// this ensures that all traces get on at least one island
		for (XTrace trace : log) {
			int i = random.nextInt(nrOfPieces);
			while (logs[i].size() >= size) {
				i = random.nextInt(nrOfPieces);
			}
			logs[i].add(trace);
		}

		return logs;
	}

}
