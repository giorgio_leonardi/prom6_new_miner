package org.processmining.plugins.heuristicsnet.miner.genetic.miner.settings;

import java.util.Date;

import org.processmining.plugins.heuristicsnet.miner.genetic.GeneticMinerConstants;

public class GeneticMinerSettings {

	protected int initialPopulationType = 0;
	protected int populationSize = 2;
	protected double elitismRate = 0.04;
	protected int maxGeneration = 2;
	protected int fitnessType = 2;
	protected int selectionType = 1;
	protected double crossoverRate = 0.8;
	protected int crossoverType = 0;
	protected double mutationRate = 0.2;
	protected int mutationType = 0;
	protected double power = 1;
	protected boolean keepStatistics = false;
	protected String filename = null;

	protected long seed = 1;

	private double stopFitness;

	public double getStopFitness() {
		return stopFitness;
	}

	public void setStopFitness(double stopFitness) {
		this.stopFitness = stopFitness;
	}

	public GeneticMinerSettings(int populationSize2, int maxNumGenerations, double mutationRate2, int crossoverType2,
			double crossoverRate2, long seed2, int fitnessType2, int selectionMethodType, double elitismRate2,
			int mutationType2, double power2, int initialPopulationType2, boolean keepStatistics2, String filename2,
			double stopCnd) {
		// TODO Auto-generated constructor stub
		populationSize = populationSize2;
		maxGeneration = maxNumGenerations;
		mutationRate = mutationRate2;
		crossoverRate = crossoverRate2;
		crossoverType = crossoverType2;
		seed = seed2;
		fitnessType = fitnessType2;
		selectionType = selectionMethodType;
		elitismRate = elitismRate2;
		mutationType = mutationType2;
		power = power2;
		initialPopulationType = initialPopulationType2;
		keepStatistics = keepStatistics2;
		filename = filename2;
		stopFitness = stopCnd;

	}

	public GeneticMinerSettings() {
		// TODO Auto-generated constructor stub
		populationSize = GeneticMinerConstants.POPULATION_SIZE;
		maxGeneration = GeneticMinerConstants.MAX_GENERATION;
		mutationRate = GeneticMinerConstants.MUTATION_RATE;
		crossoverRate = GeneticMinerConstants.CROSSOVER_RATE;
		crossoverType = GeneticMinerConstants.CROSSOVER_TYPE;
		seed = GeneticMinerConstants.SEED;
		fitnessType = GeneticMinerConstants.FITNESS_TYPE;
		selectionType = GeneticMinerConstants.SELECTION_TYPE;
		elitismRate = GeneticMinerConstants.ELITISM_RATE;
		mutationType = GeneticMinerConstants.MUTATION_TYPE;
		power = GeneticMinerConstants.POWER;
		initialPopulationType = GeneticMinerConstants.INITIAL_POPULATION_TYPE;
		keepStatistics = true;
		filename = //System.getProperty("user.dir") +
		"geneticminer" + (new Date()).hashCode();
		stopFitness = 0.8;
	}

	public int getInitialPopulationType() {
		return initialPopulationType;
	}

	public void setInitialPopulationType(int initialPopulationType) {
		this.initialPopulationType = initialPopulationType;
	}

	public int getPopulationSize() {
		return populationSize;
	}

	public void setPopulationSize(int populationSize) {
		if (populationSize < 0) {
			this.populationSize = 10;
		} else {
			this.populationSize = populationSize;
		}
	}

	public double getElitismRate() {
		return elitismRate;
	}

	public void setElitismRate(double elitismRate) {
		this.elitismRate = elitismRate;
	}

	public int getMaxGeneration() {
		return maxGeneration;
	}

	public void setMaxGeneration(int maxGenration) {
		maxGeneration = maxGenration;
	}

	public int getFitnessType() {
		return fitnessType;
	}

	public void setFitnessType(int fitnessType) {
		this.fitnessType = fitnessType;
	}

	public int getSelectionType() {
		return selectionType;
	}

	public void setSelectionType(int selectionType) {
		this.selectionType = selectionType;
	}

	public double getCrossoverRate() {
		return crossoverRate;
	}

	public void setCrossoverRate(double crossoverRate) {
		this.crossoverRate = crossoverRate;
	}

	public int getCrossoverType() {
		return crossoverType;
	}

	public void setCrossoverType(int crossoverType) {
		this.crossoverType = crossoverType;
	}

	public double getMutationRate() {
		return mutationRate;
	}

	public void setMutationRate(double mutationRate) {
		this.mutationRate = mutationRate;
	}

	public int getMutationType() {
		return mutationType;
	}

	public void setMutationType(int mutationType) {
		this.mutationType = mutationType;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}

	public boolean isKeepStatistics() {
		return keepStatistics;
	}

	public void setKeepStatistics(boolean keepStatistics) {
		this.keepStatistics = keepStatistics;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public long getSeed() {
		return seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}

	public String toString() {

		return "\ninitialPopulationType = " + initialPopulationType + "\npopulationSize = " + populationSize
				+ "\nelitismRate = " + elitismRate + "\nmaxGeneration = " + maxGeneration + "\nfitnessType = "
				+ fitnessType + "\nselectionType = " + selectionType + "\ncrossoverRate = " + crossoverRate
				+ "\ncrossoverType = " + crossoverType + "\nmutationRate = " + mutationRate + "\nmutationType = "
				+ mutationType + "\npower = " + power + "\nseed = " + seed;
	}

	public GeneticMinerSettings copy() {
		// TODO Auto-generated method stub

		GeneticMinerSettings settings = new GeneticMinerSettings(populationSize, maxGeneration, mutationRate,
				crossoverType, crossoverRate, seed, fitnessType, selectionType, elitismRate, mutationType, power,
				initialPopulationType, keepStatistics, filename, stopFitness);

		return settings;
	}

}
