package org.processmining.plugins.heuristicsnet.miner.genetic.miner;

import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.models.heuristics.HeuristicsNet;

@Plugin(name = "Mine Heuristic Net using Genetic Miner", parameterLabels = { "Log", "Settings", "Initial Population" }, returnLabels = { "Best-fitting Heuristic Net" }, returnTypes = { HeuristicsNet.class }, userAccessible = true, help = "Genetic Miner Plug-in")
public class GeneticMinerPlugin {

	@UITopiaVariant(uiLabel = "Mine Heuristic Net using Genetic Miner", affiliation = UITopiaVariant.EHV, author = "A.K. Alves de Medeiros", email = "c.c.bratosin@tue.nl")
	@PluginVariant(variantLabel = "GUI Settings", requiredParameterLabels = { 0 })
	public static HeuristicsNet GeneticMiner(UIPluginContext context, XLog log) {
		HeuristicsNet[] population = GeneticMinerPlugin4UITopia.GeneticMinerGUIDefault2(context, log).getPopulation();
		return population[population.length - 1];
	}
}
