package org.processmining.plugins.heuristicsnet.miner.genetic.miner;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import org.deckfour.xes.info.XLogInfo;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.events.Logger.MessageLevel;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.models.heuristics.impl.HNSubSet;
import org.processmining.plugins.heuristicsnet.miner.genetic.GeneticMinerStatistics;
import org.processmining.plugins.heuristicsnet.miner.genetic.VisualizeEvolution;
import org.processmining.plugins.heuristicsnet.miner.genetic.fitness.Fitness;
import org.processmining.plugins.heuristicsnet.miner.genetic.fitness.FitnessFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.Crossover;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.CrossoverFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.Mutation;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.MutationFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.miner.settings.GeneticMinerSettings;
import org.processmining.plugins.heuristicsnet.miner.genetic.population.BuildPopulation;
import org.processmining.plugins.heuristicsnet.miner.genetic.population.InitialPopulationFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.population.NextPopulationFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.selection.SelectionMethod;
import org.processmining.plugins.heuristicsnet.miner.genetic.selection.SelectionMethodFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.util.DescriptiveStatistics;
import org.processmining.plugins.heuristicsnet.miner.genetic.util.MethodsOverHeuristicsNets;

public class GeneticMiner {

	private XLogInfo logInfo = null;
	private PluginContext context = null;

	private double[] mean;
	private double[] variance;
	private double[] standardDeviation;
	private double[] bestFitness;
	private int populationNumber;
	private double elapsedTime;
	//private String filename = null;
	private GeneticMinerSettings settings;
	private long startTime;

	/*
	 * [HV] The field GeneticMiner.populationSize is never read locally private
	 * int populationSize;
	 */

	public GeneticMiner(PluginContext context, XLogInfo logInfo) {
		this.logInfo = logInfo;
		this.context = context;
	}

	public GeneticMiner(PluginContext context2, XLogInfo logInfo2, GeneticMinerSettings settings) {
		// TODO Auto-generated constructor stub
		this(context2, logInfo2);
		this.settings = settings;

	}

	public GeneticMinerSettings getSettings() {
		return settings;
	}

	public void changeSettings(GeneticMinerSettings settings) {
		this.settings = settings;
	}

	public XLogInfo getLogInfo() {
		return logInfo;
	}

	public HeuristicsNet[] mine() {

		if (settings == null) {
			settings = new GeneticMinerSettings();
		}

		startTime = (new Date()).getTime();

		String filename = settings.getFilename() + ".csv";

		//context.getProgress().setMaximum(0);
		//context.getProgress().setMaximum(settings.getMaxGeneration());
		//context.getProgress().setCaption("Mining a Heuristics nets...");
		//context.getProgress().setIndeterminate(false);

		//TODO - Make changes to consider the actual input parameters!
		//Not sure, but perhaps I need another constructor
		HeuristicsNet[] population = new HeuristicsNet[settings.getPopulationSize()];
		Fitness fitness = FitnessFactory.getFitness(settings.getFitnessType(), logInfo,
				FitnessFactory.ALL_FITNESS_PARAMETERS);
		Random generator = new Random(settings.getSeed());
		populationNumber = 0;

		mean = new double[settings.getMaxGeneration()];
		variance = new double[settings.getMaxGeneration()];
		standardDeviation = new double[settings.getMaxGeneration()];
		bestFitness = new double[settings.getMaxGeneration()];

		//building the initial population
		population = InitialPopulationFactory.getPopulation(settings.getInitialPopulationType(), generator, logInfo,
				settings.getPower()).build(population);
		population = fitness.calculate(population);
		updateStatistics(populationNumber, population, filename);
		populationNumber++;

		//building the next generations
		SelectionMethod selectionMethod = SelectionMethodFactory.getSelectionMethods(settings.getSelectionType(),
				generator);

		Crossover crossover = CrossoverFactory.getCrossover(settings.getCrossoverType(), generator);
		Mutation mutation = MutationFactory.getMutation(settings.getMutationType(), generator, settings
				.getMutationRate());
		BuildPopulation buildNextPopulation = NextPopulationFactory.getPopulation(selectionMethod, generator, settings
				.getCrossoverRate(), settings.getMutationRate(), settings.getElitismRate(), crossover, mutation);

		double bf = 0;

		for (; (populationNumber < settings.getMaxGeneration()) && (settings.getStopFitness() > bf); populationNumber++) {
			population = buildNextPopulation.build(population);
			population = fitness.calculate(population);
			updateStatistics(populationNumber, population, filename);
	//		context.getProgress().setValue(populationNumber);
			bf = bestFitness[populationNumber];
		}

		//Cleaning the HeuristicsNets, and increasingly ordering them in the population based on their fitness measures
		population = MethodsOverHeuristicsNets.removeUnusedElements(population, fitness);
		Arrays.sort(population);

		//context.log("The Best HeuristicsNet has fitness: " + population[population.length - 1].getFitness()
		//		+ "  and it looks like this: ", MessageLevel.DEBUG);
		//context.log(population[population.length - 1].toString(), MessageLevel.DEBUG);

		elapsedTime = (((new Date()).getTime() - startTime) / 1000.0);
		//context.log("Time taken: " + elapsedTime + " seconds", MessageLevel.DEBUG);

		//		if (settings.isKeepStatistics() && (settings.getFilename()!=null)) {
		//			writeToFile();
		//		}

		return population;
	}

	public HeuristicsNet[] mineOneStep(HeuristicsNet[] initialPop, Random generator) {
		startTime = (new Date()).getTime();

		String filename = settings.getFilename() + ".csv";

		//TODO - Make changes to consider the actual input parameters!
		//Not sure, but perhaps I need another constructor

		Fitness fitness = FitnessFactory.getFitness(settings.getFitnessType(), logInfo,
				FitnessFactory.ALL_FITNESS_PARAMETERS);
		if (generator == null) {
			generator = new Random(settings.getSeed());
		}

		mean = new double[settings.getMaxGeneration()];
		variance = new double[settings.getMaxGeneration()];
		standardDeviation = new double[settings.getMaxGeneration()];
		bestFitness = new double[settings.getMaxGeneration()];

		//building the initial population
		HeuristicsNet[] population = initialPop;
		/*
		 * [HV] The field GeneticMiner.populationSize is never read locally
		 * populationSize = population.length;
		 */
		//building the next generations
		SelectionMethod selectionMethod = SelectionMethodFactory.getSelectionMethods(settings.getSelectionType(),
				generator);

		Crossover crossover = CrossoverFactory.getCrossover(settings.getCrossoverType(), generator);
		Mutation mutation = MutationFactory.getMutation(settings.getMutationType(), generator, settings
				.getMutationRate());
		BuildPopulation buildNextPopulation = NextPopulationFactory.getPopulation(selectionMethod, generator, settings
				.getCrossoverRate(), settings.getMutationRate(), settings.getElitismRate(), crossover, mutation);

		populationNumber = 0;

		population = buildNextPopulation.build(population);
		population = fitness.calculate(population);
		updateStatistics(populationNumber, population, filename);
		elapsedTime = (((new Date()).getTime() - startTime) / 1000.0);

		populationNumber++;

		return population;

	}

	public HeuristicsNet[] mineOneStepWithNewPopulationSize(HeuristicsNet[] initialPop, int newPS) {

		startTime = (new Date()).getTime();

		String filename = settings.getFilename() + ".csv";
		Random generator = new Random(settings.getSeed());

		Fitness fitness = FitnessFactory.getFitness(settings.getFitnessType(), logInfo,
				FitnessFactory.ALL_FITNESS_PARAMETERS);

		settings.setPopulationSize(newPS);

		mean = new double[settings.getMaxGeneration()];
		variance = new double[settings.getMaxGeneration()];
		standardDeviation = new double[settings.getMaxGeneration()];
		bestFitness = new double[settings.getMaxGeneration()];

		//building the initial population
		HeuristicsNet[] population = new HeuristicsNet[newPS];

		//building the next generations
		SelectionMethod selectionMethod = SelectionMethodFactory.getSelectionMethods(settings.getSelectionType(),
				generator);

		Crossover crossover = CrossoverFactory.getCrossover(settings.getCrossoverType(), generator);
		Mutation mutation = MutationFactory.getMutation(settings.getMutationType(), generator, settings
				.getMutationRate());
		BuildPopulation buildNextPopulation = NextPopulationFactory.getPopulation(selectionMethod, generator, settings
				.getCrossoverRate(), settings.getMutationRate(), settings.getElitismRate(), crossover, mutation);

		populationNumber = 0;
		//logActions("BuildPopulation", "start", System.currentTimeMillis(), settings.getFilename() + "log");

		population = buildNextPopulation.buildWithNewPS(initialPop, newPS);
		population = fitness.calculate(population);

		updateStatistics(populationNumber, population, filename);

		elapsedTime = (((new Date()).getTime() - startTime) / 1000.0);

		populationNumber++;

		return population;
	}

	public HeuristicsNet[] mineStepsWithOtherEventClasses(HeuristicsNet[] initialPop, HNSubSet protectedActivities,
			Random generator, VisualizeEvolution visualization) {
		startTime = (new Date()).getTime();

		String filename = settings.getFilename() + ".csv";

		//TODO - Make changes to consider the actual input parameters!
		//Not sure, but perhaps I need another constructor

		Fitness fitness = FitnessFactory.getFitness(settings.getFitnessType(), logInfo,
				FitnessFactory.ALL_FITNESS_PARAMETERS);
		if (generator == null) {
			generator = new Random(settings.getSeed());
		}

		settings.setPopulationSize(initialPop.length);

		mean = new double[settings.getMaxGeneration()];
		variance = new double[settings.getMaxGeneration()];
		standardDeviation = new double[settings.getMaxGeneration()];
		bestFitness = new double[settings.getMaxGeneration()];

		//building the initial population
		HeuristicsNet[] population = initialPop;

		SelectionMethod selectionMethod = SelectionMethodFactory.getSelectionMethods(settings.getSelectionType(),
				generator);

		Crossover crossover = CrossoverFactory.getCrossover(settings.getCrossoverType(), generator);
		Mutation mutation = MutationFactory.getMutation(settings.getMutationType(), generator, settings
				.getMutationRate());
		BuildPopulation buildNextPopulation = NextPopulationFactory.getPopulation(selectionMethod, generator, settings
				.getCrossoverRate(), settings.getMutationRate(), settings.getElitismRate(), crossover, mutation);

		populationNumber = 0;

		double bf = 0;

		for (; (populationNumber < settings.getMaxGeneration()) && visualization.isShowing()
				&& (settings.getStopFitness() > bf); populationNumber++) {
			population = buildNextPopulation.build(population, protectedActivities);
			population = fitness.calculate(population);
			updateStatistics(populationNumber, population, filename);
			context.getProgress().setValue(populationNumber);
			visualization.updateChartPanel(bestFitness[populationNumber], mean[populationNumber]);
			String meanString = Double.toString(mean[populationNumber]);
			String bestString = Double.toString(bestFitness[populationNumber]);
			visualization.log("[" + populationNumber + "]: mean ="
					+ (meanString.length()>7?meanString.substring(0, 7):meanString) + " bestFitness="
					+ (bestString.length()>7?bestString.substring(0, 7):bestString), MessageLevel.NORMAL);
			bf = bestFitness[populationNumber];
		}

		elapsedTime = (((new Date()).getTime() - startTime) / 1000.0);
		Arrays.sort(population);

		return population;

	}

	public HeuristicsNet[] mineOneStepWithOtherEventClasses(HeuristicsNet[] initialPop, HNSubSet protectedActivities,
			Random generator) {
		startTime = (new Date()).getTime();

		String filename = settings.getFilename() + ".csv";

		//TODO - Make changes to consider the actual input parameters!
		//Not sure, but perhaps I need another constructor

		Fitness fitness = FitnessFactory.getFitness(settings.getFitnessType(), logInfo,
				FitnessFactory.ALL_FITNESS_PARAMETERS);
		if (generator == null) {
			generator = new Random(settings.getSeed());
		}
		//		populationNumber = 0;

		//I take care that the population have the same size
		//assumption is that the population is for the log that I have up 
		//unfortunately don't think is a way to verify this since no connection was
		//implemented yet 
		//you can verify it by just comparing the XEventClasses from the log with the given
		//ones

		settings.setPopulationSize(initialPop.length);

		mean = new double[settings.getMaxGeneration()];
		variance = new double[settings.getMaxGeneration()];
		standardDeviation = new double[settings.getMaxGeneration()];
		bestFitness = new double[settings.getMaxGeneration()];

		//building the initial population
		HeuristicsNet[] population = initialPop;

		SelectionMethod selectionMethod = SelectionMethodFactory.getSelectionMethods(settings.getSelectionType(),
				generator);

		Crossover crossover = CrossoverFactory.getCrossover(settings.getCrossoverType(), generator);
		Mutation mutation = MutationFactory.getMutation(settings.getMutationType(), generator, settings
				.getMutationRate());
		BuildPopulation buildNextPopulation = NextPopulationFactory.getPopulation(selectionMethod, generator, settings
				.getCrossoverRate(), settings.getMutationRate(), settings.getElitismRate(), crossover, mutation);

		populationNumber = 0;

		population = buildNextPopulation.build(population, protectedActivities);
		population = fitness.calculate(population);
		updateStatistics(populationNumber, population, filename);
		elapsedTime = (((new Date()).getTime() - startTime) / 1000.0);

		populationNumber++;

		return population;

	}

	private void updateStatistics(int populationNumber, HeuristicsNet[] population, String filename) {
		if (settings.isKeepStatistics()) {
			Arrays.sort(population);
			mean[populationNumber] = DescriptiveStatistics.mean(population);
			standardDeviation[populationNumber] = DescriptiveStatistics.standardDeviation(population);
			variance[populationNumber] = DescriptiveStatistics.variance(population);
			bestFitness[populationNumber] = population[population.length - 1].getFitness();

		}
	}

	public HeuristicsNet[] mineFromPopulation(HeuristicsNet[] initialPop, Random generator) {
		// TODO Auto-generated method stub

		startTime = (new Date()).getTime();

		String filename = settings.getFilename() + ".csv";

		context.getProgress().setMaximum(0);
		context.getProgress().setMaximum(settings.getMaxGeneration());
		context.getProgress().setCaption("Mining a Heuristics nets...");
		context.getProgress().setIndeterminate(false);

		//TODO - Make changes to consider the actual input parameters!
		//Not sure, but perhaps I need another constructor

		Fitness fitness = FitnessFactory.getFitness(settings.getFitnessType(), logInfo,
				FitnessFactory.ALL_FITNESS_PARAMETERS);
		if (generator == null) {
			generator = new Random(settings.getSeed());
		}
		populationNumber = 0;

		//I take care that the population have the same size
		//assumption is that the population is for the log that I have up 
		//unfortunately don't think is a way to verify this since no connection was
		//implemented yet

		settings.setPopulationSize(initialPop.length);

		mean = new double[settings.getMaxGeneration()];
		variance = new double[settings.getMaxGeneration()];
		standardDeviation = new double[settings.getMaxGeneration()];
		bestFitness = new double[settings.getMaxGeneration()];

		//building the initial population
		HeuristicsNet[] population = initialPop;
		updateStatistics(populationNumber, population, filename);
		populationNumber++;

		//building the next generations
		SelectionMethod selectionMethod = SelectionMethodFactory.getSelectionMethods(settings.getSelectionType(),
				generator);

		Crossover crossover = CrossoverFactory.getCrossover(settings.getCrossoverType(), generator);
		Mutation mutation = MutationFactory.getMutation(settings.getMutationType(), generator, settings
				.getMutationRate());
		BuildPopulation buildNextPopulation = NextPopulationFactory.getPopulation(selectionMethod, generator, settings
				.getCrossoverRate(), settings.getMutationRate(), settings.getElitismRate(), crossover, mutation);

		for (; populationNumber < settings.getMaxGeneration(); populationNumber++) {
			population = buildNextPopulation.build(population);
			population = fitness.calculate(population);
			updateStatistics(populationNumber, population, filename);
			context.getProgress().setValue(populationNumber);
		}

		//Cleaning the HeuristicsNets, and increasingly ordering them in the population based on their fitness measures

		population = MethodsOverHeuristicsNets.removeUnusedElements(population, fitness);

		Arrays.sort(population);

		elapsedTime = (((new Date()).getTime() - startTime) / 1000.0);

		return population;
	}

	public GeneticMinerStatistics getCurrentStatistics() {

		return new GeneticMinerStatistics(populationNumber, (System.currentTimeMillis() - startTime) / 1000,
				mean[populationNumber - 1], standardDeviation[populationNumber - 1], variance[populationNumber - 1],
				bestFitness[populationNumber - 1]);

	}

	public HeuristicsNet[] mine(VisualizeEvolution visualization) {
		// TODO Auto-generated method stub

		if (settings == null) {
			settings = new GeneticMinerSettings();
		}

		startTime = (new Date()).getTime();

		String filename = settings.getFilename() + ".csv";

		context.getProgress().setMaximum(0);
		context.getProgress().setMaximum(settings.getMaxGeneration());
		context.getProgress().setCaption("Mining a Heuristics nets...");
		context.getProgress().setIndeterminate(false);

		//TODO - Make changes to consider the actual input parameters!
		//Not sure, but perhaps I need another constructor
		HeuristicsNet[] population = new HeuristicsNet[settings.getPopulationSize()];
		Fitness fitness = FitnessFactory.getFitness(settings.getFitnessType(), logInfo,
				FitnessFactory.ALL_FITNESS_PARAMETERS);
		Random generator = new Random(settings.getSeed());
		populationNumber = 0;

		mean = new double[settings.getMaxGeneration()];
		variance = new double[settings.getMaxGeneration()];
		standardDeviation = new double[settings.getMaxGeneration()];
		bestFitness = new double[settings.getMaxGeneration()];

		//building the initial population
		population = InitialPopulationFactory.getPopulation(settings.getInitialPopulationType(), generator, logInfo,
				settings.getPower()).build(population);
		population = fitness.calculate(population);
		updateStatistics(populationNumber, population, filename);
		populationNumber++;

		//building the next generations
		SelectionMethod selectionMethod = SelectionMethodFactory.getSelectionMethods(settings.getSelectionType(),
				generator);

		Crossover crossover = CrossoverFactory.getCrossover(settings.getCrossoverType(), generator);
		Mutation mutation = MutationFactory.getMutation(settings.getMutationType(), generator, settings
				.getMutationRate());
		BuildPopulation buildNextPopulation = NextPopulationFactory.getPopulation(selectionMethod, generator, settings
				.getCrossoverRate(), settings.getMutationRate(), settings.getElitismRate(), crossover, mutation);

		double bf = 0;

		for (; (populationNumber < settings.getMaxGeneration()) && visualization.isShowing()
				&& (settings.getStopFitness() > bf); populationNumber++) {
			population = buildNextPopulation.build(population);
			population = fitness.calculate(population);
			updateStatistics(populationNumber, population, filename);
			visualization.updateChartPanel(bestFitness[populationNumber], mean[populationNumber]);
			String meanString = Double.toString(mean[populationNumber]);
			String bestString = Double.toString(bestFitness[populationNumber]);
			visualization.log("[" + populationNumber + "]: mean ="
					+ (meanString.length()>7?meanString.substring(0, 7):meanString) + " bestFitness="
					+ (bestString.length()>7?bestString.substring(0, 7):bestString), MessageLevel.NORMAL);
			context.getProgress().setValue(populationNumber);
			bf = bestFitness[populationNumber];
		}

		//Cleaning the HeuristicsNets, and increasingly ordering them in the population based on their fitness measures
		population = MethodsOverHeuristicsNets.removeUnusedElements(population, fitness);
		Arrays.sort(population);
		context.log("The Best HeuristicsNet has fitness: " + population[population.length - 1].getFitness()
				+ "  and it looks like this: ", MessageLevel.DEBUG);
		context.log(population[population.length - 1].toString(), MessageLevel.DEBUG);

		elapsedTime = (((new Date()).getTime() - startTime) / 1000.0);
		context.log("Time taken: " + elapsedTime + " seconds", MessageLevel.DEBUG);

		//		if (settings.isKeepStatistics() && (settings.getFilename()!=null)) {
		//			writeToFile();
		//		}

		return population;
	}

	public void changeLog(XLogInfo logInfo2) {
		// TODO Auto-generated method stub
		logInfo = logInfo2;
	}

	public HeuristicsNet[] mineStepsWithOtherEventClasses(HeuristicsNet[] initialPop, HNSubSet protectedActivities,
			Random generator) {
		// TODO Auto-generated method stub
		startTime = (new Date()).getTime();

		String filename = settings.getFilename() + ".csv";

		//TODO - Make changes to consider the actual input parameters!
		//Not sure, but perhaps I need another constructor

		Fitness fitness = FitnessFactory.getFitness(settings.getFitnessType(), logInfo,
				FitnessFactory.ALL_FITNESS_PARAMETERS);
		if (generator == null) {
			generator = new Random(settings.getSeed());
		}

		settings.setPopulationSize(initialPop.length);

		mean = new double[settings.getMaxGeneration()];
		variance = new double[settings.getMaxGeneration()];
		standardDeviation = new double[settings.getMaxGeneration()];
		bestFitness = new double[settings.getMaxGeneration()];

		//building the initial population
		HeuristicsNet[] population = initialPop;

		SelectionMethod selectionMethod = SelectionMethodFactory.getSelectionMethods(settings.getSelectionType(),
				generator);

		Crossover crossover = CrossoverFactory.getCrossover(settings.getCrossoverType(), generator);
		Mutation mutation = MutationFactory.getMutation(settings.getMutationType(), generator, settings
				.getMutationRate());
		BuildPopulation buildNextPopulation = NextPopulationFactory.getPopulation(selectionMethod, generator, settings
				.getCrossoverRate(), settings.getMutationRate(), settings.getElitismRate(), crossover, mutation);

		populationNumber = 0;

		double bf = 0;

		for (; (populationNumber < settings.getMaxGeneration()) && (settings.getStopFitness() > bf); populationNumber++) {
			population = buildNextPopulation.build(population, protectedActivities);
			population = fitness.calculate(population);
			updateStatistics(populationNumber, population, filename);
			context.getProgress().setValue(populationNumber);
			bf = bestFitness[populationNumber];
		}

		elapsedTime = (((new Date()).getTime() - startTime) / 1000.0);
		Arrays.sort(population);

		return population;

	}

}
