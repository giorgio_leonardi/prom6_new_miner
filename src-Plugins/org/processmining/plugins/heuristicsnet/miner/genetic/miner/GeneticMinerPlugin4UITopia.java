package org.processmining.plugins.heuristicsnet.miner.genetic.miner;

import java.util.Collection;
import java.util.Iterator;

import javax.swing.JPanel;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.plugins.heuristicsnet.array.visualization.HeuristicsNetArrayObject;
import org.processmining.plugins.heuristicsnet.miner.genetic.VisualizeEvolution;
import org.processmining.plugins.heuristicsnet.miner.genetic.gui.GeneticMinerSplitLogParameters;
import org.processmining.plugins.heuristicsnet.miner.genetic.gui.GeneticMinerUIInputParameters;
import org.processmining.plugins.heuristicsnet.miner.genetic.miner.settings.GeneticMinerSettings;

@Plugin(name = "Genetic Miner", parameterLabels = { "Log", "Settings", "Initial Population" }, returnLabels = { "Mined Models" }, returnTypes = { HeuristicsNetArrayObject.class }, userAccessible = true, help = "Genetic Miner Plug-in")
public class GeneticMinerPlugin4UITopia {
	// TODO - Add documentation

	//@UITopiaVariant(uiLabel = "Genetic Miner", affiliation = UITopiaVariant.EHV, author = "A.K. Alves de Medeiros", email = "c.c.bratosin@tue.nl")
	@PluginVariant(variantLabel = "GUI Settings", requiredParameterLabels = { 0 })
	public static HeuristicsNetArrayObject GeneticMinerGUIDefault2(UIPluginContext context, XLog log) {
		// TODO - Build different plug-ins that already receive log info!!!
		// Note that, the default classifier are been used at the moment

		// System.out.println("GUI settings");
		XLogInfo logInfo = XLogInfoFactory.createLogInfo(log);

		GeneticMinerUIInputParameters panel = new GeneticMinerUIInputParameters();

		/**
		 * Create a dialog containing this panel.
		 */

		GeneticMinerSettings settings = new GeneticMinerSettings();

		InteractionResult result = context.showConfiguration("Genetic Miner Parameters", panel);
		
		if (result == InteractionResult.CANCEL){
			return null;
		}

		settings = panel.getSettings();

		context.getProvidedObjectManager().createProvidedObject("Genetic Miner Settings", settings, context);

		GeneticMiner GMobject = new GeneticMiner(context, logInfo, settings);

		if (panel.visualization()) {
			VisualizeEvolution visualization = new VisualizeEvolution(null, "GeneticMinerEvolution", true);
			visualization.showModal();
			return new HeuristicsNetArrayObject(GMobject.mine(visualization));
		}
		//

		return new HeuristicsNetArrayObject(GMobject.mine());
	}

	@UITopiaVariant(uiLabel = "Sample based Genetic Miner", affiliation = UITopiaVariant.EHV, author = "C.C. Bratosin", email = "c.c.bratosin@tue.nl")
	@PluginVariant(variantLabel = "Split the log", requiredParameterLabels = { 0 })
	public static HeuristicsNetArrayObject GeneticMinerSplitTheLog(UIPluginContext context, XLog log) {
		// TODO - Build different plug-ins that already receive log info!!!
		// Note that, the default classifier are been used at the moment

		int nofSteps = 2;
		int currentStep = 0;

		// GeneticMinerUIInputParameters panel = new
		// GeneticMinerUIInputParameters();
		// GeneticMinerMasterUIInputParameters panel2 = new
		// GeneticMinerMasterUIInputParameters();
		InteractionResult result = InteractionResult.NEXT;

		JPanel[] mySteps = new JPanel[2];

		mySteps[0] = new GeneticMinerUIInputParameters();
		mySteps[1] = new GeneticMinerSplitLogParameters(log.size());

		GeneticMinerSettings settings = new GeneticMinerSettings();
		int percentage = 20;
		int addInterval = 20;

		while (true) {
			result = context.showWizard("Iterative Genetic Miner", currentStep == 0, currentStep == nofSteps - 1,
					mySteps[currentStep]);
			switch (result) {
				case NEXT :
					currentStep++;
					break;
				case PREV :
					currentStep--;
					break;
				case FINISHED :
					settings = ((GeneticMinerUIInputParameters) mySteps[0]).getSettings();
					percentage = ((GeneticMinerSplitLogParameters) mySteps[1]).getPercentage();
					addInterval = ((GeneticMinerSplitLogParameters) mySteps[1]).getAddInterval();

					return IterativeGeneticMiner.GeneticMinerSplitTheLogInternal(context, log, settings, percentage,
							addInterval, ((GeneticMinerUIInputParameters) mySteps[0]).visualization());
				default :
					return null;
			}
		}

	}
	
//	@UITopiaVariant(uiLabel = "Genetic Miner Log Split - Smart Sampling", affiliation = UITopiaVariant.EHV, author = "C.C. Bratosin", email = "c.c.bratosin@tue.nl")
//	@PluginVariant(variantLabel = "Split the log - SmartSample", requiredParameterLabels = { 0 })
//	public static HeuristicsNetArrayObject GeneticMinerSplitTheLogSmartSampling(UIPluginContext context, XLog log) {
//		// TODO - Build different plug-ins that already receive log info!!!
//		// Note that, the default classifier are been used at the moment
//
//		int nofSteps = 2;
//		int currentStep = 0;
//
//		// GeneticMinerUIInputParameters panel = new
//		// GeneticMinerUIInputParameters();
//		// GeneticMinerMasterUIInputParameters panel2 = new
//		// GeneticMinerMasterUIInputParameters();
//		InteractionResult result = InteractionResult.NEXT;
//
//		JPanel[] mySteps = new JPanel[2];
//
//		mySteps[0] = new GeneticMinerUIInputParameters();
//		mySteps[1] = new GeneticMinerSplitLogParameters(log.size());
//
//		GeneticMinerSettings settings = new GeneticMinerSettings();
//		int percentage = 20;
//		int addInterval = 20;
//
//		while (true) {
//			result = context.showWizard("Iterative Genetic Miner", currentStep == 0, currentStep == nofSteps - 1,
//					mySteps[currentStep]);
//			switch (result) {
//				case NEXT :
//					currentStep++;
//					break;
//				case PREV :
//					currentStep--;
//					break;
//				case FINISHED :
//					settings = ((GeneticMinerUIInputParameters) mySteps[0]).getSettings();
//					percentage = ((GeneticMinerSplitLogParameters) mySteps[1]).getPercentage();
//					addInterval = ((GeneticMinerSplitLogParameters) mySteps[1]).getAddInterval();
//
//					return IterativeGeneticMinerSmarSampling.GeneticMinerSplitTheLog(context, log, settings, percentage,
//							addInterval, ((GeneticMinerUIInputParameters) mySteps[0]).visualization());
//				default :
//					return null;
//			}
//		}
//
//	}

	// TODO - Add a help
	@UITopiaVariant(uiLabel = "Genetic Miner - from initial population", affiliation = UITopiaVariant.EHV, author = "A.K. Alves de Medeiros, C.C. Bratosin", email = "c.c.bratosin@tue.nl")
	@PluginVariant(variantLabel = "With Initial Population and Settings", requiredParameterLabels = { 0, 2 })
	public static HeuristicsNetArrayObject GeneticMinerPanel(UIPluginContext context, XLog log,
			HeuristicsNetArrayObject initialPop) {
		// TODO - Build different plug-ins that already receive log infos!!!
		// Note that, the default classifier are been used at the moment

		// System.out.println("GUI settings");
		XLogInfo logInfo = XLogInfoFactory.createLogInfo(log);
		if (checkPopulation(logInfo, initialPop.getPopulation())) {

			GeneticMinerUIInputParameters panel = new GeneticMinerUIInputParameters();

			/**
			 * Create a dialog containing this panel.
			 */

			GeneticMinerSettings settings = new GeneticMinerSettings();
			
			
			InteractionResult result = context.showConfiguration("Genetic Miner Parameters", panel);
			
			if (result == InteractionResult.CANCEL){
				return null;
			}
			
			context.getProvidedObjectManager().createProvidedObject("Genetic Miner Settings", settings, context);

			GeneticMiner GMobject = new GeneticMiner(context, logInfo, settings);

			return new HeuristicsNetArrayObject(GMobject.mineFromPopulation(initialPop.getPopulation(), null));
		} else {
			// throw new
			// ConnectionDoesntExistException("The genetic miner population and log are not connected",
			// null, new Object[]{log, initialPop});
		}
		return null;
	}

	/**
	 * This works correctly iff the population was created from a complete log
	 * or at least a complete set of XEventClasses
	 * 
	 * If this is not the case then it is possible that the two XEventClasses
	 * intersect but are not equal.
	 * 
	 * E.g. if I have a log with A B, C, D event classes and I split the log in
	 * two parts one having A B C and the other B C D the check will fail even
	 * if it was the same log
	 * 
	 * 
	 * @param logInfo
	 * @param initialPop
	 */
	private static boolean checkPopulation(XLogInfo logInfo, HeuristicsNet[] initialPop) {
		// TODO Auto-generated method stub
		XEventClasses classes = logInfo.getEventClasses();
		Collection<XEventClass> events = classes.getClasses();

		boolean check = true;

		for (int i = 0; i < initialPop.length; i++) {
			Collection<XEventClass> eventsOfIndividual = initialPop[i].getActivitiesMappingStructures()
					.getXEventClasses().getClasses();
			if (!(compareEventClasses(events, eventsOfIndividual))) {
				check = false;
				break;
			}
		}

		return check;

	}

	private static boolean compareEventClasses(Collection<XEventClass> events1, Collection<XEventClass> events2) {
		// TODO Auto-generated method stub

		boolean check = true;

		Iterator<XEventClass> iterator = events1.iterator();
		while (iterator.hasNext()) {
			if (events2.contains(iterator.next())) {

			} else {
				check = false;
				break;
			}

		}
		return check;
	}

}
