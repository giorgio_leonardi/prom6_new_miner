package org.processmining.plugins.heuristicsnet.miner.genetic.selection;

import java.util.Random;

public class SelectionMethodFactory {
	public SelectionMethodFactory() {
	}

	public static String[] getAllSelectionMethodsTypes() {
		return new String[] { "Tournament", "Tournament 5" };
	}

	public static SelectionMethod getSelectionMethods(int indexSelectionMethodType, Random generator) {
		SelectionMethod object = null;
		switch (indexSelectionMethodType) {
			case 0 :
				object = new TournamentSelection(generator);
				break;
			case 1 :
				object = new TournamentSelection5(generator);
				break;
		}

		return object;
	}

}
