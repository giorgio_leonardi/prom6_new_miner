package org.processmining.plugins.heuristicsnet.miner.genetic.selection;

import java.util.Arrays;
import java.util.Random;

import org.processmining.models.heuristics.HeuristicsNet;

public class TournamentSelection5 implements SelectionMethod {

	private static final int NUM_INDIVIDUALS = 5;
	private Random generator = null;

	public TournamentSelection5(Random gen) {
		generator = gen;
	}

	public HeuristicsNet[] select(HeuristicsNet[] population) {

		HeuristicsNet[] selectedIndividuals = null;
		HeuristicsNet[] result = null;

		selectedIndividuals = new HeuristicsNet[NUM_INDIVIDUALS];

		for (int i = 0; i < selectedIndividuals.length; i++) {
			selectedIndividuals[i] = population[generator.nextInt(population.length)].copy();
		}

		Arrays.sort(selectedIndividuals);

		result = new HeuristicsNet[1];

		result[0] = selectedIndividuals[selectedIndividuals.length - 1].copy();

		selectedIndividuals = null;

		return result;
	}

}
