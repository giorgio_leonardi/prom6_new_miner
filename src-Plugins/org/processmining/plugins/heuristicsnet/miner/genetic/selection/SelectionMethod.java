package org.processmining.plugins.heuristicsnet.miner.genetic.selection;

import org.processmining.models.heuristics.HeuristicsNet;

public interface SelectionMethod {
	public HeuristicsNet[] select(HeuristicsNet[] population);
}
