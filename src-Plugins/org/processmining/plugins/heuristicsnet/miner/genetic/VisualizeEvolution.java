package org.processmining.plugins.heuristicsnet.miner.genetic;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.processmining.framework.plugin.events.Logger.MessageLevel;

public class VisualizeEvolution extends JFrame {

	/**
	 * 
	 */

	//somecolors
	protected Color colorBg = new Color(30, 30, 30);
	protected Color colorFontBg = new Color(20, 20, 20, 140);
	public static Color colorNormal = new Color(250, 250, 250, 180);
	public static Color colorWarning = new Color(240, 200, 40, 200);
	public static Color colorError = new Color(250, 40, 40, 200);
	public static Color colorDebug = new Color(170, 170, 160, 200);
	public static Color colorTest = new Color(20, 250, 20, 200);

	private static final long serialVersionUID = 1L;

	private JPanel chartPanel;
	private MessagePanel messagePanel;
	private final JPanel buttonPanel = new JPanel();
	private boolean close;

	/*
	 * [HV] The field VisualizeEvolution.frame is never read locally private
	 * Frame frame;
	 */

	GridBagConstraints constraints = new GridBagConstraints();

	private final JPanel mainPanel = new JPanel();

	private TimeSeriesCollection datasetForChart;
	private JButton stopButton;

	public VisualizeEvolution(Frame frame, String title, boolean modal) {
		super(title);

		/*
		 * [HV] The field VisualizeEvolution.frame is never read locally
		 * this.frame = frame;
		 */

		setResizable(false);

		try {
			jbInit();
			pack();
			/**
			 * Set a nice size for the dialog, pack() gives ugly results.
			 * Assumption: A screen is at least 900x600...
			 */
			//setSize(900, 600);
			/**
			 * And center the dialog on the screen.
			 */
			setLocationRelativeTo(null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void jbInit() {
		// TODO Auto-generated method stub
		createChartPanel();
		createMessagePanel();
		createButtonPanel();

		GridBagConstraints constraints = new GridBagConstraints();
		GridBagLayout layout = new GridBagLayout();
		mainPanel.setLayout(layout);

		// Chart PANEL
		constraints.weightx = 1.0;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.fill = GridBagConstraints.NORTH;
		mainPanel.add(chartPanel, constraints);

		// Message PANEL
		constraints.weightx = 1.0;
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.fill = GridBagConstraints.NORTH;
		mainPanel.add(messagePanel, constraints);

		// Button PANEL
		constraints.weightx = 1.0;
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.fill = GridBagConstraints.NORTH;
		mainPanel.add(buttonPanel, constraints);

		constraints.gridx = 0;
		constraints.gridy = 0;
		layout.setConstraints(this, constraints);
		this.add(mainPanel);

		// LISTENERS

	}

	private void createMessagePanel() {
		// TODO Auto-generated method stub
		messagePanel = new MessagePanel();
		messagePanel.setPreferredSize(new java.awt.Dimension(500, 200));
	}

	public void log(String message, MessageLevel messageLevel) {
		messagePanel.log(message, messageLevel);
	}

	/**
	 * The chart Panel shows a chart with the evolution of the fitness I should
	 * also have some timestamps :-S associated in order this to become nice
	 * 
	 * 
	 */
	private void createChartPanel() {
		// TODO Auto-generated method stub

		final TimeSeries bestFitness = new TimeSeries("Best Fitness", Millisecond.class);
		final TimeSeries meanFitness = new TimeSeries("Mean Fitness", Millisecond.class);

		datasetForChart = new TimeSeriesCollection();
		datasetForChart.addSeries(bestFitness);
		datasetForChart.addSeries(meanFitness);

		final JFreeChart chart = ChartFactory.createXYLineChart("Fitness Evolution", // chart title
				"Time", // domain axis label
				"Fitness Value", // range axis label
				datasetForChart, // data
				PlotOrientation.VERTICAL, true, // include legend
				true, false);

		final XYPlot plot = chart.getXYPlot();
		//final NumberAxis domainAxis = new NumberAxis("Time");

		//here I say what is on the y axis
		final NumberAxis rangeAxis = new NumberAxis("Fitness");
		rangeAxis.setAutoRange(true);
		rangeAxis.setFixedAutoRange(2.5);
		//plot.setDomainAxis(domainAxis);
		plot.setRangeAxis(rangeAxis);

		//here I say that I want the x axis to show me time
		ValueAxis axis = new DateAxis("Time");
		axis.setAutoRange(true);
		//axis.setFixedAutoRange(120000.0);
		plot.setDomainAxis(axis);

		//this part is to set the series colors and width
		XYItemRenderer xyir = plot.getRenderer();
		xyir.setSeriesPaint(0, colorNormal);
		xyir.setSeriesStroke(0, new BasicStroke(3));

		xyir.setSeriesPaint(1, colorTest);
		xyir.setSeriesStroke(1, new BasicStroke(3));

		//here is about the legend
		chart.getLegend().setBackgroundPaint(colorBg);
		chart.getLegend().setItemPaint(colorNormal);

		//this is about the background and other things like this
		chart.setBackgroundPaint(new Color(130, 130, 130));
		plot.setOutlinePaint(new Color(20, 20, 20, 160));
		plot.setBackgroundPaint(new Color(20, 20, 20, 160));
		chartPanel = new ChartPanel(chart);

		chartPanel.setPreferredSize(new java.awt.Dimension(500, 200));

	}

	public void updateChartPanel(double bestFitnessValue, double meanFitnessValue) {
		datasetForChart.getSeries(0).addOrUpdate(new Millisecond(), bestFitnessValue);
		datasetForChart.getSeries(1).addOrUpdate(new Millisecond(), meanFitnessValue);
	}

	public void createButtonPanel() {

		GridBagLayout gbl = new GridBagLayout();

		//JButton sendIndividualsButton;
		//JButton stopIslandButton;
		JButton closeButton;

		/*
		 * [HV] The local variable stop is never read boolean stop = false;
		 */

		stopButton = new JButton("Stop");
		stopButton.setToolTipText("Stops this Island");
		stopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				//stop = true;
				//disableSendIndividuals();
				messagePanel.log("Island stopped", MessageLevel.NORMAL);
			}

		});

		closeButton = new JButton("Close");
		closeButton.setToolTipText("Close the dialog");
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close = true;
				setVisible(false);
			}
		});

		buttonPanel.setLayout(gbl);

		// STOP button
		constraints.weightx = 1.0;
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.fill = GridBagConstraints.BOTH;
		buttonPanel.add(stopButton, constraints);

		// CLOSE button
		constraints.weightx = 1.0;
		constraints.gridx = 2;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.fill = GridBagConstraints.BOTH;
		buttonPanel.add(closeButton, constraints);

	}

	/**
	 * Show the dialog and wait until the user has pressed Close.
	 * 
	 * @return Whether the user has pressed Close.
	 */
	public boolean showModal() {
		setVisible(true);
		return close;
	}

}
