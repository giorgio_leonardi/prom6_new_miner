package org.processmining.plugins.heuristicsnet.miner.genetic;

public class GeneticMinerStatistics {
	protected double mean;
	protected double variance;
	protected double standardDeviation;
	protected double bestFitness;
	protected double elapsedTime;
	protected int currentGeneration;

	public GeneticMinerStatistics(int maxGeneration) {
		// TODO Auto-generated constructor stub

		//		mean = new double[maxGeneration];
		//		variance = new double[maxGeneration];
		//		standardDeviation =new double[maxGeneration];
		//		bestFitness = new double[maxGeneration];

	}

	/**
	 * 
	 * @param currentGeneration
	 * @param elapsedTime
	 * @param mean
	 * @param standardDeviation
	 * @param variance
	 * @param bestFitness
	 */
	public GeneticMinerStatistics(int currentGeneration, double elapsedTime, double mean, double standardDeviation,
			double variance, double bestFitness) {
		super();
		this.bestFitness = bestFitness;
		this.currentGeneration = currentGeneration;
		this.elapsedTime = elapsedTime;
		this.mean = mean;
		this.standardDeviation = standardDeviation;
		this.variance = variance;
	}

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public double getVariance() {
		return variance;
	}

	public void setVariance(double variance) {
		this.variance = variance;
	}

	public double getStandardDeviation() {
		return standardDeviation;
	}

	public void setStandardDeviation(double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}

	public double getBestFitness() {
		return bestFitness;
	}

	public void setBestFitness(double bestFitness) {
		this.bestFitness = bestFitness;
	}

	public double getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(double elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public int getCurrentGeneration() {
		return currentGeneration;
	}

	public void setCurrentGeneration(int currentGeneration) {
		this.currentGeneration = currentGeneration;
	}

}