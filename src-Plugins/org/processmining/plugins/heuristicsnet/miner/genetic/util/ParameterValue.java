package org.processmining.plugins.heuristicsnet.miner.genetic.util;

public class ParameterValue {

	private String parameter = "";
	private double defaultValue = 0.0d;
	private double minValue = 0.0d;
	private double maxValue = 1.0d;
	private String description = "";
	private double stepSize = 0.1d;

	/**
	 * Creates the object.
	 * 
	 * @param parameterName
	 *            String - name of the parameter.
	 * @param defaultvalue
	 *            double - value of the parameter.
	 * @param minValue
	 *            double - minimum value of the parameter.
	 * @param maxValue
	 *            double - maximum value of the parameter.
	 * @param stepSize
	 *            double - size of the increment for this value. (Useful for
	 *            interfaces)
	 * @param parameterDescription
	 *            String - the parameter description.
	 */

	public ParameterValue(String parameterName, double defaultvalue, double minValue, double maxValue, double stepSize,
			String parameterDescription) {
		parameter = parameterName;
		defaultValue = defaultvalue;
		this.minValue = minValue;
		this.maxValue = maxValue;
		description = parameterDescription;
		this.stepSize = stepSize;
	}

	public double getMaxValue() {
		return maxValue;
	}

	public double getMinValue() {
		return minValue;
	}

	public double getStepSize() {
		return stepSize;
	}

	public String getParameter() {
		return parameter;
	}

	public double getDefaultValue() {
		return defaultValue;
	}

	public String getDescription() {
		return description;
	}
}
