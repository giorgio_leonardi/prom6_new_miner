package org.processmining.plugins.heuristicsnet.miner.genetic;

import java.awt.Color;

import javax.swing.JPanel;

import org.processmining.framework.plugin.events.Logger.MessageLevel;
import org.processmining.framework.util.ui.SlickerMessageBar;

public class MessagePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1349279385028819327L;

	private final SlickerMessageBar messages = new SlickerMessageBar();

	/*
	 * [HV] The field IslandMessagePanel.p is never read locally private
	 * JScrollPane p;
	 */

	protected static Color COLOR_OUTER_BG = new Color(130, 130, 130);
	protected static Color COLOR_INNER_BG = new Color(170, 170, 170);

	public MessagePanel() {
		// TODO Auto-generated constructor stub

		//p = new JScrollPane(messages);

		//SlickerSwingUtils.injectBackgroundColor(p.getViewport().getView(), COLOR_INNER_BG);
		//JScrollBar hBar = p.getHorizontalScrollBar();
		//JScrollBar vBar = p.getVerticalScrollBar();

		//hBar.setUI(new SlickerScrollBarUI(hBar, COLOR_OUTER_BG, new Color(40, 40, 40), new Color(70, 70, 70), 4, 13));
		//vBar.setUI(new SlickerScrollBarUI(vBar, COLOR_OUTER_BG, new Color(40, 40, 40), new Color(70, 70, 70), 4, 13));
		messages.setPreferredSize(new java.awt.Dimension(500, 200));
		this.add(messages);

	}

	public synchronized void log(String message, MessageLevel messageLevel) {
		messages.receiveMessage(message, messageLevel);
	}

}
