package org.processmining.plugins.heuristicsnet.miner.genetic.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.fluxicon.slickerbox.factory.SlickerFactory;

public class GeneticMinerSplitLogParameters extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2950616655818563341L;
	private JLabel percentageLabel = SlickerFactory.instance().createLabel("NrOfSamples");
	private JTextField percentage = new JTextField();
	
	private JLabel addIntervalLabel =  SlickerFactory.instance().createLabel("Number of Traces");
	private JTextField addInterval = new JTextField();
	
//	private JLabel sameSampleLabel =  SlickerFactory.instance().createLabel("Same sample per run");
//	private JCheckBox sameSample = new JCheckBox();
	
	private int nrTraces = 0;
	
	public GeneticMinerSplitLogParameters(int nrTRaces) {
		
		nrTraces = nrTRaces;

		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void jbInit() {
		// TODO Auto-generated method stub
		
		JPanel somePanel = SlickerFactory.instance().createRoundedPanel();
		somePanel.setLayout(new GridBagLayout());
		int y = 0;
		int x = 0;

		addInterval.setPreferredSize(new Dimension(50, 21));
		addInterval.setText(nrTraces+"");
		addInterval.setEnabled(false);
		
	
		percentage.setPreferredSize(new Dimension(50, 21));
		percentage.setText((new Integer(10)).toString());
		
			
		somePanel.add(addIntervalLabel, new GridBagConstraints(0, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		somePanel.add(addInterval, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
		//		optionsPanel.add(new HelpIcon(HelpMessagesLibrary.SELECTION_TYPE), new GridBagConstraints(1, x + y, 1, 1,
		//				0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));

		somePanel.add(percentageLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		somePanel.add(percentage, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
		
//		sameSample.setSelected(false);
//		somePanel.add(sameSampleLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
//		somePanel.add(sameSample, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
//				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
//		
		this.setLayout(new BorderLayout());
		this.add(somePanel, BorderLayout.NORTH);
		
		
	}
	
	
	public int getAddInterval() {
		try {
			if ((Integer.parseInt(addInterval.getText()) <= 0) ) {
				throw new Exception();
			}
		} catch (Exception e) {
			addInterval.setText(Integer.toString(20));
		}

		return Integer.parseInt(addInterval.getText());
	}
	
	public int getPercentage() {
		try {
			if ((Integer.parseInt(percentage.getText()) <= 0) ) {
				throw new Exception();
			}
		} catch (Exception e) {
			percentage.setText(Integer.toString(20));
		}

		return Integer.parseInt(percentage.getText());
	}



//	/**
//	 * @return the sameSample
//	 */
//	public boolean getSameSample() {
//		return sameSample.isSelected();
//	}

}
