package org.processmining.plugins.heuristicsnet.miner.genetic.gui;

/**
 * This class contains all the help messages for the GeneticMiner parameters.
 * 
 * @author carmen
 * 
 */
public class GMHelpMessagesLibrary {

	/**
	 * Index of the default population type to be used during the mining. This
	 * index is based on the array in the class
	 * <code>InitialPopulationFactory</code>, method
	 * <code>getInitialPopulationTypes()</code>.
	 */
	public static final String INITIAL_POPULATION_TYPE = "<html>Sets how the initial population should be built. The possible options are:"
			+ "<ul><li>No Duplicates Activities, i.e. Every event in the log is associated with at most one activity <br>"
			+ " in the individual."
			+ "<li>Causal Heuristics (Duplicates+Arcs), i.e. Use heuristics to set the amount of duplicates for each <br>"
			+ " event in the log  and the arcs among the tasks. The amount of duplicates is set based on the<br>"
			+ " causal relation. The arcs are set based on the follows relation."
			+ "<li>Causal Heuristics (Duplicates), i.e. Uses the <i>causal</i> relation to set the amount of<br>"
			+ " duplicates per task, but the arcs are randomly set. "
			+ "<li>Follows Heuristics, i.e. Uses the <i>follows</i> relation to set the amount<br>"
			+ " of duplicates per task and  the arcs among the tasks.</ul></html>";

	/**
	 * Default size of the population with <code>HeuristicsNet</code> objects.
	 */
	public static final String POPULATION_SIZE = "Sets the number of individuals that are going to be used during the search";

	/**
	 * Default value of the elitism rate. The number of objects in a population
	 * that will be directly copied to the next population is equal to
	 * <code>ELITISM_RATE</code> times <code>POPULATION_SIZE</code>.
	 */
	public static final String ELITISM_RATE = "Sets the percentage of the fittest individuals in the current generation that are going to be copied to the next generation.";

	/**
	 * Default value for the maximum number of population to be created during a
	 * run of the genetic algorithm.
	 */
	public static final String MAX_GENERATION = "Sets the maximum number of times that the genetic algorithm can iterate. ";

	/**
	 * Index of the default fitness type to be used during the mining. This
	 * index is based on the array in the class <code>FitnessFactory</code>,
	 * method <code>getAllFitnessTypes()</code>.
	 */
	public static final String FITNESS_TYPE = "<html>Sets the type of the fitness that the Genetic Miner plug-in uses to assess the quality of an individual.  "
			+ "The quality of an individual is basically set by its replaying of the log traces. <br>"
			+ "This replay can use a Stop Semantics parsing or a Continuous Semantics one. <br>"
			+ " The Stop Semantics parsing stops whenever the task to be parsed is not enabled. <br>"
			+ " The Continuous Semantics parsing works in a different way: whenever a task is not enabled, <br>"
			+ " the problem is registered (for instance, the number of missing tokens), and the task is fired anyway. <br>"
			+ " The fitness types Proper Completion and Stop Semantics use stop semantics parsing. <br>"
			+ " The other fitness types use a continuous semantics one. <br>"
			+ "The available fitness types work as following: "
			+ "<ul><li>Proper Completion, i.e.   For every <code>HeuristicsNet</code> in a population,"
			+ " this class calculates the ratio of the traces in a log that<br>"
			+ " could be completely replayed without problems and without tokens being left behind. "
			+ "<li>Stop Semantics, i.e.  Calculates the fitness of <code>HeuristicsNet</code> objects"
			+ " in a population based on a weighed sum of <ul><li>(i) the ratio of parsed"
			+ " activities, <li> (ii) the ratio of completed traces and"
			+ "<li> (iii) the ratio of properly completed traces.</ul>"
			+ "<li>Continuous Semantics, i.e.  Calculates the fitness of <code>HeuristicsNet</code> objects"
			+ " in a population based on a weighed sum of <ul><li>(i) the ratio of"
			+ " parsed activities without problems and <li>(ii) the ratio of"
			+ " properly completed traces.</ul>"
			+ "<li>Improved Continuous Sematics, i.e.  Calculates the fitness of <code>HeuristicsNet</code> objects"
			+ " in a population based on the ratio of activities in a log that could be parsed (or replayed) without<br>"
			+ " problems. The ratio contains a punishment component based on the amount of problems encountered during the log replay<br>"
			+ " <i>and</i> the amount of traces with parsing problems. In a nutshell, this punishment factor benefits the<br>"
			+ " <code>HeuristicsNet</code> objects that have fewer problems scattered in fewer traces."
			+ " <p> Note that the parsing semantics of this fitness measure"
			+ " is a continuous one (i.e.,  the log replay does not stop"
			+ " when problems are encountered). "
			+ "<li>Extra Behavior, i.e.  Calculates the fitness of <code>HeuristicsNet</code> objects"
			+ " in a population based on <ul><li>(i) the improved continuous"
			+ " semantics fitness of this object"
			+ " (see {@link ImprovedContinuousSemantics}) <i>minus</i>"
			+ " <li>(ii.a) the weighed ratio of"
			+ " concurrently enabled tasks during the log replay and <li>(ii.b)"
			+ " the weighed ratio of activities with a same label (i.e., duplicates)"
			+ "  sharing common input or output elements.</ul></ul></html>";

	/**
	 * Index of the default selection type to be used during the mining. This
	 * index is based on the array in the class
	 * <code>SelectionMethodFactory</code>, method
	 * <code>getAllSelectionMethodsTypes()</code>.
	 */
	public static final String SELECTION_TYPE = "<html>Sets how the parents for genetic operators are going to be chosen. "
			+ "Both methods are based on a tournament. The method type Tournament <br> "
			+ "works by randomly selecting two individuals in the population and returning <br>"
			+ " the fittest individual in 75% of the times, and the less fit individual in 25% of the times. <br>"
			+ " The method type Tournament5 randomly selects five individuals in the population <br>"
			+ "and always returns the fittest individual.</html>";

	/**
	 * Default value for the crossover rate.
	 */
	public static final String CROSSOVER_RATE = "<html>Sets the probability that two parents are going to be recombined to create two "
			+ "offsprings for the next generation. If the probability is equal to 0,<br>"
			+ " then, after the crossover, the offsprings are just like the parents.</html>";

	/**
	 * Index of the default crossover type to be used during the mining. This
	 * index is based on the array in the class <code>CrossoverFactory</code>,
	 * method <code>getAllCrossoverTypes()</code>.
	 */
	public static final String CROSSOVER_TYPE = "<html>Sets how two parents (selected individuals) are going to be recombined. "
			+ " The only available crossover type is Enhanged that works as follows: <br>"
			+ "swaps subsets in the input/output set of a task. With equal probability,<br>"
			+ "the swapped subsets can be: <ul><li>(i) included into the input/output set, "
			+ "<li>(ii) merged with some unswapped subsets in the input/output sets, or"
			+ "<li> (iii) add the swapped sets and remove from some unswapped subsets intersecting <br> "
			+ "elements that are in the swapped ones.</ul></html>";

	/**
	 * Default value for the mutation rate.
	 */
	public static final String MUTATION_RATE = "Sets the probability that an individual is going to be mutated.";

	/**
	 * Index of the default mutation type to be used during the mining. This
	 * index is based on the array in the class <code>MutationFactory</code>,
	 * method <code>getAllMutationTypes()</code>.
	 */
	public static final String MUTATION_TYPE = "<html>Sets how an individual is going to be randomly modified. The mutation point is a task. <br>"
			+ "The available mutation type is Enhanced and "
			+ "it performs one of the following operations with equal probability:"
			+ "<ul><li> (i) add a task to a subset in the input/output set of the mutated task,"
			+ "<li> (ii) remove a task from a subset in the input/output set of a mutated task, or"
			+ "<li> (iii) re-arrange the subsets of the input/output set of the mutated task.</ul></html>";

	/**
	 * Default value of the power factor to be used when computing the
	 * dependency relations.
	 */
	public static final String POWER = "<html>Sets the power value that is used by the heuristics to build <br>"
			+ "the initial population.In a nutshell, the heuristics works as follows:<br>"
			+ " the more often a task t1 is directly followed by a task t2 <br> "
			+ "(i.e. the subtrace t1t2 appears in traces in the log), <br>"
			+ "the higher the probability that individuals are built with a dependency (or arc) <br>"
			+ "from t1 to t2.The power value is used to control the influence of the heuristics <br>"
			+ "in the probability of setting a dependency between two tasks. <br>"
			+ "Higher values for power value lead to the inferring of fewer dependencies <br>"
			+ "between two tasks in the event log, and vice-versa. </html>";

	/**
	 * Default value used execute one run the genetic algorithm.
	 */
	public static final String SEED = "<html>Sets the seed that is used to generate random number for this plug-in. <br>"
			+ "The parameter is used to ensure the repeatability of the results.</html>";

	public static String STOP_CONDITION = "The algorithm stops when at least one individual with a higher fitness than the stop fitness value is discovered.";

}
