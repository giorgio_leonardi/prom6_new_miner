package org.processmining.plugins.heuristicsnet.miner.genetic.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.processmining.contexts.util.ToolTipComboBox;
import org.processmining.plugins.heuristicsnet.miner.genetic.GeneticMinerConstants;
import org.processmining.plugins.heuristicsnet.miner.genetic.fitness.FitnessFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.CrossoverFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.MutationFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.miner.settings.GeneticMinerSettings;
import org.processmining.plugins.heuristicsnet.miner.genetic.population.InitialPopulationFactory;
import org.processmining.plugins.heuristicsnet.miner.genetic.selection.SelectionMethodFactory;

import com.fluxicon.slickerbox.factory.SlickerFactory;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

@SuppressWarnings("serial")
public class GeneticMinerUIInputParameters extends JPanel {

	private final JPanel mainPanel = SlickerFactory.instance().createRoundedPanel();
	private final JPanel genOpPanel = SlickerFactory.instance().createRoundedPanel();

	private final JPanel fitnessPanel = SlickerFactory.instance().createRoundedPanel();

	private JLabel initialPopulationTypeLabel;
	private final ToolTipComboBox initialPopulationType = new ToolTipComboBox(InitialPopulationFactory
			.getInitialPopulationTypes());
	private final JPanel otherOptionsPanel = SlickerFactory.instance().createRoundedPanel();
	private final JPanel geneticOptionsPanel = SlickerFactory.instance().createRoundedPanel();

	private final JCheckBox useGenOp = SlickerFactory.instance().createCheckBox("Keep statistics", true);

	private final JCheckBox viualization = SlickerFactory.instance().createCheckBox("Visualization", false);

	private JLabel popSizeLabel;
	private final JTextField popSize = new JTextField();
	private JLabel maxGenerationLabel;
	private final JTextField maxGeneration = new JTextField();
	private JLabel elitismLabel;
	private final JTextField elitismRate = new JTextField();
	private JLabel seedLabel;
	private final JTextField seed = new JTextField();

	private JLabel fitnessTypeLabel;
	private final ToolTipComboBox fitnessType = new ToolTipComboBox(FitnessFactory.getAllFitnessTypes());

	private final JTextField crossoverRate = new JTextField();
	private JLabel crossoverLabel;
	private JLabel selectionMethodTypeLabel;
	private final ToolTipComboBox selectionMethodType = new ToolTipComboBox(SelectionMethodFactory
			.getAllSelectionMethodsTypes());
	private JLabel crossoverTypeLabel;
	private final ToolTipComboBox crossoverType = new ToolTipComboBox(CrossoverFactory.getAllCrossoverTypes());
	private JLabel mutationTypeLabel;
	private final ToolTipComboBox mutationType = new ToolTipComboBox(MutationFactory.getAllMutationTypes());
	private final JTextField mutationRate = new JTextField();
	private JLabel mutationLabel;

	private final JTextField power = new JTextField();
	private JLabel powerLabel;

	private final JLabel stopConditionLabel = SlickerFactory.instance().createLabel("Stop Fitness Value");
	private final JTextField stopCondition = new JTextField("0.8");

	// the report if I keep statistics

	//	private JLabel reportLabel = SlickerFactory.instance().createLabel(
	//			"Report: ");
	//	private JTextField reportField = new JTextField(12);
	//	private JButton reportBrow = new JButton("Browse...");
	//	private JPanel rPanel = SlickerFactory.instance().createRoundedPanel();
	//	private String reportName = "report";
	//	private File reportFile = new File(reportName);

	// String curDir;

	public GeneticMinerUIInputParameters() {

		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public GeneticMinerSettings getSettings() {
		return new GeneticMinerSettings(getPopulationSize(), getMaxNumGenerations(), getMutationRate(),
				getCrossoverType(), getCrossoverRate(), getSeed(), getFitnessType(), getSelectionMethodType(),
				getElitismRate(), getMutationType(), getPower(), getInitialPopulationType(), getKeepStatistics(),
				getFilename(), getStopCondition());
	}

	private double getStopCondition() {
		try {
			if (Double.parseDouble(stopCondition.getText()) <= 0) {
				throw new Exception();
			}
		} catch (Exception e) {
			stopCondition.setText(Double.toString(0.8));
		}

		return Double.parseDouble(stopCondition.getText());
	}

	private long getSeed() {
		try {
			Long.parseLong(seed.getText());
		} catch (Exception e) {
			seed.setText(Long.toString(GeneticMinerConstants.SEED));
		}

		return Long.parseLong(seed.getText());
	}

	private int getPopulationSize() {
		try {
			if (Integer.parseInt(popSize.getText()) <= 0) {
				throw new Exception();
			}
		} catch (Exception e) {
			popSize.setText(Integer.toString(GeneticMinerConstants.POPULATION_SIZE));
		}

		return Integer.parseInt(popSize.getText());
	}

	private int getMaxNumGenerations() {
		try {
			if (Integer.parseInt(maxGeneration.getText()) < 0) {
				throw new Exception();
			}
		} catch (Exception e) {
			maxGeneration.setText(Integer.toString(GeneticMinerConstants.MAX_GENERATION));
		}

		return Integer.parseInt(maxGeneration.getText());

	}

	private double getElitismRate() {
		double e = GeneticMinerConstants.ELITISM_RATE;
		try {
			e = Double.parseDouble(mutationRate.getText());
			if (e > 1.0) {
				e = 1.0;
				throw new NumberFormatException();
			}
		} catch (NumberFormatException nfe) {
			elitismRate.setText(Double.toString(e));
		}

		return Double.parseDouble(elitismRate.getText());
	}

	private double getMutationRate() {
		double m = GeneticMinerConstants.MUTATION_RATE;
		try {
			m = Double.parseDouble(mutationRate.getText());
			if (m > 1.0) {
				m = 1.0;
				throw new NumberFormatException();
			}
		} catch (NumberFormatException nfe) {
			mutationRate.setText(Double.toString(m));
		}

		return m;
	}

	private double getPower() {
		double p = GeneticMinerConstants.POWER;
		try {
			p = Long.parseLong(power.getText());
		} catch (NumberFormatException nfe) {
			power.setText(Double.toString(p));
		}

		if ((p % 2) == 0) {
			p += 1; // making the power odd!
			power.setText(Double.toString(p));
		}

		return p;
	}

	private int getInitialPopulationType() {
		return initialPopulationType.getSelectedIndex();
	}

	private int getCrossoverType() {
		return crossoverType.getSelectedIndex();
	}

	private int getMutationType() {
		return mutationType.getSelectedIndex();
	}

	private int getSelectionMethodType() {
		return selectionMethodType.getSelectedIndex();
	}

	private double getCrossoverRate() {
		double d = GeneticMinerConstants.CROSSOVER_RATE;

		try {
			d = Double.parseDouble(crossoverRate.getText());
			if (d > 1.0) {
				d = 1.0;
				throw new NumberFormatException();
			}
		} catch (NumberFormatException nfe) {
			crossoverRate.setText(Double.toString(d));
		}

		return Double.parseDouble(crossoverRate.getText());
	}

	private boolean getKeepStatistics() {
		return useGenOp.isSelected();
	}

	public boolean visualization() {
		return viualization.isSelected();
	}

	private String getFilename() {
		//		return reportName;
		return null;
	}

	private int getFitnessType() {
		return fitnessType.getSelectedIndex();
	}

	private void jbInit() throws Exception {

		int x = 0;
		int y = 0;

		// ---- options for genetic operations
		// -----------------------------------------
		selectionMethodTypeLabel = SlickerFactory.instance().createLabel("Selection method type");
		selectionMethodType.setSelectedIndex(GeneticMinerConstants.SELECTION_TYPE);

		crossoverLabel = SlickerFactory.instance().createLabel("Crossover rate");
		crossoverRate.setPreferredSize(new Dimension(50, 21));
		crossoverRate.setText(Double.toString(GeneticMinerConstants.CROSSOVER_RATE));

		crossoverTypeLabel = SlickerFactory.instance().createLabel("Crossover type");
		crossoverType.setSelectedIndex(GeneticMinerConstants.CROSSOVER_TYPE);

		mutationTypeLabel = SlickerFactory.instance().createLabel("Mutation type");
		mutationLabel = SlickerFactory.instance().createLabel("Mutation rate");
		mutationRate.setPreferredSize(new Dimension(50, 21));
		mutationRate.setText(Double.toString(GeneticMinerConstants.MUTATION_RATE));
		mutationType.setSelectedIndex(GeneticMinerConstants.MUTATION_TYPE);

		genOpPanel.setLayout(new GridBagLayout());

		//		useGenOp.setText("Keep statistics");
		//		useGenOp.setSelected(true);

		// here I make the reportpanel

		//		reportLabel.setForeground(Color.black);
		//		reportField.setText(reportName);
		//
		//		rPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		//
		//		rPanel.add(reportLabel);
		//		rPanel.add(reportField);
		//		rPanel.add(reportBrow); // rPanel.setBackground(SKYBLUE);
		//		rPanel.setVisible(useGenOp.isSelected());
		//		reportBrow.addActionListener(new FileOutHandler());
		//		reportBrow.setText("Browse");
		//
		//		useGenOp.addActionListener(new ActionListener() {
		//			public void actionPerformed(ActionEvent e) {
		//				rPanel.setVisible(useGenOp.isSelected());
		//				// notUsingGenOpPanel.setVisible(!useGenOp.isSelected());
		//			}
		//		});

		// genOpPanel.setLayout(new BorderLayout());
		//		genOpPanel.add(useGenOp, new GridBagConstraints(0, x + (++y), 1, 1,
		//				0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
		//				new Insets(0, 5, 0, 0), 0, 0));
		//		genOpPanel.add(rPanel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0,
		//				0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
		//				new Insets(0, 33, 0, 0), 0, 0));

		genOpPanel.add(viualization, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));

		// ------ other options
		// --------------------------------------------------------
		popSizeLabel = SlickerFactory.instance().createLabel("Population size");
		popSize.setPreferredSize(new Dimension(50, 21));
		popSize.setText(Integer.toString(GeneticMinerConstants.POPULATION_SIZE));

		initialPopulationTypeLabel = SlickerFactory.instance().createLabel("Initial population type");
		initialPopulationType.setSelectedIndex(GeneticMinerConstants.INITIAL_POPULATION_TYPE);

		maxGenerationLabel = SlickerFactory.instance().createLabel("Max number generations");
		maxGeneration.setPreferredSize(new Dimension(50, 21));
		maxGeneration.setText(Integer.toString(GeneticMinerConstants.MAX_GENERATION));

		seedLabel = SlickerFactory.instance().createLabel("Seed");
		seed.setPreferredSize(new Dimension(50, 21));
		seed.setText(Long.toString(GeneticMinerConstants.SEED));

		powerLabel = SlickerFactory.instance().createLabel("Power value");
		power.setPreferredSize(new Dimension(50, 21));
		power.setText(Double.toString(GeneticMinerConstants.POWER));
		stopCondition.setPreferredSize(new Dimension(50, 21));

		fitnessTypeLabel = SlickerFactory.instance().createLabel("Fitness type");
		fitnessType.setSelectedIndex(GeneticMinerConstants.FITNESS_TYPE);

		fitnessType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (FitnessFactory.getAdvancedFitnessParametersNames(fitnessType.getSelectedIndex()).length > 0) {
					fitnessPanel.setVisible(true);
				} else {
					fitnessPanel.setVisible(false);
				}
			}
		});

		elitismLabel = SlickerFactory.instance().createLabel("Elitism rate");
		elitismRate.setPreferredSize(new Dimension(50, 21));
		elitismRate.setText(Double.toString(GeneticMinerConstants.ELITISM_RATE));

		otherOptionsPanel.setLayout(new GridBagLayout());
		y = 0;

		otherOptionsPanel.add(selectionMethodTypeLabel, new GridBagConstraints(0, x + y, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(selectionMethodType, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.SELECTION_TYPE), new GridBagConstraints(1, x + y, 1,
				1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));

		otherOptionsPanel.add(crossoverTypeLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.CROSSOVER_TYPE), new GridBagConstraints(1, x + y, 1,
				1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(crossoverType, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		otherOptionsPanel.add(crossoverLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.CROSSOVER_RATE), new GridBagConstraints(1, x + y, 1,
				1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(crossoverRate, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		otherOptionsPanel.add(mutationTypeLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.MUTATION_TYPE), new GridBagConstraints(1, x + y, 1, 1,
				0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(mutationType, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		otherOptionsPanel.add(mutationLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.MUTATION_RATE), new GridBagConstraints(1, x + y, 1, 1,
				0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(mutationRate, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		otherOptionsPanel.add(stopConditionLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.STOP_CONDITION), new GridBagConstraints(1, x + y, 1,
				1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(stopCondition, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		// generation size
		otherOptionsPanel.add(popSizeLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.POPULATION_SIZE), new GridBagConstraints(1, x + y, 1,
				1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(popSize, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		// initial population type
		otherOptionsPanel.add(initialPopulationTypeLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.INITIAL_POPULATION_TYPE), new GridBagConstraints(1, x
				+ y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(initialPopulationType, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		// max runs
		otherOptionsPanel.add(maxGenerationLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.MAX_GENERATION), new GridBagConstraints(1, x + y, 1,
				1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(maxGeneration, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
		// seed
		otherOptionsPanel.add(seedLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.SEED), new GridBagConstraints(1, x + y, 1, 1, 0.0,
				0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(seed, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		// power
		otherOptionsPanel.add(powerLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.POWER), new GridBagConstraints(1, x + y, 1, 1, 0.0,
				0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(power, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		// elitism rate
		otherOptionsPanel.add(elitismLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.ELITISM_RATE), new GridBagConstraints(1, x + y, 1, 1,
				0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(elitismRate, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		// firing semantics

		otherOptionsPanel.add(fitnessTypeLabel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		otherOptionsPanel.add(new HelpIcon(GMHelpMessagesLibrary.FITNESS_TYPE), new GridBagConstraints(1, x + y, 1, 1,
				0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 1, 0, 0), 0, 0));
		otherOptionsPanel.add(fitnessType, new GridBagConstraints(2, x + y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));

		// ------ putting main panel together
		// ------------------------------------------
		geneticOptionsPanel.setLayout(new GridBagLayout());
		y = 0;
		geneticOptionsPanel.add(otherOptionsPanel, new GridBagConstraints(0, x + y, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		geneticOptionsPanel.add(fitnessPanel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

		geneticOptionsPanel.add(genOpPanel, new GridBagConstraints(0, x + (++y), 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

		mainPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		mainPanel.add(geneticOptionsPanel, null);

		setLayout(new BorderLayout());
		this.add(mainPanel, BorderLayout.NORTH);
	}

	//	class FileOutHandler implements ActionListener {
	//		public void actionPerformed(ActionEvent ae) {
	//			JFileChooser chooser = new JFileChooser();
	//			chooser.setMultiSelectionEnabled(false);
	//			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	//			chooser.setSelectedFile(reportFile); // default output
	//			int option = chooser.showSaveDialog(genOpPanel);
	//			if (option == JFileChooser.APPROVE_OPTION) {
	//				reportFile = chooser.getSelectedFile();
	//				try {
	//					reportName = reportFile.getCanonicalPath();
	//				} catch (IOException e) {
	//				}
	//				;
	//				reportField.setText(reportName);
	//			} else {/* addcancelled message here */
	//			}
	//		}
	//	}

}
