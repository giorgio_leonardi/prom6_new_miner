package org.processmining.plugins.heuristicsnet.miner.genetic.gui;

import javax.swing.JLabel;
import javax.swing.UIManager;

class HelpIcon extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	HelpIcon(String message) {
		super(UIManager.getIcon("OptionPane.questionIcon"));
		//addMouseListener(new HelpPopUp(message));
		setToolTipText(message);
	}

}
