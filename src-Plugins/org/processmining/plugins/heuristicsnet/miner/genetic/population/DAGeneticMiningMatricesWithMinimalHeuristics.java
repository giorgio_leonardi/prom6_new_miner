package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.info.XLogInfo;
import org.processmining.framework.util.Pair;
import org.processmining.models.heuristics.impl.HNSubSet;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;

/**
 * <p>
 * This class uses the follows matrices to set the maximum number of duplicated
 * tasks that an individual (HeuristicsNet) can have. Basically, the number of
 * duplicates of a task <i>t</i> is the minimum between the number of tasks that
 * directly follow <i>t</i> and the number of tasks that are directly followed
 * by <i>t</i>.
 * </p>
 * 
 * <p>
 * The arcs are set based on the dependency matrix and the power value. The more
 * often a task <i>t</i> is followed by a task <i>t'</i>, the higher the
 * probability that an arc from a duplicate of <i>t</i> to a duplicate of
 * <i>t'</i> will be set.
 * </p>
 * 
 * @author Ana Karla A. de Medeiros
 */

public class DAGeneticMiningMatricesWithMinimalHeuristics extends DAGeneticMiningMatrices {

	//TODO - Review and update documentation

	public DAGeneticMiningMatricesWithMinimalHeuristics(Random gen, XLogInfo log, double power) {
		super(gen, log, power);
	}

	@Override
	protected Map<XEventClass, Integer> setDuplicatesMapping(XEventClasses eventClasses) {

		Map<XEventClass, Integer> inFollowsRelations = new HashMap<XEventClass, Integer>();
		Map<XEventClass, Integer> outFollowsRelations = new HashMap<XEventClass, Integer>();
		Map<XEventClass, Integer> numberDuplicateActivitiesPerEvent = new HashMap<XEventClass, Integer>();

		//Checking how many duplicate activities should be created for the events
		//in the log. The used heuristics is as follows:
		//For every event, the #activities = min(#inputFollowsRelations, #outputFollowsRelations) for this event

		for (Pair<XEventClass, XEventClass> pairEventsFollowsRelation : basicFollows.keySet()) {

			XEventClass firstElement = pairEventsFollowsRelation.getFirst();
			XEventClass secondElement = pairEventsFollowsRelation.getSecond();

			int newNumberInFollowsRelations = inFollowsRelations.containsKey(secondElement) ? inFollowsRelations
					.get(secondElement) + 1 : 1;
			inFollowsRelations.put(secondElement, newNumberInFollowsRelations); //not that the new number has already been increased by one!

			int newNumberOutFollowsRelations = outFollowsRelations.containsKey(firstElement) ? outFollowsRelations
					.get(firstElement) + 1 : 1;
			outFollowsRelations.put(firstElement, newNumberOutFollowsRelations); //not that the new number has already been increased by one!

		}

		//Creating the actual duplicate activities
		Iterator<XEventClass> iterator = eventClasses.getClasses().iterator();
		while (iterator.hasNext()) {
			XEventClass event = iterator.next();
			int numberInFollowsRelationsForThisEvent = inFollowsRelations.containsKey(event) ? inFollowsRelations.get(
					event).intValue() : MINIMUM_NUMBER_ACTIVITIES_PER_EVENT;
			int numberOutFollowsRelationsForThisEvent = outFollowsRelations.containsKey(event) ? outFollowsRelations
					.get(event).intValue() : MINIMUM_NUMBER_ACTIVITIES_PER_EVENT;
			int numberOfDuplicateActivitiesForThisEvent = Math.max(numberInFollowsRelationsForThisEvent,
					numberOutFollowsRelationsForThisEvent);
			numberDuplicateActivitiesPerEvent.put(event, new Integer(numberOfDuplicateActivitiesForThisEvent));
		}

		return numberDuplicateActivitiesPerEvent;
	}

	public DoubleMatrix2D buildCausalMatrix() {

		double random;

		//creating the causal matrix that considers duplicate tasks
		int causalMatrixDimensionSize = activitiesMappingStructures.getActivitiesMapping().length;
		causal = new SparseDoubleMatrix2D(causalMatrixDimensionSize, causalMatrixDimensionSize);

		//getting the XEventClass events to build the causal matrix for
		Set<XEventClass> setOfEvents = activitiesMappingStructures.getReverseActivitiesMapping().keySet();

		//for every combination of events, decide if a causal relation should
		//be set in the matrix "causal"
		for (XEventClass fromEvent : setOfEvents) {
			for (XEventClass toEvent : setOfEvents) {
				random = generator.nextDouble();
				if (random < Math.pow(depRelBuilder.getFollowsDependency(fromEvent, toEvent), power)) {
					//randomly choose a duplicate from row to column
					//and add an arc
					Map<XEventClass, HNSubSet> reverseActivitiesMapping = activitiesMappingStructures
							.getReverseActivitiesMapping();

					//randomly choosing activities to set the causality relations
					int duplicateTask_row = reverseActivitiesMapping.get(fromEvent).get(
							generator.nextInt(reverseActivitiesMapping.get(fromEvent).size()));
					int duplicateTask_column = reverseActivitiesMapping.get(toEvent).get(
							generator.nextInt(reverseActivitiesMapping.get(toEvent).size()));
					//setting the actual causality relation between the randomly chosen
					//activities for the given duplicates
					causal.set(duplicateTask_row, duplicateTask_column, 1);
				}

			}
		}

		return causal;

	}

}
