package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import java.util.Random;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.info.XLogInfo;
import org.processmining.models.heuristics.impl.ActivitiesMappingStructures;
import org.processmining.plugins.heuristicsnet.miner.genetic.GeneticMinerConstants;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix1D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;

/**
 * 
 * @author Ana Karla Alves de Medeiros
 * 
 */
public class GeneticMiningMatricesWithHeuristics implements GeneticMiningMatrices {

	//TODO - Add/Review documentation

	private double power = GeneticMinerConstants.POWER;
	private DoubleMatrix2D causal = null;
	private DoubleMatrix1D end = null;
	private DoubleMatrix1D start = null;

	private Random generator = null;
	private DependencyRelationBuilder depRelBuilder = null;

	private ActivitiesMappingStructures activitiesMappingStructures = null;

	public GeneticMiningMatricesWithHeuristics(Random gen, XLogInfo logReader) {
		this(gen, logReader, GeneticMinerConstants.POWER);
	}

	public GeneticMiningMatricesWithHeuristics(Random gen, XLogInfo logReader, double power) {
		this.power = power;
		generator = gen;
		depRelBuilder = new DependencyRelationBuilder(logReader);
		activitiesMappingStructures = new ActivitiesMappingStructures(logReader.getEventClasses());

		buildMatrices(activitiesMappingStructures.getActivitiesMapping().length);
	}

	public GeneticMiningMatricesWithHeuristics(Random gen, DependencyRelationBuilder drb, double power) {
		this.power = power;
		generator = gen;
		depRelBuilder = drb;
		activitiesMappingStructures = new ActivitiesMappingStructures(depRelBuilder.getXEventClasses());

		buildMatrices(activitiesMappingStructures.getActivitiesMapping().length);
	}

	private void buildMatrices(int size) {

		causal = new SparseDoubleMatrix2D(size, size);
		buildCausalMatrix();

		start = new SparseDoubleMatrix1D(size);
		buildStartMatrix();

		end = new SparseDoubleMatrix1D(size);
		buildEndMatrix();
	}

	public DoubleMatrix2D getCausalMatrix() {
		return causal;

	}

	public DoubleMatrix1D getEndMatrix() {
		return end;
	}

	public DoubleMatrix1D getStartMatrix() {
		return start;
	}

	public void buildAllMatrices() {
		buildCausalMatrix();
		buildEndMatrix();
		buildStartMatrix();

	}

	public DoubleMatrix2D buildCausalMatrix() {

		double random = 0;
		for (int row = 0; row < causal.rows(); row++) {
			for (int column = 0; column < causal.columns(); column++) {
				random = generator.nextDouble();
				XEventClass rowEvent = activitiesMappingStructures.getActivitiesMapping()[row];
				XEventClass columnEvent = activitiesMappingStructures.getActivitiesMapping()[column];
				if (random < Math.pow(depRelBuilder.getFollowsDependency(rowEvent, columnEvent), power)) {
					causal.set(row, column, 1);
				} else {
					causal.set(row, column, 0);
				}
			}
		}

		return causal;
	}

	public DoubleMatrix1D buildEndMatrix() {

		double random = 0;
		for (int row = 0; row < end.size(); row++) {
			random = generator.nextDouble();
			XEventClass endEvent = activitiesMappingStructures.getActivitiesMapping()[row];
			if (random < Math.pow(depRelBuilder.getEndDependency(endEvent), power)) {
				end.set(row, 1);
			} else {
				end.set(row, 0);
			}
		}
		return end;

	}

	public DoubleMatrix1D buildStartMatrix() {

		double random = 0;
		for (int row = 0; row < start.size(); row++) {
			random = generator.nextDouble();
			XEventClass startEvent = activitiesMappingStructures.getActivitiesMapping()[row];
			if (random < Math.pow(depRelBuilder.getStartDependency(startEvent), power)) {
				start.set(row, 1);
			} else {
				start.set(row, 0);
			}
		}

		return start;

	}

	public ActivitiesMappingStructures getActivitiesMappingStructures() {
		return activitiesMappingStructures;
	}

}
