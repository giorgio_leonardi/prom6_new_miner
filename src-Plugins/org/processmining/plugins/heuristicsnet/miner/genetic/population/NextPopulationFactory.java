package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import java.util.Random;

import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.Crossover;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.Mutation;
import org.processmining.plugins.heuristicsnet.miner.genetic.selection.SelectionMethod;

public class NextPopulationFactory {

	public static BuildPopulation getPopulation(SelectionMethod selectionMethod, Random generator,
			double crossoverRate, double mutationRate, double elitismRate, Crossover crossover, Mutation mutation) {

		BuildPopulation nextPopulation = null;

		nextPopulation = new BuildNextGeneration(selectionMethod, generator, crossoverRate, mutationRate, elitismRate,
				crossover, mutation);

		return nextPopulation;
	}

}
