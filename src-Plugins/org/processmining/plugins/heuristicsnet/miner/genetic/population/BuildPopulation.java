package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.models.heuristics.impl.HNSubSet;

/**
 * 
 * @author Ana Karla Alves de Medeiros
 * 
 */
public interface BuildPopulation {

	/**
	 * Builds a new population of HeuristicsNet objects. The new population has
	 * the same size of the old population.
	 * 
	 * @param oldPopulation
	 *            Array with the HeuristicsNet objects to be replaced in the
	 *            population.
	 * @return Array with new HeuristicsNet objects. The returned population has
	 *         the same size of the old population.
	 */
	public HeuristicsNet[] build(HeuristicsNet[] oldPopulation, HeuristicsNet[]... newPopulations);

	public HeuristicsNet[] build(HeuristicsNet[] population, HNSubSet protectedActivities,
			HeuristicsNet[]... newPopulations);

	public HeuristicsNet[] buildWithNewPS(HeuristicsNet[] population, int newPS);

}
