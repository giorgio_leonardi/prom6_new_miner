package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import java.util.Map;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.info.XLogInfo;
import org.processmining.framework.util.Pair;
import org.processmining.plugins.log.logabstraction.BasicLogRelations;
import org.processmining.plugins.log.logabstraction.LogRelations;

public class DependencyRelationBuilder {
	//TODO - Update documentation

	//private LogRelations logAbstraction = null;
	private Map<XEventClass, Integer> L1L = null; //Length-One Loop
	private Map<Pair<XEventClass, XEventClass>, Integer> L2L = null; //Length-Two Loop
	private Map<Pair<XEventClass, XEventClass>, Integer> follows = null;
	private Map<XEventClass, Integer> start = null;
	private Map<XEventClass, Integer> end = null;
	private XEventClasses events = null;

	public DependencyRelationBuilder(XLogInfo logInfo) {

		LogRelations logAbstraction = new BasicLogRelations(logInfo.getLog());
		events = logInfo.getEventClasses();
		follows = logAbstraction.getDirectFollowsDependencies();
		start = logAbstraction.getStartTraceInfo();
		end = logAbstraction.getEndTraceInfo();
		L2L = logAbstraction.getLengthTwoLoops();
		L1L = logAbstraction.getLengthOneLoops();

	}

	public double getFollowsDependency(XEventClass fromElement, XEventClass toElement) {

		double numerator = 0;
		double denominator = 0;

		if (!fromElement.equals(toElement)) {
			//structures necessary to retrieve the values of the dependencies
			Pair<XEventClass, XEventClass> pairFromTo = new Pair<XEventClass, XEventClass>(fromElement, toElement);
			Pair<XEventClass, XEventClass> pairToFroM = new Pair<XEventClass, XEventClass>(toElement, fromElement);

			//retrieving the values for length-two loops
			int L2LvalueFromTo = (L2L.containsKey(pairFromTo) ? L2L.get(pairFromTo).intValue() : 0);
			if (L2LvalueFromTo > 0) {
				int L2LvalueToFrom = (L2L.containsKey(pairToFroM) ? L2L.get(pairToFroM).intValue() : 0);
				numerator = L2LvalueFromTo + L2LvalueToFrom;
				denominator = numerator;
			} else {//elements are not in a length-two loop
				//retrieving the values for normal follows relations
				int followsValueFromTo = (follows.containsKey(pairFromTo) ? follows.get(pairFromTo).intValue() : 0);
				int followsValueToFrom = (follows.containsKey(pairToFroM) ? follows.get(pairToFroM).intValue() : 0);

				numerator = followsValueFromTo - followsValueToFrom;
				denominator = followsValueFromTo + followsValueToFrom;
			}
		} else {
			Pair<XEventClass, XEventClass> pairFromFrom = new Pair<XEventClass, XEventClass>(fromElement, fromElement);
			numerator = (L1L.containsKey(pairFromFrom) ? L1L.get(pairFromFrom).intValue() : 0);
			denominator = numerator;
		}

		return numerator / (denominator + 1);

	}

	public double getStartDependency(XEventClass event) {
		return getDependency(start, event);

	}

	public double getEndDependency(XEventClass event) {
		return getDependency(end, event);
	}

	private double getDependency(Map<XEventClass, Integer> dependency, XEventClass event) {

		int value = (dependency.containsKey(event) ? dependency.get(event).intValue() : 0);

		return value / (value + 1);

	}

	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("<< Start >>").append("\n");
		sb.append(start).append("\n");
		sb.append("<< End >>").append("\n");
		sb.append(end).append("\n");
		sb.append("<< Follows >>").append("\n");
		sb.append(follows).append("\n");
		sb.append("<< L1L >>").append("\n");
		sb.append(L1L).append("\n");
		sb.append("<< L2L >>").append("\n");
		sb.append(L2L).append("\n");

		return sb.toString();
	}

	public XEventClasses getXEventClasses() {
		// TODO Auto-generated method stub
		return events;
	}

}
