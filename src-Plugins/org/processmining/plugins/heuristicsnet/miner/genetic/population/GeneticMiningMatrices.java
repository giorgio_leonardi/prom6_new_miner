package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import org.processmining.models.heuristics.impl.ActivitiesMappingStructures;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;

public interface GeneticMiningMatrices {
	//TODO - Add documentation
	public DoubleMatrix2D getCausalMatrix();

	public DoubleMatrix1D getEndMatrix();

	public DoubleMatrix1D getStartMatrix();

	public void buildAllMatrices();

	public DoubleMatrix2D buildCausalMatrix();

	public DoubleMatrix1D buildStartMatrix();

	public DoubleMatrix1D buildEndMatrix();

	public ActivitiesMappingStructures getActivitiesMappingStructures();
}