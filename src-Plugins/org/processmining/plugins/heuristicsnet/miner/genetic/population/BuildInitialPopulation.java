package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import java.util.Random;

import org.deckfour.xes.info.XLogInfo;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.models.heuristics.impl.HNSet;
import org.processmining.models.heuristics.impl.HNSubSet;
import org.processmining.models.heuristics.impl.HeuristicsNetImpl;
import org.processmining.plugins.heuristicsnet.miner.genetic.util.MethodsOverHeuristicsNets;

public class BuildInitialPopulation implements BuildPopulation {

	private GeneticMiningMatrices geneticMiningMatrices = null;
	private HeuristicsNet[] population = null;
	private Random generator = null;

	public BuildInitialPopulation(Random gen, XLogInfo log, GeneticMiningMatrices genMining) {
		generator = gen;
		geneticMiningMatrices = genMining;
	}

	/**
	 * Fills in oldPopulation with new individuals.
	 * 
	 * @param oldPopulation
	 *            EnhancedHeuristicsNet[] population to be filled in.
	 * @return Enhanced HeuristicsNet[] population with new individuals. There
	 *         are no duplicated individuals in this population.
	 */

	public HeuristicsNet[] build(HeuristicsNet[] oldPopulation, HeuristicsNet[]... heuristicsNets) {

		int size = 0;
		HNSet singleton = null;

		population = oldPopulation;

		size = population.length;

		for (int i = 0; i < size; i++) {
			//create an individual
			population[i] = new HeuristicsNetImpl(geneticMiningMatrices.getActivitiesMappingStructures());

			//create its input/output sets
			for (int j = 0; j < population[i].size(); j++) {
				population[i].setInputSet(j, buildInputSet(j));
				population[i].setOutputSet(j, buildOutputSet(j));

				if (geneticMiningMatrices.getStartMatrix().get(j) > 0) {
					//because this is the artificial START tasks....
					if (population[i].getStartActivities() == null) {
						HNSubSet startTasks = new HNSubSet();
						startTasks.add(j);
						population[i].setStartActivities(startTasks);

					} else {
						population[i].getStartActivities().add(j);
					}
					singleton = new HNSet();
					singleton.add(population[i].getAllElementsOutputSet(j));
					population[i].setOutputSet(j, singleton);
				}

				if (geneticMiningMatrices.getEndMatrix().get(j) > 0) {
					//because this is the artificial END tasks....
					if (population[i].getEndActivities() == null) {
						HNSubSet endTasks = new HNSubSet();
						endTasks.add(j);
						population[i].setEndActivities(endTasks);
					} else {
						population[i].getEndActivities().add(j);
					}
					singleton = new HNSet();
					singleton.add(population[i].getAllElementsInputSet(j));
					population[i].setInputSet(j, singleton);
				}

			}

			//generate new matrices for next individual
			geneticMiningMatrices.buildAllMatrices();
		}

		return population;
	}

	private HNSet buildInputSet(int index) {

		return MethodsOverHeuristicsNets.buildHNSet(geneticMiningMatrices.getStartMatrix().get(index),
				geneticMiningMatrices.getCausalMatrix().viewColumn(index), generator);
	}

	private HNSet buildOutputSet(int index) {
		return MethodsOverHeuristicsNets.buildHNSet(geneticMiningMatrices.getEndMatrix().get(index),
				geneticMiningMatrices.getCausalMatrix().viewRow(index), generator);
	}

	//this should not be implemented here
	public HeuristicsNet[] build(HeuristicsNet[] population, HNSubSet protectedActivities,
			HeuristicsNet[]... newPopulations) {
		// TODO Auto-generated method stub
		return null;
	}

	public HeuristicsNet[] buildWithNewPS(HeuristicsNet[] population, int newPS) {
		// TODO Auto-generated method stub
		return null;
	}

}
