package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import java.util.Random;

import org.deckfour.xes.info.XLogInfo;

public class InitialPopulationFactory {
	//TODO Add documentation
	//TODO Implement missing classes

	public InitialPopulationFactory() {
	}

	/**
	 * The initial population types are:
	 * <p>
	 * 1) <b>No Duplicate Activities:</b> Use heuristics to set the amount of
	 * duplicates per arcs among the tasks. Every event in the log has at most
	 * one activity in the HeuristicsNet. The arcs are set based on the causal
	 * relation.
	 * </p>
	 * <p>
	 * 2) <b>Causal Heuristics (Duplicates+Arcs):</b> Use heuristics to set the
	 * amount of duplicates per task and the arcs among the tasks. The amount of
	 * duplicates is set based on the causal relation. The arcs are set based on
	 * the follows relation.
	 * </p>
	 * <p>
	 * 3) <b>Causal Heuristics (Duplicates):</b> Uses the <i>causal</i> relation
	 * to set the amount of duplicates per task, but the arcs are randomly set.
	 * </p>
	 * <p>
	 * 4) <b>Follows Heuristics (Duplicates+Arcs):</b> Uses the <i>follows</i>
	 * relation to set the amount of duplicates per task and the arcs among the
	 * tasks.
	 * </p>
	 * 
	 * @return String[] With the supported heuristics to build the initial
	 *         population.
	 */
	public static String[] getInitialPopulationTypes() {
		return new String[] { "No Duplicate Activities", "Causal Heuristics (Duplicates+Arcs)",
				"Causal Heuristics (Duplicates)", "Follows Heuristics (Duplicates+Arcs)" };
	}

	/**
	 * This method provides an object to build an initial population.
	 * 
	 * @param indexPopulationType
	 *            int One of the types returned by method
	 *            <i>getInitialPopulationTypes()</i>
	 * @param gen
	 *            Random The generator to be used.
	 * @param log
	 *            LogReader The log to be used.
	 * @param power
	 *            double The power value to be used.
	 * @return BuildPopulation
	 */
	public static BuildPopulation getPopulation(int indexPopulationType, Random gen, XLogInfo log, double power) {
		BuildPopulation object = null;
		GeneticMiningMatrices genMinMatrices = null;

		switch (indexPopulationType) {
			case 0 :
				genMinMatrices = new GeneticMiningMatricesWithHeuristics(gen, log, power);
				object = new BuildInitialPopulation(gen, log, genMinMatrices);
				break;

			case 1 :
				genMinMatrices = new DAGeneticMiningMatricesWithHeuristicsActivitiesArcs(gen, log, power);
				object = new BuildInitialPopulation(gen, log, genMinMatrices);
				break;

			case 2 :
				genMinMatrices = new DAGeneticMiningMatricesWithHeuristics(gen, log, power);
				object = new BuildInitialPopulation(gen, log, genMinMatrices);
				break;

			case 3 :
				genMinMatrices = new DAGeneticMiningMatricesWithMinimalHeuristics(gen, log, power);
				object = new BuildInitialPopulation(gen, log, genMinMatrices);
				break;
		}

		return object;
	}

	/**
	 * This method provides an object to build an initial population.
	 * 
	 * @param indexPopulationType
	 *            int One of the types returned by method
	 *            <i>getInitialPopulationTypes()</i>
	 * @param gen
	 *            Random The generator to be used.
	 * @param log
	 *            LogReader The log to be used.
	 * @param power
	 *            double The power value to be used.
	 * @return BuildPopulation
	 */
	public static BuildPopulation getPopulation(int indexPopulationType, Random gen, XLogInfo log,
			DependencyRelationBuilder drb, double power) {
		BuildPopulation object = null;
		GeneticMiningMatrices genMinMatrices = null;

		genMinMatrices = new GeneticMiningMatricesWithHeuristics(gen, drb, power);
		object = new BuildInitialPopulation(gen, log, genMinMatrices);

		return object;
	}

}
