package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.info.XLogInfo;
import org.processmining.framework.util.Pair;
import org.processmining.models.heuristics.impl.ActivitiesMappingStructures;
import org.processmining.plugins.heuristicsnet.miner.genetic.GeneticMinerConstants;
import org.processmining.plugins.log.logabstraction.BasicLogRelations;
import org.processmining.plugins.log.logabstraction.LogRelations;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix1D;

/**
 * 
 * @author Ana Karla Alves de Medeiros
 * 
 */
public abstract class DAGeneticMiningMatrices implements GeneticMiningMatrices {

	protected static final int MINIMUM_NUMBER_ACTIVITIES_PER_EVENT = 1;

	//TODO - Review and update/add documentation

	protected double power = GeneticMinerConstants.POWER;

	//These structures are derived from the log and do not consider duplicate activities
	protected Map<Pair<XEventClass, XEventClass>, Double> basicCausal;
	protected Map<Pair<XEventClass, XEventClass>, Integer> basicFollows;

	protected DependencyRelationBuilder depRelBuilder;

	//These matrices do consider duplicate tasks. They are derived from the
	//basic matrices.
	protected Random generator;

	protected DoubleMatrix2D causal;
	protected DoubleMatrix1D start;
	protected DoubleMatrix1D end;

	protected ActivitiesMappingStructures activitiesMappingStructures;

	public DAGeneticMiningMatrices(Random gen, XLogInfo logReader, double power) {
		this.power = power;
		generator = gen;
		depRelBuilder = new DependencyRelationBuilder(logReader);

		//building the basic ordering relation matrices
		buildBasicOrderingRelations(logReader);
		//now we need to calculate how many duplicates per task
		activitiesMappingStructures = new ActivitiesMappingStructures(logReader.getEventClasses(),
				setDuplicatesMapping(logReader.getEventClasses()));

		//build here the matrices that take duplicate activities into account!
		buildMatrices();

	}

	/**
	 * These relations are going to be used to set the number of duplicate
	 * activities per event in the log
	 */
	private void buildBasicOrderingRelations(XLogInfo logReader) {
		LogRelations logRelations = new BasicLogRelations(logReader.getLog());
		basicFollows = logRelations.getDirectFollowsDependencies();
		basicCausal = logRelations.getCausalDependencies();

	}

	protected Map<XEventClass, Integer> setDuplicatesMapping(XEventClasses eventClasses) {

		Map<XEventClass, Integer> inCausalRelations = new HashMap<XEventClass, Integer>();
		Map<XEventClass, Integer> outCausalRelations = new HashMap<XEventClass, Integer>();
		Map<XEventClass, Integer> numberDuplicatesPerTask = new HashMap<XEventClass, Integer>();

		//Checking how many duplicate activities should be created for the events
		//in the log. The used heuristics is as follows:
		//For every event, the #activities = min(#inputCausalRelations, #outputCausalRelations) for this event

		for (Pair<XEventClass, XEventClass> pairEventsCausalRelation : basicCausal.keySet()) {

			XEventClass firstElement = pairEventsCausalRelation.getFirst();
			XEventClass secondElement = pairEventsCausalRelation.getSecond();

			int newNumberInCausalRelations = inCausalRelations.containsKey(secondElement) ? inCausalRelations
					.get(secondElement) + 1 : 1;
			inCausalRelations.put(secondElement, newNumberInCausalRelations); //not that the new number has already been increased by one!

			int newNumberOutCausalRelations = outCausalRelations.containsKey(firstElement) ? outCausalRelations
					.get(firstElement) + 1 : 1;
			outCausalRelations.put(firstElement, newNumberOutCausalRelations); //not that the new number has already been increased by one!

		}

		//Creating the actual duplicate activities
		Iterator<XEventClass> iterator = eventClasses.getClasses().iterator();
		while (iterator.hasNext()) {
			XEventClass event = iterator.next();
			int numberInCausalRelationsForThisEvent = inCausalRelations.containsKey(event) ? inCausalRelations.get(
					event).intValue() : MINIMUM_NUMBER_ACTIVITIES_PER_EVENT;
			int numberOutCausalRelationsForThisEvent = outCausalRelations.containsKey(event) ? outCausalRelations.get(
					event).intValue() : MINIMUM_NUMBER_ACTIVITIES_PER_EVENT;
			int numberOfDuplicateActivitiesForThisEvent = Math.max(numberInCausalRelationsForThisEvent,
					numberOutCausalRelationsForThisEvent);
			numberDuplicatesPerTask.put(event, new Integer(numberOfDuplicateActivitiesForThisEvent));
		}

		return numberDuplicatesPerTask;
	}

	private void buildMatrices() {

		buildCausalMatrix();

		buildStartMatrix();

		buildEndMatrix();
	}

	public DoubleMatrix2D getCausalMatrix() {
		return causal;

	}

	public DoubleMatrix1D getEndMatrix() {
		return end;
	}

	public DoubleMatrix1D getStartMatrix() {
		return start;
	}

	public void buildAllMatrices() {
		buildCausalMatrix();
		buildStartMatrix();
		buildEndMatrix();
	}

	public DoubleMatrix1D buildEndMatrix() {
		double random = 0;

		end = new SparseDoubleMatrix1D(activitiesMappingStructures.getActivitiesMapping().length);

		for (int row = 0; row < end.size(); row++) {
			random = generator.nextDouble();
			XEventClass endEvent = activitiesMappingStructures.getActivitiesMapping()[row];
			if (random < Math.pow(depRelBuilder.getEndDependency(endEvent), power)) {
				end.set(row, 1);
			} else {
				end.set(row, 0);
			}
		}
		return end;
	}

	public DoubleMatrix1D buildStartMatrix() {
		double random = 0;

		start = new SparseDoubleMatrix1D(activitiesMappingStructures.getActivitiesMapping().length);

		for (int row = 0; row < start.size(); row++) {
			random = generator.nextDouble();
			XEventClass startEvent = activitiesMappingStructures.getActivitiesMapping()[row];
			if (random < Math.pow(depRelBuilder.getStartDependency(startEvent), power)) {
				start.set(row, 1);
			} else {
				start.set(row, 0);
			}
		}

		return start;

	}

	public ActivitiesMappingStructures getActivitiesMappingStructures() {
		return activitiesMappingStructures;
	}

}
