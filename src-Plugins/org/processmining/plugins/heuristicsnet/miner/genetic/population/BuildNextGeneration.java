package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import java.util.Arrays;
import java.util.Random;

import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.models.heuristics.impl.HNSubSet;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.Crossover;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.Mutation;
import org.processmining.plugins.heuristicsnet.miner.genetic.selection.SelectionMethod;

public class BuildNextGeneration implements BuildPopulation {

	//TODO - Review and update documentation

	private double elitismRate = 0;
	private SelectionMethod selectionMethod = null;
	private Random generator = null;
	private double crossoverRate = 0;
	private double mutationRate = 0;
	private Crossover crossover = null;
	private Mutation mutation = null;

	public BuildNextGeneration(SelectionMethod selectionMethod, Random generator, double crossoverRate,
			double mutationRate, double elitismRate, Crossover crossover, Mutation mutation) {

		this.selectionMethod = selectionMethod;
		this.generator = generator;

		this.elitismRate = elitismRate;
		this.crossoverRate = crossoverRate;
		this.mutationRate = mutationRate;

		this.crossover = crossover;
		this.mutation = mutation;

	}

	/*
	 * Creates a new population based on the old population. The genetic
	 * operator (crossover and mutation are used to build the next generation. A
	 * percentage of the best individuals (elite) of the old population is
	 * directly copied to the new population.
	 * 
	 * @param oldPopulation <b>increasingly</b> sorted array.
	 * 
	 * @return new population.
	 */

	public HeuristicsNet[] build(HeuristicsNet[] oldPopulation, HeuristicsNet[]... heuristicsNets) {
		double numElite = 0;
		double random = 0;
		int indexNewPopulation = 0;
		int numOffsprings = 2;
		HeuristicsNet[] newPopulation = null;
		HeuristicsNet[] selectedParents = null;
		HeuristicsNet[] offsprings = null;

		//copying the best individuals to next generation...
		numElite = oldPopulation.length * elitismRate;
		newPopulation = new HeuristicsNet[oldPopulation.length];

		//Ordering the individuals in the old population
		//in order to get the best one.
		//NOTE: we assume that the array oldPopulation is increasingly sorted!!!
		Arrays.sort(oldPopulation);

		indexNewPopulation = 0;
		// here the elitism is applied
		for (int indexOldPopulation = (oldPopulation.length - 1); indexNewPopulation < numElite; indexNewPopulation++, indexOldPopulation--) {
			newPopulation[indexNewPopulation] = oldPopulation[indexOldPopulation].copy();
		}

		while (indexNewPopulation < newPopulation.length) {

			//applying tournament selection...
			selectedParents = new HeuristicsNet[numOffsprings];
			for (int i = 0; i < selectedParents.length; i++) {
				selectedParents[i] = selectionMethod.select(oldPopulation)[0];
			}
			//applying crossover operation...
			random = generator.nextDouble();
			if (random < crossoverRate) {
				offsprings = crossover.doCrossover(selectedParents);

			} else {
				//no crossover
				offsprings = selectedParents;
			}

			//applying mutation operation for offsprings...
			if (mutationRate > 0) {
				for (int i = 0; i < offsprings.length; i++) {
					mutation.doMutation(offsprings[i]);
				}
			}

			for (int i = 0; (i < offsprings.length) && (indexNewPopulation < newPopulation.length); i++) {
				newPopulation[indexNewPopulation] = offsprings[i];
				indexNewPopulation++;
			}

		}

		return newPopulation;
	}

	// for self-adaptive DGM
	// build the next generation with a new population size
	// the buildWithNewPS is called when there is a increase on the PS ( for both get from local and get from others)
	public HeuristicsNet[] buildWithNewPS(HeuristicsNet[] oldPopulation, int newPS) {

		int indexNewPopulation = 0;
		//int extraPop = newPS - oldPopulation.length;
		HeuristicsNet[] selectedParents = null;
		HeuristicsNet[] offsprings = null;
		int numOffsprings = 2;
		double random = 0;

		HeuristicsNet[] newPopulation = new HeuristicsNet[newPS];
		//Arrays.sort(oldPopulation);			 
		//System.out.println("buildWithNewPS");
		// copy all the ind. from the old to the new POP
		for (int indexOldPopulation = 0; indexOldPopulation < oldPopulation.length; indexNewPopulation++, indexOldPopulation++) {
			newPopulation[indexNewPopulation] = oldPopulation[indexOldPopulation].copy();
		}

		// apply genetic operators on the OLD pop get the extra population 
		while (indexNewPopulation < newPS) {

			//applying tournament selection...
			selectedParents = new HeuristicsNet[numOffsprings];
			for (int i = 0; i < selectedParents.length; i++) {
				selectedParents[i] = selectionMethod.select(oldPopulation)[0];
			}
			//applying crossover operation...
			random = generator.nextDouble();
			if (random < crossoverRate) {
				offsprings = crossover.doCrossover(selectedParents);

			} else {
				//no crossover
				offsprings = selectedParents;
			}

			//applying mutation operation for offsprings...
			if (mutationRate > 0) {
				for (int i = 0; i < offsprings.length; i++) {
					mutation.doMutation(offsprings[i]);
				}
			}
			// add the offsprings to the new Pop
			for (int i = 0; (i < offsprings.length) && (indexNewPopulation < newPS); i++) {
				newPopulation[indexNewPopulation] = offsprings[i];
				indexNewPopulation++;
			}
		}// end of while of applying genetic operators		

		return newPopulation;
	}

	public HeuristicsNet[] build(HeuristicsNet[] oldPopulation, HNSubSet protectedActivities,
			HeuristicsNet[]... populations) {
		double numElite = 0;
		double random = 0;
		int indexNewPopulation = 0;
		int numOffsprings = 2;
		HeuristicsNet[] newPopulation = null;
		HeuristicsNet[] selectedParents = null;
		HeuristicsNet[] offsprings = null;

		//copying the best individuals to next generation...
		numElite = oldPopulation.length * elitismRate;
		newPopulation = new HeuristicsNet[oldPopulation.length];

		//Ordering the individuals in the old population
		//in order to get the best one.
		//NOTE: we assume that the array oldPopulation is increasingly sorted!!!
		Arrays.sort(oldPopulation);

		indexNewPopulation = 0;
		// here the elitism is applied
		for (int indexOldPopulation = (oldPopulation.length - 1); indexNewPopulation < numElite; indexNewPopulation++, indexOldPopulation--) {
			newPopulation[indexNewPopulation] = oldPopulation[indexOldPopulation].copy();
		}

		while (indexNewPopulation < newPopulation.length) {

			//applying tournament selection...
			selectedParents = new HeuristicsNet[numOffsprings];
			for (int i = 0; i < selectedParents.length; i++) {
				selectedParents[i] = selectionMethod.select(oldPopulation)[0];
			}
			//applying crossover operation...
			random = generator.nextDouble();
			if (random < crossoverRate) {
				offsprings = crossover.doCrossover(selectedParents, protectedActivities);

			} else {
				//no crossover
				offsprings = selectedParents;
			}

			//applying mutation operation for offsprings...
			if (mutationRate > 0) {
				for (int i = 0; i < offsprings.length; i++) {
					mutation.doMutation(offsprings[i], protectedActivities);
				}
			}

			for (int i = 0; (i < offsprings.length) && (indexNewPopulation < newPopulation.length); i++) {
				newPopulation[indexNewPopulation] = offsprings[i];
				indexNewPopulation++;
			}

		}

		return newPopulation;
	}
}
