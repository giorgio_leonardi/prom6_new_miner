package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.info.XLogInfo;
import org.processmining.framework.util.Pair;
import org.processmining.models.heuristics.impl.HNSubSet;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;

/**
 * <p>
 * This class uses the causal matrices to set the maximum number of duplicated
 * tasks that an individual (HeuristicsNet) can have. Basically, the number of
 * duplicates of a task <i>t</i> is the minimum between the number of tasks that
 * causally follow <i>t</i> and the number of tasks that are causally followed
 * by <i>t</i>.
 * </p>
 * 
 * <p>
 * The arcs are set in the following way: Whenever a task <i>t</i> causally
 * follows a tasks <i>t'</i>, there is a 50% probability that the individual has
 * an arc from <i>t</i> to <i>t'</i>.
 * </p>
 * 
 * @author Ana Karla A. de Medeiros
 */

public class DAGeneticMiningMatricesWithHeuristics extends DAGeneticMiningMatrices {
	//TODO - Review/Update/Add documentation

	public DAGeneticMiningMatricesWithHeuristics(Random gen, XLogInfo logReader, double power) {
		super(gen, logReader, power);
	}

	public DoubleMatrix2D buildCausalMatrix() {

		//creating the causal matrix that considers duplicate tasks
		int causalMatrixDimensionSize = activitiesMappingStructures.getActivitiesMapping().length;
		causal = new SparseDoubleMatrix2D(causalMatrixDimensionSize, causalMatrixDimensionSize);

		//getting the XEventClass events to build the causal matrix for
		Set<XEventClass> setOfEvents = activitiesMappingStructures.getReverseActivitiesMapping().keySet();

		//for every combination of events, decide if a causal relation should
		//be set in the matrix "causal"
		//Note that, if there is a basic causality relation between two
		//events, then there is a 50% probability of setting a causal 
		//relation for these events
		for (XEventClass fromEvent : setOfEvents) {
			for (XEventClass toEvent : setOfEvents) {
				Pair<XEventClass, XEventClass> pairFromTo = new Pair<XEventClass, XEventClass>(fromEvent, toEvent);
				double basicCausalValue = basicFollows.containsKey(pairFromTo) ? basicFollows.get(pairFromTo) : 0;
				if (basicCausalValue > 0) {
					if (generator.nextBoolean()) {
						//randomly choose a duplicate from row to column
						//and add an arc
						Map<XEventClass, HNSubSet> reverseActivitiesMapping = activitiesMappingStructures
								.getReverseActivitiesMapping();

						//randomly choosing activities to set the causality relations
						int duplicateTask_row = reverseActivitiesMapping.get(fromEvent).get(
								generator.nextInt(reverseActivitiesMapping.get(fromEvent).size()));
						int duplicateTask_column = reverseActivitiesMapping.get(toEvent).get(
								generator.nextInt(reverseActivitiesMapping.get(toEvent).size()));
						//setting the actual causality relation between the randomly chosen
						//activities for the given duplicates
						causal.set(duplicateTask_row, duplicateTask_column, 1);
					}
				}

			}
		}

		return causal;

	}
}
