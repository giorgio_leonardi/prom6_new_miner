package org.processmining.plugins.heuristicsnet.miner.genetic.population;

import java.util.Arrays;
import java.util.Random;

import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.models.heuristics.impl.HNSubSet;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.Crossover;
import org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations.Mutation;
import org.processmining.plugins.heuristicsnet.miner.genetic.selection.SelectionMethod;

public class BuildNextGenerationForForcedCrossover implements BuildPopulation {
	//TODO - Review and update documentation

	private double elitismRate = 0;
	private SelectionMethod selectionMethod = null;
	private Random generator = null;
	private double crossoverRate = 0;
	private double mutationRate = 0;
	private Crossover crossover = null;
	private Mutation mutation = null;

	public BuildNextGenerationForForcedCrossover(SelectionMethod selectionMethod, Random generator,
			double crossoverRate, double mutationRate, double elitismRate, Crossover crossover, Mutation mutation) {

		this.selectionMethod = selectionMethod;
		this.generator = generator;

		this.elitismRate = elitismRate;
		this.crossoverRate = crossoverRate;
		this.mutationRate = mutationRate;

		this.crossover = crossover;
		this.mutation = mutation;
	}

	/*
	 * Creates a new population based on the old population. The genetic
	 * operator (crossover and mutation are used to build the next generation. A
	 * percentage of the best individuals (elite) of the old population is
	 * directly copied to the new population.
	 * 
	 * @param oldPopulation <b>increasingly</b> sorted array.
	 * 
	 * @return new population.
	 */

	public HeuristicsNet[] build(HeuristicsNet[] oldPopulation, HeuristicsNet[]... pop) {
		double numElite = 0;
		double random = 0;
		int indexNewPopulation = 0;
		int indexReceivedPopulation = 0;
		int numOffsprings = 2;
		HeuristicsNet[] newPopulation = null;
		HeuristicsNet[] selectedParents = null;
		HeuristicsNet[] offsprings = null;

		if (pop == null) {
			return oldPopulation;
		}

		HeuristicsNet[] receivedPopulation = pop[0];

		newPopulation = new HeuristicsNet[oldPopulation.length];

		// do the forced crossover
		while ((indexNewPopulation < newPopulation.length) && (indexReceivedPopulation < receivedPopulation.length)) {
			int poz = generator.nextInt(oldPopulation.length);

			//applying selection - one parent from received population and another from old population (randomly selected)
			selectedParents = new HeuristicsNet[numOffsprings];
			selectedParents[0] = receivedPopulation[indexReceivedPopulation];
			selectedParents[1] = oldPopulation[poz];

			offsprings = crossover.doCrossover(selectedParents);

			for (int i = 0; (i < offsprings.length) && (indexNewPopulation < newPopulation.length); i++) {
				newPopulation[indexNewPopulation] = offsprings[i];
				indexNewPopulation++;
			}

			indexReceivedPopulation++;
		}

		// is there are still space left in the new population
		if (indexNewPopulation < newPopulation.length) {
			//copying the best individuals to next generation...
			numElite = oldPopulation.length * elitismRate;

			//Ordering the individuals in the old population
			//in order to get the best one.
			//NOTE: we assume that the array oldPopulation is increasingly sorted!!!
			Arrays.sort(oldPopulation);

			// here the elitism is applied
			for (int indexOldPopulation = (oldPopulation.length - 1); (indexNewPopulation < newPopulation.length)
					&& (numElite >= 1); indexNewPopulation++, indexOldPopulation--) {
				newPopulation[indexNewPopulation] = oldPopulation[indexOldPopulation].copy();
				numElite--;
			}

			while (indexNewPopulation < newPopulation.length) {

				//applying tournament selection...
				selectedParents = new HeuristicsNet[numOffsprings];
				for (int i = 0; i < selectedParents.length; i++) {
					selectedParents[i] = selectionMethod.select(oldPopulation)[0];
				}
				//applying crossover operation...
				random = generator.nextDouble();
				if (random < crossoverRate) {
					offsprings = crossover.doCrossover(selectedParents);

				} else {
					//no crossover
					offsprings = selectedParents;
				}

				//applying mutation operation for offsprings...
				if (mutationRate > 0) {
					for (int i = 0; i < offsprings.length; i++) {
						mutation.doMutation(offsprings[i]);
					}
				}

				for (int i = 0; (i < offsprings.length) && (indexNewPopulation < newPopulation.length); i++) {
					newPopulation[indexNewPopulation] = offsprings[i];
					indexNewPopulation++;
				}

			}
		}

		return newPopulation;
	}

	public HeuristicsNet[] build(HeuristicsNet[] oldPopulation, HNSubSet protectedActivities, HeuristicsNet[]... pop) {
		// TODO Auto-generated method stub
		double numElite = 0;
		double random = 0;
		int indexNewPopulation = 0;
		int indexReceivedPopulation = 0;
		int numOffsprings = 2;
		HeuristicsNet[] newPopulation = null;
		HeuristicsNet[] selectedParents = null;
		HeuristicsNet[] offsprings = null;

		if (pop == null) {
			return null;
		}

		HeuristicsNet[] receivedPopulation = pop[0];

		newPopulation = new HeuristicsNet[oldPopulation.length];

		// do the forced crossover
		while ((indexNewPopulation < newPopulation.length) && (indexReceivedPopulation < receivedPopulation.length)) {
			random = generator.nextDouble();

			//applying selection - one parent from received population and another from old population (randomly selected)
			selectedParents = new HeuristicsNet[numOffsprings];
			selectedParents[0] = receivedPopulation[indexReceivedPopulation];
			selectedParents[1] = oldPopulation[(int) random];

			offsprings = crossover.doCrossover(selectedParents, protectedActivities);

			for (int i = 0; (i < offsprings.length) && (indexNewPopulation < newPopulation.length); i++) {
				newPopulation[indexNewPopulation] = offsprings[i];
				indexNewPopulation++;
			}

			indexReceivedPopulation++;
		}

		// is there are still space left in the new population
		if (indexNewPopulation < newPopulation.length) {

			//copying the best individuals to next generation...
			numElite = oldPopulation.length * elitismRate;

			//Ordering the individuals in the old population
			//in order to get the best one.
			//NOTE: we assume that the array oldPopulation is increasingly sorted!!!
			Arrays.sort(oldPopulation);

			// here the elitism is applied
			for (int indexOldPopulation = (oldPopulation.length - 1); (numElite <= 1)
					&& (indexNewPopulation < newPopulation.length); indexNewPopulation++, indexOldPopulation--) //???									  
			{
				newPopulation[indexNewPopulation] = oldPopulation[indexOldPopulation].copy();
			}

			while (indexNewPopulation < newPopulation.length) {

				//applying tournament selection...
				selectedParents = new HeuristicsNet[numOffsprings];
				for (int i = 0; i < selectedParents.length; i++) {
					selectedParents[i] = selectionMethod.select(oldPopulation)[0];
				}
				//applying crossover operation...
				random = generator.nextDouble();
				if (random < crossoverRate) {
					offsprings = crossover.doCrossover(selectedParents, protectedActivities);

				} else {
					//no crossover
					offsprings = selectedParents;
				}

				//applying mutation operation for offsprings...
				if (mutationRate > 0) {
					for (int i = 0; i < offsprings.length; i++) {
						mutation.doMutation(offsprings[i], protectedActivities);
					}
				}

				for (int i = 0; (i < offsprings.length) && (indexNewPopulation < newPopulation.length); i++) {
					newPopulation[indexNewPopulation] = offsprings[i];
					indexNewPopulation++;
				}

			}
		}

		return newPopulation;
	}

	public HeuristicsNet[] buildWithNewPS(HeuristicsNet[] population, int newPS) {
		// TODO Auto-generated method stub
		return null;
	}
}
