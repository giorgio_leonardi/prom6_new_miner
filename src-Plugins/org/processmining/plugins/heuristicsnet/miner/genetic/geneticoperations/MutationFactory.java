package org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations;

import java.util.Random;

/**
 * 
 * @author Ana Karla Alves de Medeiros
 * 
 */
public class MutationFactory {
	//TODO - Add documentation

	public MutationFactory() {
	}

	public static String[] getAllMutationTypes() {
		return new String[] { "Enhanced" };
	}

	public static Mutation getMutation(int indexMutationType, Random generator, double mutationRate) {
		Mutation object = null;
		switch (indexMutationType) {
			case 0 :
				object = new EnhancedMutation(generator, mutationRate);
				break;
		}
		return object;
	}

}
