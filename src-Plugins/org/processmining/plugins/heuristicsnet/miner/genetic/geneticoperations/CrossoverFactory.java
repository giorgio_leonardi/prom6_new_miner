package org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations;

import java.util.Random;

/**
 * 
 * @author Ana Karla Alves de Medeiros
 * 
 */

public class CrossoverFactory {
	//TODO - Add documentation
	//TODO - Note: only the crossover and mutation over individuals
	//with duplicates have been migrated.

	public CrossoverFactory() {
	}

	public static String[] getAllCrossoverTypes() {
		return new String[] { "Enhanced" };
	}

	public static Crossover getCrossover(int indexCrossoverType, Random generator) {
		Crossover object = null;
		switch (indexCrossoverType) {
			case 0 :
				object = new EnhancedCrossover(generator); //old "Duplicates Enhanced"
				break;
		}
		return object;
	}
}
