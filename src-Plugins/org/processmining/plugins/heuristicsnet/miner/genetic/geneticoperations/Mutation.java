package org.processmining.plugins.heuristicsnet.miner.genetic.geneticoperations;

import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.models.heuristics.impl.HNSubSet;

/**
 * 
 * @author Ana Karla Alves de Medeiros
 * 
 */
public interface Mutation {
	public HeuristicsNet doMutation(HeuristicsNet ind);

	/**
	 * Added by Carmen Bratosin
	 * 
	 * The idea is that there are activities that for some reason we don't want
	 * to change there input-output sets
	 * 
	 * @param ind
	 * @param protectedActivities
	 * @return
	 */
	public HeuristicsNet doMutation(HeuristicsNet ind, HNSubSet protectedActivities);
}
