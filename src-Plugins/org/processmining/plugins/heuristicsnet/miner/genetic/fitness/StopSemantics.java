package org.processmining.plugins.heuristicsnet.miner.genetic.fitness;

import java.util.Iterator;
import java.util.Random;

import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.model.XTrace;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.models.heuristics.impl.StopSemanticsParser;

/**
 * Calculates the fitness of <code>HeuristicsNet</code> objects in a population
 * based on a weighed sum of (i) the ratio of parsed activities, (ii) the ratio
 * of completed traces and (iii) the ratio of properly completed traces.
 * <p>
 * Additional methods are provided to set and retrieve the values of the three
 * different weights used for every part of the sum above.
 * 
 * @author Ana Karla Alves de Medeiros
 * 
 */
public class StopSemantics implements Fitness {

	private XLogInfo logInfo = null;
	private double[] numParsedWMEs = null; //WME = Workflow Model Element
	private double[] numCompletedPIs = null; //PI = process instance
	private double[] numProperlyCompletedPIs = null;
	private HeuristicsNet[] population = null;
	private StopSemanticsParser[] parser = null;
	private MapIdenticalHeuristicsNets mapping = null;

	private Random generator = null;
	private double numParsedConstant = 0.20; //weight for the parsed activities
	private double numCompletedConstant = 0.30; //weight for the traces that could be completed during the log replay
	private double numProperlyCompletedConstant = 0.50; //weight for the traces that could be **properly** completed during the log replay

	/**
	 * Constructs a new stop semantics fitness for the given log. All fitness
	 * values calculated by this object for populations of
	 * <code>HeuristicsNet</code> will be based on this log.
	 * 
	 * @param logInfo
	 *            information about the log
	 */
	public StopSemantics(XLogInfo logInfo) {
		this.logInfo = logInfo;
		generator = new Random(Long.MAX_VALUE);
	}

	/**
	 * Calculates the stop semantics fitness of every <code>HeuristicsNet</code>
	 * in the population
	 * 
	 * @param population
	 *            array containing the <code>HeuristicsNet</code> for which a
	 *            fitness value will be calculated
	 */
	public HeuristicsNet[] calculate(HeuristicsNet[] population) {

		this.population = population;
		mapping = new MapIdenticalHeuristicsNets(this.population);

		createParser();
		ProperCompletion.resetDuplicatesActualFiringAndArcUsage(this.population);
		createFitnessVariables();
		calculatePartialFitness();

		return assignFitness();
	}

	/**
	 * Returns the value used to weigh the impact of the ratio of parsed
	 * activities during the log replay
	 * 
	 * @return double value of this weight
	 */
	public double getNumParsedConstant() {
		return numParsedConstant;
	}

	/**
	 * Returns the value used to weigh the impact of the ratio of completed
	 * traces during the log replay
	 * 
	 * @return double value of this weight
	 */
	public double getNumCompletedConstant() {
		return numCompletedConstant;
	}

	/**
	 * Returns the value used to weigh the impact of the ratio of properly
	 * completed traces during the log replay
	 * 
	 * @return double value of this weight
	 */
	public double getNumProperlyCompletedConstant() {
		return numProperlyCompletedConstant;
	}

	/**
	 * Sets the value used to weigh the impact of the ratio of parsed activities
	 * during the log replay
	 * 
	 * @param newValue
	 *            new double value of this weight
	 */
	public void setNumParsedConstant(double newValue) {
		numParsedConstant = newValue;
	}

	/**
	 * Sets the value used to weigh the impact of the ratio of completed traces
	 * during the log replay
	 * 
	 * @param newValue
	 *            new double value of this weight
	 */
	public void setNumCompletedConstant(double newValue) {
		numCompletedConstant = newValue;
	}

	/**
	 * Sets the value used to weigh the impact of the ratio of properly
	 * completed traces during the log replay
	 * 
	 * @param newValue
	 *            new double value of this weight
	 */
	public void setNumProperlyCompletedConstant(double newValue) {
		numProperlyCompletedConstant = newValue;
	}

	private HeuristicsNet[] assignFitness() {

		double fitness = 0;
		double parsed = 0;
		double completed = 0;
		double properlyCompleted = 0;
		int indexIdenticalIndividual = 0;

		for (int i = 0; i < population.length; i++) {

			indexIdenticalIndividual = mapping.getIndenticalHeuristicsNet(i);

			if (indexIdenticalIndividual < 0) {

				parsed = (numParsedWMEs[i] / logInfo.getNumberOfEvents());
				completed = (numCompletedPIs[i] / logInfo.getNumberOfTraces());
				properlyCompleted = (numProperlyCompletedPIs[i] / logInfo.getNumberOfTraces());
				fitness = (numParsedConstant * parsed) + (numCompletedConstant * completed)
						+ (numProperlyCompletedConstant * properlyCompleted);

				population[i].setFitness(fitness);

			} else {

				population[i].setFitness(population[indexIdenticalIndividual].getFitness());
				population[i].setActivitiesActualFiring(population[indexIdenticalIndividual]
						.getActivitiesActualFiring());
				population[i].setArcUsage(population[indexIdenticalIndividual].getArcUsage());

			}

		}

		return population;

	}

	private void calculatePartialFitness() {

		XTrace pi = null;
		int numSimilarPIs = 0;

		Iterator<XTrace> logReaderInstanceIterator = logInfo.getLog().iterator();
		while (logReaderInstanceIterator.hasNext()) {
			pi = logReaderInstanceIterator.next();
			//TODO - Get the correct number of grouped traces!
			//numSimilarPIs = MethodsForWorkflowLogDataStructures.getNumberSimilarProcessInstances(pi);
			numSimilarPIs = 1;
			for (int i = 0; i < population.length; i++) {
				if (mapping.getIndenticalHeuristicsNet(i) < 0) { //we need to compute the partial fitness
					parser[i].parse(pi);
					//partial assignment to variables
					numParsedWMEs[i] += (parser[i].getNumParsedElements() * numSimilarPIs);
					if (parser[i].getCompleted()) {
						numCompletedPIs[i] += numSimilarPIs;
					}
					if (parser[i].getProperlyCompleted()) {
						numProperlyCompletedPIs[i] += numSimilarPIs;
					}
				}
			}
		}
	}

	private void createFitnessVariables() {
		numParsedWMEs = new double[population.length];
		numCompletedPIs = new double[population.length];
		numProperlyCompletedPIs = new double[population.length];
	}

	private void createParser() {
		//creating a parser for every individual
		parser = new StopSemanticsParser[population.length];
		for (int i = 0; i < parser.length; i++) {
			if (mapping.getIndenticalHeuristicsNet(i) < 0) {
				parser[i] = new StopSemanticsParser(population[i], generator);
			}
		}
	}

}
