package org.processmining.plugins.heuristicsnet.miner.genetic.fitness;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.model.XTrace;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.models.heuristics.impl.ExtraBehaviorParser;
import org.processmining.models.heuristics.impl.HNSet;
import org.processmining.models.heuristics.impl.HNSubSet;

/**
 * Calculates the fitness of <code>HeuristicsNet</code> objects in a population
 * based on (i) the improved continuous semantics fitness of this object (see
 * {@link ImprovedContinuousSemantics}) <i>minus</i> (ii.a) the weighed ratio of
 * concurrently enabled tasks during the log replay and (ii.b) the weighed ratio
 * of activities with a same label (i.e., duplicates) sharing common input or
 * output elements.
 * 
 * <p>
 * Additional methods are provided to set and retrieve the values of the two
 * different weights used for every part of the subtraction above. Furthermore,
 * the parsing semantics is a continuous one (i.e., the log replay does not stop
 * when problems are encountered).
 * 
 * 
 * @author Ana Karla Alves de Medeiros
 * 
 */
public class ExtraBehaviorPunishment implements Fitness {
	private XLogInfo logInfo = null;
	private HeuristicsNet[] population = null;
	private ExtraBehaviorParser[] parser = null;

	private double[] numPIsWithMissingTokens = null; //PI = process instance
	private double[] numMissingTokens = null; //PI = process instance
	private double[] numPIsWithExtraTokensLeftBehind = null;
	private double[] numExtraTokensLeftBehind = null;
	private double[] numParsedWMEs = null;
	private double[] numEnabledWMEs = null;
	private double[] numIntersectingDuplicatesPerInputElement;
	private double[] numIntersectingDuplicatesPerOutputElement;
	private double maximumNumberIntersectingElements = 0.0;
	private MapIdenticalHeuristicsNets mapping = null;
	private double kappa = FitnessFactory.ALL_FITNESS_PARAMETERS[FitnessFactory.INDEX_KAPPA];
	private double gamma = FitnessFactory.ALL_FITNESS_PARAMETERS[FitnessFactory.INDEX_GAMMA];

	private Random generator = null;

	//	//added for computing the fitness AET for a given individual
	//	private long executionTime[] = null;

	/**
	 * 
	 * Constructs a new extra behavior punishment fitness for the given log. All
	 * fitness values calculated by this object for populations of
	 * <code>HeuristicsNet</code> will be based on this log and the two values
	 * to weigh the ratios in the subtraction of this fitness.
	 * 
	 * @param logInfo
	 *            information about the log
	 * @param kappa
	 *            double weight for the ratio of concurrently enabled activities
	 * @param gamma
	 *            double weight for the ratio of duplicates sharing common input
	 *            or output elements
	 */
	public ExtraBehaviorPunishment(XLogInfo logInfo, double kappa, double gamma) {
		generator = new Random(Long.MAX_VALUE);
		this.logInfo = logInfo;
		this.kappa = kappa;
		this.gamma = gamma;

	}

	/**
	 * Calculates the extra behavior punishment fitness of every
	 * <code>HeuristicsNet</code> in the population
	 * 
	 * @param population
	 *            array containing the <code>HeuristicsNet</code> for which a
	 *            fitness value will be calculated
	 */
	public HeuristicsNet[] calculate(HeuristicsNet[] population) {

		this.population = population;

		//		//for computation purposes
		//		executionTime =  new long[population.length];
		//		
		//		for (int i=0;i<executionTime.length;i++){
		//			executionTime[i] = 0;
		//		}

		mapping = new MapIdenticalHeuristicsNets(this.population);
		createParser();
		createFitnessVariables();
		ProperCompletion.resetDuplicatesActualFiringAndArcUsage(this.population);
		calculatePartialFitness();
		calculateStructuralPunishment();

		return assignFitness();

	}

	private void calculateStructuralPunishment() {

		//Motivation: The union sets of the subsets in the I/O of duplicates
		//should not intersect. In other words, duplicates cannot share input or
		//output elements. Whenever this situation occurs, the individual
		//is punished. The more common elements the duplicates have, the higher
		//the punishment.

		maximumNumberIntersectingElements = 0;
		//calculating the punishment for every individual in the population
		for (int i = 0; i < population.length; i++) {
			//			long startTime = System.currentTimeMillis();
			numIntersectingDuplicatesPerInputElement[i] = checkIntersectingElements(population[i]
					.getActivitiesMappingStructures().getReverseActivitiesMapping(), population[i]
					.getActivitiesMappingStructures().getActivitiesMapping(), population[i].getInputSets());
			numIntersectingDuplicatesPerOutputElement[i] = checkIntersectingElements(population[i]
					.getActivitiesMappingStructures().getReverseActivitiesMapping(), population[i]
					.getActivitiesMappingStructures().getActivitiesMapping(), population[i].getOutputSets());

			if (maximumNumberIntersectingElements < (numIntersectingDuplicatesPerInputElement[i] + numIntersectingDuplicatesPerOutputElement[i])) {
				maximumNumberIntersectingElements = numIntersectingDuplicatesPerInputElement[i]
						+ numIntersectingDuplicatesPerOutputElement[i];
			}

			//			executionTime[i]=executionTime[i]+(System.currentTimeMillis()-startTime);

		}

	}

	private double checkIntersectingElements(Map<XEventClass, HNSubSet> mapFromEventsToActivities,
			XEventClass[] mapFromActivitiesToEvents, HNSet[] sets) {
		double structuralPunishment = 0;

		for (XEventClass event : mapFromEventsToActivities.keySet()) {
			//creating the counter for the intersections...
			Map<XEventClass, Integer> intersection = new HashMap<XEventClass, Integer>();

			if (mapFromEventsToActivities.get(event).size() > 1) { //the element has duplicates...
				for (int k = 0; k < mapFromEventsToActivities.get(event).size(); k++) {
					ArrayList<XEventClass> union = mapToDuplicateLabel(mapFromActivitiesToEvents, HNSet
							.getUnionSet(sets[mapFromEventsToActivities.get(event).get(k)]));
					for (int m = 0; m < union.size(); m++) {
						Integer numberOfIntersections = new Integer(1);
						if (intersection.containsKey(union.get(m))) {
							numberOfIntersections = new Integer(numberOfIntersections.intValue()
									+ intersection.get(union.get(m)).intValue());
						}
						intersection.put(union.get(m), numberOfIntersections);
					}

				}

				//add the partial punishment due to intersections...
				for (XEventClass eventWithIntersections : intersection.keySet()) {
					if (intersection.get(eventWithIntersections).intValue() > 1) {
						structuralPunishment += intersection.get(eventWithIntersections).intValue();
					}
				}
			}
		}
		return structuralPunishment;
	}

	private ArrayList<XEventClass> mapToDuplicateLabel(XEventClass[] mapActivitiesToEvents, HNSubSet unionSet) {

		ArrayList<XEventClass> mapping = new ArrayList<XEventClass>();

		for (int i = 0; i < unionSet.size(); i++) {
			mapping.add(mapActivitiesToEvents[unionSet.get(i)]);
		}

		return mapping;
	}

	private void createParser() {
		//creating a parser for every individual
		parser = new ExtraBehaviorParser[population.length];
		for (int i = 0; i < parser.length; i++) {
			if (mapping.getIndenticalHeuristicsNet(i) < 0) {
				parser[i] = new ExtraBehaviorParser(population[i], generator);
			}
		}
	}

	private void createFitnessVariables() {
		numPIsWithMissingTokens = new double[population.length];
		numMissingTokens = new double[population.length];
		numPIsWithExtraTokensLeftBehind = new double[population.length];
		numExtraTokensLeftBehind = new double[population.length];
		numParsedWMEs = new double[population.length];
		numEnabledWMEs = new double[population.length];
		numIntersectingDuplicatesPerInputElement = new double[population.length];
		numIntersectingDuplicatesPerOutputElement = new double[population.length];
	}

	private void calculatePartialFitness() {

		XTrace pi = null;
		int numSimilarPIs = 0;
		int numMissingTokens = 0;
		int numExtraTokensLeftBehind = 0;

		Iterator<XTrace> logReaderInstanceIterator = logInfo.getLog().iterator();
		while (logReaderInstanceIterator.hasNext()) {
			pi = logReaderInstanceIterator.next();
			//TODO - Call here the correct method to get the number of pis!
			//			numSimilarPIs = MethodsForWorkflowLogDataStructures.
			//							getNumberSimilarProcessInstances(pi);
			numSimilarPIs = 1;
			for (int i = 0; i < population.length; i++) {
				if (mapping.getIndenticalHeuristicsNet(i) < 0) { //we need to compute the partial fitness

					//					long startTime = System.currentTimeMillis();

					parser[i].parse(pi);

					//					executionTime[i]=executionTime[i]+(System.currentTimeMillis()-startTime);

					//partial assignment to variables
					numMissingTokens = parser[i].getNumMissingTokens();
					if (numMissingTokens > 0) {
						numPIsWithMissingTokens[i] += numSimilarPIs;
						this.numMissingTokens[i] += (numMissingTokens * numSimilarPIs);
					}

					numExtraTokensLeftBehind = parser[i].getNumExtraTokensLeftBehind();
					if (numExtraTokensLeftBehind > 0) {
						numPIsWithExtraTokensLeftBehind[i] += numSimilarPIs;
						this.numExtraTokensLeftBehind[i] += (numExtraTokensLeftBehind * numSimilarPIs);
					}
					numParsedWMEs[i] += (parser[i].getNumParsedElements() * numSimilarPIs);
					numEnabledWMEs[i] += (parser[i].getNumTotalEnabledElements() * numSimilarPIs);

				}
			}
		}
	}

	/*
	 * fitness = f1 - kappa * f2 - gamma * f3; where: f1 is based on parsed
	 * tasks, missing tokens and tokens left behind. f2 is based on the number
	 * of enabled tasks during the parsing. f3 is based on the number of
	 * duplicates with intersecting I/O elements.
	 */

	private HeuristicsNet[] assignFitness() {

		double fitness = 0;
		double f1 = 0;
		double f2 = 0;
		double f3 = 0;

		double numATEsAtLog = 0;
		double numPIsAtLog = 0;
		double missingTokensDenominator = 0.001;
		double unusedTokensDenominator = 0.001;
		double maxEnabledWMEs = Double.MIN_VALUE;
		int indexIdenticalIndividual = 0;

		numATEsAtLog = logInfo.getNumberOfEvents();
		numPIsAtLog = logInfo.getNumberOfTraces();
		maxEnabledWMEs = getMaxValue(numEnabledWMEs);

		for (int i = 0; i < population.length; i++) {

			indexIdenticalIndividual = mapping.getIndenticalHeuristicsNet(i);

			if (indexIdenticalIndividual < 0) {

				missingTokensDenominator = numPIsAtLog - numPIsWithMissingTokens[i] + 1;

				unusedTokensDenominator = numPIsAtLog - numPIsWithExtraTokensLeftBehind[i] + 1;

				f1 = (numParsedWMEs[i] - ((numMissingTokens[i] / missingTokensDenominator) + (numExtraTokensLeftBehind[i] / unusedTokensDenominator)))
						/ numATEsAtLog;

				f2 = numEnabledWMEs[i];

				f2 /= maxEnabledWMEs; //normalizing...

				if (maximumNumberIntersectingElements > 0) {
					f3 = (numIntersectingDuplicatesPerInputElement[i] + numIntersectingDuplicatesPerOutputElement[i])
							/ maximumNumberIntersectingElements;
				} else {
					f3 = 0.0;
				}

				fitness = f1 - kappa * f2 - gamma * f3;

				population[i].setFitness(fitness);

				//				log(executionTime[i],fitness);

			} else {

				population[i].setFitness(population[indexIdenticalIndividual].getFitness());
				population[i].setActivitiesActualFiring(population[indexIdenticalIndividual]
						.getActivitiesActualFiring());
				population[i].setArcUsage(population[indexIdenticalIndividual].getArcUsage());
				//				log(executionTime[i],fitness);

			}
		}

		return population;

	}

	//	private void log(long l, double fitness) {
	//		// TODO Auto-generated method stub
	//		
	//		
	//				try {
	//		
	//					BufferedWriter writer = new BufferedWriter(new FileWriter("experiments/FitnessExecutionTime"+ ".csv", true));
	//		
	//					//writer.write("PopulationNumber, Mean, Variance, StandardDeviation, BestFitness \n");
	//		
	//					//for (int i = 0; i < settings.getMaxGeneration(); i++) {
	//					writer.write(l + ", " + fitness + "\n");
	//					//}
	//		
	//					//			writer.write("Elapsed Time " + elapsedTime + "\n");
	//					//
	//					//			writer.write(settings.toString() + "\n");
	//					//
	//					//			writer.write((new Date()).toString() + "\n");
	//		
	//					writer.close();
	//		
	//				}
	//		
	//				catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//		
	//	}

	private double getMaxValue(double[] array) {
		double max = Double.MIN_VALUE;
		for (int i = 0; i < array.length; i++) {
			if (max < array[i]) {
				max = array[i];
			}
		}
		return max;
	}

}
