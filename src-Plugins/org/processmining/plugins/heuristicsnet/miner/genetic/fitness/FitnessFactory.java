package org.processmining.plugins.heuristicsnet.miner.genetic.fitness;

import org.deckfour.xes.info.XLogInfo;
import org.processmining.plugins.heuristicsnet.miner.genetic.util.ParameterValue;

/**
 * Factory class to build fitness objects. These fitness objects will be used
 * during the log replay.
 * 
 * @author Ana Karla Alves de Medeiros
 * 
 */
public class FitnessFactory {

	/**
	 * Default value of the KAPPA weight that is used by the
	 * {@link ExtraBehaviorPunishment} fitness type.
	 */
	public static double KAPPA = 0.025;

	/**
	 * Default value of the GAMMA weight that is used by the
	 * {@link ExtraBehaviorPunishment} fitness type.
	 */
	public static double GAMMA = 0.025;

	/**
	 * Array containing the values for the KAPPA and GAMMA weights
	 */
	public static double[] ALL_FITNESS_PARAMETERS = new double[] { KAPPA, GAMMA };

	/**
	 * Index of KAPPA value in the array <code>ALL_FITNESS_PARAMETERS</code>
	 */
	public static int INDEX_KAPPA = 0;

	/**
	 * Index of GAMMA value in the array <code>ALL_FITNESS_PARAMETERS</code>
	 */
	public static int INDEX_GAMMA = 1;

	/**
	 * Constructs a fitness factory object.
	 */
	public FitnessFactory() {
	}

	/**
	 * Retrieves an array containing the fitness types supported by this factory
	 * class.
	 * 
	 * @return array with the names of the supported fitness types
	 */
	public static String[] getAllFitnessTypes() {
		return new String[] { "ProperCompletion", "StopSemantics", "ContinuousSemantics",
				"ImprovedContinuousSemantics", "ExtraBehavior" };
	}

	/**
	 * Provides the names of the parameters used for a certain fitness types.
	 * When the fitness does not have a special parameter (e.g. kappa ou gamma),
	 * the returned array is empty.
	 * 
	 * @param fitnessIndex
	 *            index of the fitness type.
	 * @return String[] array containing the names of the parameters. If the
	 *         fitness does not require any special parameter, an empty array is
	 *         returned.
	 */
	public static ParameterValue[] getAdvancedFitnessParametersNames(int fitnessIndex) {
		ParameterValue[] parametersNames = null;
		switch (fitnessIndex) {
			case 0 :
				parametersNames = new ParameterValue[0];
				break;
			case 1 :
				parametersNames = new ParameterValue[0];
				break;
			case 2 :
				parametersNames = new ParameterValue[0];
				break;
			case 3 :
				parametersNames = new ParameterValue[0];
				break;
			case 4 :
				parametersNames = new ParameterValue[2];
				parametersNames[0] = new ParameterValue("           Extra Behavior Punishment  ", 0.025/* default */,
						0.000/* min */, 1.000/* max */, 0.001/* step size */,
						"Sets the amount of punishment (kappa) for the extra behavior that an individual allows for.");
				parametersNames[1] = new ParameterValue("           Duplicates With Common In/Out Tasks Punishment  ",
						0.025/* default */, 0.000/* min */, 1.000/* max */, 0.001/*
																				 * step
																				 * size
																				 */,
						"Sets the amount of punishment (gamma) for the amount of duplicates sharing input/output tasks that an individual has.");
				break;
		}

		return parametersNames;
	}

	/**
	 * Creates the specified fitness type object.
	 * 
	 * @param indexFitnessType
	 *            index of the fitness type.
	 * @param logInfo
	 *            information about the log to be parsed by the created fitness
	 *            type object.
	 * @param parameters
	 *            array of doubles with the parameters to be used by the
	 *            specified fitness type.
	 * @return an object of the selected fitness type.
	 */
	public static Fitness getFitness(int indexFitnessType, XLogInfo logInfo, double[] parameters) {
		Fitness object = null;
		switch (indexFitnessType) {
			case 0 :
				object = new ProperCompletion(logInfo);
				break;
			case 1 :
				object = new StopSemantics(logInfo);
				break;
			case 2 :
				object = new ContinuousSemantics(logInfo);
				break;
			case 3 :
				object = new ImprovedContinuousSemantics(logInfo);
				break;
			case 4 :
				object = new ExtraBehaviorPunishment(logInfo, parameters[0], parameters[1]);
				break;
		}
		return object;
	}
}
