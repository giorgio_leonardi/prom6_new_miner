/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.plugins.heuristicsnet.array.visualization;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.models.heuristics.HeuristicsNetGraph;
import org.processmining.models.jgraph.ProMJGraphVisualizer;
import org.processmining.plugins.heuristicsnet.miner.genetic.gui.DoubleClickTable;
import org.processmining.plugins.heuristicsnet.miner.genetic.util.MethodsOverHeuristicsNets;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Ana Karla A. de Medeiros.
 * @version 1.0
 */

public class HeuristicsMinerVisualizationPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5929578796370746442L;

	private HeuristicsNet[] population = null;

	//private XLog log = null;

	private JTable table = null;
	private JPanel graphPanel = null;
	private JCheckBox showSplitJoinSemantics = null;

	private HeuristicsNetGraph result = null;

	public HeuristicsMinerVisualizationPanel(PluginContext context, HeuristicsNet[] pop) {
		population = pop;
		//this.log = log;

		MethodsOverHeuristicsNets.decreasinglyOrderPopulation(population);

		//population = MethodsOverHeuristicsNets.removeDuplicates(population);
		try {
			jbInit();
			//if the table has a single element, we already show it!
			if (table.getRowCount() == 1) {
				table.changeSelection(0, 0, false, false);
				updateGraph();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		//		Message.add("<GeneticAlgorithmPlugins nofIndividuals=\"" + population.length +
		//				"\" bestFitness=\"" + population[0].getFitness() + "\" worstFitness=\"" + population[population.length-1].getFitness()+"\">",
		//				Message.TEST);
	}

	public JComponent getVisualization() {
		return this;
	}

	//	public XLog getLogReader() {
	//		return log;
	//	}

	private void updateGraph() {

		if (table.getSelectedRowCount() == 0) {
			JOptionPane.showMessageDialog(this, "Please select a process instance first.");
			return;
		}

		graphPanel.removeAll();
		//result =new HeuristicsNetGraph(population[table.getSelectedRow()], "Heuristics Net",true);
		result = new HeuristicsNetGraph(population[table.getSelectedRow()], "Heuristics Net",
				showSplitJoinSemantics.isSelected());
		//providedObjects = result.getProvidedObjects();

		graphPanel.add(ProMJGraphVisualizer.instance().visualizeGraphWithoutRememberingLayout(result), BorderLayout.CENTER);
		graphPanel.validate();
		graphPanel.repaint();

	}

	private void jbInit() throws Exception {
		JSplitPane splitPane;
		JPanel bottomPanel = new JPanel(new BorderLayout());
		JPanel buttonsPanel = new JPanel();
		JPanel messagePanel = new JPanel();
		JButton updateGraphButton = new JButton("Update graph");
		showSplitJoinSemantics = new JCheckBox("Display split/join semantics");

		messagePanel.add(new JLabel("Please select one or more process instances and press 'Update graph'."));
		graphPanel = new JPanel(new BorderLayout());
		graphPanel.add(messagePanel, BorderLayout.CENTER);

		table = new DoubleClickTable(new IndividualTableModel(population), updateGraphButton);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		updateGraphButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateGraph();
			}
		});

		showSplitJoinSemantics.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateGraph();
			}
		});

		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, new JScrollPane(table), graphPanel);

		splitPane.setDividerLocation(250);
		splitPane.setOneTouchExpandable(true);

		buttonsPanel.add(updateGraphButton);
		bottomPanel.add(buttonsPanel, BorderLayout.WEST);
		bottomPanel.add(showSplitJoinSemantics, BorderLayout.EAST);

		setLayout(new BorderLayout());
		this.add(splitPane, BorderLayout.CENTER);
		this.add(bottomPanel, BorderLayout.SOUTH);
	}

}

@SuppressWarnings("serial")
class IndividualTableModel extends AbstractTableModel {

	private final HeuristicsNet[] data;

	public IndividualTableModel(HeuristicsNet[] data) {
		this.data = data;
	}

	public String getColumnName(int col) {
		return "Population";
	}

	public int getRowCount() {
		return data.length;
	}

	public int getColumnCount() {
		return 1;
	}

	public Object getValueAt(int row, int column) {
		return "Individual " + row + ", fitness " + data[row].getFitness();
	}

}
