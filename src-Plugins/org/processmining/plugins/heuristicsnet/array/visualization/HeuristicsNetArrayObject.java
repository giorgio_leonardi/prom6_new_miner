package org.processmining.plugins.heuristicsnet.array.visualization;

import org.processmining.models.heuristics.HeuristicsNet;

public class HeuristicsNetArrayObject {

	protected HeuristicsNet[] population;

	public HeuristicsNetArrayObject(HeuristicsNet[] pop) {

		population = pop;

	}

	public HeuristicsNet[] getPopulation() {
		return population;
	}

}
