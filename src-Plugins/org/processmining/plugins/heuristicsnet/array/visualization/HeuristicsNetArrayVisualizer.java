package org.processmining.plugins.heuristicsnet.array.visualization;

import javax.swing.JComponent;

import org.processmining.contexts.uitopia.annotations.Visualizer;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.models.heuristics.HeuristicsNet;

@Plugin(name = "Visualize HeuristicsNet Array", parameterLabels = { "HeuristicsNetArray", "HeuristicsNetArray2" }, returnLabels = { "HN Visualization" }, returnTypes = { JComponent.class })
@Visualizer
public class HeuristicsNetArrayVisualizer {

	@PluginVariant(requiredParameterLabels = { 0 })
	public static JComponent visualize(PluginContext context, HeuristicsNetArrayObject population) {
		//return ProMJGraphVisualizer.visualizeGraph(new HeuristicsNetGraph(population[population.length-1], "Heuristics Net",false), context.getProgress());

		HeuristicsMinerVisualizationPanel visualizer = new HeuristicsMinerVisualizationPanel(context, population
				.getPopulation());

		return visualizer.getVisualization();

	}

	@PluginVariant(requiredParameterLabels = { 1 })
	public static JComponent visualize(PluginContext context, HeuristicsNet[] population) {

		HeuristicsMinerVisualizationPanel visualizer = new HeuristicsMinerVisualizationPanel(context, population);

		return visualizer.getVisualization();

	}

}