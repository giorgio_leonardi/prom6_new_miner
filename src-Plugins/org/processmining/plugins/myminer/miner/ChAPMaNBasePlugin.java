package org.processmining.plugins.myminer.miner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import ioUtility.FileManager;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.models.andOrGraph.GraphAndOr;



public class ChAPMaNBasePlugin {


@Plugin(name = "ChaPMan base",
parameterLabels = {"Log"},
returnLabels = {"Mined Models"},
returnTypes = {GraphAndOr.class},
userAccessible = true,
help = "ChaPMan")
	
	@UITopiaVariant(affiliation = "UniversitÓ di Torino", author = "Luca Canensi", email = "canensi@di.unito.it")
	
	public static GraphAndOr mine(UIPluginContext context ,XLog log){
	
		ParametersPanel parameters = new ParametersPanel();
		
		InteractionResult result = context.showConfiguration("ChAPMaN Parameters", parameters);
		if (result.equals(InteractionResult.CANCEL))
		{
			context.getFutureResult(0).cancel(true);
		}
		
		MyMiner m=new MyMiner(log,parameters.getSettings());
	
		GraphAndOr g=m.mine();
		
		FileManager fm=new FileManager();
		File f=new File("grafo.gv");
		try {
			fm.writeGraph(g,f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return g;
	}
}
