package org.processmining.plugins.myminer.miner;

import java.io.InputStream;

import org.processmining.contexts.uitopia.annotations.*;
import org.processmining.framework.abstractplugins.*;
import org.processmining.framework.plugin.*;
import org.processmining.framework.plugin.annotations.*;

@Plugin(name = "Import Person",
        parameterLabels = { "Filename" },
        returnLabels = { "Person" },
        returnTypes = { Integer.class })
@UIImportPlugin(description = "Integer",
                extensions = { "integer" })
public class ChapmanImport extends AbstractImportPlugin {
  @Override
  protected Integer importFromStream(final PluginContext context,
                                    final InputStream input,
                                    final String filename,
                                    final long fileSizeInBytes) {
    try {
      context.getFutureResult(0).setLabel("Person imported from " + filename);
    } catch (final Throwable _) {
      // Don't care if this fails
    }
    Integer result = new Integer(3);
    // Fill in object from input
    return result;
  }
}