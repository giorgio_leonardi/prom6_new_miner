package org.processmining.plugins.myminer.miner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.processmining.models.andOrGraph.DirectedGraphEdgeImpl;
import org.processmining.models.andOrGraph.Edge;
import org.processmining.models.andOrGraph.Event;
import org.processmining.models.andOrGraph.EventsNode;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.models.andOrGraph.Pair;

public class MyMiner {

	private XLog log;//log iniziale

	private GraphAndOr net;//modello creato
 
	private Set<String> eventEliminate;//insieme degli eventi eliminati

	private ArrayList<Event> events;
	
	private ChAPMaNSettings settings;	
	
	
	public MyMiner(XLog log, ChAPMaNSettings chAPMaNSettings) {
		this.log = log;		
		eventEliminate = new TreeSet<String>();
		new TreeSet<String>();
		settings=chAPMaNSettings;
	}
	
	
	// mine only trace
	public GraphAndOr mineTrace(Integer trace) {
		
		// evento iniziale delle tracce
		String start = XLogInfoImpl.NAME_CLASSIFIER.getClassIdentity(log.get(trace)
				.get(0));
	
		// elenco delle tracce
		Set<Integer> newLog = new TreeSet<Integer>();
		newLog.add(new Integer(trace));
		
		// insieme degli eventi
		events = getEvents(newLog, 0);
		
		// costruisco il grafo vuoto
		net = new GraphAndOr(log.getAttributes().get("concept:name").toString(),true);
		
		// inserisco il nodo di partenza
		Event startEvent = events.get(getEventPos(start, events));
		

		EventsNode startNode = new EventsNode(startEvent, false,
				startEvent.getCount());
		
		net.addNode(startNode);
		
		// mining
		mine(newLog, startNode, 0, 0);		
		
		return net;
	}
	
		

	public GraphAndOr mine() {
	
		// evento iniziale delle tracce
		String start = XLogInfoImpl.NAME_CLASSIFIER.getClassIdentity(log.get(0)
				.get(0));
		// elenco delle tracce
		Set<Integer> newLog = new TreeSet<Integer>();
		for (int i = 0; i < log.size(); i++) 
			newLog.add(new Integer(i));
		
		// insieme degli eventi
		events = getEvents(newLog, 0);
		
		//System.out.println(events);
		
		// se occorrono un numero di volte minore della soglia li
		// elimino(inserisco nell'insieme degli eliminati)

		for (Event e : events)
			if ((e.getCount() / (double) newLog.size()) < settings.getEventThreshold())
				eventEliminate.add(e.getName());
		
		// costruisco il grafo vuoto
		net = new GraphAndOr(log.getAttributes().get("concept:name").toString(),true);
		
		// inserisco il nodo di partenza
		Event startEvent = events.get(getEventPos(start, events));
		

		EventsNode startNode = new EventsNode(startEvent, false,
				startEvent.getCount());
		
		net.addNode(startNode);
		// startNode.printOcc();
		
		// mining
		mine(newLog, startNode, 0, 0);
		// stampo il grafo
		// System.out.println(net.toString());
		
		LogsMap.init();
		
		//clono la rete per non perdere il riferimento di quella originale
		GraphAndOr clone_net = net.clone();
		
		//la logsmap viene riempita con i riferimenti alle tracce 
		for(int i = 0; i < log.size(); i++) {
			ArrayList<String> attributes = new ArrayList<>();
			
			for(int j = 0; j < log.get(i).size(); j++) 
				attributes.add(log.get(i).get(j).getAttributes().toString());
			
			LogsMap.put(i, attributes);

			/* 	creo un grafo per ogni traccia
			 	il fatto che il metodo mine modifica la net principale � la causa del clone iniziale */
			GraphAndOr graph_trace = mineTrace(new Integer(i));
			clone_net.insertTraceGraph(new Integer(i), graph_trace);
		}			
		
		//assegno rete clone alla net che adesso avr� anche la tabella delle tracce
		net = clone_net;
		//aggiunto il log al grafo
		net.setLog(log);
		
		return net;
	}

	private void mine(Set<Integer> log2, EventsNode node, int index, int nJolly) {

		boolean jolly = false;
		Set<Integer> traceJolly = null;
		ConnectionManager cm = new ConnectionManager(log);
		//ArrayList<Event> events = getEvents(log2, index);

		// System.out.println("-------EVENTI dopo "+
		// node.getLabel()+" -----------");
		// System.out.println(events);

		// estraggo i successori (Evento,numero di volte in cui compare come
		// successore)
		ArrayList<Event> succ = getSuccessor(log2, index + 1);

		//System.out.println("-------SUCCESSORI di "+node.getLabel()+" -----------");
		//System.out.println(succ);
		
		if (succ.size() == 0)
			return;
		
		// conta i successori
		int count = log2.size();

		// successori non frequenti
		ArrayList<Event> elim = SuccNotFreq(succ, count);

		// System.out.println("-------SUCCESSORI ELIMINATI-----------");
		// System.out.println(elim);

		// elimino i successori non frequenti
		succ.removeAll(elim);

		// lista delle tracce in cui una successione diretta fra 2 eventi �
		// stata trovata
		// la posizione nell'arraylist corrisponde alla posizione nell'arraylist
		// dei successori.
		// ogni posizione contiene l'insieme degli eventi con cui il successore
		// � in AND

		cm.setTraces(log2, index + 1);
		// valuto le connessioni fra successori
		evaluateConnection(cm, succ);
		
		// costruisco gli insiemi di eventi in and
		ArrayList<Event> andSets = checkAnd(succ, cm);

		//TODO sarebbe meglio eliminare i sottoinsiemi degli and direttamente dentro checkAND
		removeSubSets(andSets);

		addDirectFollow(succ,andSets,cm);
		
		// controllo se esistono eventi da eliminare in and fra loro
		if (elim.size() > 1) {
			evaluateConnection(cm, elim);

			ArrayList<Event> andElim = checkAnd(elim, cm);
			// rimuovo i sottoinsiemi generati
			removeSubSets(andElim);
			// se esistono eventi in and che superano la soglia allora non li
			// elimino pi�
			for (Event ev : andElim) {
				if (ev.isAnd()) {
					int tot = ev.getCount();
					if (tot / (double) count >= settings.getFrequencyThreshold()) 
						andSets.add(ev);						
					}
			}
		}

		// se non ci sono pi� successori esco
		if (succ.size() == 0)
			return;
		// controllo se gli eventi che devono essere eliminati tutto insieme
		// formano un evento JOLLY
		if ((!isUnderThreshold(elim, count))
				&& nJolly < settings.getNumberJolly())
			jolly = true;
		
		
		
	
		ArrayList<Pair<Event,Set<Integer>>> logs = new ArrayList<Pair<Event,Set<Integer>>>();
		// costruisco i sottolog
		for (Event set : andSets) {
			Set<Integer> logRed = new TreeSet<Integer>();
			// per ogni insieme di eventi estraggo tutte le tracce in cui
			// compaiono questi eventi in and
			logRed=(Set<Integer>) set.getTrace();
			logs.add(new Pair<Event, Set<Integer>>(set,logRed));
				for (String name:set.getEvents()){
					int pos=getPos(succ, name);
					if (pos!=-1)
						succ.get(pos).removeTrace(logRed);
				}
		}
		for (Event e:succ){
			if (e.getTrace().size()>0){
				int pos=getPos(andSets, e.getName());
				if (pos==-1)
				{ 
					andSets.add(e);
					Collections.sort(andSets);
					logs.add(new Pair<Event, Set<Integer>>(e,(Set<Integer>) e.getTrace()));
					System.out.println(e + ((Set<Integer>) e.getTrace()).toString());
					Collections.sort(logs);
				}
				else {
					logs.get(pos).getSecond().addAll(e.getTrace());
					for (String perm:e.getPermutations())
						logs.get(pos).getFirst().addOccurs(e.getOccurs(perm),perm);
				}
			}else e.resetTrace();
		}

		// System.out.println(andSets);

		// se devo costruire il nodo jolly estraggo tutte le tracce che
		// riguardano eventi da eliminare
		//if (jolly)
		//	traceJolly = getTraceJolly(log2, logs);

		// se nn esiste costruisco il nodo del grafo relativo e lo aggiungo
		EventsNode nodeEv = node;

		// iteratore sui sottolog creati
		// ultimo nodo prima degli insiemi di and da considerare
		// itero sugli insiemi di and

		for (Pair<Event,Set<Integer>> elem :logs) {
			
			if (elem.getSecond().size()/(double)log2.size()<settings.getFrequencyThreshold()) continue;
			// se l'insieme � costituito da almeno due eventi siamo
			// effettivamente in presenza di un and

			EventsNode child;
			// l'evento finale � salvato ed unico in tutto il grafo
			// if (set.contains(endEvent))
			// child=endNode;
			// else{
			// inserisco un nuovo nodo
			
			child = new EventsNode(elem.getFirst(), false, elem.getSecond().size());
			net.addNode(child);
			// child.printOcc();
			// }
			
			Edge edge = new Edge(nodeEv, child, Math.round(elem.getSecond().size()
					/ (double) log2.size() * 100) / 100.0);
			// inserisco un arco
			net.addEdge(edge);
			nodeEv.addOutEdge(edge);
			child.addInEdge(edge);
			// itero su tutti gli elementi dell'insieme
			mine(elem.getSecond(), child, index + elem.getFirst().getDimension(), nJolly);
		}
		/*
		// se c'� il jolly costruisco un nodo e riparto il mining dall'insieme
		// degli eventi che rappresenta
		if (jolly) {
			Set<Event> jollyEvent = new TreeSet<Event>();
			// insieme di eventi rappresentati dal jolly
			for (Event p : elim) {
				jollyEvent.add(p);
			}
			//TODO gestire i nodi jolly
			// nuovo nodo jolly
			EventsNode child = 
				new EventsNode(jollyEvent, true,	traceJolly.size());
			
			net.addNode(child);
			// child.printOcc();
			Edge edge = new Edge(nodeEv, child, Math.round(traceJolly.size()
					/ (double) log2.size() * 100) / 100.0);
			net.addEdge(edge);
			nodeEv.addOutEdge(edge);
			child.addInEdge(edge);
			// itero su tutti gli elementi dell'insieme
			mine(traceJolly, child, index + 1, nJolly + 1);
		}*/

		return;

	}

	@SuppressWarnings("unchecked")
	private void addDirectFollow(ArrayList<Event> succ,
			ArrayList<Event> andSets, ConnectionManager cm) {

		for (Event ev:succ){
			if (getPos(andSets,ev.getName())==-1){
				Set<Event> evSucc=cm.getSuccessor(ev.getName());
				if (evSucc!=null){
					Event newEv=new Event(ev.getName());
					ArrayList<Pair<Integer, Integer>> occ;
						for (Event e:evSucc){
							occ = (ArrayList<Pair<Integer, Integer>>) ev.getOccurs().clone();
							ArrayList<Integer> occSucc = cm.getTrace(ev, e);
							if (occSucc!=null){
								for (Pair<Integer, Integer> o:ev.getOccurs()){
									if (!occSucc.contains(o.getFirst()))
										occ.remove(o);
								}
								newEv.setOccur(occ, log);
							}
						}
						if (newEv.getCount()>0)
							{
							andSets.add(newEv);
							Collections.sort(andSets);
							}
					}
				}
			}
	}

	private ArrayList<Event> copySucc(
			ArrayList<Event> succ) {
		
		ArrayList<Event> copy=new ArrayList<Event>();
		for (Event p:succ){
			try {
				copy.add((Event) p.clone());
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		
		return copy;
	}

	private void printSet(Set<Event> set) {

		for (Event e : set)
			System.out.print(e.getName() + "    ");
		System.out.println();
	}

	/*** eliminazione di sottoinsiemi da sets */
	private void removeSubSets(ArrayList<Event> andSets) {

		int i = 0;
		while (i < andSets.size()) {
			Event set = andSets.get(i);
			int j = 0;
			boolean eliminato = false;
			while (j < andSets.size() && !eliminato) {
				if (i != j) {
					Event comp = andSets.get(j);
					
					if (set.isSubset(comp)) {
//						System.out.println("Eliminato "+set);
						andSets.remove(i);
						eliminato = true;
					}
				}
				j++;
			}
			if (!eliminato)
				i++;
		}
	}


	/** valuta la dipendenza fra elementi in set */
	private void evaluateConnection(ConnectionManager cm,
			ArrayList<Event> succ) {
		/* per tutte le coppie di successori calcolo la dipendenza */
		for (int i = 0; i < succ.size() - 1; i++) {
			Event s1 = succ.get(i);
			for (int j = i + 1; j < succ.size(); j++) {

				Event s2 = succ.get(j);
				// per lo stesso evento non ha senso
				if (!s1.equals(s2)) {
					// System.out.println("Confronto evento "+s1.getFirst().getName()+" e "+s2.getFirst().getName());
					// calcolo la connessione fra l'evento s1 e s2
					cm.evaluateConnection(s1, s2);
				}
			}
		}
	}

	/**
	 * valuta le connessioni fra elementi in succ. Ogni set rappresenta eventi
	 * in and. Ogni posizione dell'arrayList insiemi in Xor
	 * @param log2 
	 */
	private ArrayList<Event> checkAnd(
			ArrayList<Event> succ, ConnectionManager cm) {

		Set<Event> r = new TreeSet<Event>();
		Set<Event> x = new TreeSet<Event>();
		ArrayList<Set<Event>> out = new ArrayList<Set<Event>>();
		Set<Event> p = new TreeSet<>();
		for (Event s : succ)
			p.add(s);
		BronKerbosch(r, p, x, out, cm, succ);
		
		return makeAndEvent(out);
	}

	private ArrayList<Event> makeAndEvent(ArrayList<Set<Event>> out) {
		ArrayList<Event> result=new ArrayList<Event>();
		for (Set<Event> set:out){
			if (set.size() > 1){
				Iterator<Event> it=set.iterator();
				String name="";
				ArrayList<Pair<Integer, Integer>> occ=new ArrayList<Pair<Integer,Integer>>();
				while (it.hasNext()){
					Event temp=it.next();
					name+=temp.getName()+"&&";
					occ.addAll(temp.getOccurs());
				}
				Event e=new Event(name.substring(0,name.length()-2));
				e.setOccur(occ,log);
				result.add(e);
			}
			else { 
				if (set.iterator().hasNext())	result.add(set.iterator().next());
				}
		}
		return result;
	}

	/** trova le clique */
	private void BronKerbosch(Set<Event> r, Set<Event> p, Set<Event> x,
			ArrayList<Set<Event>> out, ConnectionManager cm,
			ArrayList<Event> succ) {
 
		// tutto uguale all'algoritmo base presente in letteratura
		if (p.isEmpty() && x.isEmpty()) {
			out.add(r);
			return;
		}

		Set<Event> oldP = new TreeSet<Event>(p);
		Set<Event> oldX = new TreeSet<Event>(x);

		for (Event e : p) {
			Set<Event> newR = new TreeSet<Event>(r);
			Set<Event> newX = new TreeSet<Event>(oldX);
			Set<Event> newP = new TreeSet<Event>(oldP);
			Set<Event> eNeighbor = neighbor(cm, e, oldP);
			newR.add(e);
			if (eNeighbor != null) {
				newP.retainAll(eNeighbor);
				newX.retainAll(eNeighbor);
			} else {
				newP.clear();
				newX.clear();
			}

			/*
			 * controllo se l'insieme formato � veramente un AND andando a
			 * controllarele tracce
			 */
			if (!isInAnd(newR, cm))
				newR.remove(e);

			BronKerbosch(newR, newP, newX, out, cm, succ);
			oldP.remove(e);
			oldX.add(e);
		}

	}

	/** controlla se gli elementi in set sono in and fra loro */
	private boolean isInAnd(Set<Event> set, ConnectionManager cm) {

		if (set.size() <= 2)
			return true;
		Iterator<Event> it = set.iterator();
		/* crea l'insieme della tracce dell'and */
		Event event1;
		Set<Integer> traces = new TreeSet<Integer>();
		Event event2;
		while (it.hasNext()) {
			Iterator<Event> itInt = set.iterator();
			event1 = it.next();
			while (itInt.hasNext()) {
				event2 = itInt.next();
				if (event2.compareTo(event1) > 0) {
					traces.addAll(cm.getAndTrace(event1, event2));
				}
			}
		}
		
		for (Event e:set){
			double perc=rateOfOccurrences(e,traces);
			//ogni evento deve occorrere in tutte le tracce dell'and
			if (perc<1)
				return false;
		}
		
		return true;
	}

	private double rateOfOccurrences(Event e, Set<Integer> traces) {
		
		int pos=getPos(events, e.getName());
		if (pos!=-1){
			double occ=events.get(pos).numberOccInTraces(traces);
			return occ/traces.size();
		}
		else return 0;
	}

	/** trova i vicini di un evento u nel grafo dei successori */
	private Set<Event> neighbor(ConnectionManager cm, Event u, Set<Event> p) {
		Set<Event> neighbor = cm.getAnd(u.getName());
		if (neighbor!=null)
				neighbor.retainAll(p);
		return neighbor;
	}

	/** controlla se un insieme di eventi � sotto la soglia JOLLY */
	private boolean isUnderThreshold(ArrayList<Event> elim,
			int tot) {
		int count = 0;
		for (Event p : elim)
			count += p.getCount();
		return (count / (double) tot) < settings.getJollyThreshold();
	}

	/**
	 * ritorna l'insieme degli eventi rispettivamente all'insieme dei nomi di
	 * eventi presenti in andEventString
	 */
	private Set<Event> getEvents(Set<String> andEventString,
			ArrayList<Event> events) {
		Set<Event> result = new TreeSet<Event>();

		for (String s : andEventString) {
			int pos = getEventPos(s, events);
			if (pos != -1)
				result.add(events.get(pos));
		}
		return result;
	}

	/**
	 * ritorna le tracce in cui compaiono gli eventi jolly come successori
	 * diretti
	 */
	private Set<Integer> getTraceJolly(Set<Integer> log2,
			ArrayList<Set<Integer>> logs) {
		Set<Integer> result = new TreeSet<Integer>(log2);
		for (Set<Integer> s : logs)
			result.removeAll(s);

		return result;
	}

	/**
	 * ritorna i successori presenti nelle tracce in log2 a partire dalla
	 * posizione index
	 * 
	 * 
	 * **/
	private ArrayList<Event> getSuccessor(Set<Integer> log2,
			int index) {
		
		String ev;
		int pos;
		ArrayList<Event> succ = new ArrayList<Event>();
		for (Integer nTrace : log2) {
			if (index < log.get(nTrace).size()) {
				ev = XLogInfoImpl.NAME_CLASSIFIER.getClassIdentity(log.get(
						nTrace).get(index));
				if ((pos = getPos(succ, ev)) != -1) {
					succ.get(pos).inc(nTrace,index,ev);
				} else {
					Event e = new Event(ev);
					e.inc(nTrace, index,ev);
					insertSuccessors(succ, e);
				}
			}
		}
		return succ;
	}


	/**
	 * ritorna gli eventi poco frequenti
	 * 
	 * **/
	private ArrayList<Event> SuccNotFreq(
			ArrayList<Event> succ, int cont) {
		ArrayList<Event> result = new ArrayList<Event>();

		for (int i = 0; i < succ.size(); i++) {
			if ((succ.get(i).getCount() / (double) cont) < settings.getFrequencyThreshold())
				result.add(succ.get(i));
		}
		return result;
	}

	/**
	 * conta quanti eventi successori ci sono nell'intera lista succ
	 * */
	private int count(ArrayList<Event> succ) {
		int c = 0;
		for (Event s : succ)
			c += s.getCount();
		return c;
	}

	/**
	 * ritorna la lista degli eventi diversi presenti nel log
	 * 
	 */
	private ArrayList<Event> getEvents(Set<Integer> log2, int index) {
		int pos;

		ArrayList<Event> events = new ArrayList<Event>();
		String name;

		// System.out.println("Esamino "+start+" che � in and con "+sameSet);
		if (log2.isEmpty())
			return null;
		/* controllo tutte le tracce per vedere quali sono gli eventi presenti */
		for (Integer i : log2) {
			 //System.out.println("-----nuova traccia -----");
			/* itero su tutti gli eventi della traccia */
			for (int j = index; j < log.get(i).size(); j++) {
				// nome dell'evento
				name = XLogInfoImpl.NAME_CLASSIFIER.getClassIdentity(log.get(i)
						.get(j));
				//System.out.println(i+" "+j+"-"+name+"-"+name.length() );
				if (name.length()>0 && !eventEliminate.contains(name)) {
					// controllo se ho gi� visto l'evento in precedenza
					pos = getEventPos(name, events);
					// se � la prima volta lo inserisco
					if (pos == -1)
						insertEvent(name, i.intValue(), j, events);
					// altrimento incremento il conteggio e registro la traccia
					// in cui ho trovato l'occorrenza
					else
						events.get(pos).inc(i.intValue(), j,name);
					// System.out.println("-"+name);
				}
			}
		}
		return events;
	}


	/** inserimento di un nuovo evento */
	private void insertEvent(String name, int nT, int nE,
			ArrayList<Event> events) {
		int i = 0;
		while (i < events.size() && events.get(i).getName().compareTo(name) < 0) {
			i++;
		}
		//System.out.println(name);
		Event newEvent = new Event(name);
		newEvent.inc(nT, nE,name);
		
		events.add(i, newEvent);
	}

	/**
	 * ritorna la posizione dell'evento di nome name nell'arraylist succ, -1 se
	 * non esiste
	 * */
	private int getPos(ArrayList<Event> succ, String name) {

		int left = 0;
		int right = succ.size() - 1;
		return binarySearch(succ, name, left, right);

	}

	/** ricerca binaria */
	private int binarySearch(ArrayList<Event> succ, String name,
			int left, int right) {
		if (right < left) {
			return -1;
		}

		int mid = (left + right) >>> 1;
		String e = succ.get(mid).getName();
		if (name.compareTo(e) > 0) {
			return binarySearch(succ, name, mid + 1, right);
		} else if (name.compareTo(e) < 0) {
			return binarySearch(succ, name, left, mid - 1);
		} else {
			return mid;
		}
	}

	/** posizione dell'evento se esiste, altrimenti -1* */
	private int getEventPos(String name, ArrayList<Event> events) {

		int left = 0;
		int right = events.size() - 1;
		return binarySearch(name, left, right, events);
	};

	/** ricerca binaria */
	private int binarySearch(String searchValue, int left, int right,
			ArrayList<Event> events) {
		if (right < left) {
			return -1;
		}

		int mid = (left + right) >>> 1;
		if (searchValue.compareTo(events.get(mid).getName()) > 0) {
			return binarySearch(searchValue, mid + 1, right, events);
		} else if (searchValue.compareTo(events.get(mid).getName()) < 0) {
			return binarySearch(searchValue, left, mid - 1, events);
		} else {
			return mid;
		}
	}

	/** inserimento di un nuovo successore */
	private void insertSuccessors(ArrayList<Event> successors,
			Event ev) {
		int i = 0;
		while (i < successors.size()
				&& successors.get(i).getName()
						.compareTo(ev.getName()) < 0) {
			i++;
		}

		successors.add(i, ev);
	}

	/**passo di merge dopo che il modello di processo � stato appreso*/
	
	public GraphAndOr merge() {
		// recupero tutte le foglie
		Set<Node> leafs=new LinkedHashSet<Node>(net.getLeafs());
		//tolgo i duplicati
		//System.out.println("Nodi foglia"+ leafs);
		leafs=removeDuplicate(leafs);
		Iterator<Node> leafsIt=leafs.iterator();
		//ne prendo una per una 
		while (leafsIt.hasNext()){
			Node leaf=leafsIt.next();
			//recupero tutti nodi in cui compare la label di questo nodo
			Set<Node> leafCopy=net.getNodesByLabel(leaf.getLabel());
			Iterator<Node> leafCopyIt = leafCopy.iterator();
			Set<Node> newLeaf=new TreeSet<Node>(leafCopy);
			//tutti i nodi che hanno dei figli diversi da quello in esame non li posso fondere
			while(leafCopyIt.hasNext()){
				Node node = leafCopyIt.next();
				if (!node.equals(leaf)){
					if (!checkDescendants(node,leaf))
						newLeaf.remove(node);
				}
			}
			//insieme di nodi da fondere
			leafCopy=newLeaf;
			if (leafCopy.size()>1){
				//tutti quelli che rimangono li fondo e aggiorno gli archi
				Node newNode=mergeNode(leafCopy);
				//aggiungo i genitori del nuovo nodo creato
				for(DirectedGraphEdgeImpl e:newNode.getInEdge()){
					leafs.add(e.getSource());
				}
				//rimuovo gli archi ai nodi fusi
				leafs.remove(leaf);
				//rimuovo duplicati dall'insieme dei nodi e resetto l'iteratore
				leafs=removeDuplicate(leafs);
				leafsIt=leafs.iterator();
			}
			}	
		return net;
	}
	
	//ritorna vero se hanno gli stessi discendenti, false altrimenti
		private boolean checkDescendants(Node node, Node leaf) {
			//lista degli archi uscenti del nodo 1
			Set<DirectedGraphEdgeImpl> nodeEdge = node.getOutEdge();
			//lista degli archi uscenti del nodo 2
			Set<DirectedGraphEdgeImpl> node2Edge = leaf.getOutEdge();
			//se una lista di archi uscenti � vuota e l'altra � piena ritorno false.
			if (nodeEdge.isEmpty()&&node2Edge.isEmpty())
				return true;
				
			/*mappa in cui viene inserito un nodo appartenente all'insieme dei successori del nodo 1, con valore false.
			 * Quando controllo l'insieme degli archi uscenti del nodo2, controllo che il nodo successore sia nella mappa;
			 * se � presente setto il rispettivo valore a true, altrimenti ritorno false. 		
			 */
			
			Map<String,Boolean> nodeDesc=new TreeMap<String,Boolean>();
			//inserimento in mappa
			for (DirectedGraphEdgeImpl<EventsNode, EventsNode> e:nodeEdge)
				nodeDesc.put(e.getTarget().getLabel(),false);
			//controllo dell'insieme di archi uscenti del secondo nodo
			for (DirectedGraphEdgeImpl<EventsNode, EventsNode> e:node2Edge){
				//se il nodo successore � presente nella mappa setto a true il suo valore
				if (nodeDesc.containsKey(e.getTarget().getLabel()))
					nodeDesc.put(e.getTarget().getLabel(), true);
				//altrimenti ritorno false
				else return false;
			}
			//al termine due nodi hanno gli stessi discendenti se non ci sono valori false nella mappa.
			if (nodeDesc.values().contains(false))
				return false;
			Set<Integer> traces = node.getTraces();
			traces.retainAll(leaf.getTraces());
			return traces.isEmpty();
		}

		//rimuove i nodi con la stessa label lasciandone al massimo 1.
		private Set<Node> removeDuplicate(Set<Node> leafs) {
			Set<String> label=new TreeSet<String>();
			Set<Node> result=new TreeSet<Node>(leafs);
			for(Node e:leafs){
				if (!label.add(e.getLabel()))
					result.remove(e);
			}
			return result;
		}

		
		private EventsNode mergeNode(Set<Node> leafCopy) {
			//iterator sui nodi da fondere
			Iterator<Node> leafCopyIt = leafCopy.iterator();
			//contatore occorrenze
			int occ=0;
			//nuovo evento da creare
			Event newEvent=null;
			//nuova lista delle occorrenze
			ArrayList<Pair<Integer, Integer>> newOcc=new ArrayList<Pair<Integer,Integer>>();
			//per tutti i nodi da fondere estraggo le occorrenze e le aggiungo al nuovo nodo da aggiungere
			while(leafCopyIt.hasNext()){
				Node node=leafCopyIt.next();
				occ+=node.getOccNumber();
				//se non ho ancora creato il nuovo nodo, prendo il nome e lo creo
				if (newEvent==null)
					newEvent=new Event(node.getLabel());
				
					newOcc.addAll(node.getEvent().getOccurs());
					//occ+=e.getCount();
				newEvent.setOccur(newOcc, log);
			}
		
			EventsNode newNode = new EventsNode(newEvent, false, occ);
			//lo aggiungo alla rete
			net.addNode(newNode);
			//ora devo collegare tutti gli archi che andavano nei nodi fusi al nuovo nodo
			Set<Edge> edges = net.getEdges();
			Set<Edge> remove =new TreeSet<Edge>();
			Set<Edge> add =new TreeSet<Edge>();
			for (Edge edge:edges){
				//inserisco un arco in cui il nuovo nodo � la sorgente
				if (leafCopy.contains(edge.getSource())){
					remove.add(edge);
					if (!leafCopy.contains(edge.getTarget()))
						add.add(new Edge(newNode,edge.getTarget(),Double.parseDouble(edge.getLabel())));
					else add.add(new Edge(newNode,newNode,Double.parseDouble(edge.getLabel())));
				}
				//inserisco un arco in cui il nuovo nodo � il destinatario
				if (leafCopy.contains(edge.getTarget())){
					remove.add(edge);
					if (!leafCopy.contains(edge.getSource()))
						add.add(new Edge(edge.getSource(),newNode,Double.parseDouble(edge.getLabel())));
				}
			}
			//rimuovo i vecchi archi 
			for(Edge edge:remove)
				net.removeEdge(edge);
			//aggiungo i nuovi
			for(Edge edge:add){
				net.addEdge(edge);
				edge.getSource().addOutEdge(edge);
				edge.getTarget().addInEdge(edge);
			}
			//ora elimino i nodi fusi
			for (Node e:leafCopy)
				net.removeNode(e);
			return newNode;
		}
}
