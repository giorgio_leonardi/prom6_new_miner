package org.processmining.plugins.myminer.miner;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.processmining.models.andOrGraph.DirectedGraphEdgeImpl;
import org.processmining.models.andOrGraph.Edge;
import org.processmining.models.andOrGraph.Event;
import org.processmining.models.andOrGraph.EventsNode;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.models.graphbased.NodeID;

/*classe che fa il merge di cammini uguali*/
public class MergingStep {

	// rete di partenza
	private GraphAndOr net;

	/* costruttore */
	public MergingStep(GraphAndOr net) {
		this.net = net;
	}

	/** passo di merge dopo che il modello di processo � stato appreso */
	public GraphAndOr merge() {
		// recupero tutte le foglie
		Queue<Node> leafs = new LinkedList<Node>(net.getLeafs());
		// tolgo i duplicati
		// System.out.println("Nodi foglia"+ leafs);
		// leafs=removeDuplicate(leafs);
		Iterator<Node> leafsIt = leafs.iterator();
		// ne prendo una per una
		while (leafsIt.hasNext()) {
			Node leaf = leafsIt.next();
			// insieme delle copie presenti nella lista delle foglie
			Set<Node> newLeaf = retrieveCopy(leaf.getLabel(), leaf, leafs);
			// System.out.println(leaf.getLabel());
			// System.out.println(newLeaf.size());
			if (mergeNode(newLeaf, leafs))
				leafsIt = leafs.iterator();

		}
		return net;
	}

	private Set<Node> retrieveCopy(String label, Node leaf, Queue<Node> q) {
		// recupero tutti nodi in cui compare la label di questo nodo
		Set<Node> leafCopy = net.getNodesByLabel(label);
		Iterator<Node> leafCopyIt = leafCopy.iterator();
		Set<Node> newLeaf = new TreeSet<Node>(leafCopy);
		// tutti i nodi che hanno dei figli diversi da quello in esame non li
		// posso fondere
		while (leafCopyIt.hasNext()) {
			Node node = leafCopyIt.next();
			if (!node.equals(leaf))
				if (!q.contains(node) || !checkDescendants(node, leaf))
					newLeaf.remove(node);

		}
		return newLeaf;
	}

	// ritorna vero se hanno gli stessi discendenti, false altrimenti
	@SuppressWarnings("unchecked")
	private boolean checkDescendants(Node node, Node leaf) {
		// lista degli archi uscenti del nodo 1
		@SuppressWarnings("rawtypes")
		Set<DirectedGraphEdgeImpl> nodeEdge = node.getOutEdge();
		// lista degli archi uscenti del nodo 2
		@SuppressWarnings("rawtypes")
		Set<DirectedGraphEdgeImpl> node2Edge = leaf.getOutEdge();
		// se una lista di archi uscenti � vuota e l'altra � piena ritorno
		// false.
		if ((nodeEdge.isEmpty() && !node2Edge.isEmpty())
				|| (node2Edge.isEmpty() && !nodeEdge.isEmpty()))
			return true;
		/*
		 * mappa in cui viene inserito un nodo appartenente all'insieme dei
		 * successori del nodo 1, con valore false. Quando controllo l'insieme
		 * degli archi uscenti del nodo2, controllo che il nodo successore sia
		 * nella mappa; se � presente setto il rispettivo valore a true,
		 * altrimenti ritorno false.
		 */

		Map<NodeID, Boolean> nodeDesc = new TreeMap<NodeID, Boolean>();
		// inserimento in mappa
		for (DirectedGraphEdgeImpl<EventsNode, EventsNode> e : nodeEdge)
			nodeDesc.put(e.getTarget().getId(), false);
		// controllo dell'insieme di archi uscenti del secondo nodo
		for (DirectedGraphEdgeImpl<EventsNode, EventsNode> e : node2Edge) {
			// se il nodo successore � presente nella mappa setto a true il suo
			// valore
			if (nodeDesc.containsKey(e.getTarget().getId()))
				nodeDesc.replace(e.getTarget().getId(), true);
			// altrimenti ritorno false
			else
				return false;
		}
		// al termine due nodi hanno gli stessi discendenti se non ci sono
		// valori false nella mappa.
		if (nodeDesc.values().contains(false))
			return false;
		Set<Integer> traces = node.getTraces();
		traces.retainAll(leaf.getTraces());
		return traces.isEmpty();
	}

	/*
	 * leafCopy nodi da fondere prossimi nodi da fondere
	 */

	private boolean mergeNode(Set<Node> leafCopy, Queue<Node> leafs) {
		if (leafCopy.size() <= 1)
			return false;
		// iterator sui nodi da fondere
		Iterator<Node> leafCopyIt = leafCopy.iterator();
		// contatore occorrenze
		int occ = 0;
		// nuovo evento da creare
		Event newEvent = null;
		// per tutti i nodi da fondere estraggo le occorrenze e le aggiungo al
		// nuovo nodo da aggiungere
		while (leafCopyIt.hasNext()) {
			Node node = leafCopyIt.next();
			occ += node.getOccNumber();
			// se non ho ancora creato il nuovo nodo, prendo il nome e lo creo
			if (newEvent == null)
				newEvent = new Event(node.getLabel());
			// aggiungo le occorrenze
			for (String s : node.getEvent().getPermutations())
				newEvent.addOccurs((node.getEvent().getOccurs(s)), s);
		}
		// Creo il nuovo nodo da inserire nel grafo
		EventsNode newNode = new EventsNode(newEvent, false, occ);
		// System.out.println(newNode.getLabel());
		// lo aggiungo alla rete
		net.addNode(newNode);
		// ora devo collegare tutti gli archi che andavano nei nodi fusi al
		// nuovo nodo
		Set<Edge> edges = net.getEdges();
		Set<Edge> remove = new TreeSet<Edge>();
		Set<Edge> add = new TreeSet<Edge>();
		for (Edge edge : edges) {
			// inserisco un arco in cui il nuovo nodo � la sorgente
			if (leafCopy.contains(edge.getSource())) {
				remove.add(edge);
				if (!leafCopy.contains(edge.getTarget()))
					add.add(new Edge(newNode, edge.getTarget(), Double
							.parseDouble(edge.getLabel())));
				else
					add.add(new Edge(newNode, newNode, Double.parseDouble(edge
							.getLabel())));
			}
			// inserisco un arco in cui il nuovo nodo � il destinatario
			if (leafCopy.contains(edge.getTarget())) {
				remove.add(edge);
				if (!leafCopy.contains(edge.getSource()))
					add.add(new Edge(edge.getSource(), newNode, Double
							.parseDouble(edge.getLabel())));
			}
		}
		// rimuovo i vecchi archi
		for (Edge edge : remove)
			net.removeEdge(edge);
		// aggiungo i nuovi
		for (Edge edge : add) {
			// System.out.println(edge);
			net.addEdge(edge);
			edge.getSource().addOutEdge(edge);
			edge.getTarget().addInEdge(edge);
		}
		// ora elimino i nodi fusi
		for (Node e : leafCopy)
			net.removeNode(e);

		// aggiungo i genitori del nuovo nodo creato
		for (DirectedGraphEdgeImpl<?, ?> e : newNode.getInEdge()) {
			leafs.add(e.getSource());
		}
		// rimuovo i nodi fusi
		leafs.removeAll(leafCopy);
		// rimuovo duplicati dall'insieme dei nodi
		// leafs=removeDuplicate(leafs);
		return true;
	}

	public Node mergeNode(Set<Node> nodeMerge, String label,boolean isHard) {
		if (nodeMerge.size() == 1)
			return nodeMerge.iterator().next();

		Iterator<Node> it = nodeMerge.iterator();

		// nodo sopravvissuto
		Node newNode=null;
		
		if (!isHard){
			newNode= new EventsNode(new Event(label), false, 0);
			net.addNode(newNode);
		}else{
			newNode = it.next();
			it.remove();
		}
		Set<Edge> remove = new HashSet<Edge>();
		while (it.hasNext()) {
			Node n = it.next();
			it.remove();
			for (DirectedGraphEdgeImpl edge : n.getOutEdge()) {
				// inserisco un arco in cui il nuovo nodo � la sorgente
				remove.add((Edge) edge);
				Edge newEdge = new Edge((EventsNode) newNode, edge.getTarget(),
						Double.parseDouble(edge.getLabel()));
				edge.getTarget().removeInEdge(edge);
				edge.getTarget().addInEdge(newEdge);
				newNode.addOutEdge(newEdge);
				net.addEdge(newEdge);
			}
			// inserisco un arco in cui il nuovo nodo � il destinatario
			for (DirectedGraphEdgeImpl edge : n.getInEdge()) {

				remove.add((Edge) edge);
				Edge newEdge = new Edge(edge.getSource(), (EventsNode) newNode,
						Double.parseDouble(edge.getLabel()));
				edge.getSource().addOutEdge(newEdge);
				edge.getSource().removeOutEdge(edge);
				newNode.addInEdge(newEdge);
				net.addEdge(newEdge);
			}
			net.removeNode(n);
		}
		// rimuovo i vecchi archi
		for (Edge edge : remove) {
			net.removeEdge(edge);
		}

		return newNode;
	}
	
	
	public Set<String>  getSuperSets(Collection<Node> nodes) {
		Map<String,Set<String>> map=new HashMap<String,Set<String>>();
		Set<String> vec;
		//per tutti i nodi nella collezione estraggo i superset presenti nella rete
		for (Node n:nodes){
			Collection<String> events = n.getEvent().getEvents();
			for (String s:events){
				if ((vec=map.get(s))==null)
					vec=new HashSet<String>();
				for (Node nodeInGraph:net.getNodesByEvents(s))
				vec.addAll(nodeInGraph.getEvent().getEvents());
				map.put(s, vec);
			}
		}
		
		Set<Set<String>> b=new HashSet<Set<String>>(map.values());
		
		Set<String> leafs=new LinkedHashSet<String>(b.size());
		
		for(Set<String> s:b){
			String label="";
			for (String event:s){
				label+=event+"&&";
			}
			leafs.add(label.substring(0,label.length()-2));
		}
		return leafs;
		
	}
	
}
