package org.processmining.plugins.myminer.miner;


public class ChAPMaNSettings {

	private static double andThreshold=Settings.AND_THRESHOLD;
	private static double freqThreshold=Settings.FREQ_THRESHOLD;
	private static double jollyThreshold=Settings.JOLLY_THRESHOLD;
	private static int numJolly=Settings.N_JOLLY_THRESHOLD;
	private static double eventThreshold=Settings.EVENT_THRESHOLD;
	
	public double getAndThreshold() {
		// TODO Auto-generated method stub
		return andThreshold;
	}

	

	public int getNumberJolly() {
		return numJolly;
	}

	public void setNumberJolly(int n){
		numJolly=n;
	}
	public double getEventThreshold() {
		return eventThreshold;
	}
	
	public void setEventThreshold(double t) {
		eventThreshold=t;
	}

	public double getFrequencyThreshold() {
		return freqThreshold;
	}

	public void setFrequencyThreshold(double t) {
		freqThreshold=t;
	}
	
	public double getJollyThreshold() {
		return jollyThreshold;
	}

	public void setJollyThreshold(double t) {
		jollyThreshold=t;
	}
	public void setAndThreshold(double t) {
		andThreshold=t;
	}

}
