package org.processmining.plugins.myminer.miner;


import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.models.andOrGraph.GraphAndOr;

public class ChaPMaNFullPlugin2 {
	/** Get user time in nanoseconds. */
	public static long getUserTime() {
		ThreadMXBean bean = ManagementFactory.getThreadMXBean();
		return bean.isCurrentThreadCpuTimeSupported() ? bean
				.getCurrentThreadUserTime() : 0L;
	}

	@Plugin(name = "ChaPMan full 2",
			parameterLabels = {"Log"},
			returnLabels = {"Mined Models"},
			returnTypes = {GraphAndOr.class},
			userAccessible = true,
			help = "ChaPMan")
				
				@UITopiaVariant(affiliation = "UniversitÓ di Torino", author = "Luca Canensi", email = "canensi@di.unito.it")
				public static GraphAndOr mine(UIPluginContext context ,XLog log){
					boolean usertime= true;
					long time1= 0, time2= 0;
					
					if (usertime)
						time1 = getUserTime();
				
					GraphAndOr g=ChAPMaNBasePlugin.mine(context, log);
					MergingStep ms=new MergingStep(g);

					g = ms.merge();
					
					if (usertime) {
						time2 = getUserTime();
						System.out.println("Chapman running time: " + (double)(time2 - time1)/1000000.0 + " milliseconds\n");
					}

					return g;
				}
}
