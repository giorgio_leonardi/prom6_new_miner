package org.processmining.plugins.myminer.miner;

import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.models.andOrGraph.GraphAndOr;

public class ChAPMaNMergedPlugin {

	@Plugin(name = "ChaPMan merging step",
			parameterLabels = {"Grafo ottenuto da ChAPMaN base"},
			returnLabels = {"Merged Models"},
			returnTypes = {GraphAndOr.class},
			userAccessible = true,
			help = "ChaPMan merged")
				
				@UITopiaVariant(affiliation = "UniversitÓ di Torino", author = "Luca Canensi", email = "canensi@di.unito.it")
				public static GraphAndOr mine(PluginContext context ,GraphAndOr graph){
					
					MergingStep m=new MergingStep(graph);
					GraphAndOr g=m.merge();
					return g;
					
				}
			}

