package org.processmining.plugins.myminer.miner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.models.andOrGraph.Event;
import org.processmining.models.andOrGraph.Pair;

public class ConnectionManager {
	
	//per ogni evento memorizzo l'insieme di eventi in and
	private Map<String,Set<Event>> xorAnd;
	//per ogni evento memorizzo l'insieme di eventi in relazione di successione
	private Map<String,Set<Event>> successione;
	//per ogni coppia di eventi memorizzo il valore di dipendenza calcolato
	private Map<String,Double> dep; 
	//log
	private XLog log;
	//lista delle tracce in cui compare una relazione di dipendenza diretta
	private Map<String,ArrayList<Integer>> traceDep;
	
	private Set<Integer> traces;
	
	private int index;
	
	/**costruttore*/
	public ConnectionManager(XLog log) {
		xorAnd = new TreeMap<String,Set<Event>>();
		successione = new TreeMap<String,Set<Event>>();
		traceDep=new TreeMap<String, ArrayList<Integer>>();
		dep=new TreeMap<String,Double>();
		this.log=log;
	}

	/**valuta il tipo di relazione fra e1 e e2*/
	public void evaluateConnection(Event e1,Event e2)
	{
		double depDir,depInv;
		ArrayList<Integer> traceListDir=new ArrayList<Integer>();
		ArrayList<Integer> traceListInv=new ArrayList<Integer>();
		//dipendenza da s1 a s2
		depDir=evaluateDependency(e1,e2,traceListDir);
		//dipendenza da s2 a s1
		depInv=evaluateDependency(e2,e1,traceListInv);
		
		//System.out.println(e1.getName()+"->"+e2.getName()+": "+depDir);
		//System.out.println(e2.getName()+"->"+e1.getName()+": "+depInv);
		
		//salvo la dipendenza calcolata
		dep.put(e1.getName()+"->"+e2.getName(), depDir);
		dep.put(e2.getName()+"->"+e1.getName(), depInv);
		//memorizzo l'insieme delle tracce
		traceDep.put(e1.getName()+"->"+e2.getName(), traceListDir);
		traceDep.put(e2.getName()+"->"+e1.getName(), traceListInv);
		
		//entrambe sopra soglia: AND
		if (depDir>=Settings.AND_THRESHOLD && depInv>=Settings.AND_THRESHOLD)
		{
			if (Math.abs(depDir-depInv)<=Settings.DEP_THRESHOLD){
				Set<Event> s= xorAnd.get(e1.getName());
				//controllo se gi� gli insiemi esistono altrimenti li devo creare
				if (s==null)
					s=new TreeSet<Event>();
				s.add(e2);
				xorAnd.put(e1.getName(),s);
	
				s= xorAnd.get(e2.getName());
				if (s==null)
					s=new TreeSet<Event>();
				s.add(e1);
				xorAnd.put(e2.getName(),s);
			
				
			//	System.out.println("I due eventi sono in AND");
			}
			else {
				//System.out.println("sono in successione, perch� sopra soglia ma differenti");
				//relazione di successione diretta
				if (depDir>depInv){
					Set<Event> s= successione.get(e1.getName());
					if (s==null)
						s=new TreeSet<Event>();
					s.add(e2);
					successione.put(e1.getName(),s);
				}
				//relazione di successione inversa
				else {
					Set<Event> s= successione.get(e2.getName());
					if (s==null)
						s=new TreeSet<Event>();
					s.add(e1);
					successione.put(e2.getName(),s);
				}
			}
		}
			
		//una sola sopra soglia: SUCCESSIONE
		//diretta
		if (depInv<Settings.AND_THRESHOLD)
			if (depDir>=Settings.AND_THRESHOLD){
				Set<Event> s= successione.get(e2.getName());
				if (s==null)
					s=new TreeSet<Event>();
				s.add(e2);
				successione.put(e1.getName(),s);
			//	System.out.println("I due eventi sono in successione "+e1+"->"+e2);
			}
		//inversa
		if (depDir<Settings.AND_THRESHOLD)
			if (depInv>=Settings.AND_THRESHOLD)
			{
				Set<Event> s= successione.get(e2.getName());
				if (s==null)
					s=new TreeSet<Event>();
				s.add(e1);
				successione.put(e2.getName(),s);
				
				//System.out.println("I due eventi sono in successione"+e2+"->"+e1);
			}
		//entrambe sotto soglia: XOR
		if (depDir<Settings.AND_THRESHOLD && depInv<Settings.AND_THRESHOLD)
		{
		//	System.out.println("I due eventi sono in XOR");
		}
		
	}
	
	/**valuta la dipendenza */
	private double evaluateDependency(Event e1, Event e2, ArrayList<Integer> traceList) {
		//occorrenze del primo evento
		ArrayList<Pair<Integer, Integer>> occS1 = e1.getOccurs();
		//occorrenze del secondo evento
		ArrayList<Pair<Integer, Integer>> occS2 = e2.getOccurs();

		double succS1 = 0;//successori del primo evento
		double precS2 = 0;//predecessori del secondo evento
		int countDep = 0;//primo evento seguito da secondo evento
		//controllo tutte le occorrenze del primo evento
		for (Pair<Integer, Integer> o : occS1) {
			//tracce in cui occorre
			XTrace trace = log.get(o.getFirst());
			//incremento i successori
			if (trace.size() != o.getSecond() + 1) {
				succS1++;
				//se il successore � il secondo evento lo conto
				String name = XLogInfoImpl.NAME_CLASSIFIER.getClassIdentity(trace.get(o.getSecond() + 1));
				if (name.equals(e2.getName())) {
					countDep++;
					traceList.add(o.getFirst());
				}
			}
		}

		//controllo i predecessori del secondo evento
		for (Pair<Integer, Integer> o : occS2) {
			if (o.getSecond() != 0)
				precS2++;
		}

		//formula fuzzy miner
		return 0.5 * (countDep / succS1 + countDep / (countDep+precS2));

	}
	
	private ArrayList<Pair<Integer, Integer>> getOccurs(String e1) {
		ArrayList<Pair<Integer, Integer>> occ=new ArrayList<Pair<Integer, Integer>>();
		
		for (Integer i:traces){
			for (int j=index;j<log.get(i).size();j++)
			{
				String name = XLogInfoImpl.NAME_CLASSIFIER.getClassIdentity(log.get(i).get(j));
				if (name.equals(e1)){
					occ.add(new Pair<Integer, Integer>(i, j));
					break;
				}
			}
		}
		return occ;
	}

	/**torna l'insieme andXor*/
	public Map<String,Set<Event>> getConnection(){
		return xorAnd;
		
	}
	
	/**torna le tracce*/
	public Map<String,ArrayList<Integer>> getTrace (){
		return traceDep;
	}

	/**ritorna l'insieme degli eventi in and con e*/
	public Set<Event> getAnd(String e) {
		try {
			Set<Event> res= new TreeSet<Event>();
			if (xorAnd.get(e)!=null){
				for (Event ev:xorAnd.get(e))
					res.add((Event) ev.clone());
			
				return res;
			}
			return null;
		} catch (CloneNotSupportedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return null;
			}
	}

	/**ritorna l'insieme delle tracce in cui compaiono e ed e2 in relazione di successione diretta*/
	public ArrayList<Integer> getTrace(Event e, Event e2) {
		
		return traceDep.get(e.getName()+"->"+e2.getName());
	}

	/**lista delle tracce in cui compare l'and fra e ed e2*/
	public ArrayList<Integer> getAndTrace(Event e, Event e2) {
		ArrayList<Integer> dir=getTrace(e,e2);
		ArrayList<Integer> inv=getTrace(e2,e);
		@SuppressWarnings("unchecked")
		ArrayList<Integer> tot=(ArrayList<Integer>)dir.clone();
		tot.addAll(inv);
		return tot;
	}
	
	/**ritorna l'insieme delle tracce in cui compare l'evento e*/
	public ArrayList<Integer> getTrace(Event e){
		ArrayList<Pair<Integer,Integer>> occ=e.getOccurs();
		ArrayList<Integer> result=new ArrayList<Integer>();
		for (Pair<Integer, Integer > p:occ)
		{
			result.add(p.getFirst());
		}
		
		return result;
	}
	
	/**ritorna la dipendenza calcolata in precedenza*/
	public double getDependency(Event a, Event b){
		Double d=dep.get(a.getName()+"->"+b.getName());
		if (d==null)
			return 0.0;
		else return d.doubleValue();
	}

	/**ritorna l'insieme degli eventi in relazione di successione con s*/
	public Set<Event> getSuccessor(String s) {
		// TODO Auto-generated method stub
		return successione.get(s);
	}

	public void setTraces(Set<Integer> log2, int i) {
		// TODO Auto-generated method stub
		traces=log2;
		index=i;
	}
}