package org.processmining.plugins.myminer.miner;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.fluxicon.slickerbox.components.NiceIntegerSlider;
import com.fluxicon.slickerbox.components.NiceSlider.Orientation;
import com.fluxicon.slickerbox.factory.SlickerDecorator;
import com.fluxicon.slickerbox.factory.SlickerFactory;



public class ParametersPanel extends JPanel{


	/**
	 * 
	 */
	private static final long serialVersionUID = -413716823803599197L;
	private ChAPMaNSettings settings;
	private JPanel thresholdsPanel;
	private JLabel thresholdTitle;
	private NiceIntegerSlider t6;
	private JSlider eventSlider;
	private JSlider frequencySlider;
	private JSlider ANDSlider;
	private JSlider JollySlider;
	private JLabel l1;
	private JLabel l2;
	private JLabel l3;
	private JLabel l4;
	private JLabel l5;

	
	
	public ParametersPanel() {
		
		this.settings = new ChAPMaNSettings();
		this.init();
	}
	
	private void init(){

		SlickerFactory factory = SlickerFactory.instance();
		this.thresholdsPanel = factory.createRoundedPanel(15, Color.LIGHT_GRAY);
	
		
		this.thresholdTitle = factory.createLabel("Thresholds");
		this.thresholdTitle.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 18));
		this.thresholdTitle.setForeground(new Color(40,40,40));
		
		
		
		this.t6 = factory.createNiceIntegerSlider("", 0, 10, this.settings.getNumberJolly(), Orientation.HORIZONTAL);
		
		
		FlowLayout customLayout = new FlowLayout(FlowLayout.LEFT);
		customLayout.setHgap(2);
		int customPanelWidth = 405;
		JPanel doubleSliderPanel1 =  new JPanel(customLayout);
		doubleSliderPanel1.setBackground(Color.lightGray);
		JPanel doubleSliderPanel2 =  new JPanel(customLayout);
		doubleSliderPanel2.setBackground(Color.lightGray);
		JPanel doubleSliderPanel3 =  new JPanel(customLayout);
		doubleSliderPanel3.setBackground(Color.lightGray);
		JPanel doubleSliderPanel4 =  new JPanel(customLayout);
		doubleSliderPanel4.setBackground(Color.lightGray);
		JPanel doubleSliderPanel5 =  new JPanel(customLayout);
		doubleSliderPanel5.setBackground(Color.lightGray);
		
		final JLabel eventSliderLabel = new JLabel(this.settings.getEventThreshold()+"");
		eventSliderLabel.setPreferredSize(new Dimension(60, 20));
		final JLabel frequencySliderLabel = new JLabel(this.settings.getFrequencyThreshold()+"");
		frequencySliderLabel.setPreferredSize(new Dimension(60, 20));
		final JLabel ANDSliderLabel = new JLabel(this.settings.getAndThreshold()+"");
		ANDSliderLabel.setPreferredSize(new Dimension(60, 20));
		final JLabel JollySliderLabel = new JLabel(this.settings.getJollyThreshold()+"");
		JollySliderLabel.setPreferredSize(new Dimension(60, 20));
	
		eventSlider = new JSlider(0, 100, (int)(settings.getEventThreshold()*100));
		eventSlider.setPreferredSize(new Dimension(customPanelWidth - 60, 20));
		eventSlider.setBackground(Color.lightGray);
		eventSlider.addChangeListener(new ChangeListener() {
			
			public void stateChanged(ChangeEvent e) {
				String labelStr = String.valueOf(eventSlider.getValue()/100.0);
				eventSliderLabel.setText(labelStr);
				settings.setEventThreshold(eventSlider.getValue()/100.0);
			}
		});
		frequencySlider = new JSlider(0, 100, (int)(settings.getFrequencyThreshold()*100));
		frequencySlider.setPreferredSize(new Dimension(customPanelWidth - 60, 20));
		frequencySlider.setBackground(Color.lightGray);
		frequencySlider.addChangeListener(new ChangeListener() {
			
			public void stateChanged(ChangeEvent e) {
				String labelStr = String.valueOf(frequencySlider.getValue()/100.0);
				frequencySliderLabel.setText(labelStr);
				settings.setFrequencyThreshold(frequencySlider.getValue()/100.0);
			}
		});
		ANDSlider = new JSlider(0, 100, (int)(settings.getAndThreshold()*100));
		ANDSlider.setPreferredSize(new Dimension(customPanelWidth - 60, 20));
		ANDSlider.setBackground(Color.gray);
		ANDSlider.addChangeListener(new ChangeListener() {
			
			public void stateChanged(ChangeEvent e) {
				String labelStr = String.valueOf(ANDSlider.getValue()/100.0);
				ANDSliderLabel.setText(labelStr);
				settings.setAndThreshold(ANDSlider.getValue()/100.0);
			}
		});
		JollySlider = new JSlider(0, 100, (int)(settings.getJollyThreshold()*100));
		JollySlider.setPreferredSize(new Dimension(customPanelWidth - 60, 20));
		JollySlider.setBackground(Color.gray);
		JollySlider.addChangeListener(new ChangeListener() {
			
			public void stateChanged(ChangeEvent e) {
				String labelStr = String.valueOf(JollySlider.getValue()/100.0);
				JollySliderLabel.setText(labelStr);
				settings.setJollyThreshold(JollySlider.getValue()/100.0);
			}
		});
		
		doubleSliderPanel1.add(eventSlider);
		doubleSliderPanel1.add(eventSliderLabel);
		doubleSliderPanel2.add(frequencySlider);
		doubleSliderPanel2.add(frequencySliderLabel);
		doubleSliderPanel3.add(ANDSlider);
		doubleSliderPanel3.add(ANDSliderLabel);
		doubleSliderPanel4.add(JollySlider);
		doubleSliderPanel4.add(JollySliderLabel);
		
		SlickerDecorator.instance().decorate(eventSlider);
		SlickerDecorator.instance().decorate(eventSliderLabel);
		eventSliderLabel.setForeground(new Color(50,50,50));
		SlickerDecorator.instance().decorate(frequencySlider);
		SlickerDecorator.instance().decorate(frequencySliderLabel);
		frequencySliderLabel.setForeground(new Color(50,50,50));
		SlickerDecorator.instance().decorate(ANDSlider);
		SlickerDecorator.instance().decorate(ANDSliderLabel);
		ANDSliderLabel.setForeground(new Color(50,50,50));
		SlickerDecorator.instance().decorate(JollySlider);
		SlickerDecorator.instance().decorate(JollySliderLabel);
		JollySliderLabel.setForeground(new Color(50,50,50));
		
		this.l1 = factory.createLabel("Event Threshold:");
		this.l1.setHorizontalAlignment(SwingConstants.RIGHT);
		this.l1.setForeground(new Color(40,40,40));
		this.l2 = factory.createLabel("Frequency Threshold:");
		this.l2.setHorizontalAlignment(SwingConstants.RIGHT);
		this.l2.setForeground(new Color(40,40,40));
		this.l3 = factory.createLabel("AND threshold");
		this.l3.setHorizontalAlignment(SwingConstants.RIGHT);
		this.l3.setForeground(new Color(40,40,40));
		this.l4 = factory.createLabel("Jolly Threshold:");
		this.l4.setHorizontalAlignment(SwingConstants.RIGHT);
		this.l4.setForeground(new Color(40,40,40));
		this.l5 = factory.createLabel("Jolly Number:");
		this.l5.setHorizontalAlignment(SwingConstants.RIGHT);
		this.l5.setForeground(new Color(40,40,40));
		
						
		this.thresholdsPanel.setLayout(null);
		this.thresholdsPanel.add(this.thresholdTitle);
		this.thresholdsPanel.add(this.l1);
		//this.thresholdsPanel.add(this.t1);
		this.thresholdsPanel.add(doubleSliderPanel1);
		this.thresholdsPanel.add(this.l2);
		//this.thresholdsPanel.add(this.t2);
		this.thresholdsPanel.add(doubleSliderPanel2);
		this.thresholdsPanel.add(this.l3);
		//this.thresholdsPanel.add(this.t3);
		this.thresholdsPanel.add(doubleSliderPanel3);
		this.thresholdsPanel.add(this.l4);
		//this.thresholdsPanel.add(this.t4);
		this.thresholdsPanel.add(doubleSliderPanel4);
		this.thresholdsPanel.add(this.l5);
		
		this.thresholdsPanel.add(this.t6);
		
		this.thresholdsPanel.setBounds(0, 50, 520, 240);
		this.thresholdTitle.setBounds(10, 10, 200, 30);
		this.l1.setBounds(25, 50, 100, 20);
		this.l2.setBounds(25, 80, 100, 20);
		this.l3.setBounds(20, 110, 105, 20);
		this.l4.setBounds(20, 140, 105, 20);
		this.l5.setBounds(25, 170, 100, 20);
		doubleSliderPanel1.setBounds(122, 45, customPanelWidth+20, 25);
		doubleSliderPanel2.setBounds(122, 75, customPanelWidth+20, 25);
		doubleSliderPanel3.setBounds(122, 105, customPanelWidth+20, 25);
		doubleSliderPanel4.setBounds(122, 135, customPanelWidth+20, 25);
		this.t6.setBounds(122, 165, 360, 20);
		
				
		this.setLayout(null);
		this.add(this.thresholdsPanel);
		
		this.validate();
		this.repaint();
	}

	public ChAPMaNSettings getSettings() {
		return settings;
	}

	
}
