package org.processmining.plugins.myminer.miner;

public class Settings {

	protected final static double EVENT_THRESHOLD=0.00;//soglia sulla frequenza degli eventi
	protected final static double AND_THRESHOLD=0.1;//0.1 soglia usata nella formula per la scoperta dell'and
	protected static final double DEP_THRESHOLD = 1;//soglia usata nella differenza fra eventi sopra la soglia AND_THRESHOLD
	public static final double FREQ_THRESHOLD = 0.1;//0.1soglia sulla frequenza dei successori 
	public static final double THRESHOLD = 0.3;//0.3soglia sul rapporto fra successori in relazione di successione
	public static final double JOLLY_THRESHOLD = 0.25;
	public static final double N_ARY_THRESHOLD = 0.1;
	public static final int N_JOLLY_THRESHOLD =0;
}
