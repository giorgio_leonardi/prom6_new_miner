package org.processmining.plugins.myminer.miner;

import java.util.ArrayList;
import java.util.HashMap;

public class LogsMap {
	
	private static HashMap<Integer, ArrayList<String>> logsMap;
	
	public static void init() {
		logsMap = new HashMap<>();
	}
	
	public static boolean put(Integer code, ArrayList<String> content) {
		ArrayList<String> value = logsMap.get(code);
		
		if(value != null)
			return false;
		
		logsMap.put(code, content);
		
		return true;
	}
	
	public static ArrayList<String> get(Integer code) {
		return logsMap.get(code);
	}
	
	@Override
	public String toString() {
		return logsMap.toString();
	}

}
