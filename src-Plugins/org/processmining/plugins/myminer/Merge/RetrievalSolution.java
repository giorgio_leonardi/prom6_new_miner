package org.processmining.plugins.myminer.Merge;

import java.util.Vector;

import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.models.graphbased.NodeID;
import org.processmining.plugins.myminer.index.IndexTable;
import org.processmining.plugins.myminer.index.Match;

public class RetrievalSolution {
	private Vector<IndexTable> sol;

	public RetrievalSolution(){
		sol=new Vector<IndexTable>();
	}
	
	public Vector<IndexTable> getSol() {
		return sol;
	}

	public void setSol(Vector<IndexTable> sol) {
		this.sol = sol;
	}

	public void removeDuplicates() {
		RetrievalSolution copy;
		copy = new RetrievalSolution();
		int i=0;
		int j;
		for (i=0;i<sol.size()-1;i++){
			for (j=i+1;j<sol.size();j++){
				if (sol.get(i).samePath(sol.get(j)))
					break;
			}
			if (j==sol.size())
				copy.sol.add(sol.get(i));
		}
		copy.sol.add(sol.get(sol.size()-1));
		sol=copy.sol;
	}
	
	public RetrievalSolution clone(){
		RetrievalSolution copy=new RetrievalSolution();
		copy.sol=new Vector<IndexTable>(this.sol.size());
		for (int i=0;i<this.sol.size();i++){
			copy.sol.add(i,(IndexTable) sol.get(i).clone());
		}
		return copy;
		
	}
	
	public String toString(){
		String s="";
		for (IndexTable it:sol)
			s=s+"\n"+it.toString();
		return s;
	}

	public void reWrite(GraphAndOr g) {
		for (IndexTable it:sol){
			for (Match m:it.getMatches()){
				NodeID id=m.getNode().getId();
				Node n=g.getCopy(id);
				if (n!=null)
					m.setNode(n);
			}
		}
	}
	
}
