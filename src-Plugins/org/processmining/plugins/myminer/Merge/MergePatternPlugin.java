package org.processmining.plugins.myminer.Merge;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.plugins.myminer.traceRetrieval.TraceRetrieval;
public class MergePatternPlugin{
@Plugin(name = "ChaPMan merge pattern",
parameterLabels = {"Grafo ottenuto da ChAPMaN ","Retrieval Solution"},
returnLabels = {"Merged Models"},
returnTypes = {GraphAndOr.class},
userAccessible = true,
help = "ChaPMan merged")
	
@UITopiaVariant(affiliation = "UniversitÓ di Torino", author = "Luca Canensi", email = "canensi@di.unito.it")
	public static GraphAndOr merge(UIPluginContext context ,GraphAndOr graph,RetrievalSolution sol) {
		ParametersMergePanel parameters = new ParametersMergePanel();
		InteractionResult intRs = context.showConfiguration("", parameters);
		if (intRs.equals(InteractionResult.CANCEL))
		{
			context.getFutureResult(0).cancel(true);
		}
		
		RetrievalSolution curr=sol.clone();
		curr.removeDuplicates();
		
		GraphAndOr g=graph.clone();
		graph.printNodesID();
		System.out.println("clone");
		g.printNodesID();
		curr.reWrite(g);
		g.reset();
		
		if (parameters.isStrong())
			TraceRetrieval.mergeStrong(curr.getSol(), g);
		else TraceRetrieval.mergeWeak(curr.getSol(), g);
		g.simplifies();
		
	return g;
}
}