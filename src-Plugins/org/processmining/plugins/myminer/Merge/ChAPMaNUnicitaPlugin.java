package org.processmining.plugins.myminer.Merge;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.models.andOrGraph.DirectedGraphEdgeImpl;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.models.graphbased.NodeID;
import org.processmining.plugins.myminer.miner.MergingStep;

public class ChAPMaNUnicitaPlugin {

	@Plugin(name = "ChaPMan unicit� degli eventi", parameterLabels = { "Modello ottenuto da un esecuzione del miner ChAPMaN" }, returnLabels = { "Generalized Model" }, returnTypes = { GraphAndOr.class }, userAccessible = true, help = "ChaPMan unicit�")
	@UITopiaVariant(affiliation = "Universit� di Torino", author = "Luca Canensi", email = "canensi@di.unito.it")
	/*public static GraphAndOr mine(UIPluginContext context, GraphAndOr graph)
			throws IOException {
		ParametersUniqPanel parameters = new ParametersUniqPanel();
		InteractionResult intRs = context.showConfiguration("", parameters);
		if (intRs.equals(InteractionResult.CANCEL))
		{
			context.getFutureResult(0).cancel(true);
		}
		Collection<Node> nodes = graph.getUniqueNode(parameters.isHard());
		// System.out.println(nodes);
		Iterator<Node> it = nodes.iterator();
		while (it.hasNext()) {
			GraphAndOr subgraph = new GraphAndOr("", true);
			Node n = it.next();
			subgraph.addNode(n);

			MergingStep m = new MergingStep(graph);
			// creo l'oggetto che si occuper� di ricercare gli isomorfismi
			VF2IsomorphismTester test = new VF2IsomorphismTester();
			// risultato
			ArrayList<Map<NodeID, NodeID>> result = test.findIsomorphism(graph,
					subgraph, parameters.isHard());
			System.out.println("Elementi da fondere: " + result.size());
			// foglie del sottografo da cui devo iniziare a fondere
			Set<Node> leafs = subgraph.getLeafs();
			System.out.println(leafs);
			Iterator<Node> leafsIt = leafs.iterator();
			// per ogni nodo del sottografo partendo dalle foglie
			while (leafsIt.hasNext()) {
				// nodo da fondere
				Node n1 = leafsIt.next();
				// cerco il nodo nel risultato dell'isomorfismo
				Set<Node> nodeMerge = getNodeToMerge(result, n1.getId(), graph);
				// fondo tutti i nodi trovati
				m.mergeNode(nodeMerge);
				// inserisco nei nodi da fondere i genitori del nodo fuso nel
				// sottografo
				for (DirectedGraphEdgeImpl<?, ?> edge : n1.getInEdge()) {
					leafs.add(edge.getSource());
				}
				// elimino il nodo esaminato dalla lista dei nodi da esaminare
				leafs.remove(n);
				// ricostruisco l'iteratore
				leafsIt = leafs.iterator();
			}

		}
		graph.setAnnotated(false);
		return graph;
	}
*/
	public static GraphAndOr merge(UIPluginContext context, GraphAndOr g)throws IOException {
		ParametersUniqPanel parameters = new ParametersUniqPanel();
		InteractionResult intRs = context.showConfiguration("", parameters);
		if (intRs.equals(InteractionResult.CANCEL))
		{
			context.getFutureResult(0).cancel(true);
		}
		GraphAndOr graph = g.clone();
		MergingStep m = new MergingStep(graph);
		// foglie 
		Set<Node> leafs = graph.getLeafs();
		// insieme dei nodi da esaminare
		Set<String> s=new LinkedHashSet<String>();
		Set<String> examined=new TreeSet<String>();
		//lo riempo con le foglie
		for (Node n:leafs)
				 s.add(n.getLabel());
		//se devo fare un merge soft costruisco i superset
		if (!parameters.isHard())
			s=m.getSuperSets(leafs);
		Iterator<String> it=s.iterator();
		while (it.hasNext())
		{
			//estraggo un elemento di cui fare il merge
			String node=it.next();
			it.remove();
			
			//nodi da mergiare
			Set<Node> copy ;
			if (parameters.isHard())
				copy = graph.getNodesByLabel(node);	
			else copy=graph.getNodesByEvents(node);
			
			Node newNode=m.mergeNode(copy,node,parameters.isHard());
			
			for (DirectedGraphEdgeImpl<?, ?> edge : newNode.getInEdge()) {
				if (!isAlreadyChecked(s,examined,edge.getSource().getLabel(),parameters.isHard())){
					Set<Node> temp=new HashSet<Node>();
					temp.add(edge.getSource());
					s.addAll(m.getSuperSets(temp));
				}
			}
			examined.add(node);
			it=s.iterator();
		}
	return graph;
	}
	
	
	
	



	public static boolean isAlreadyChecked(Set<String> s,
			Set<String> examined, String label, boolean hard) {
		Set<String> temp=new HashSet<String>();
		if (!hard){
			for (String aus:s){
				temp.addAll(Arrays.asList(aus.split("&&")));
			}
			for (String aus:examined){
				temp.addAll(Arrays.asList(aus.split("&&")));
			}
			int i=0;
			String[] aus = label.split("&&");
			while (i<aus.length && !temp.contains(aus[i]))i++;
			return i!=aus.length;
		}else{
		     return examined.contains(label) || s.contains(label);
		}
	}



	private static Set<Node> getNodeToMerge(
			ArrayList<Map<NodeID, NodeID>> result, NodeID nodeID,
			GraphAndOr graph) {
		Set<Node> nodes = new HashSet<Node>();

		for (Map<NodeID, NodeID> m : result) {
			NodeID id = m.get(nodeID);
			Node n = graph.getNodeByID(id);
			if (n != null)
				nodes.add(n);
			else
				return null;
		}
		return nodes;
	}
}
