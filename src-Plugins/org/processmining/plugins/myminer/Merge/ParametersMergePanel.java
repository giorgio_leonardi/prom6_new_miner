package org.processmining.plugins.myminer.Merge;

import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.fluxicon.slickerbox.factory.SlickerDecorator;
import com.fluxicon.slickerbox.factory.SlickerFactory;

public class ParametersMergePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4890241192154353732L;

	private boolean strong=true;
	private boolean weak=false;
	private JPanel panel;

	private JLabel title;
	private JRadioButton strong_rb;
	private JRadioButton weak_rb;
	
	private ButtonGroup bg;
	
	private JLabel strong_lbl;
	private JLabel weak_lbl;
	
	public ParametersMergePanel(){
		SlickerFactory factory = SlickerFactory.instance();
		this.panel = factory.createRoundedPanel(10, Color.LIGHT_GRAY);
	
		
		this.title = factory.createLabel("Tipo merge");
		this.title.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 18));
		this.title.setForeground(new Color(40,40,40));

		
		this.panel.setLayout(null);
		this.panel.add(this.title);

		this.panel.setBounds(0, 50, 420, 180);
		this.title.setBounds(10, 10, 200, 30);

		strong_rb=new JRadioButton("Strong");
		strong_rb.setBackground(Color.LIGHT_GRAY);
		strong_rb.setSelected(strong);
		
		
		weak_rb=new JRadioButton("Weak");
		weak_rb.setBackground(Color.LIGHT_GRAY);
		weak_rb.setSelected(weak);
		
		bg=new ButtonGroup();
		bg.add(strong_rb);
		bg.add(weak_rb);
		
		strong_rb.addChangeListener(new ChangeListener() {
	
	@Override
	public void stateChanged(ChangeEvent e) {
		strong=!strong;
	}
});
		
		weak_rb.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				weak=!weak;
			}
		});
		
		strong_lbl = factory.createLabel("Strong merge");
		strong_lbl.setHorizontalAlignment(SwingConstants.RIGHT);
		strong_lbl.setForeground(new Color(40,40,40));
		
		weak_lbl = factory.createLabel("Weak merge");
		weak_lbl.setHorizontalAlignment(SwingConstants.RIGHT);
		weak_lbl.setForeground(new Color(40,40,40));
		
		SlickerDecorator.instance().decorate(strong_rb);
		panel.add(strong_rb);
		panel.add(strong_lbl);
		SlickerDecorator.instance().decorate(weak_rb);
		panel.add(weak_rb);
		panel.add(weak_lbl);
		this.setLayout(null);
		this.add(this.panel);
		strong_rb.setBounds(15, 40, 25, 20);
		strong_lbl.setBounds(20,40,160,20);
		weak_rb.setBounds(15, 80, 25, 20);
		weak_lbl.setBounds(20,80,160,20);
		
		this.validate();
		this.repaint();

	}
	
	public boolean isStrong(){
		return strong;
	}
	public boolean isWeak(){
		return weak;
	}
}
