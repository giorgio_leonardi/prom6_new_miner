package org.processmining.plugins.myminer.simOntology;


import java.util.ArrayList;

import org.processmining.models.andOrGraph.GraphAndOr;



public class Node {
	
	public static final int WIDTH = 50;
	public static final int HEIGHT = 50;
	private int x;
	private int y;
	private int id;
	private boolean select;
	private int fatherX;
	private int fatherY;
	private boolean isInsert;
	private int idFather;
	private ArrayList<String[]> table;
	private GraphAndOr graph;

	public Node(int x, int y,int id){
		
		this.x = x;
		this.y = y;
		this.fatherX = 0;
		this.fatherY = 0;
		this.id = id;
		this.select = false;
		this.isInsert = false;
		this.table = null;
		this.graph = null;
	}
	

	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getFatherX() {
		return fatherX;
	}
	
	public void setFatherX(int fatherX) {
		this.fatherX = fatherX;
	}
	
	public int getFatherY() {
		return fatherY;
	}
	
	public void setFatherY(int fatherY) {
		this.fatherY = fatherY;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public boolean getSelect() {
		return select;
	}
	
	public void setSelect() {
		this.select = true;
	}
	
	public void setUnselect() {
		this.select = false;
	}
	public boolean getIsInsert() {
		return isInsert;
	}
	
	public void setIsInsert() {
		this.isInsert = true;
	}
	
	public int getIdFather() {
		return idFather;
	}
	
	public void setIdFather(int idFather) {
		this.idFather = idFather;
	}
	
	public void setTable(ArrayList<String[]> table) {
		this.table = table;
	}
	
	public ArrayList<String[]> getTable() {
		return table;
	}


	public void setGraph(GraphAndOr graph) {
		this.graph = graph;
		
	}
	
	public GraphAndOr getGraph() {
		return graph;
	}
	
}
