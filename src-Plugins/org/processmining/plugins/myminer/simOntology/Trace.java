package org.processmining.plugins.myminer.simOntology;

import java.util.ArrayList;

import org.processmining.plugins.myminer.miner.LogsMap;

public class Trace {
	
	private Integer code;
	private ArrayList<String> content;
	
	public Trace(Integer code) {
		this.code = code;
		content = LogsMap.get(code);		
	}
	
	@Override
	public String toString() {
		return content.toString();
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public ArrayList<String> getContent() {
		return content;
	}

	public void setContent(ArrayList<String> content) {
		this.content = content;
	}

}
