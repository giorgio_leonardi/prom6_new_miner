package org.processmining.plugins.myminer.simOntology;

import java.util.Vector;

import org.processmining.plugins.myminer.index.Match;

public class Path {
	
	private Vector<Match> path;
	
	public Path(Vector<Match> path) {
		this.path = path;
	}
	
	@Override
	public String toString() {
		return path.toString(); 
	}

}
