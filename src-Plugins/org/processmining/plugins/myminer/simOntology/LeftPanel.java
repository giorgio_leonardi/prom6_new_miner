package org.processmining.plugins.myminer.simOntology;

import java.awt.Color;

import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

public class LeftPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	
	//Relazione tra Id e Nodo
	public static HashMap<Integer, Node> hmNode;
	
	//Memorizza la " forma " dei nodi
	static ArrayList<Shape> circles = new ArrayList<>();
	
	//L'ultimo nodo inserito, che si pu� settare con la funzione setLastRed, permette di colore l'ultimo nodo di rosso
	//Questo perch� se si effettua un operazione tipo merge, viene creato un nuovo nodo nell'albero di versione ma non viene colorato
	//di rosso e crea confusione
	private int[] lastInsert = null;
	
	
	public LeftPanel() {
		//Set panel
		setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		FlowLayout flowLayout_2 = (FlowLayout)getLayout();
		flowLayout_2.setHgap(250);
		
		
		hmNode = new HashMap<Integer, Node>();
		
		
		//Crea il nodo radice
		if(hmNode.size()==0) {
			int[] circleValues = generateValues();
            int x = circleValues[0];
            int y = circleValues[1];
            int width = 50;
            int height = 50;
			circles.add(new Ellipse2D.Double(x, y, width, height));
            repaint();
		}		
		
		addMouseListener(new MouseAdapter() {
        	@Override
            public void mousePressed(MouseEvent e) {
        		//Ottiene il punto clickato
        		Point p = e.getPoint();
        		int i=0;
        		//Se viene cliccato un nodo, l'ultimo selezionato diventa null in modo tale che il repaint colori di rosso quello cliccato
        		lastInsert = null;
        		
        		//Per ogni " cerchio/shape "
        		for (Shape shape: circles){
        			
        			//Se il cerchio contiene il punto
        		    if (shape.contains(p)) {
        		    	
        		    	//Setta tutti i nodi come non selezionati
        		    	for(int j=0;j<hmNode.size();j++) {
        		    		hmNode.get(j).setUnselect();
                		}
        		    	Graphics g = getGraphics();
        		    	Graphics2D g2d = (Graphics2D)g;
        		    	
        		    	//Disegna tutti i nodi grigi
        		    	for (Shape shape2: circles)
                		{	
        		    		g2d.setColor(Color.GRAY);
        		    		g2d.fill(shape2);
            		    	
                		}
        		    	//Disegna il nodo seleazionato di rosso
        		    	Color c = Color.RED;
        		    	g2d.setColor(c);
        		    	g2d.fill(shape);
        		    	
        		    	//Setta selezionato il nodo rosso
        		    	hmNode.get(i).setSelect();
        		    	
        		    	//Se il nodo ha un grafo settato lo visualizza
        		    	if(hmNode.get(i).getGraph()!=null) {        		    		
            		    	SIMplugin.setGraph(hmNode.get(i).getGraph());
        		    	}
        		    	else {
        		    		//Errore 
        		    		System.out.println("(Left panel) Errore");
        		    	}
        		    	
        		    	//Setta le righe della tabella inerente al nodo selezioanto
        		    	UpPanel.setRows();
        		    	
        		    	//visualizza il bottone "traces" solo se il nodo cliccato corrisponde al nodo M0
                		if(hmNode.get(i).getId() == 0) {
                			//DownPanel.setTraces(SIMplugin.getTraces());
                			DownPanel.enableTracesButton(true);
                		}
                		else
                			DownPanel.enableTracesButton(false);
        		    }
        		    i++;
        		}
        		
        		//Ridisegna chiamando implicitamente paintComponent
        		repaint();
            }
        });
	}

	@Override
	protected void paintComponent(Graphics g){
	    super.paintComponent(g);
	    
	    Graphics2D nodo = (Graphics2D)g;
	    Graphics2D edge = (Graphics2D) g;
	    int i = 0;
	    
	    int fatherX=0;
		int fatherY=0;
		int fatherId=0;
		
		
		//Prende le coordinate del padre 
		for(int j=0;j<hmNode.size();j++) {
	    	if(hmNode.get(j).getSelect()) {
	    		fatherX = hmNode.get(j).getX();
	    		fatherY = hmNode.get(j).getY();
	    		fatherId = j;
	    	}
	    }
		
		
	    for (Shape shape : circles){
	    	
	    	//Se l'hashmap non contiene la chiave
	    	if(!hmNode.containsKey(i)) {
	    		
	    		//Crea un nuovo nodo, con X, Y e l'id del nodo
	    		Node n = new Node(shape.getBounds().x,shape.getBounds().y,i);
	    		
	    		//Aggiunge il nodo all'hm node
	    		hmNode.put(i, n);
	    		
	    		//Se il nodo e' la radice
	    		if(i==0){
	    			hmNode.get(i).setX(1500);
					hmNode.get(i).setY(0);
					hmNode.get(i).setSelect();
	    		}
	    		//Se e' un qualsiasi altro nodo
	    		else {
					hmNode.get(i).setFatherX(fatherX);
					hmNode.get(i).setFatherY(fatherY);
					hmNode.get(i).setIdFather(fatherId);
					//Copia le righe della tabella del  padre
		    		UpPanel.setRowsFather(fatherId,i);
		    		
	    		}

	    		//Setta il grafo iniziale
	    		if(lastInsert == null)
	    			hmNode.get(i).setGraph(SIMplugin.getGraph());
	    	}
	    	
	    	//Permette la colorazione dell'ultimo nodo inserito di rosso
	    	if(lastInsert != null)
		    	for(Map.Entry<Integer, Node> entry: hmNode.entrySet()) {
		    		// lastInsert[0] la posizione X
		    		// lastInsert[1] la posizione y
					if(entry.getValue().getX() == lastInsert[0] && entry.getValue().getY() == lastInsert[1]) {
						entry.getValue().setSelect();
						entry.getValue().setGraph(SIMplugin.getGraph());
					}
					else
						entry.getValue().setUnselect();
				}
	    	
	    	UpPanel.setRows();
	    	
	    	//Se il nodo e' selezionato lo colora di rosso linee
	    	if(hmNode.get(i).getSelect()) {
	    		
		    	Color c = Color.RED;
		    	nodo.setColor(c);
		    	nodo.fill(shape);
		    	nodo.draw( shape );
		    	
	    	}
	    	//Altrimenti grigio
	    	else {

		    	nodo.setColor(Color.GRAY);
		    	nodo.fill(shape);
	    		nodo.draw(shape);
	    	}
	    	
	    	//Setta scritte
	    	nodo.setColor(Color.WHITE);
	    	
	    	nodo.drawString("M" + hmNode.get(i).getId(), shape.getBounds().x+25, shape.getBounds().y+25);
	    	nodo.setColor(Color.BLACK);
	    	//nodo.drawString("K" + hmNode.get(i).getX(), shape.getBounds().x+50, shape.getBounds().y+25);
	    	
	    	if(shape.getBounds().y != 0) {
	    		//Se il padre e' selezionato setta le linee a rosse
	    		int tFatherId = hmNode.get(i).getIdFather();
		    	if(hmNode.get(tFatherId).getSelect()) {
		    		Color c = Color.RED;
		    		edge.setColor(c);
		    	}
				Line2D lin = new Line2D.Float(hmNode.get(i).getFatherX()+25, hmNode.get(i).getFatherY()+25, hmNode.get(i).getX()+25, hmNode.get(i).getY()+25);
				edge.draw(lin);
				
		    }
	    	i++;
	    }
	 }
	
	//Dato in input le coordinate di un nodo(x2,y1), calcola la distanza da un altro punto(x2,y2)
	public static int distanzaPunti(int x1,int y1, int x2, int y2) {
		
		int X = (int) Math.pow((x1-x2), 2.0);
		int Y = (int) Math.pow((y1-y2), 2.0);
		int result = (int) Math.sqrt(X+Y);
		return result;
		
	}
	
	
	public static  int[] generateValues() {
		boolean search = false;
		//Risultato da ritornare
		int[] values = new int[2];
		int fatherX=0;
		int fatherY=0;
		//Cerca il nodo selezionato
		for(int j=0;j<hmNode.size();j++) {
		    //Se e' selezionato prende come riferimento i valori delle sue coordinate
		    if(hmNode.get(j).getSelect()) {
		    	fatherX = hmNode.get(j).getX();
		    	fatherY = hmNode.get(j).getY();
		    	search = true;
		    }
		 }
		if(search) {
			int distanza = 1000000;
			int x1=0;
			int y1=0;
			for(int i=0;i<100;i++) {
				for(int j=0;j<100;j++) {
					if(i!=0 && j!=0){
						//Pari, coordinate destra
						if(j%2==0) {
							if(j==0) x1 = 200;
							else x1 = 250+50*(j-1);
						}
						//Dispari, coordinate sinistra
						else x1 = 250-50*j;
					}
					y1 = fatherY + 75;
					
					//Calcola distanza dei punti
					int distP = distanzaPunti(x1,y1,fatherX,fatherY);
					boolean nodeIsPresent = false;
					for(int k = 0;k<hmNode.size();k++) {
						
						if(hmNode.get(k).getX() == x1 && hmNode.get(k).getY() == y1) nodeIsPresent = true;
						else if(x1==0 && y1==0)nodeIsPresent = true;
					}
							
					//Se e' inferiore rispetto le distanze calcolate in precedenza
					if(distP < distanza && !nodeIsPresent) {
							distanza = distP;
							values[0] = x1;
							values[1] = y1;
					}
				}
			}
		}
		//Genera il nodo radice
		else{
			values[0] = 1500;
			values[1] = 0;
		}
		return values;
	}
	
	/**
	 * Questa funzione permette di colorare l'ultimo nodo creato, ad esempio da una funzione che crea un nuovo modello di processo, di rosso essendo quello selezionato
	 * @param circleValues:nodo da colorare di rosso
	 */
	public void setLastRed(int[] circleValues) {
		lastInsert = circleValues;		
	}
	
	public static HashMap<Integer,Node> getHmNode (){
		return hmNode;
	}
	
}
