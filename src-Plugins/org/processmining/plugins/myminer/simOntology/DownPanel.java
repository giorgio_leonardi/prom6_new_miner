package org.processmining.plugins.myminer.simOntology;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.BevelBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.plugins.myminer.abstraction.Abstraction;


public class DownPanel extends JPanel{
	
	private static HashMap<Integer, ArrayList<DefaultMutableTreeNode>> tableLevel;

	private static final long serialVersionUID = 1L;
	public JTextField textField;//---
	private JButton btnSP;
	private JLabel lblTF;
	private JLabel lblP;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBox;
	private static JComboBox<Object> cbLivelliAstrazione, cbTipoAstrazione;
	private JButton btnSA;
	private JButton btnAA;
	private static JButton btnT;
	private static JButton btnAbstractionLevel;
	public String merge;
	public String query;
	
	private static Set<Integer> traces;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public DownPanel() {
		
		tableLevel = new HashMap<Integer, ArrayList<DefaultMutableTreeNode>>();
		
		setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(20);
		
		traces = new HashSet<>();
		
		//Set Label
		lblTF = new JLabel("Pattern:");
		lblP = new JLabel("Operator:");
		
		//Set TextField
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 22));
		textField.setColumns(24);
		textField.getDocument().addDocumentListener(new DocumentListener() {
					
			public void changedUpdate(DocumentEvent e) {
				changed();
			}
					
			public void removeUpdate(DocumentEvent e) {
				changed();
			}
					
			public void insertUpdate(DocumentEvent e) {
				changed();
			}
					
			public void changed() {
				if(textField.getText().isEmpty()) {
					btnSP.setEnabled(false);
					btnSA.setEnabled(false);
					btnAA.setEnabled(false);
					comboBox.setEnabled(false);
				}
						
				else {
					btnSP.setEnabled(true);
					btnSA.setEnabled(true);
					btnAA.setEnabled(true);
					comboBox.setEnabled(true);
				}
			}
		});
		
		//Set Button
		btnSP = new JButton("Search Path");
		btnSP.setEnabled(false);
		btnSA = new JButton("Selective Apply");
		btnSA.setEnabled(false);
		btnAA = new JButton("Apply All");
		btnAA.setEnabled(false);
		btnT = new JButton("Traces");
		btnT.setEnabled(false);
		
		//Aggiunge azione al click di btnSP --> Mostra il risultato della query
		btnSP.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				//Prende il testo per inserirlo nella tabella inerente al nodo
				query = textField.getText();
				//UpPanel.addRows(query);
				
				//Esegue la query e mostra  il grafo ottenuto
				try {
					SIMplugin.setGraphQuery(query);
				} catch (UserCancelledException e1) {
					e1.printStackTrace();
				}
			}
			
		});
		
		//Combo Box
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {
				"Weak\t\t\t", "Strong \t\t\t", 
				"Unicit� Weak \t\t\t", "Unicit� Strong \t\t\t"}));
		
		btnAA.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				SIMplugin.setSelective(false);
				SIMplugin.applyMerge(comboBox.getSelectedIndex());
				merge = (String) comboBox.getSelectedItem();
				SIMplugin.coordinate();
				UpPanel.addRows(merge,query);
				//Imposta i vari campi come vuoti
				textField.setText("");
				merge = "";
				query="";
			}
			
		});
		
		//Aggiunge azione al bottone --> Applica e mostra il risultato del tipo di merge selezionato
		btnSA.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				SIMplugin.setSelective(true);
				SIMplugin.applyMerge(comboBox.getSelectedIndex());
				merge = (String) comboBox.getSelectedItem();
				SIMplugin.coordinate();
				UpPanel.addRows(merge,query);
				//Imposta i vari campi come vuoti
				textField.setText("");
				merge = "";
				query="";
			}
			
		});
		
		btnT.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!traces.isEmpty()) 
					JOptionPane.showConfirmDialog(SIMplugin.frame, new TracesPanel(traces), "Elenco delle tracce che rispondono alla query", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
			}
			
		});
		
		//Astrai livelli
		btnAbstractionLevel = new JButton("Abstraction level");
		btnAbstractionLevel.setEnabled(true);
		cbLivelliAstrazione = new JComboBox();
		cbTipoAstrazione = new JComboBox();
		cbTipoAstrazione.setModel(new DefaultComboBoxModel(new String[] {
				"Automatic\t\t\t", "Interactive\t\t\t"}));
		
		btnAbstractionLevel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				int level = cbLivelliAstrazione.getSelectedIndex();
				int type = cbTipoAstrazione.getSelectedIndex();
				
				GraphAndOr gg = null;
				 
			    if(type==Abstraction.TYPE_AUTOMATIC) 
					gg = SIMplugin.ActAbstractByLevel(tableLevel.get(level), Abstraction.TYPE_AUTOMATIC);
			    else
			     if(type==Abstraction.TYPE_INTERACTIVE) 
						gg = SIMplugin.ActAbstractByLevel(tableLevel.get(level), Abstraction.TYPE_INTERACTIVE);
			    		 
			     // Se non � nullo, astrazione eseguita quindi nuovo grafo
				if(gg!=null) {
					SIMplugin.coordinate();
					if(type==Abstraction.TYPE_AUTOMATIC) 
						UpPanel.addRows(Integer.toString(level),"Automatic abstraction of level");	
					else
				     if(type==Abstraction.TYPE_INTERACTIVE)
							UpPanel.addRows(Integer.toString(level),"Interactive abstraction of level");	
				} 
				else
					JOptionPane.showMessageDialog(SIMplugin.frame, 
												  "The level "+ level + " cannot be abstract or has already been abstracted",
												  "Error in abstraction",
												  JOptionPane.ERROR_MESSAGE);
			}			
		});
		

		add(lblTF);
		add(textField);
		add(btnSP);
		add(lblP);
		add(comboBox);
		add(btnSA);
		add(btnAA);
		add(btnT);
		add(btnAbstractionLevel);
		add(cbLivelliAstrazione);
		add(cbTipoAstrazione);
	}
	
	//imposta le tracce recuperabili che rispondono alle query
	public static void setTraces(Set<Integer> t) {	
		traces = t;		
	}

	public static void enableTracesButton(boolean enable) {
		btnT.setEnabled(enable);
	}
	
	@SuppressWarnings("unchecked")
	public void setAbstractionLevel(JTree tree) {
		
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) tree.getModel().getRoot();
		ArrayList<TreeNode> root_child = Collections.list(root.children());
		
		for(TreeNode child:root_child) {
			scanChild(0,(DefaultMutableTreeNode)child);
		}
		
		ArrayList<String> level = new ArrayList<String>();
		
		for(Entry<Integer,ArrayList<DefaultMutableTreeNode>> entry:tableLevel.entrySet())
			level.add(entry.getKey().toString());

		cbLivelliAstrazione.setModel(new DefaultComboBoxModel<Object>(level.toArray()));
	}
	
	@SuppressWarnings("unchecked")
	private static void scanChild(int level, DefaultMutableTreeNode node) {
		if(node.isLeaf()) 
			return;
		
		ArrayList<TreeNode> root_child = Collections.list(node.children());
		
		for(TreeNode child:root_child) {
			scanChild(level+1, (DefaultMutableTreeNode)child);
			populateTableLevel(level, node);
		}
			
	}

	private static void populateTableLevel(int level, DefaultMutableTreeNode node) {
		
		if(tableLevel.containsKey(level)) {
			ArrayList<DefaultMutableTreeNode> listaNodi = tableLevel.get(level);
			if(!listaNodi.contains(node))
				listaNodi.add(node);
			tableLevel.put(level, listaNodi);
		}
		else {
			ArrayList<DefaultMutableTreeNode> listaNodi = new ArrayList<DefaultMutableTreeNode>();
			listaNodi.add(node);
			tableLevel.put(level, listaNodi);
		}
			
	}

	public static HashMap<Integer, ArrayList<DefaultMutableTreeNode>> getLevelTable(){
		return tableLevel;
	}
}
