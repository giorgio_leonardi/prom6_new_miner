package org.processmining.plugins.myminer.simOntology;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;

public class UpPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	public static JTable table;
	public static DefaultTableModel dm;
	public static ArrayList<String[]> rowsData;
	public static HashMap<Integer, Tabella> hmTable;
	static int countRow = 0;
	
	private static boolean selective = false;
	
	public void setSelective(boolean sel) {
		selective = sel;
	}
	
	public UpPanel() {
		//Set border
		setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		//Set Layot
		FlowLayout flowLayout_1 = (FlowLayout) getLayout();
		
		//Set altezza
		flowLayout_1.setVgap(60);
		table  =  new JTable();
		hmTable = new HashMap<Integer,Tabella> ();
		//countRow = 0;
		
		dm = new DefaultTableModel() ;
		table.setModel(new DefaultTableModel(
				
				new String[] {
					"OP CODE", "OP TYPE", "PATH", "SELECTIVE ?", "PARTS"
				}, 0
			));
		table.setPreferredScrollableViewportSize(new Dimension(1600,100));
		add(table);
		add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS));
		
		
	}


	public static void setRowsFather(int idFather, int id) {
		//Setta dm a zero
		dm.setNumRows(0);
		
		int hmTableSize = hmTable.size();
				
		//Aggiunge a dm le string[] relative del nodo selezionato
		for(int k=0;k<hmTableSize;k++) {
			if(hmTable.get(k).getId() == idFather) {
					Tabella t = new Tabella(id,hmTable.get(k).getStringa());
					hmTable.put(hmTable.size(), t);

			}	
			dm.addRow(hmTable.get(k).getStringa());
			//System.out.println("Aggiungo riga " + hmTable.get(k));
		}
		
	}
	
	//Aggiunge riga alla tabella 
	public static void addRows(String query,String merge) {
		
		dm = (DefaultTableModel) table.getModel();
		
		countRow++;
		String[] rowData = {Integer.toString(countRow), merge,query, "NO", " "};
		
		if(selective)
			rowData[3] = "YES"; 
		
		//Ottiene la dimensione di hm Table
		int hmTableSize = hmTable.size();
		
		if(!hmTable.containsKey(hmTableSize)) {
			Tabella t = new Tabella(LeftPanel.getHmNode().size(),rowData);
			hmTable.put(hmTableSize, t);
			
		}
	}
	
	public static void setRows() {
		
		
		//Setta dm a zero
		dm.setNumRows(0);
		
		// Prende l'id del nodo selezionato
		int idSelectedNode = searchSelectedNode();
		
		//Prende dimensione hm table
		int dimHmTable = hmTable.size();
		
		//Aggiunge a dm le string[] relative del nodo selezionato
		for(int k=0;k<dimHmTable;k++) {
			if(hmTable.get(k).getId() == idSelectedNode) {
				dm.addRow(hmTable.get(k).getStringa());
				//System.out.println("Aggiungo riga " + hmTable.get(k));
			}
		}
		
	}
	
	//Ritorna l'id del nodo selezionato
	public static int searchSelectedNode() {
		int idSelectedNode = 0;
		for(int i=0;i<LeftPanel.getHmNode().size();i++) {
			if(LeftPanel.getHmNode().get(i).getSelect()) {
				idSelectedNode  = i;
			}
		}
		return idSelectedNode;
	}

}
