package org.processmining.plugins.myminer.index;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import org.processmining.plugins.myminer.fsm.Vertex;



public class IndexEntry implements Cloneable{

	private Set<String> element;
	private boolean delay;
	private int indexStartMatch;
	private int indexEndMatch;
	
	public int getIndexStartMatch() {
		return indexStartMatch;
	}

	public int getIndexEndMatch() {
		return indexEndMatch;
	}


	public IndexEntry(String el) {
		element=new HashSet<String>();
		delay=el==null;
		indexEndMatch=-1;
		indexStartMatch=-1;
		element.add(el);
	}
	
	public IndexEntry(Set<String> el) {
		element=el;
		delay=el==null;
		indexEndMatch=-1;
		indexStartMatch=-1;
	}

	public IndexEntry() {
		element=new TreeSet<String>();
		delay=false;
		indexEndMatch=-1;
		indexStartMatch=-1;
	}

	public Set<String> getElementQuery() {
		return element;
	}
	public void setElementQuery(Set<String> element) {
		this.element = element;
	}
	public boolean isDelay() {
		return delay;
	}
	
	public void setDelay(boolean delay) {
		this.delay = delay;
	}
	
	public String toString(){
		return element.toString();
	}

	public void updateEntry(int m) {
		if (indexStartMatch==-1)
			indexStartMatch=m;
		indexEndMatch=m;
	}

	public Object clone(){  
		
	    try{ 
	    	IndexEntry ie=new IndexEntry();
			ie.delay=this.delay;
			if (ie.delay==false){
				ie.element=new TreeSet<String>();
				for (String s:element)
					ie.element.add(new String(s));
			}
			else ie.element=this.element;
			ie.indexEndMatch=this.indexEndMatch;
			ie.indexStartMatch=this.indexStartMatch;
	        return ie;  
	    }catch(Exception e){ 
	        return null; 
	    }
	}
	
}
