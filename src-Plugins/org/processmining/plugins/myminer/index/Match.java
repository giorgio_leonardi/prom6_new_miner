package org.processmining.plugins.myminer.index;



import java.util.Set;
import java.util.TreeSet;

import org.processmining.models.andOrGraph.Node;

public class Match implements Cloneable{

	private Set<String> element=new TreeSet<String>();
	
	private Node node;

	public Match(String symbol, Node node2) {
		node=node2;
		element.add(symbol);
	}

	public Match() {
		node=null;
		element=new TreeSet<String>();
	}

	public Set<String> getElement() {
		return element;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public void update(String symbol) {
		element.add(symbol);
	}
	
	public Object clone(){  
		Match m=new Match();
	    try{ 
	    	m.element=new TreeSet<String>();
	    	for (String s:element)
	    		m.element.add(new String(s));
	    	m.node=node;
	        return m; 
	    }catch(Exception e){ 
	        return null; 
	    }
	}
	
	public String toString(){
		return node.getLabel()+"//"+element.toString();
	}
}
