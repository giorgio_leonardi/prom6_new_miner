package org.processmining.plugins.myminer.index;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.processmining.models.andOrGraph.Node;


public class IndexTable implements Cloneable{
	Vector<IndexEntry> table;
	Vector<Match> matches;
	
	public IndexTable(){
		table=new Vector<IndexEntry>();
		matches=new Vector<Match>();
	}
	
	// testing
	public Vector<IndexEntry> getTable() {
		return table;
	}	
	
	public String toString(){
		String res="";
		for (IndexEntry ie:table){
			if (!ie.isDelay())
				res+=ie.getElementQuery()+" :";
			else res+="Delay :";
			for (int i=ie.getIndexStartMatch();i>=0&& i<=ie.getIndexEndMatch();i++)
				res+=matches.get(i).toString()+"-";
			res+="\n";
		}
		return res;
	}

	public int addEntry(String el) {
		IndexEntry entry=new IndexEntry(el);
		table.add(entry);
		return table.size()-1;
	}


	public int addEntry(Set<String> el) {
		IndexEntry entry=new IndexEntry(el);
		table.add(entry);
		return table.size()-1;
		
	}


	
	public void updateEntry(int pos, String symbol,Node node) {
		int end=table.get(pos).getIndexEndMatch();
		if (!matches.isEmpty() && end!=-1 && matches.get(end).getNode().equals(node)){
			matches.lastElement().update(symbol);
		}
		else {
			matches.add(new Match(symbol,node));
			int m=matches.size()-1;
			table.get(pos).updateEntry(m);
		}
		
	}
	

	public Object clone(){  
		IndexTable it=new IndexTable();
	     try{ 
	    	for (IndexEntry t:table){
	    		it.addEntry((IndexEntry) t.clone());
	    	}
	    	for (Match m:matches){
	    		it.addMatch((Match) m.clone());
	    	}
	        return it;  
	    }catch(Exception e){ 
	        return null; 
	    }
	}


	private void addMatch(Match clone) {
		this.matches.add(clone);
	}


	private void addEntry(IndexEntry clone) {
		this.table.add(clone);
	}


	public Vector<Match> getMatches() {
		return matches;
		
	}


	public Vector<Match> getQueryMatches(int i) {
		IndexEntry ie=table.get(i);
		return new Vector<Match>(matches.subList(ie.getIndexStartMatch(), ie.getIndexEndMatch()+1));
	}


	public void setResult() {
		for (IndexEntry t:table)
			{
			for (int i=t.getIndexStartMatch();i>=0&&i<=t.getIndexEndMatch();i++){
				if (t.isDelay())
					matches.get(i).getNode().setRetrieve(Node.DUMMY_RETRIEVED);
				else matches.get(i).getNode().setRetrieve(Node.QUERY_RETRIEVED);
			}
		}
	}


	public int size() {
		
		return table.size();
	}


	public IndexEntry getQueryPattern(int i) {
		return table.get(i);
	}


	public boolean samePath(IndexTable indexTable) {
		
		Iterator<Match> mIT = matches.iterator();
		Iterator<Match> otMIT = indexTable.matches.iterator();
		Node prevN1=null,prevN2=null;
		while (mIT.hasNext() && otMIT.hasNext()){
			Match m = mIT.next();
			Match otM = otMIT.next();
			Node n1=m.getNode();
			Node n2=otM.getNode();
			if (!n1.equals(n2) && !n1.equals(prevN2))
				return false;
			if (!n2.equals(n1) && !n2.equals(prevN1))
				return false;
			prevN1=n1;
			prevN2=n2;
		}
		return true;
	}
	
	
}
