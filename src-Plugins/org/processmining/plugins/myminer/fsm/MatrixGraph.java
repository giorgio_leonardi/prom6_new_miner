package org.processmining.plugins.myminer.fsm;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;


public class MatrixGraph implements Graph {

	private Integer numberOfVertex;
	private Integer numberOfEdges;
	
	private Integer matrixDim = 100;
	
	private Vector<Vertex> v;
	private Set<String>[][] matrix;
	
	MatrixGraph()
	{
		numberOfVertex = 0;
		numberOfEdges = 0;
		
		this.v = new Vector<Vertex>();
		matrix = new HashSet[matrixDim][matrixDim];
		
		for(int i = 0; i < matrixDim; i++)
		{
			for(int j = 0; j < matrixDim; j++)
			{
				matrix[i][j] = new HashSet<String>();
			}
		}
		
	}
	
	MatrixGraph(int initialNumberOfVertex)
	{
		numberOfVertex = 0;
		numberOfEdges = 0;
		
		matrixDim = initialNumberOfVertex;
		
		this.v = new Vector<Vertex>();
		matrix = new HashSet[matrixDim][matrixDim];
		
		for(int i = 0; i < matrixDim; i++)
		{
			for(int j = 0; j < matrixDim; j++)
			{
				matrix[i][j] = new HashSet<String>();
			}
		}
		
	}
	
	@Override
	public int getNumberOfVertex()
	{
		return this.numberOfVertex;
	}
	
	@Override
	public int getNumberOfEdges()
	{
		return this.numberOfEdges;
	}
	
	private int indexOf(Vertex v)
	{
		return this.v.indexOf(v);
	}

	private int indexOf(String v)
	{
		for(Vertex ver: this.v)
		{
			if(ver.getName().equals(v))
			{
				return this.v.indexOf(ver);
			}
		}
		return -1;
	}
	
	@Override
	public Collection<Vertex> getVertex() {
		return this.v;
	}

	@Override
	public Vertex getVertex(String name) {
		for(Vertex ver: this.v)
		{
			if(ver.getName().equals(name))
			{
				return ver;
			}
		}
		return null;
	}

	@Override
	public boolean addVertex(Vertex v) {
		if(this.v.contains(v)) return false;
		if(!this.v.contains(new Vertex("EMPTY_VERTEX")))
			{
				if(this.v.add(v))
					{
						numberOfVertex += 1;
						return true;
					}
				return false;
			}
		for(int i = 0; i< this.v.size(); i++)
		{
			if(this.v.get(i).equals(new Vertex("EMPTY_VERTEX")))
			{
				this.v.set(i, v);
				numberOfVertex += 1;
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean addVertex(String v) {
		if(this.v.contains(new Vertex(v))) return false;
		if(!this.v.contains(new Vertex("EMPTY_VERTEX")))
			{
				if(this.v.add(new Vertex(v)))
				{
					numberOfVertex += 1;
					return true;
				}
			return false;
			}
		
		for(int i = 0; i< this.v.size(); i++)
		{
			if(this.v.get(i).equals(new Vertex("EMPTY_VERTEX")))
			{
				this.v.set(i, new Vertex(v));
				numberOfVertex += 1;
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean addAllVertex(Collection<Vertex> v) {
		boolean addedAll = true;
		for(Vertex ver: v)
		{
			if(!addVertex(ver))
			{
				addedAll = false;
			}
		}
		return addedAll;
	}

	@Override
	public boolean addEdge(Vertex from, Vertex to, String symbol) {
		if(!this.v.contains(from) || !this.v.contains(to)) return false;
		if((matrix[indexOf(from)][indexOf(to)]).add(symbol))
		{
			numberOfEdges += 1;
			return true;
		}
		return false;
	}

	@Override
	public boolean setVertexName(Vertex oldVertex, String newVertexName) {
		return (this.v.contains(oldVertex))?
				this.v.set(indexOf(oldVertex), new Vertex(newVertexName))==null? false : true : false;
	}

	@Override
	public boolean containsEdge(Edge e) {
		return (this.v.contains(new Vertex(e.getFrom())) && this.v.contains(new Vertex(e.getTo())))?	
				matrix[indexOf(e.getFrom())][indexOf(e.getTo())].contains(e.getSymbol()) : false;
	}

	@Override
	public boolean containsEdge(Vertex from, Vertex to) {
		return (this.v.contains(from) && this.v.contains(to))?	
				!matrix[indexOf(from)][indexOf(to)].isEmpty() : false;
	}

	@Override
	public boolean containsVertex(Vertex v) {
		return this.v.contains(v);
	}

	@Override
	public Collection<Edge> edgeOf(Vertex v) {
		
		Collection<Edge> result = new Vector<Edge>();
		
		if(!this.v.contains(v) || v.equals(new Vertex("EMPTY_VERTEX")))	return result;
		
		for(int i=0; i<this.v.size(); i++)
		{
			for(String s: matrix[indexOf(v)][i])
			{
				if(!result.add(new Edge(s,this.v.get(indexOf(v)).getName() , this.v.get(i).getName())))	return null;
			}
		}
		return result;
	}

	@Override
	public Set<Edge> edgesOf(Vertex v) {
		
		Set<Edge> result = new HashSet<Edge>();
		if(!this.v.contains(v))	return result;
		
		for(int i=0; i<matrixDim; i++)
		{
			for(int j=0; j<matrixDim; j++)
			{
				if(i==this.indexOf(v) || j==this.indexOf(v))
				{
					for(String s: matrix[i][j])
					{
						result.add(new Edge(s, this.v.get(i).getName(), this.v.get(j).getName()));
					}
				}
			}
		}
		return result;
	}

	@Override
	public Set<Edge> edgeSet() {
		
		Set<Edge> result = new HashSet<Edge>();
		
		for(int i=0; i<matrixDim; i++)
		{
			for(int j=0; j<matrixDim; j++)
			{
				for(String s: matrix[j][i])
				{
					if(!result.add(new Edge(s, this.v.get(j).getName() , this.v.get(i).getName())))	return null;
				}
			}
		}
		return result;
	}

	@Override
	public Set<Edge> getAllEdges(Vertex from, Vertex to) {
		
		Set<Edge> result = new HashSet<Edge>();
		if(!this.v.contains(from) || !this.v.contains(to))	return result;
		
		for(String symbol: matrix[indexOf(from)][indexOf(to)])
		{
			if(!result.add(new Edge(symbol, from.getName() , to.getName())))	return null;
		}
		
		return result;
	}

	@Override
	public Set<Vertex> vertexSet() {
		Set<Vertex> result = new HashSet<Vertex>();
		
		for(Vertex ver: this.v)
		{
			result.add(ver);
		}
		
		return result;
	}

	@Override
	public boolean removeAllEdges(Collection<Edge> e) {
		boolean removedAll = true;
		for(Edge edg: e)
		{
			if(!this.removeEdge(edg)) removedAll = false;
		}
		return removedAll;
	}

	@Override
	public boolean removeEdge(Edge e) {
		if(this.v.contains(new Vertex(e.getTo())) && this.v.contains(new Vertex(e.getFrom())))
		{
			if(this.matrix[indexOf(e.getFrom())][indexOf(e.getTo())].remove(e.getSymbol()))
			{
				numberOfEdges -= 1;
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean removeEdge(Vertex from, Vertex to, String symbol) {
		if(this.v.contains(from) && this.v.contains(to))
		{
			if(this.matrix[indexOf(from)][indexOf(to)].remove(symbol))
			{
				numberOfEdges -= 1;
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean removeAllEdges(Vertex from, Vertex to) {
		if(this.v.contains(from) && this.v.contains(to))
		{
			numberOfEdges -= this.matrix[indexOf(from)][indexOf(to)].size();
			this.matrix[indexOf(from)][indexOf(to)].clear();
			return true;
		}
		return false;
	}

	@Override
	public boolean removeVertex(Vertex v) {
		if(!this.v.contains(v)) return false;
		
		
		for(int i= 0; i<this.v.size(); i++)
		{
			this.numberOfEdges -= matrix[indexOf(v)][i].size();
			matrix[indexOf(v)][i].clear();
			
			this.numberOfEdges -= matrix[i][indexOf(v)].size();
			matrix[i][indexOf(v)].clear();
		}
		
		this.v.set(indexOf(v), new Vertex("EMPTY_VERTEX"));
		numberOfVertex -= 1;
		
		return true;
	}
	
	@Override
	public boolean removeAllVertices(Collection<Vertex> v) {
		boolean removedAll = true;
		
		for(Vertex ver: v)
		{
			if(this.v.contains(ver))
			{
				if(!this.removeVertex(ver))	removedAll = false;
			}
		}
		
		return removedAll;
	}

	public String toString()
	{
		Vector<Vertex> vertex = new Vector<Vertex>();
		vertex.addAll(this.v);
		while(vertex.remove(new Vertex("EMPTY_VERTEX")));
		
		String result = new String("");
		result += "\n\tnumero di Vertici:\t"+numberOfVertex
				  +"\n\tnumero di Archi:\t"+numberOfEdges+"\n"
				  +"\n\tVertici: "+vertex
				  +"\nArchi:\n";
		
		if(numberOfEdges==0)	result += "Non ci sono Archi!\n";
		else	for(Vertex ver: this.v)
				{
					if(!ver.equals(new Vertex("EMPTY_VERTEX")) && !this.edgeOf(ver).isEmpty()) 
					{
						for(Edge e: this.edgeOf(ver))
						{
							result += e.toString();
						}
						if(!this.edgeOf(ver).isEmpty())	result +="\n";
					}
				}
		
		return result;
	}

}
