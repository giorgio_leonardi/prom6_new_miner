package org.processmining.plugins.myminer.fsm;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;


public class NonDeterministicFSM extends FSM{
	
	/*
	 * Istanzia un automa a stati finiti non deterministico con un solo stato [0] 
	 * e nessuno stato finale.	*/
	protected NonDeterministicFSM()
	{
		super();
	}
	

	/*
	 * A partire dall'espressione regolare genera l'automa a stati finiti 
	 * non deterministico */
	NonDeterministicFSM(String regExTA)
	{
		super();
		fromRegExToFSM(regExTA);
	}
	
	/*
	 * Come nel caso precedente ma con la possibilità aggiuntiva di definire 
	 * il nome dello stato iniziale e quello del primo stato da cui partire 
	 * per la generazione.	*/
	NonDeterministicFSM(String regExTA, String startNodeName, String firstStartName)
	{
		super(startNodeName, firstStartName);
		fromRegExToFSM(regExTA);
	}
	
	/* 
	 * Data una espressione reglare che denota una TA (Temporal Abstraction), restituisce l'indice
	 * dell'ultimo stato dell'automa non deterministico costruito a partire dall'espressione regoleare;		*/
	private Integer indexOfLastState(String regExTA)
	{
		Integer y= RegExTA.numberOfSymbols(regExTA)+(Integer.parseInt(this.getFirstStateDefault())-1);
		return y;
	}
	
	/*
	 * la generazione dell'automa a stati finiti crea gli stati chiamandoli con nomi sequenziali
	 * a partire da firstStateDefault; ciò vuol dire che per ottenere il nome del n-esimo stato
	 * generato bisognerà applicare la formula:
	 * 		nome = n + (firstStateDefault -1)	
	 * 
	 * Per una questione di flessibilità lo stato iniziale è concettualmente separato da tutti
	 * gli altri stati generati, il nome dello stato iniziale è denotato dal parametro startStateDefault		*/
	private String nameOf(int position, String regExTA)
	{
		return String.valueOf(position +(Integer.parseInt(this.getFirstStateDefault())-1));
	}
	
	/*
	 * Data l'espressione regolare, viene generato uno stato per ogni simbolo dell'espressione;		*/
	private void generateAllStates(String regExTA)
	{
		for (Integer i=Integer.parseInt(this.getFirstStateDefault());
				i<=indexOfLastState(regExTA); 
				i++)
		{
			this.addState(i.toString());
		}
	}
	
	/*
	 * 
	 * for each transition [δ(startState, symbol) = state] in first() do begin
	 * 		
	 * 		if symbol == * then
	 * 
	 * 			for each symbol s in alphabet do begin
	 * 
	 * 				add transition δ(startState, s) = state;
	 * 			end for each
	 * 
	 * 		else add transition δ(startState, s) = state;
	 * 
	 * 	end for each
	 * 
	 * for each state A != startState in fsm do begin
	 * 
	 * 		for each transition [δ(A, symbol) = state] in follow(A) do begin
	 * 		
	 * 				if symbol == * then
	 * 
	 * 					for each symbol s in alphabet do begin
	 * 
	 * 						add transition δ(A, s) = state;
	 * 					end for each
	 * 
	 * 				else add transition δ(A, s) = state;
	 * 
	 * 				if follow(A) is empty then
	 * 
	 * 						add A onto finalState;
	 * 				end if
	 * 			
	 * 		end for each
	 * 		
	 * end 
	 * 
	 * end algorithm;		*/
	private void generateAllTransition(String regExTA)
	{
		RegExTA rE = new RegExTA(regExTA);
		
		for(Integer i: rE.first())
		{
			if(rE.getSymbol(i).equals(RegExTA.getTokens().get(0)))
			{
				for(String character: RegExTA.getAlphabet())
				{
					this.addTransition(this.getStartState(), 
							  	 (nameOf(i, regExTA)),
							  	 character);
				}
			}
			else this.addTransition(this.getStartState(), 
							  (nameOf(i, regExTA)),
							  rE.getSymbol(i));
		}
		
		for(Integer i=1; i<this.getNumberOfState();i++)
		{
			if(rE.follow(i).isEmpty())
			{
				this.addFinalState(nameOf(i, regExTA));
			}
			else
			{
				for(Integer to: rE.follow(i))
					if(rE.getSymbol(to).equals(RegExTA.getTokens().get(0)))
					{
						for(String character: RegExTA.getAlphabet())
						{
							this.addTransition(nameOf(i, regExTA), 
										 nameOf(to, regExTA),
									  	 character);
						}
					}
					else this.addTransition(nameOf(i, regExTA),
								   nameOf(to, regExTA),
								   rE.getSymbol(to));
			}
		}
		if(this.getNumberOfState()==1) this.addFinalState(this.getStartState());
	}
	

	/*
	 * Fast construction of a non deterministic automaton M' from a marked axpression E'
	 * 
	 * 	- M' has a start state plus a state for each marked symbol a in E'
	 * 
	 * 	- Construct a transition from the start state to the state for a if and
	 * 		only if a ∈ first(E'); construct a transition from the state for b 
	 * 		to the state for a if and only if a ∈ follow(b)
	 * 
	 * 	- The start state is an accepting state if and only if δ(E') = 1; the state for
	 * 		a is an accepting state if and only if !∃ follow(a)			*/
	private void fromRegExToFSM(String regExTA)
	{
		generateAllStates(regExTA);
		
		generateAllTransition(regExTA);
	}


	/*simula l'automa a partire dallo stato p.second() considerando come simbolo di transizione l'etichetta del nodo p.first()*/
	public Set<String> simulate(Collection<String> symbols, String state) {
		Set<String> result=new TreeSet<String>();
		result.add(state);
		Set<String> new_result=new TreeSet<String>();
		for (String s:symbols){
			for (String stato:result)
				new_result.addAll(getStateReachableFrom(stato, s));
			if (new_result.isEmpty())
				return null;
			new_result=epsilonClosure(new_result);
			result=new TreeSet<String>(new_result);
			new_result.clear();
		}
		return result;
	}

	public boolean isFinalState(Set<String> new_states) {
		Collection<String> finalState = getFinalStates();
		for (String s:new_states){
			if (finalState.contains(s))
				return true;
		}
		return false;
	}
}
