package org.processmining.plugins.myminer.fsm;

public class Edge implements Comparable<Edge> {
	
	private String from;
	private String to;
	private String symbol;
	
	Edge()
	{
		this.from = new String("");
		this.to = new String("");
		this.symbol = new String("");
	}
	
	Edge(String symbol)
	{
		this.from = new String("");
		this.to = new String("");
		this.symbol = new String(symbol);
	}
	
	Edge(String symbol, String from, String to)
	{
		this.from = new String(from);
		this.to = new String(to);
		this.symbol = new String(symbol);
	}
	
	public void setFrom(String from)
	{
		this.from = from;
	}
	
	public void setTo(String to)
	{
		this.to = to;
	}
	
	public void setSymbol(String symbol)
	{
		this.symbol = symbol;
	}
	
	public String getFrom()
	{
		return this.from;
	}
	
	public String getTo()
	{
		return this.to;
	}
	
	public String getSymbol()
	{
		return this.symbol;
	}

	@Override
	public int compareTo(Edge edge1) {
		
		return  this.symbol.equals(edge1.symbol)?  (this.from.equals(edge1.from))? (this.to.compareTo(edge1.to)):(this.from.compareTo(edge1.from)) :(this.symbol.compareTo(edge1.symbol));
	}
	
	@Override
    public int hashCode() {
		return this.symbol.hashCode() + 7*(this.from.hashCode() + 7*(this.to.hashCode()));
	}
	
	@Override
	public boolean equals(Object o)
	{
		return (o.getClass()==Edge.class && this.compareTo((Edge)o)==0)? true:false;
	}
	
	@Override
	public String toString()
	{
		return "["+this.from+"  -->  "+this.symbol+"  -->  "+this.to+"]\t";
	}
}
