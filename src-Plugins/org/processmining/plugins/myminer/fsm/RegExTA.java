package org.processmining.plugins.myminer.fsm;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.StringTokenizer;

/*
 * RegExTA (Regular Expression Temporal Abstraction) offre un insieme di metodi necessari ad effettuare
 * la conversione di una stringa appartenente al linguaggio delle Temporal Abstraction in una espressione 
 * regolare che la riconosce.
 * 
 * In particolare offre il metodo statico toRegexTA(String TA) , esso funziona sull'asserzone che la stringa
 * ricevuta in input (TA) sia esattamente una stringa appartenente al linguaggio delle Temporal Abstraction 
 * (in caso contrario viene lanciata un eccezione)
 * 
 * Vengono definite delle variabili di classe statiche che caratterizzano alcuni aspetti della conversione
 * 
 * String[] regExs: 
 * 		Le TA di default sono stringhe del tipo:
 * 				[(min,max)string]+ (min,max)
 * 		L'espressione regolare precedente è formata dal concatenamento di due espressioni:
 * 				regEx1 = [(min,max)string]+
 * 				regEx2 = (min,max)
 * 		Al fine di offrire una maggior modularità del codice ho implementato le funzioni:
 * 				getRegEx()
 * 				setRegEx()
 * 		esse mi permettono di modificare alcuni aspetti della struttura primaria delle TA
 * 
 * String tokenSymbol:
 * 		Identifica il token che nelle espressioni regolari che rappresenta qualsiasi simbolo 
 * 		del linguaggio (' * ' di default)
 * 
 * String tokenEpsilon:
 * 		Identifica il token che nelle espressioni regolari rappresenta il simbolo neutro
 * 		(null di default)
 * 
 * String tokenSentinels:
 * 		Identifica i tokens sentinella delle espressioni regolari ( '('')' e '['']' di default)
 * 
 * String tokenListSeparator:
 * 		Identifica il token separatore interno alle liste liste ( ' , ' di default)
 * 
 * String tokenGroupSeparator:
 * 		Identifica il token separatore di gruppi (liste) ( ' | ' di default)
 * 
 * Al fine di rendere più gestibile e personalizzabile la scelta dei token, vengono implementate 
 * le funzioni:
 * 		setTokens()
 * 		getTokens()
 * */

public class RegExTA {
	
	private static String[] regExs = {"(\\Q(\\E\\d+,\\d+\\Q)\\E[a-zA-Z ,_]+)","(\\Q(\\E\\d+,\\d+\\Q)\\E$)"};
	private static String tokenSymbol = new String("*");
	private static String tokenEpsilon = new String("null");
	private static String[] tokenSentinels = {"(",")","[","]"};
	private static String tokenListSeparator = new String(",");
	private static String tokenGroupDelimiter = new String("|");
	private static Set<String> alphabet = new HashSet<String>();
	
	private String regExTA;
	
	RegExTA(String regExTA)
	{
		this.regExTA = regExTA;
		if (alphabet.isEmpty())
			alphabet.addAll(getAllSymbol());
	}
	
	public static String[] getRegEx()
	{
		return regExs;
	}
	
	
	public static String[] setRegExs(String[] newRegExs)
	{
		return regExs = newRegExs;
	}
	
	
	public static Vector<String> getTokens()
	{
		Vector<String> tokens = new Vector<String>();
		tokens.add(tokenSymbol);
		tokens.add(tokenEpsilon);
		tokens.add(tokenSentinels[0]+" "+tokenSentinels[1]+"\t"+tokenSentinels[2]+" "+tokenSentinels[3]);
		tokens.add(tokenListSeparator);
		tokens.add(tokenGroupDelimiter);
		return tokens;
	}
	
	public static void setTokens(String newTokenSymbol,String newTokenEpsilon, String[] newTokenSentinels, String newTokenListSeparator,String newTokenGroupDelimiter )
	{
		tokenSymbol = newTokenSymbol;
		tokenEpsilon = newTokenEpsilon;
		tokenSentinels = newTokenSentinels;
		tokenListSeparator = newTokenListSeparator;
		tokenGroupDelimiter = newTokenGroupDelimiter;
	}
	
	public static Set<String> getAlphabet()
	{
		return alphabet;
	}
	
	public static void setAlphabet(Collection<String> newAlphabet)
	{
		alphabet = new HashSet<String>(newAlphabet);
	}
	
	public static void addAllCharToAlphabet(Collection<String> characters)
	{
		alphabet.addAll(characters);
	}
	
	public static void addCharToAlphabet(String character)
	{
		alphabet.add(character);
	}
	
	public static boolean removeAllCharFromAlphabet(Collection<String> characters)
	{
		return alphabet.removeAll(characters);
	}
	
	public static boolean removeCharFromAlphabet(String character)
	{
		return alphabet.remove(character);
	}
	
	/* Restituisce la stringa rappresentante l'espressione regolare che denota un range.
	 * Riceve il valore minimo, quello massimo e genera l'espressione regolare
	 * che denota tale range.
	 * 
	 * Es:
	 * 		min = 1
	 * 		max = 3
	 * 
	 * 		range = *(null,*,**)		*/
	private static String parseRange(int min, int max)
	{
		int  i;
		String result = new String("");
		for (i=0;i<min;i++)
		{
			result = i==min-1? result+tokenSymbol : result+tokenSymbol+tokenListSeparator;
		}

		if (i<max) result += tokenSentinels[0]+tokenEpsilon+tokenGroupDelimiter;
		for(int count = 0;i<max; count++,i++)
		{
			for (int j=0;j<=max && j<=count ;j++)
			{
				result = j==count? result+tokenSymbol : result+tokenSymbol+tokenListSeparator;
			}
			result = i==max-1? result+tokenSentinels[1] : result+tokenGroupDelimiter;
		}
		
		return result;
	}
	
	/* Riceve una stringa sub-TA ed  effettua il parsing.
	 * 
	 * Può ricevere stringhe del tipo:
	 * 		[(min,max)string]+
	 * 		(min,max)			
	 * */
	private static String parseRegEx(String regEx, boolean mode)
	{
		String []splitted = regEx.split("[\\Q"+tokenSentinels[0]+"\\E"+tokenListSeparator+"\\D]");

		Integer min = Integer.parseInt(splitted[1]);
		Integer max = Integer.parseInt(splitted[2]);
		String regExResult =  parseRange(min,max);
		
		
		/*mode:
		 * 		true se la stringa regEx è del tipo : [(min,max)string]+
		 * 		false se la stringa regEx è del tipo : (min,max)		*/
		if(mode)
		{
			regExResult += tokenListSeparator;
			splitted[0] = regEx.replaceAll("[\\d+\\Q"+tokenSentinels[0]+"\\E\\Q"+tokenSentinels[1]+"\\E]", "").replaceFirst(tokenListSeparator, "");
			regExResult += splitted[0];
			
		}
		return regExResult;
	}
	
	
	
	public Set<String> getAllSymbol()
	{
		Set<String> result = new HashSet<String>();
		
		StringTokenizer st = new StringTokenizer(regExTA, 
												 tokenSymbol
												 +tokenEpsilon
												 +tokenListSeparator
												 +tokenSentinels[0]
												 +tokenSentinels[1]
												 +tokenGroupDelimiter	);
		
		while(st.hasMoreTokens())
		{
			result.add(st.nextToken());
		}
		
		return result;
		
	}
	
	
	public String getSymbol(int symbolPosition)
	{
		StringTokenizer st = new StringTokenizer(regExTA, tokenListSeparator+tokenSentinels[0]+tokenSentinels[1]+tokenGroupDelimiter);

		for(int i=1; i<symbolPosition; i++)
		{
			st.nextToken();
		}
		return st.nextToken();
	}
	
	public Collection<Integer> first()
	{
		Collection<Integer> result = new Vector<Integer>(); 
		StringTokenizer st;
		
		
		if(this.regExTA.startsWith(tokenSentinels[0]))// la stringa inizia con una parentesi
		{
			/* per trovare la prima parentesi chiusa uso "indexOf" e poi utilizzo il metodo "substring(0, indexOf(""))"*/
			st = new StringTokenizer(this.regExTA.substring(0, this.regExTA.indexOf(tokenSentinels[1])), 
					tokenSentinels[0]+tokenSentinels[1]+tokenGroupDelimiter);
			
			for(int oldPosition = 1; 
					st.hasMoreTokens();
					oldPosition += (new StringTokenizer(st.nextToken(), 
							tokenListSeparator+tokenSentinels[1]+tokenGroupDelimiter)).countTokens())
			{
				result.add(oldPosition);
			}
			
			return result;
		}
		
		result.add(1);
		return result;
	}
	
	public Collection<Integer> follow(int symbolPosition)
	{
		Collection<Integer> result = new Vector<Integer>();
		String subString = new String();
		StringTokenizer st = new StringTokenizer(regExTA, tokenListSeparator+tokenSentinels[0]+tokenSentinels[1]+tokenGroupDelimiter);
		
		for(int i=1; i<=symbolPosition; i++)
		{
			if(st.hasMoreTokens())	st.nextToken();
			/*lancio un eccezione per comunicare l'errore*/
			else return result;
		}
		if (!st.hasMoreTokens())	return result;
		
		subString = st.nextToken("\0");
		
		if (subString.startsWith(tokenListSeparator))  subString=subString.substring(1);//","
		
		if(subString.startsWith(tokenGroupDelimiter)) // "|"
		{
			
			if((subString.substring(0, subString.indexOf(tokenSentinels[1])).length()>=2) && 
			   (subString.substring(0, subString.indexOf(tokenSentinels[1])).length()!=subString.length()-1))	
				result.add(symbolPosition +
						new StringTokenizer(subString.substring(0, subString.indexOf(tokenSentinels[1])), 
						tokenListSeparator+tokenSentinels[0]+tokenSentinels[1]+tokenGroupDelimiter).countTokens()
						+ 1);
				
			
		}
		else if(subString.startsWith(tokenSentinels[1]) ) // ")" 
		{
			/* restituisco symbolposition + 1 se ci sono altri simboli	*/
			if(subString.length()>1) result.add(symbolPosition +1);
		}
		else if(subString.startsWith(tokenSentinels[0])) // "("
		{
			int oldPosition = symbolPosition+1;
			
			
			st = new StringTokenizer(subString.substring(0, subString.indexOf(tokenSentinels[1])), 
					tokenSentinels[0]+tokenSentinels[1]+tokenGroupDelimiter);
			
			while(st.hasMoreTokens())
			{
				result.add(oldPosition);
				oldPosition += (new StringTokenizer(st.nextToken(), 
						tokenListSeparator+tokenSentinels[1]+tokenGroupDelimiter)).countTokens();
			}
		}
		else /* restituisco symbolposition + 1 se ci sono altri simboli	*/
				if(subString.length()>=1) result.add(symbolPosition +1);
		return result;
	}
	
	public static int numberOfSymbols(String regExTA)
	{
		StringTokenizer st = new StringTokenizer(regExTA, tokenListSeparator+tokenSentinels[0]+tokenSentinels[1]+tokenGroupDelimiter);
		
		return st.countTokens();
	}
	
	/* Effettua la conversione da stringa rappresentante TA a espressione regolare che la denota
	 * 
	 * Es:
	 * 		TA: (1,3)S,S(3,4)
	 * 		regExTA: *(null|*|*,*)]S,S[*,*,*(null|*)
	 * 
	 * Per fare ciò spezza la TA in due sott-espressioni:
	 * 		(1,3)S,S
	 * 		(3,4)
	 * ed effettua il parsing delle due in modo separato.	*/
	public static String toRegExTA(String TA)
	{
		String result = new String("");
		Pattern pattern = Pattern.compile(regExs[0]);
		Matcher matcher = pattern.matcher(TA);
		while (matcher.find()) {
            result+=parseRegEx(matcher.group(), true);
            result += tokenListSeparator;
        }
		
		pattern = Pattern.compile(regExs[1]);
		matcher = pattern.matcher(TA);
		matcher.find();
		result+=parseRegEx(matcher.group(), false);

		return result;
	}
}


