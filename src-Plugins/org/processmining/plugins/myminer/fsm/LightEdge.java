package org.processmining.plugins.myminer.fsm;


public class LightEdge implements Comparable<LightEdge> {
	private String to;
	private String symbol;
	
	LightEdge()
	{
		this.to = new String("");
		this.symbol = new String("");
	}
	

	LightEdge(String symbol)
	{
		this.to = new String("");
		this.symbol = new String(symbol);
	}
	
	LightEdge(String symbol, String to)
	{
		this.to = new String(to);
		this.symbol = new String(symbol);
	}
	
	public void setTo(String to)
	{
		this.to = to;
	}
	
	public void setSymbol(String symbol)
	{
		this.symbol = symbol;
	}
	
	public String getTo()
	{
		return this.to;
	}
	
	public String getSymbol()
	{
		return this.symbol;
	}

	@Override
	public int compareTo(LightEdge edge1) {
		
		return  this.symbol.equals(edge1.symbol)?  (this.to.compareTo(edge1.to)):(this.symbol.compareTo(edge1.symbol));
	}
	
	@Override
    public int hashCode() {
		return this.symbol.hashCode() + 7*this.to.hashCode();
	}
	
	@Override
	public boolean equals(Object o)
	{
		return (o.getClass()==LightEdge.class && this.compareTo((LightEdge)o)==0)? true:false;
	}
	
	@Override
	public String toString()
	{
		return "["+this.symbol+"\t-->\t"+this.to+"]\t";
	}
}
