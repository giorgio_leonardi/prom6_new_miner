package org.processmining.plugins.myminer.fsm;
//import java.io.Console;
import java.util.Collection;
import java.util.Vector;




public class TestGraph {
	
	private static char rndChar () {
	    int rnd = (int) (Math.random() * 52); // or use Random or whatever
	    char base = (rnd < 26) ? 'A' : 'a';
	    return (char) (base + rnd % 26);

	}
	
	public static void main(String[] args)
	{
		Graph listGraph = new ListGraph(), matrixGraph = new MatrixGraph();
		boolean fine = false;
		String temp = "";
		Collection<Vertex> vertices = new Vector<Vertex>();
		Collection<Edge> edges = new Vector<Edge>();
		
		String temp1,temp2,temp3;

		Console console = new Console();
		

		while(!fine)
		{
			
			temp = console.readLine("%nEnter your chose:" +
									"\n\t0- genarate default graph" +
									"\n\t1- print graph" +
									"\n\t2- addEdge()" +
									"\n\t3- addVertex(Vertex v)" +
									"\n\t4- addVertex(String v)" +
									"\n\t5- addAllVertex()" +
									"\n\t6- containsEdge(Edge e)" +
									"\n\t7- containsEdge(Vertex from, Vertex to)" +
									"\n\t8- containsVertex(Vertex v)" +
									"\n\t9- edgeSet()" +
									"\n\t10- edgesOf(Vertex v)" +
									"\n\t11- edgeOf(Vertex v)" +
									"\n\t12- getAllEdges(Vertex from, Vertex to)" +
									"\n\t13- getVertex()" +
									"\n\t14- getVertex(String name)" +
									"\n\t15- removeAllEdges(Collection<Edge> e)" +
									"\n\t16- removeAllEdges(Vertex from, Vertex to)" +
									"\n\t17- removeAllVertices(Collection<Vertex> n)" +
									"\n\t18- removeEdge(Edge e)" +
									"\n\t19- removeEdge(Vertex from, Vertex to, String symbol)" +
									"\n\t20- removeVertex(Vertex n)" +
									"\n\t21- vertexSet()" +
									"\n\t22- setVertexName(Vertex oldVertex, String newVertexName)" +
									"\n\t23- esci\n" );

			if(temp.equals("0"))
			{
				for(Integer i=0; i <78; i++)
				{
					listGraph.addVertex(new Vertex(i.toString()));
					matrixGraph.addVertex(new Vertex(i.toString()));
				}
				for(Integer i=0; i<(Math.random()*20); i++)
				{
					temp1 = String.valueOf(((int)(100*Math.random())+1)%78);
					temp2 = String.valueOf(((int)(100*Math.random())+1)%78);
					temp3 = ""+rndChar();
					
					listGraph.addEdge(new Vertex(temp1), new Vertex(temp2), temp3);
					matrixGraph.addEdge(new Vertex(temp1), new Vertex(temp2), temp3);
				}
				
			}
			else if(temp.equals("1"))
			{
				System.out.println(""
								   +"\nGrafo con lista di adiacenza:\n"+listGraph.toString()
								   +"\nGrafo con matrice di adiacenza:\n"+matrixGraph.toString());
			}
			else if(temp.equals("2"))
			{
				temp1 = console.readLine("%nInserisci il vertice origine ");
				temp2 = console.readLine("%nInserisci il vertice destinazione ");
				temp3 = console.readLine("%nInserisci il peso dell'arco ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.addEdge(new Vertex(temp1), new Vertex(temp2), temp3)
								   +"\nmatrixGraph: "+matrixGraph.addEdge(new Vertex(temp1), new Vertex(temp2), temp3));
			}
			else if(temp.equals("3"))
			{
				temp1 = console.readLine("%nInserisci il nome del vertice da aggiungere ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.addVertex(new Vertex(temp1))
								   +"\nmatrixGraph: "+matrixGraph.addVertex(new Vertex(temp1)));
			}
			else if(temp.equals("4"))
			{
				temp1 = console.readLine("%nInserisci il nome del vertice da aggiungere ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.addVertex(temp1)
								   +"\nmatrixGraph: "+matrixGraph.addVertex(temp1));
			}
			else if(temp.equals("5"))
			{
				while(!(temp1 = console.readLine("%nInserisci il nome del vertice da aggiungere (stop per terminare) ")).equals("stop"))
				{
					vertices.add(new Vertex(temp1));
				}
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.addAllVertex(vertices)
								   +"\nmatrixGraph: "+matrixGraph.addAllVertex(vertices));
			}
			else if(temp.equals("6"))
			{
				temp1 = console.readLine("%nInserisci il vertice origine ");
				temp2 = console.readLine("%nInserisci il vertice destinazione ");
				temp3 = console.readLine("%nInserisci il peso dell'arco ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.containsEdge(new Edge(temp3,temp1, temp2))
								   +"\nmatrixGraph: "+matrixGraph.containsEdge(new Edge(temp3,temp1, temp2)));
			}
			else if(temp.equals("7"))
			{
				temp1 = console.readLine("%nInserisci il vertice origine ");
				temp2 = console.readLine("%nInserisci il vertice destinazione ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.containsEdge(new Vertex(temp1), new Vertex(temp2))
								   +"\nmatrixGraph: "+matrixGraph.containsEdge(new Vertex(temp1), new Vertex(temp2)));
			}
			else if(temp.equals("8"))
			{
				temp1 = console.readLine("%nInserisci il nome del vertice ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.containsVertex(new Vertex(temp1))
								   +"\nmatrixGraph: "+matrixGraph.containsVertex(new Vertex(temp1)));
			}
			else if(temp.equals("9"))
			{
				System.out.println(""
						   +"\nlistGraph: "+listGraph.edgeSet()
						   +"\nmatrixGraph: "+matrixGraph.edgeSet());
			}
			else if(temp.equals("10"))
			{
				temp1 = console.readLine("%nInserisci il nome del vertice ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.edgesOf(new Vertex(temp1))
								   +"\nmatrixGraph: "+matrixGraph.edgesOf(new Vertex(temp1)));
			}
			else if(temp.equals("11"))
			{
				temp1 = console.readLine("%nInserisci il nome del vertice ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.edgeOf(new Vertex(temp1))
								   +"\nmatrixGraph: "+matrixGraph.edgeOf(new Vertex(temp1)));
			}
			else if(temp.equals("12"))
			{
				temp1 = console.readLine("%nInserisci il vertice origine ");
				temp2 = console.readLine("%nInserisci il vertice destinazione ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.getAllEdges(new Vertex(temp1), new Vertex(temp2))
								   +"\nmatrixGraph: "+matrixGraph.getAllEdges(new Vertex(temp1), new Vertex(temp2)));
			}
			else if(temp.equals("13"))
			{
				System.out.println(""
						   +"\nlistGraph: "+listGraph.getVertex()
						   +"\nmatrixGraph: "+matrixGraph.getVertex());
			}
			else if(temp.equals("14"))
			{
				temp1 = console.readLine("%nInserisci il nome del vertice ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.getVertex(temp1)
								   +"\nmatrixGraph: "+matrixGraph.getVertex(temp1));
			}
			else if(temp.equals("15"))
			{
				while(!(temp1 = console.readLine("%nInserisci il vertice sorgente (stop per terminare) ")).equals("stop")
				   && !(temp2 = console.readLine("%nInserisci il vertice destinazione (stop per terminare) ")).equals("stop")
				   && !(temp3 = console.readLine("%nInserisci il peso dell'arco (stop per terminare) ")).equals("stop"))
				{
					edges.add(new Edge(temp3,temp1,temp2));
				}
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.removeAllEdges(edges)
								   +"\nmatrixGraph: "+matrixGraph.removeAllEdges(edges));
			}
			else if(temp.equals("16"))
			{
				temp1 = console.readLine("%nInserisci il vertice origine ");
				temp2 = console.readLine("%nInserisci il vertice destinazione ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.removeAllEdges(new Vertex(temp1), new Vertex(temp2))
								   +"\nmatrixGraph: "+matrixGraph.removeAllEdges(new Vertex(temp1), new Vertex(temp2)));
			}
			else if(temp.equals("17"))
			{
				while(!(temp1 = console.readLine("%nInserisci il nome del vertice da aggiungere (stop per terminare) ")).equals("stop"))
				{
					vertices.add(new Vertex(temp1));
				}
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.removeAllVertices(vertices)
								   +"\nmatrixGraph: "+matrixGraph.removeAllVertices(vertices));
			}
			else if(temp.equals("18"))
			{
				temp1 = console.readLine("%nInserisci il vertice origine ");
				temp2 = console.readLine("%nInserisci il vertice destinazione ");
				temp3 = console.readLine("%nInserisci il peso dell'arco ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.removeEdge(new Edge(temp3,temp1, temp2))
								   +"\nmatrixGraph: "+matrixGraph.removeEdge(new Edge(temp3,temp1, temp2)));
			}
			else if(temp.equals("19"))
			{
				temp1 = console.readLine("%nInserisci il vertice origine ");
				temp2 = console.readLine("%nInserisci il vertice destinazione ");
				temp3 = console.readLine("%nInserisci il peso dell'arco ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.removeEdge(new Vertex(temp1), new Vertex(temp2), temp3)
								   +"\nmatrixGraph: "+matrixGraph.removeEdge(new Vertex(temp1), new Vertex(temp2), temp3));
			}
			else if(temp.equals("20"))
			{
				temp1 = console.readLine("%nInserisci il nome del vertice ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.removeVertex(new Vertex(temp1))
								   +"\nmatrixGraph: "+matrixGraph.removeVertex(new Vertex(temp1)));
			}
			else if(temp.equals("21"))
			{
				System.out.println(""
						   +"\nlistGraph: "+listGraph.vertexSet()
						   +"\nmatrixGraph: "+matrixGraph.vertexSet());
			}
			else if(temp.equals("22"))
			{
				temp1 = console.readLine("%nInserisci il nome del vertice ");
				temp2 = console.readLine("%nInserisci il nuovo nome ");
				
				System.out.println(""
								   +"\nlistGraph: "+listGraph.setVertexName(new Vertex(temp1), temp2)
								   +"\nmatrixGraph: "+matrixGraph.setVertexName(new Vertex(temp1), temp2));
			}
			else if(temp.equals("23"))
			{
				fine = true;
			}
			else
			{
				System.out.println("Scelta non consentita!!");
			}
		}
	}
}
