package org.processmining.plugins.myminer.fsm;



public class TestFSM {
	
	
	
	public static void main(String[] args)
	{
		NonDeterministicFSM fsm = new NonDeterministicFSM();
		FSM detFSM ;
		boolean fine = false;
		String temp = "";

		Console console = new Console();


		while(!fine)
		{
			
			temp = console.readLine("%nEnter your chose:" +
									"\n\t1-generate nonDetFSM " +
									"\n\t2-print FSM" +
									"\n\t3-generates the FSM declaring initial state and the first state" +
									"\n\t4-addState()" +
									"\n\t5-addTransitionFromStateTo()"+
									"\n\t6-removeState()"+
									"\n\t7-removeTransitionFromState()"+
									"\n\t8-fromNonDetToDetFSM(FSM nonDetFSM)"+
									"\n\t9-"+
									"\n\t10-"+
									"\n\t11-"+
									"\n\t12-"+
									"\n\t13-"+
									"\n\t14-"+
									"\n\t15-esci\n");

			if(temp.equals("1"))
			{
				System.out.println((fsm = new NonDeterministicFSM(console.readLine("%nInserisci l'espressione regolare "))).toString());
			}
			else if(temp.equals("2"))
			{
				System.out.println(fsm.toString());
			}
			else if(temp.equals("3"))
			{
				System.out.println((fsm = new NonDeterministicFSM(console.readLine("%nInserisci l'espressione regolare "),
						console.readLine("%nInserisci il nome dello stato iniziale "),
						console.readLine("%nInserisce il nome del primo stato "))).toString());
			}
			else if(temp.equals("4"))
			{
				System.out.println(fsm.addState(console.readLine("%nInserisci il nome dello stato da aggiungere ")));
			}
			else if(temp.equals("5"))
			{
				System.out.println(fsm.addTransition(console.readLine("%nInserisci il nome dello stato da cui parte la transizione "),
						console.readLine("%nInserisci il nome dello stato in cui la transizione porta "),
						console.readLine("%nInserisci il simbolo con il quale effettuare la transizione ")));
			}
			else if(temp.equals("6"))
			{
				System.out.println(fsm.removeState(console.readLine("%nInserisci il nome dello stato da rimuovere ")));
			}
			else if(temp.equals("7"))
			{
				System.out.println(fsm.removeTransition(console.readLine("%nInserisci il nome dello stato da cui parte la transizione "),
						console.readLine("%nInserisci il nome dello stato in cui la transizione porta "),
						console.readLine("%nInserisci il simbolo con il quale viene effettuata la transizione la transizione ")));
			}
			else if(temp.equals("8"))
			{
				System.out.println(detFSM = new DeterministicFSM(fsm));
			}
			else if(temp.equals("9"))
			{
				fine = true;
			}
			else if(temp.equals("10"))
			{
				fine = true;
			}
			else if(temp.equals("11"))
			{
				fine = true;
			}
			else if(temp.equals("12"))
			{
				fine = true;
			}
			else if(temp.equals("13"))
			{
				fine = true;
			}
			else if(temp.equals("14"))
			{
				fine = true;
			}
			else if(temp.equals("15"))
			{
				fine = true;
			}
			else
			{
				System.out.println("Scelta non consentita!!");
			}
		}
	}
}
