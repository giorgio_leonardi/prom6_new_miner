package org.processmining.plugins.myminer.fsm;

public class Vertex implements Comparable<Vertex> {
	private String name;
	
	Vertex()
	{
		this.name = new String();
	}
	
	Vertex(String name)
	{
		this.name = name;
	}
	
	public void setName(String newName)
	{
		this.name = newName;
	}
	
	public String getName()
	{
		return this.name;
	}

	@Override
	public int compareTo(Vertex node1) {
		return this.name.compareTo(node1.name);
	}
	
	@Override
    public int hashCode() {
		return this.name.hashCode();
	}
	
	@Override
	public boolean equals(Object o)
	{
		return (o.getClass()==Vertex.class && this.compareTo((Vertex)o)==0)? true:false;
	}
	
	@Override
	public String toString()
	{
		return this.name;
	}
}
