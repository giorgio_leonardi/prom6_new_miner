package org.processmining.plugins.myminer.fsm;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.Vector;


public abstract class FSM {
	private Graph graph;
	private String startState;
	private Collection<String> finalStates;
	
	private static String startStateDefault = "0"; 
	private static String firstStateDefault = "1"; 
	
	FSM()
	{
		this.graph = new ListGraph();
		this.startState = new String(startStateDefault );
		this.finalStates = new Vector<String>();
		this.addState(startState);
	}
	
	FSM(String newStartState)
	{
		this.graph = new ListGraph();
		startStateDefault  = newStartState;
		this.startState = new String(startState);
		this.finalStates = new Vector<String>();
		this.addState(startState);
	}
	
	FSM(String newStartState, String newFirstState)
	{
		this.graph = new ListGraph();
		startState = newStartState;
		firstStateDefault  = newFirstState;
		
		this.startState = new String(startState);
		this.finalStates = new Vector<String>();
		this.addState(startState);
	}
	
	public Graph getGraph()
	{
		return this.graph;
	}
	
	public String getStartState()
	{
		return this.startState;
	}
	
	public void setStartState(String newStartState)
	{
		startStateDefault = newStartState;
		this.startState = newStartState;
	}
	
	public boolean changeStateName(String oldState, String newState)
	{
		if(!graph.containsVertex(new Vertex(oldState))) return false;
		
		if(!this.graph.setVertexName(this.graph.getVertex(oldState), newState)) return false;
		
		if(oldState.equals(this.startState)) 
		{
			startStateDefault = newState;
			this.startState = newState;
		}
		
		if(this.finalStates.remove(oldState)) this.finalStates.add(newState);
		
		return true;
	}
	
	public Collection<String> getFinalStates()
	{
		return this.finalStates;
	}
	
	public void setFinalStates(Collection<String> finalS)
	{
		this.finalStates.clear();
		this.finalStates.addAll(finalS);
	}
	
	public boolean addFinalState(String finalS)
	{
		return this.finalStates.contains(finalS)? true : this.finalStates.add(finalS);
	}
	
	public boolean addAllFinalState(Collection<String> finalS)
	{
		boolean addedAll = true;
		for(String s: finalS)
		{
			if(!this.addFinalState(s)) addedAll = false;
		}
		return addedAll;
	}
	
	public boolean removeFinalState(String finalS)
	{
		return this.finalStates.remove(finalS);
	}
	
	public boolean removeAllFinalState(Collection<String> finalS)
	{
		return this.finalStates.removeAll(finalS);
	}
	
	public  int getNumberOfState()
	{
		return this.graph.getNumberOfVertex();
	}
	
	public int getNumberOfTransition()
	{
		return this.graph.getNumberOfEdges();
	}
	
	public void setFirstStateDefault(String newFirstState)
	{
		firstStateDefault = newFirstState;
	}
	
	public String getFirstStateDefault()
	{
		return firstStateDefault ;
	}
	
	public Collection<String> getStates()
	{
		Collection<String> result = new Vector<String>();
		
		for(Vertex n: this.graph.getVertex())
		{
			result.add(n.getName());
		}
		return result;
	}
	
	public boolean addState(String s)
	{
		return this.graph.addVertex(s);
	}
	
	public boolean addAllStates(Collection<String> s)
	{
		boolean addedAll = true;
		for(String newVertex: s)
		{
			if(!this.graph.addVertex(newVertex))	addedAll = false;
		}
		return addedAll;
	}
	
	public boolean containsTransition(String from, String to, String symbol)
	{
		return this.graph.containsEdge(new Vertex(from), new Vertex(to));
	}
	
	public boolean containsState(String state)
	{
		return this.graph.containsVertex(new Vertex(state));
	}
	
	public boolean removeState(String s)
	{
		return (this.startState.equals(s))?  false :
			   (this.graph.removeVertex(this.graph.getVertex(s)))? (this.finalStates.remove(s) || true) : false;
	}
	
	public boolean removeAllState(Collection<String> s)
	{
		boolean removedAll = true;
		for (String state: s)
		{
			if (!this.removeState(state)) removedAll = false;
		}
		return removedAll;
	}
	
	public Collection<Edge> getTransitionFromState(String s)
	{
		return this.graph.edgeOf(this.graph.getVertex(s));
	}
	
	public Set<Edge> getAllTransition()
	{
		return this.graph.edgeSet();
	}
	
	public boolean addTransition(String from, String to, String symbol)
	{
		if(!this.graph.containsVertex(new Vertex(from)) || !this.graph.containsVertex(new Vertex(to))) return false;
		return this.graph.addEdge(this.graph.getVertex(from), this.graph.getVertex(to), symbol);
	}
	
	public boolean addAllTransition(Collection<Edge> edges)
	{
		boolean addedAll = true;
		for(Edge edge: edges)
		{
			if(!this.graph.addEdge(new Vertex(edge.getFrom()), 
					new Vertex(edge.getTo()), 
					edge.getSymbol())) addedAll = false;
		}
		
		return addedAll;
	}
	
	public boolean removeTransition(String from,String to, String symbol)
	{
		return this.graph.removeEdge(this.graph.getVertex(from), this.graph.getVertex(to), symbol);
	}
	
	public String toString()
	{
		String result = new String("");
		result += "FSM:"
				+"\n\tstato iniziale:\t"+this.getStartState()
				+"\n\tNumero di stati:\t"+this.getNumberOfState()
				+"\n\tNumero di Transizioni:\t"+this.getNumberOfTransition()
				+"\n\n\tStati:\t"+this.getStates()
				+"\n\n";
		
		for(String state: this.getStates())
		{
			result +="\n\tStato "+state+" \t";
			for(Edge e: this.getTransitionFromState(state))
			{
				result += "("+e.getSymbol()+"\t->   "+e.getTo()+"\t)\t";
			}
			if(this.finalStates.contains(state))	result += "<- [FINALE]\t";
		}
				
		result +="\n\n\tstati finali:\t"+this.getFinalStates().toString();
		
		return result;
				
	}
	
	
	public Set<String> getStateReachableFrom(String s)
	{
		Set<String> result = new HashSet<String>();
		Stack<String> stack = new Stack<String>();
		String st;
		
		stack.push(s);
		
		while(!stack.isEmpty())
		{
			st = stack.pop();
			for(Edge e: this.graph.edgeOf(this.graph.getVertex(st)))
			{
				
				if(e.getSymbol().equals(RegExTA.getTokens().get(1)))
				{	result.add(e.getTo());
					stack.push(e.getTo());
				}
			}
		}
		return result;
	}
	
	
	public Set<String> getStateReachableFrom(String s, String symbol)
	{
		Set<String> result = new HashSet<String>();
		Stack<String> stack = new Stack<String>();
		String st;
		
		stack.push(s);
		
		while(!stack.isEmpty())
		{
			st = stack.pop();
			for(Edge e: this.graph.edgeOf(this.graph.getVertex(st)))
			{
				if (e.getSymbol().equals(symbol))	result.add(e.getTo());
				if(e.getSymbol().equals(RegExTA.getTokens().get(0)))
				{
					stack.push(e.getTo());
				}
			}
		}
		return result;
	}
	
	/*
	 * 
	 * push all states in T onto stack;
	 * initialize epsilonClosure(T) to T;
	 * 
	 * while stack is not empty do begin
	 * 
	 * 		pop t, the top element, off of stack;
	 * 
	 * 		for each state u with an edge from t to u labeled ε do
	 * 
	 * 				if u is not in epsiloClosure(T) do begin
	 * 						add u to epsilonClosure(T);
	 * 						push u onto stack;
	 * 				end if
	 * 
	 * 		end for
	 * end algorithm;			*/
	public Set<String> epsilonClosure(Set<String> T)
	{
		String top;
		Set<String> result = new HashSet<String>(T);
		
		Stack<String> stack = new Stack<String>();
		stack.addAll(T);
		
		while(!stack.isEmpty())
		{
			top = stack.pop();
			for(String reachable: getStateReachableFrom(top))
			{
				if(!result.contains(reachable))
				{
					result.add(reachable);
					stack.push(reachable);
				}
			}
		}
		
		return result;
	}
	
	
	
	/*
	 * push s onto stack;
	 * initialize epsilonClosure(s) to s;
	 * 
	 * while stack is not empty do begin
	 * 
	 * 		pop t, the top element, off of stack;
	 * 
	 * 		for each state u with an edge from t to u labeled ε do
	 * 
	 * 				if u is not in epsiloClosure(T) do begin
	 * 						add u to epsilonClosure(T);
	 * 						push u onto stack;
	 * 				end if
	 * 
	 * 		end for
	 * end algorithm;			*/
	public Set<String> epsilonClosure(String s)
	{
		String top;
		Set<String> result = new HashSet<String>();
		result.add(s);
		
		Stack<String> stack = new Stack<String>();
		stack.add(s);
		
		while(!stack.isEmpty())
		{
			top = stack.pop();
			for(String reachable: getStateReachableFrom(top))
			{
				if(!result.contains(reachable))
				{
					result.add(reachable);
					stack.push(reachable);
				}
			}
		}
		
		return result;
	}
	
	
	public Set<String> move(Set<String> T, String symbol)
	{
		Set<String> result = new HashSet<String>();
		for(String state: T)
		{
			result.addAll(this.getStateReachableFrom(state, symbol));
		}
		
		return result;
	}
	
	public Set<String> move(String T, String symbol)
	{
		Set<String> result = new HashSet<String>();
		StringTokenizer st = new StringTokenizer(T, RegExTA.getTokens().get(3));
		
		while(st.hasMoreTokens())
		{
			result.addAll(this.getStateReachableFrom(st.nextToken(), symbol));
		}
		
		return result;
	}
	
	public void reset(){
		startStateDefault = "0"; 
		firstStateDefault = "1"; 
	}

}
