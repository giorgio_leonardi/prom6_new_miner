package org.processmining.plugins.myminer.fsm;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;

import org.processmining.models.andOrGraph.Node;
import org.processmining.models.andOrGraph.Pair;
import org.processmining.plugins.myminer.index.IndexTable;

import permutation.Permutation;
import thomson.AnyOrder;
import thomson.Delay;
import thomson.Pattern;
import thomson.Sequence;

public class NonDeterministicFSM_Thomson extends NonDeterministicFSM{
	private int cont =0;
	
	private Map<Edge,Integer> struct=new HashMap<Edge,Integer>();  
	
	public NonDeterministicFSM_Thomson (String regExTA,IndexTable it){
		super();
		//genera i pattern della query
		Vector<Pattern> p=generatePatterns(regExTA);
		//System.out.println(p);
		//stati finali dell'automa generato dal pattern precedente
		Collection<String> previousFinalState=null;
		//stato iniziale della'automa generato dal pattern corrente
		String startState = null;
		//costruisco gli automi relativi ad ogni pattern
		for(Pattern patt:p){
			
			//se il pattern � un delay
			if (patt instanceof Delay){
				startState=delayPattern(patt,it);
			}
			//se il pattern � una sequenza
			if (patt instanceof Sequence){
				startState=sequencePattern(patt,it);
			}
			//se il apttern � un anyorder
			if (patt instanceof AnyOrder){
				startState=anyOrderPattern(patt,it);
			}
			
			//collego i pattern
			if (previousFinalState!=null)
			{
				//per ogni stato finale dell'automa del pattern precendente costruisco una epsilontransizione verso lo stato iniziale dell'automa del pattern corrente
				for (String s:previousFinalState){
					
					addTransition(s, startState, "null");
				}
				removeAllFinalState(previousFinalState);
			}
			//modifica la variabile previousFinalState
			previousFinalState=new Vector<String>(getFinalStates());
			
		//	System.out.println(patt);
			//System.out.println(this.toString());
		}
	//	System.out.println(this.toString());
	}
	
	

	private String anyOrderPattern(Pattern patt,IndexTable it) {
		//stato iniziale dell'automa
		String prev=Integer.toString(cont);
		//indice del prossimo stato
		cont++;
		//aggiungo lo stato 
		addState(prev);
		//valore da ritornare
		String start=prev;
		//prima permutazione
		Collection<String> seq = Permutation.Initialize(patt.toString());
		int pos=it.addEntry(Permutation.getElement());
		//finch� ho permutazioni devo creare l'automa corrispondente
		while (seq!=null){
			//per ogni permutazione creo l'automa che � riconoscitore di una sequenza
			for(String s:seq){
				//nuovo stato
				String to=Integer.toString(cont);
				//indice del prossimo stato
				cont++;
				//aggiungo lo stato
				this.addState(to);
				//aggiungo la transizione
				this.addTransition(prev, to, s);
				Vertex vFrom=getGraph().getVertex(prev);
				Vertex vTo=getGraph().getVertex(to);
				Set<Edge> ed = getGraph().getAllEdges(vFrom, vTo);	
				
				Edge e= ed.iterator().next();
				
				struct.put(e, pos);
				//lo stato precedente diventa il corrente
				prev=to;
			}
			//l'ultimo stato � finale
			addFinalState(prev);
			//tutte le permutaizoni hanno lo stesso stato iniziale
			prev=start;
			//prossima permutazione
			seq=Permutation.getPermutation();
		}
		
		//cancello tutte le permutazioni 
		Permutation.close();
		return start;
	}

	private String sequencePattern(Pattern patt, IndexTable it) {
		//stato iniziale
		String prev=Integer.toString(cont);
		//valore da ritornare
		String start=prev;
		//aggiungo lo stato
		addState(prev);
		//indice del prossimo stato
		cont++;
		//creo l'automa che � riconoscitore di una sequenza
		for(String s:patt.getElement()){
			//nuovo stato
			String to=Integer.toString(cont);
			//indice del prossimo stato
			cont++;
			//aggiungo lo stato
			addState(to);
			
			//aggiungo la transizione

			addTransition(prev, to,s);
			
			Vertex vFrom=getGraph().getVertex(prev);
			Vertex vTo=getGraph().getVertex(to);
			Set<Edge> ed = getGraph().getAllEdges(vFrom, vTo);	
			
			Edge e= ed.iterator().next();
			
			int pos=it.addEntry(s);
			
			struct.put(e,pos);
			
			//lo stato precedente diventa il corrente
			prev=to;
		}
		//l'ultimo stato � finale
		addFinalState(prev);
		return start;
	}



	private String delayPattern(Pattern patt, IndexTable it) {
		Delay d=(Delay)patt;
		String prev=Integer.toString(cont);
		addState(prev);
		cont++;
		String start=prev;
		int min=d.getMin();
		int max=d.getMax();
		int pos=it.addEntry((String)null);
		for (int i=0;i<max;i++)
		{
			//nuovo stato
			String to=Integer.toString(cont);
			//indice del prossimo stato
			cont++;
			//aggiungo lo stato
			this.addState(to);
			//aggiungo la transizione
			this.addTransition(prev, to, "*");
			if (i>=min) addFinalState(prev);
			//lo stato precedente diventa il corrente

			Vertex vFrom=getGraph().getVertex(prev);
			Vertex vTo=getGraph().getVertex(to);
			Set<Edge> ed = getGraph().getAllEdges(vFrom, vTo);	
			
			Edge e= ed.iterator().next();
			
			struct.put(e,pos);
			
			prev=to;
		}
	
		addFinalState(prev);
		return start;
	}



	/*genera tutti i pattern che trova nella query passata*/
	private Vector<Pattern> generatePatterns(String regExTA) {
		Vector<Pattern> res = new Vector<Pattern>();
	
		while (true)
		{
			int pos=0;
			int newPos=regExTA.indexOf(')',pos);
			if (newPos==-1) break;
			res.add(new Delay(regExTA.substring(pos+1, newPos)));
			pos=regExTA.indexOf('(',newPos);
			if (pos==-1) break;
			res.addAll(extractPatterns(regExTA.substring(newPos+1, pos)));
			regExTA=regExTA.substring(pos);
			//System.out.println("Nuova stringa " + regExTA);
		}
		return res;
	}

	private Collection<? extends Pattern> extractPatterns(String substring) {
		//System.out.println("Sottostringa: "+substring);
		Vector<Pattern> res= new Vector<Pattern>();
		String[] elem = substring.split(",");
		String seq="";
		for(String s:elem){
			//System.out.println(s);
			if (s.contains("&")){
				if (seq!="")
					res.add(new Sequence(seq.substring(0,seq.length()-1)));
				res.add(new AnyOrder(s));
				seq="";
			}
			else seq+=s+",";
		}
		if (seq!="")
			res.add(new Sequence(seq.substring(0,seq.length()-1)));
		return res;
	}

	/*calcola l'insieme di tutti gli stati raggiungibili dallo stato s con il simbolo symbols.
	 * aggiorna l'IndexTable (curr) passato come parametro aggiungendo tutti i nuovi match trovati con node
	 * */
	public Set<Pair<String,IndexTable>> getStateReachableFrom(Node node,String s, String symbol,IndexTable curr)
	{
		Set<Pair<String,IndexTable>> result = new HashSet<Pair<String,IndexTable>>();
		Stack<Pair<String,IndexTable>> stack = new Stack<Pair<String,IndexTable>>();
		Pair<String,IndexTable> st;
		
		stack.push(new Pair<String,IndexTable>(s,curr));
		
		while(!stack.isEmpty())
		{
			st = stack.pop();
			for(Edge e: this.getGraph().edgeOf(this.getGraph().getVertex(st.getFirst())))
			{
				if (e.getSymbol().equals(symbol) || e.getSymbol().equals("*"))	{
					IndexTable newT=(IndexTable) st.getSecond().clone();
					result.add(new Pair<String,IndexTable>(e.getTo(),newT));
					int pos=struct.get(e);
					newT.updateEntry(pos,symbol,node);
				}
				if(e.getSymbol().equals(RegExTA.getTokens().get(1)))
				{
					stack.push(new Pair<String,IndexTable>(e.getTo(),curr));
				}
			}
		}
		return result;
	}


	/*simula l'automa a partire dallo stato state considerando come simboli di transizione gli elementi di symbols*/
	public Set<Pair<String,IndexTable>> simulate(Node node,Vector<String> symbols, String state,
			IndexTable curr) {
	//insieme dei risultati	
	Set<Pair<String,IndexTable>> result=new HashSet<Pair<String,IndexTable>>();
	//metto la coppia stato iniziale dell'automa/curr nei risultati 
	result.add(new Pair<String,IndexTable>(state,curr));
	//set temporaneo
	Set<Pair<String,IndexTable>> new_result=new HashSet<Pair<String,IndexTable>>();
	//devo provare tutti i simboli in symbols in tutti gli stati raggiungibili
	for (String s:symbols){
		//preso un simbolo lo provo su uno stato
		for (Pair<String,IndexTable> stato:result)
			//questo insieme conterr� tutti gli stati raggiungibili dall'insieme degli stati in result usando un simbolo s
			new_result.addAll(getStateReachableFrom(node, stato.getFirst(), s,stato.getSecond()));
	    //se � nullo ho finito, non ho trovato transizioni possibili
		if (new_result.isEmpty())
			return null;
		//altrimenti faccio la epsilon chiusura
		new_result=statesAfterEpsClosure(new_result);
		//aggiorno result per il nuovo simbolo
		result=new HashSet<Pair<String,IndexTable>>(new_result);
		new_result.clear();
	}
	return result;
}



	private Set<Pair<String, IndexTable>> statesAfterEpsClosure(
			Set<Pair<String, IndexTable>> states) {
		Pair<String, IndexTable> top;
		Set<Pair<String, IndexTable>> result = new HashSet<Pair<String, IndexTable>>(states);
			
		Stack<Pair<String, IndexTable>> stack = new Stack<Pair<String, IndexTable>>();
		stack.addAll(states);
			
		while(!stack.isEmpty())
		{
			top = stack.pop();
			for(Pair<String, IndexTable> reachable: getStateReachableFrom(top))
			{
				if(!result.contains(reachable))
				{
					result.remove(top);
					result.add(reachable);
					stack.push(reachable);
				}
			}
		}
			
		return result;
		}



	private Set<Pair<String, IndexTable>> getStateReachableFrom(Pair<String, IndexTable> s) {
		Set<Pair<String, IndexTable>> result = new HashSet<Pair<String, IndexTable>>();
		Stack<Pair<String, IndexTable>> stack = new Stack<Pair<String, IndexTable>>();
		Pair<String,IndexTable> st;
		
		stack.push(s);
		
		while(!stack.isEmpty())
		{
			st = stack.pop();
			Collection<Edge> edges = this.getGraph().edgeOf(this.getGraph().getVertex(st.getFirst()));
			if (edges.size()==1)
			{
				Edge e=edges.iterator().next();
				if(e.getSymbol().equals(RegExTA.getTokens().get(1)) )
				{	
					IndexTable newT = (IndexTable) s.getSecond().clone();
					Pair<String, IndexTable> newP = new Pair<String, IndexTable>(e.getTo(), newT);
					result.add(newP);
					stack.push(newP);
				}
			}
		}
		return result;	
	}



	public IndexTable getResult(Set<Pair<String, IndexTable>> new_states) {
		Collection<String> finalState = getFinalStates();
		for (Pair<String, IndexTable> s:new_states){
			if (finalState.contains(s.getFirst()))
				return s.getSecond();
		}
		return null;
		
	}
	
	
	
}
