package org.processmining.plugins.myminer.fsm;
import java.util.Collection;
import java.util.Set;

public interface Graph {
	
	public  int getNumberOfVertex();
	public int getNumberOfEdges();

	public boolean addEdge(Vertex from, Vertex to, String symbol);	//OK
	public boolean addVertex(Vertex n);	//OK
	public boolean addVertex(String n);	//OK
	public boolean addAllVertex(Collection<Vertex> v);
	
	public boolean containsEdge(Edge e); //OK
	public boolean containsEdge(Vertex from, Vertex to); //OK
	public boolean containsVertex(Vertex v);	//OK
	
	public Set<Edge> edgeSet() ;	//OK
	public Set<Edge> edgesOf(Vertex v);	//Restituisce un set contenete tutti gli archi che toccano il vertice v	
	public Collection<Edge> edgeOf(Vertex v);	//OK
	public Set<Edge> getAllEdges(Vertex from, Vertex to);	//OK
	public Collection<Vertex> getVertex();
	public Vertex getVertex(String name);
	
	public boolean removeAllEdges(Collection<Edge> e);	//OK
	public boolean removeAllEdges(Vertex from, Vertex to);	//OK
	public boolean removeAllVertices(Collection<Vertex> n);	//OK
	public boolean removeEdge(Edge e);	//OK
	public boolean removeEdge(Vertex from, Vertex to, String symbol);	//OK
	public boolean removeVertex(Vertex n);	//OK
	
	public Set<Vertex> vertexSet();	//OK
	
	public boolean setVertexName(Vertex oldVertex, String newVertexName);	//OK
	
	public String toString();
}
