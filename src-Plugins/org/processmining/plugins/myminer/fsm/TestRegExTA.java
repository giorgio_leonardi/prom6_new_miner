package org.processmining.plugins.myminer.fsm;


public class TestRegExTA {
	
	public static void main(String[] args)
	{
		boolean fine = false;
		String temp = "";

		Console console = new Console();
        
		

		while(!fine)
		{
			
			temp = console.readLine("%nEnter your chose:" +
									"\n\t1-getRegExs" +
									"\n\t2-setRegExs" +
									"\n\t3-getTokens" +
									"\n\t4-setTokens" +
									"\n\t5-toRegExTA"+
									"\n\t6-numberOfSymbols"+
									"\n\t7-first"+
									"\n\t8-follow"+
									"\n\t9-esci\n");

			if(temp.equals("1"))
			{
				System.out.println("Espressione regolare 1:\t"+RegExTA.getRegEx()[0]);
				System.out.println("Espressione regolare 2:\t"+RegExTA.getRegEx()[1]);
			}
			else if(temp.equals("2"))
			{
				RegExTA.setRegExs(new String[]{console.readLine("%nInserisci la prima espressione regolare:\t"),
						console.readLine("%nInserisci la seconda espressione regolare:\t")});
				System.out.println("Espressioni cambiate con successo!");
			}
			else if(temp.equals("3"))
			{
				System.out.println("Tokens:\t"+
											"\n\tSymbol:\t\t\t"+RegExTA.getTokens().get(0)+
											"\n\tEpsilon:\t\t"+RegExTA.getTokens().get(1)+
											"\n\tSentinels:\t\t"+RegExTA.getTokens().get(2)+
											"\n\tList separator:\t\t"+RegExTA.getTokens().get(3)+
											"\n\tGroup delimiter:\t"+RegExTA.getTokens().get(4));
			}
			else if(temp.equals("4"))
			{
				RegExTA.setTokens(console.readLine("%n\n\tToken simbolo:\t"), 
						console.readLine("%n\n\tToken epsilon:\t"),
						new String[]{console.readLine("%n\n\tToken sentinella A1:\t"),
									 console.readLine("%n\n\tToken sentinella A2:\t"),
									 console.readLine("%n\n\tToken sentinella B1:\t"),
									 console.readLine("%n\n\tToken sentinella B2:\t")}, 
						console.readLine("%n\n\tToken separatori:\t"), 
						console.readLine("%n\n\tToken separatori dei gruppi:\t"));
			}
			else if(temp.equals("5"))
			{
				System.out.println(RegExTA.toRegExTA(console.readLine("%nInserisci l'astrazione temporale " +
																	  "da parsificare:\t")));
			}
			else if(temp.equals("6"))
			{
				System.out.println(RegExTA.numberOfSymbols(console.readLine("%nInserisci l'espressione regolare" +
						  " ricavata da una astrazione temporale:\t")));
			}
			else if(temp.equals("7"))
			{
				System.out.println(new RegExTA((console.readLine("%nInserisci l'espressione regolare" +
						  " con la quale calcolare i first symbol:\t"))).first());
			}
			else if(temp.equals("8"))
			{
				System.out.println(new RegExTA((console.readLine("%nInserisci l'espressione regolare" +
						  " con la quale calcolare i Follow symbol:\t")))
				.follow(Integer.parseInt(console.readLine("%nInserisci la posizione del simbolo:\t"))).toString());
			}
			else if(temp.equals("9"))
			{
				fine = true;
			}
			else
			{
				System.out.println("Scelta non consentita!!");
			}
		}
	}
}
