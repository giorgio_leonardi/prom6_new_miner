package org.processmining.plugins.myminer.fsm;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;


public class NodeEdge implements Comparable<NodeEdge> {
	
	private Vertex node;
	private Collection<LightEdge> edges;
	
	NodeEdge(String nodeName, Collection<LightEdge> edges)
	{
		this.edges = edges;
		this.node = new Vertex(nodeName);
	}
	
	NodeEdge(String nodeName)
	{
		this.edges = new Vector<LightEdge>();
		this.node = new Vertex(nodeName);
	}
	
	NodeEdge()
	{
		this.edges = new Vector<LightEdge>();
		this.node = new Vertex();
	}

	@Override
	public int compareTo(NodeEdge nodeEdge1) {
		
		return this.node.equals(nodeEdge1.node)? (this.edges.equals(nodeEdge1.edges)? (0):(-1)):(this.node.compareTo(nodeEdge1.node));
	}
	
	@Override
    public int hashCode() {
		return this.node.hashCode() + 7*this.edges.hashCode();
	}
	
	public Vertex getVertex()
	{
		return this.node;
	}
	
	public void setVertex(Vertex v)
	{
		this.node = v;
	}
	
	public Collection<LightEdge> getEdges()
	{
		return this.edges;
	}
	
	public Collection<LightEdge> getEdges(String symbol)
	{
		Collection<LightEdge> result = new Vector<LightEdge>();
		
		for (LightEdge e: this.edges)
		{
			if (e.getSymbol().equals(symbol))
			{
				result.add(e);
			}
		}
		return result;
	}
	
	public void setEdges(Collection<LightEdge> e)
	{
		this.edges = e;
	}
	
	
	public boolean addEdge(LightEdge e)
	{
		return (this.edges.contains(e))?	false : this.edges.add(e);
		
	}
	
	public boolean addEdge(String symbol, Vertex from, Vertex to)
	{
		return (this.node.equals(from))? this.edges.add(new LightEdge(symbol, to.getName())) : false;
	}
	
	public boolean addEdge(String symbol, String from, String to)
	{
		return (this.node.getName().equals(from))? this.edges.add(new LightEdge(symbol, to)) : false;
	}
	
	public boolean addAllEdges(Collection<LightEdge> e)
	{
		return this.edges.addAll(e);
	}
	
	public boolean containsEdge(LightEdge e)
	{
		return this.edges.contains(e);
	}
	
	public boolean containsEdgeToVertex(Vertex n)
	{
		for(LightEdge e: this.edges)
		{
			if(e.getTo().equals(n.getName()));
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean containsAllEdge(Collection<LightEdge> e)
	{
		return this.edges.containsAll(e);
	}
	
	public int outDegree()
	{
		return this.edges.size();
	}
	
	public boolean removeEdge(LightEdge e)
	{
		return this.edges.remove(e);
	}
	
	public boolean removeEdgeToVertex(Vertex n)
	{
		Iterator<LightEdge> it = this.edges.iterator();
		LightEdge e;
		
		while(it.hasNext())
		{
			e = it.next();
			if(e.getTo().equals(n.getName()));
			{
				it.remove();
				return true;
			}
		}
		return false;
	}
	
	public boolean removeAllEdges(Collection<LightEdge> e)
	{
		return this.edges.removeAll(e);
	}
	
	public String toString()
	{
		return this.node.toString()+this.edges;
	}
	
}
