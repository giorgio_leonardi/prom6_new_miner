package org.processmining.plugins.myminer.fsm;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.Vector;

/*
 * Istanzia un automa a stati finiti con un solo stato [0] 
 * e nessuno stato finale.	*/
public class DeterministicFSM extends FSM{
	
	public DeterministicFSM()
	{
		super();
	}
	
	/*
	 * A partire dall'espressione regolare genera l'automa a stati finiti 
	 * non deterministico e ne effettua la conversione in deterministico.	*/
	public DeterministicFSM(String regExTA)
	{
		super();
		long time=System.nanoTime();
		NonDeterministicFSM nfa=new NonDeterministicFSM(regExTA);
		//System.out.println(nfa);
//		System.out.println("Numero stati automa NFA: "+nfa.getNumberOfState());
//		System.out.println("Numero transizioni automa NFA: "+nfa.getNumberOfTransition());
//		System.out.println("Automa non deterministico: "+(System.nanoTime()-time)/1000000.0);
		time=System.nanoTime();
		this.fromNonDetToDetFSM(nfa);
//		System.out.println("Conversione: "+(System.nanoTime()-time)/1000000.0);
	}
	
	/*
	 * Come nel caso precedente ma con la possibilità aggiuntiva di definire 
	 * il nome dello stato iniziale e quello del primo stato da cui partire 
	 * per la generazione.	*/
	public DeterministicFSM(String regExTA, String startStateName, String firstStateName)
	{
		super();
		this.fromNonDetToDetFSM(new NonDeterministicFSM(regExTA, startStateName, firstStateName));
	}
	
	/*
	 * A partire da un automa non deterministico genera 
	 * l'automa deterministico corrispondente.	*/
	public DeterministicFSM(NonDeterministicFSM nonDetFSM)
	{
		super();
		this.fromNonDetToDetFSM(nonDetFSM);
	}
	
	/*
	 * Restituisce ill nome dello stato raggiungibile dallo stato "s" con il simbolo "symbol"
	 * Se nell'automa non è presente la transizione δ(s, symbol) restituisce una stringa vuota.	*/
	protected String getTransitionFromState(String s, String symbol)
	{
		String result = new String();
		for(Edge e: this.getGraph().edgeOf(this.getGraph().getVertex(s)))
		{
			if(e.getSymbol().equals(symbol))
				//return e.getSymbol();
				return e.getTo();
		}
		return result;
	}

	/*
	 * Dato un Set di stati T, effettua l'operazione di merge restituendo il nome del
	 * macro-stato da generare.	*/
	private String mergeName(Set<String> T)
	{
		String result = new String();
		Iterator<String> it = T.iterator();
		
		while(it.hasNext())
		{
			
			result += it.next();
			if(it.hasNext()) result += RegExTA.getTokens().get(3);
		}
		
		return result;
	}
	
	/*
	 * Dato un set di stati T e un automa a stati finiti non deterministico, restituisce:
	 * 	-	true : se in T è contenuto uno stato finale dell'automa fsm
	 * 	-	false: altrimenti
	private boolean isFinal(Set<String> T, NonDeterministicFSM fsm)
	{
		for(String state: T)
		{
			if(fsm.getFinalStates().contains(state))
				return true;
		}
		return false;
	}		*/
	
	/*
	 * Data una stringa T rappresentane un macro-stato e un automa a stati finiti non deterministico, restituisce:
	 * 	-	true : se in T è contenuto uno stato finale dell'automa fsm
	 * 	-	false: altrimenti	*/
	private boolean isFinal(String T, NonDeterministicFSM fsm)
	{
		StringTokenizer st = new StringTokenizer(T,RegExTA.getTokens().get(3));
		
		while(st.hasMoreTokens())
		{
			if(fsm.getFinalStates().contains((String)st.nextElement()))
				return true;
		}
		return false;
	}
	
	/*
	 * initialy, epsilonClosure(startState) is the only state in DetFSM and it is unmarked;
	 * 
	 * while there is an unmarkes state T in DetFSM do begin
	 * 		mark T;
	 * 
	 * 		for each input symbol a do begin
	 * 			U := epsilonClosure(move(T,a)) ;
	 * 
	 * 			if U is not in DetFSM then
	 * 				add U as an unmarked state to DetFST;
	 * 
	 * 			add transition δ(T, a) = U in DetFSM;
	 * 		end for each;
	 * 
	 * end algorithm;		*/
	private boolean fromNonDetToDetFSM(NonDeterministicFSM nonDetFSM)
	{
		
		String newStartState;
		
		this.addState((newStartState = mergeName(nonDetFSM.epsilonClosure(nonDetFSM.getStartState()))));
		
		String T;
		String U;
		
		Stack<String> stack = new Stack<String>();
		stack.push(newStartState);
		
		while(!stack.isEmpty())
		{
			T = stack.pop();
			if(isFinal(T, nonDetFSM))
				this.addFinalState(T);
			
			for(String symbol: RegExTA.getAlphabet())
			{
				U = mergeName(nonDetFSM.epsilonClosure(nonDetFSM.move(T, symbol)));
				if(!this.containsState(U) && !U.isEmpty())
				{
					this.addState(U);
					if(isFinal(U, nonDetFSM))
						this.addFinalState(U);
					
					stack.push(U);
				}
				if(!U.isEmpty()) this.addTransition(T, U, symbol);
			}
		}
		
		if(this.getTransitionFromState(nonDetFSM.getStartState()).isEmpty())
		{
			this.setStartState(newStartState);
			this.removeState(nonDetFSM.getStartState());
		}
		
		
		return true;
	}
	
	
	/*
	 * Riceve in input una stringa rappresentante una lista di stati separati da "listSparator"
	 * e restituisce un iterator con tutti gli elementi di tale lista		*/
	private Iterator<String> characters(String list, String listSeparator)
	{
		StringTokenizer st = new StringTokenizer(list, listSeparator);
		Collection<String> result = new Vector<String>();
		
		while(st.hasMoreTokens())
		{
			result.add(st.nextToken());
		}
		
		return result.iterator();
	}
	
	
	/*
	 * simulation (min , max)
	 * 
	 * 		s := start state;
	 * 		c := nextxhar
	 * 		numberOfChar = 0;
	 * 
	 * 		while c != eof do
	 * 
	 * 				s := move(s,c);
	 * 				c := nextchar;
	 * 				numberOfChar += 1;
	 * 
	 * 				if numberOfChar > max then
	 * 					return no;
	 * 
	 * 		end while;
	 * 
	 * 		if s is in finalState and numberOfChare > min then
	 * 				return yes;
	 * 		else return no;		
	 * 
	 * end algorithm;		*/
	public boolean simulation(int min, int max, String input)
	{
		String state = this.getStartState();
		Iterator<String> characters = this.characters(input, RegExTA.getTokens().get(3));
		
		int count = 0;
		
		while(characters.hasNext())
		{
			if((state = getTransitionFromState(state,characters.next())).isEmpty() || count>max) return false;
			count ++;
		}
		
		return (this.getFinalStates().contains(state) && count>=min)? true : false;
	}
	
}
