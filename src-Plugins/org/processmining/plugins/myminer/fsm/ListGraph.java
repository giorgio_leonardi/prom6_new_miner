package org.processmining.plugins.myminer.fsm;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;


public class ListGraph implements Graph {
	
	private Integer numberOfVertex;
	private Integer numberOfEdges;
	
	private Collection<NodeEdge> record;
	
	ListGraph()
	{
		record = new Vector<NodeEdge>();
		numberOfVertex = 0;
		numberOfEdges = 0;
	}
	
	@Override
	public int getNumberOfVertex()
	{
		return this.numberOfVertex;
	}
	
	@Override
	public int getNumberOfEdges()
	{
		return this.numberOfEdges;
	}
	
	@Override
	public Vector<Vertex> getVertex() {
		Vector<Vertex> result = new Vector<Vertex>();
		for(NodeEdge nE: this.record)
		{
			result.add(nE.getVertex());
		}
		return result;
	}
	
	@Override
	public Vertex getVertex(String name)
	{
		for (NodeEdge nE: this.record)
		{
			if(nE.getVertex().getName().equals(name))
			{
				return nE.getVertex();
			}
		}
		return null;
	}

	@Override
	public boolean addVertex(Vertex v) {
		
		if(this.containsVertex(v)) return false;
		if (this.record.add(new NodeEdge(v.getName())))
		{
			this.numberOfVertex += 1;
			return true;
		}
		return false;
	}
	
	@Override
	public boolean addVertex(String v) {
		
		if(this.containsVertex(new Vertex(v))) return false;
		if (this.record.add(new NodeEdge(v)))
		{
			this.numberOfVertex += 1;
			return true;
		}
		return false;
	}

	@Override
	public boolean addAllVertex(Collection<Vertex> v) {
		boolean addedAll = true;
		
		for(Vertex newN: v)
		{
			if(!this.addVertex(newN))
			{
				addedAll = false;
			}
		}
		return addedAll;
	}
	
	@Override
	public boolean setVertexName(Vertex oldVertex, String newVertexName)
	{
		Iterator<NodeEdge> it = this.record.iterator();
		NodeEdge nE;
		
		while(it.hasNext())
		{
			nE = it.next();
			if(nE.getVertex().equals(oldVertex))
			{
				nE.setVertex(new Vertex(newVertexName));
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean removeVertex(Vertex v) {
		boolean foundEdge=false, foundVertex=false;
		NodeEdge nE;
		Iterator<NodeEdge> it = this.record.iterator();
		
		while(it.hasNext())
		{
			nE = it.next();
			
			if (nE.getVertex().equals(v))
			{
				numberOfEdges -= nE.getEdges().size();
				numberOfVertex -= 1;
				
				it.remove();
				
				foundVertex = true;
				continue;
			}
			if(this.removeAllEdges(nE.getVertex(), v)) foundEdge = true;
		}
		return (foundVertex || (foundVertex && foundEdge));
	}

	@Override
	public boolean removeAllVertices(Collection<Vertex> v) {
		boolean removedAll = true;
		for(Vertex newN: v)
		{
			if(!this.removeVertex(newN))
			{
				removedAll = false;
			}
		}
		return removedAll;
	}

	@Override
	public Collection<Edge> edgeOf(Vertex v) {
		Collection<Edge> result = new Vector<Edge>();
		for(NodeEdge nE: this.record)
		{
			if(nE.getVertex().equals(v))
			{
				for(LightEdge e: nE.getEdges())
				{
					result.add(new Edge(e.getSymbol(), nE.getVertex().getName(), e.getTo()));
				}
			}
		}
		return result;
	}
	
	@Override
	public Set<Edge> edgesOf(Vertex v)
	{
		Set<Edge> result = new HashSet<Edge>();
		
		for(NodeEdge nE: this.record)
		{
			if(nE.getVertex().equals(v))
			{
				for(LightEdge e: nE.getEdges())
				{
					result.add(new Edge(e.getSymbol(), nE.getVertex().getName(), e.getTo()));
				}
			}
			else for(LightEdge e: nE.getEdges())
				{
					if(e.getTo().equals(v.getName()))
					{
						result.add(new Edge(e.getSymbol(), nE.getVertex().getName(), e.getTo()));
					}
				}
		}
		return result;
	}
	
	
	@Override
	public boolean containsEdge(Edge e)
	{
		for(NodeEdge nE: this.record)
		{
			if(nE.getEdges().contains(new LightEdge(e.getSymbol(), e.getTo())) && nE.getVertex().getName().equals(e.getFrom()))
			{
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean containsEdge(Vertex from, Vertex to)
	{
		for(NodeEdge nE: this.record)
		{
			if(nE.getVertex().equals(from))
			{
				for(LightEdge e: nE.getEdges())
				{
					if(e.getTo().equals(to.getName()))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public boolean containsVertex(Vertex v)
	{
		for(NodeEdge nE: this.record)
		{
			if(nE.getVertex().equals(v))
			{
				return true;
			}
		}
		return false;
	}
	
	
	
	private boolean addEdgeOutVertex(Vertex v, LightEdge e) {
		Iterator<NodeEdge> it = this.record.iterator();
		NodeEdge nE;
		
		while(it.hasNext())
		{
			nE = it.next();
			if(nE.getVertex().equals(v))
			{
				if(nE.addEdge(e))
				{
					
					this.numberOfEdges += 1;
					return true;
				}
				else return false;
			}
		}
		return false;
	}

	@Override
	public boolean addEdge(Vertex from, Vertex to, String symbol) {
		return (!this.containsVertex(from) || !this.containsVertex(to))? false:
			this.addEdgeOutVertex(from, new LightEdge(symbol, to.getName()));
	}
	
	
	@Override
	public Set<Edge> edgeSet()
	{
		Set<Edge> result = new HashSet<Edge>();
		for(NodeEdge nE: this.record)
		{
			for(LightEdge e: nE.getEdges())
			{
				result.add(new Edge(e.getSymbol(), nE.getVertex().getName(), e.getTo()));
			}
		}
		return result;
	}
	
	@Override
	public Set<Edge> getAllEdges(Vertex from, Vertex to)
	{
		Set<Edge> result = new HashSet<Edge>();
		for(NodeEdge nE: this.record)
		{
			if(nE.getVertex().equals(from))
			{
				for(LightEdge e: nE.getEdges())
				{
					if(e.getTo().equals(to.getName()))
					{
						result.add(new Edge(e.getSymbol(), nE.getVertex().getName(), e.getTo()));
					}
				}
			}
		}
		return result;
	}
	
	@Override
	public Set<Vertex> vertexSet()
	{
		Set<Vertex> result = new HashSet<Vertex>();
		
		for(NodeEdge nE: this.record)
		{
			result.add(nE.getVertex());
		}
		return result;
	}
	
	@Override
	public boolean removeEdge(Edge e) {
		Iterator<NodeEdge> it = this.record.iterator();
		NodeEdge nE;
		LightEdge e1;
		
		while(it.hasNext())
		{
			nE = it.next();
			
			if(nE.getVertex().getName().equals(e.getFrom()))
			{
				if (nE.getVertex().getName().equals(e.getFrom()))
				{
					Iterator<LightEdge> itEdge = nE.getEdges().iterator();
					while(itEdge.hasNext())
					{
						e1 = itEdge.next();
						if(e1.getSymbol().equals(e.getSymbol())
								&& e1.getTo().equals(e.getTo()))
						{
							itEdge.remove();
							numberOfEdges -= 1;
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	@Override
	public boolean removeEdge(Vertex from, Vertex to, String symbol)
	{
		return this.removeEdge(new Edge(symbol, from.getName(), to.getName()));
	}
	
	@Override
	public boolean removeAllEdges(Collection<Edge> e)
	{
		boolean allRemoved = true;
		
		for(Edge edge: e)
		{
			if(!this.removeEdge(edge)) allRemoved = false;
		}
		return allRemoved;
	}
	
	@Override
	public boolean removeAllEdges(Vertex from, Vertex to) {
		
		boolean removedAll = this.removeAllEdges(this.getAllEdges(from, to));
		
		return removedAll;
	}
	
	public String toString()
	{
		String result = new String("");
		result += "\n\tnumero di Vertici:\t"+numberOfVertex
				  +"\n\tnumero di Archi:\t"+numberOfEdges+"\n"
				  +"\n\tVertici: "+this.getVertex()
				  +"\nArchi:\n";
		
		if(numberOfEdges==0)	result += "Non ci sono Archi!\n";
		else	for(NodeEdge nE: this.record)
				{
					for(Edge e: this.edgeOf(nE.getVertex()))
						{
							result += e.toString();
						}
					if(!this.edgeOf(nE.getVertex()).isEmpty()) result +="\n";
				}
		
		return result;
	}
}
