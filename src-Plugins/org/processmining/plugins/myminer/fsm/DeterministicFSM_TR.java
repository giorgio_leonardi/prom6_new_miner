package org.processmining.plugins.myminer.fsm;

import java.util.Collection;
import java.util.Vector;

public class DeterministicFSM_TR extends DeterministicFSM {

	public DeterministicFSM_TR(String regEx) {
		super(regEx);
	}

	/*simula l'automa a partire dallo stato p.second() considerando come simbolo di transizione l'etichetta del nodo p.first()*/
	public String simulate(Collection<String> symbols, String state) {
		String newState;
		for (String s:symbols){
			newState = getTransitionFromState(state,s);
			if (!newState.isEmpty()) 
				state=newState;
			else return null;
		}
		return state;
	}

	/*controlla se almeno uno degli stati passati per parametro � finale.
	 * ritorna true se almeno uno � finale
	 * false altrimenti*/
	public boolean isFinalState(String state) {
		Collection<String> finalState = getFinalStates();
		return finalState.contains(state);
	}

}
