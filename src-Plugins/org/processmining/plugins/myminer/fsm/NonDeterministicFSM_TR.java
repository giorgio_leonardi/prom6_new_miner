package org.processmining.plugins.myminer.fsm;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

public class NonDeterministicFSM_TR extends NonDeterministicFSM {

	public NonDeterministicFSM_TR(String regExTA){
		super(regExTA);
	}

	/*simula l'automa a partire dallo stato p.second() considerando come simbolo di transizione l'etichetta del nodo p.first()*/
	public Set<String> simulate(Collection<String> symbols, String state) {
		Set<String> result=new TreeSet<String>();
		result.add(state);
		Set<String> new_result=new TreeSet<String>();
		for (String s:symbols){
			for (String stato:result)
				new_result.addAll(getStateReachableFrom(stato, s));
			if (new_result.isEmpty())
				return null;
			new_result=epsilonClosure(new_result);
			result=new TreeSet<String>(new_result);
			new_result.clear();
		}
		return result;
	}

	

}
