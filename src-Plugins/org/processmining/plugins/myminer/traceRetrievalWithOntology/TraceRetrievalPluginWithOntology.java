package org.processmining.plugins.myminer.traceRetrievalWithOntology;

import java.util.Vector;

import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.util.ui.widgets.helper.ProMUIHelper;
import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.models.andOrGraph.GraphAndOr;


import org.processmining.plugins.myminer.Merge.RetrievalSolution;
import org.processmining.plugins.myminer.fsm.NonDeterministicFSM_Thomson;
import org.processmining.plugins.myminer.index.IndexTable;
import org.processmining.plugins.myminer.knowledge.*;



public class TraceRetrievalPluginWithOntology {
	
@Plugin(name = "Trace Retrieval With Ontology",
		parameterLabels = {"Model", "Ontology"},
		returnLabels = {"Model","Path Retrieved", "Ontologia caricata"},
		returnTypes = {GraphAndOr.class,RetrievalSolution.class, OWLOntologia.class},
		userAccessible = true,
		help = "trace retrieval with OWLOntologia")
				
@UITopiaVariant(affiliation = "UniversitÓ di Torino", author = "Luca Canensi", email = "canensi@di.unito.it")
			
	public static Object[] retrieve(UIPluginContext context ,GraphAndOr graph, OWLOntologia ontology) throws UserCancelledException{
	
	String query = ProMUIHelper.queryForString(context,"Query","(min,max)Eventi divisi da virgola o &(min,max)....(min,max)","(2,5)C(5,10)");
	
	NonDeterministicFSM_Thomson nfa=null;
	
	Vector<IndexTable> res = null ;
	
	String regEx = query;
	
	IndexTable it =new IndexTable();
	nfa=new NonDeterministicFSM_Thomson(regEx,it);
//	System.out.println(nfa.toString());
	GraphAndOr g=graph.clone();
	g.reset();
	//graph.reset();
	res = TraceRetrieval.search_process(g, nfa,it);
	for (IndexTable r:res){
		r.setResult();
	}
	//System.out.println(res);
//	nfa.reset();
//	
//	TraceRetrieval.merge(res, g);
//	g.stampaDettagliato();
//	g.simplifies();

	Object[] ret=new Object[3];
	ret[0]=g;
	ret[1]=new RetrievalSolution();
	((RetrievalSolution)ret[1]).setSol(res);
	ret[2] = ontology;
	return ret;
	//return g;
	}
}