package org.processmining.plugins.myminer.VF2;

import org.processmining.models.andOrGraph.GraphAndOr;

public class VF2IsomorphismTester extends AbstractIsomorphismTester {


    /**
     * Creates a new {@code TypedVF2IsomorphismTester} instance
     */
    public VF2IsomorphismTester() { }

    /**
     * Returns a new {@code State} for running the VF2 algorithm.
     */ 
    protected State makeInitialState(GraphAndOr g1, 
                                     GraphAndOr g2) {
        return new StateVF2(g1, g2);
    }

}
