package org.processmining.plugins.myminer.VF2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Pair;
import org.processmining.models.graphbased.NodeID;


/**
 * An abstraction of an {@link IsomorphismTester} that relies on
 * algorithm-specific {@link State} implementations to check for edge and vertex
 * contraints when performing isomorphism testing.  
 *
 * <p> This class is a an adaptation of parts of the VFLib library.
 */
public abstract class AbstractIsomorphismTester implements IsomorphismTester {

	public static final int NULL_NODE = -1;
	
	public static int isomNumber=0;
	
	private boolean uniqueness;
	
	public static ArrayList<Map<NodeID,NodeID>> isom;
    /**
     * {@inheritDoc}
     */
    public boolean areIsomorphic(GraphAndOr g1, 
                                 GraphAndOr g2) {
       return false;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<Map<NodeID,NodeID>> findIsomorphism(GraphAndOr g1, 
                                                GraphAndOr g2,boolean uniqueness) {
       
        State state = makeInitialState(g1, g2);
        this.uniqueness=uniqueness;
        isom=new ArrayList<Map<NodeID,NodeID>>();
        
      //  System.out.println("creato stato iniziale");

        Map<NodeID,NodeID> isoMapping = new HashMap<NodeID,NodeID>();
        // If we mapped the vertices, we have to unmap the vertices in order
        // to identify the isometric pairs.
         match(state, isoMapping);
    //    	System.out.println(isomNumber);
            return isom;
        
    }

    /**
     * Creates an empty {@link State} for mapping the vertices of {@code g1} to
     * {@code g2}.
     */
    protected abstract State makeInitialState(GraphAndOr g1, 
                                              GraphAndOr g2);

    /**
     * Returns {@code true} if the graphs being matched by this state are
     * isomorphic.
     */
    private boolean match(State s) {
        if (s.isGoal())
            return true;        

        if (s.isDead()) 
            return false;

        int n1 = NULL_NODE, n2 = NULL_NODE;
        Pair<Integer,Integer> next = null;
        boolean found = false;
        while (!found && (next = s.nextPair(n1, n2)) != null) {
            n1 = next.getFirst();
            n2 = next.getSecond();
            if (s.isFeasiblePair(n1, n2,uniqueness)) {
                State copy = s.copy();
                copy.addPair(n1, n2);
                found = match(copy);
                // If we found a match, then don't bother backtracking as it
                // would be wasted effort.
                if (!found)
                    copy.backTrack();
            }
        }
        return found;
    }

    /**
     * Returns {@code true} if the graphs being matched by this state are
     * isomorphic.
     */
    private boolean match(State s, Map<NodeID,NodeID> isoMap) {
    	
    	
        if (s.isGoal()){
            isomNumber++;
          //  isoMap.putAll(s.getVertexMapping());
            isom.add(s.getVertexMapping());
           // isoMap=new HashMap<NodeID, NodeID>();
            return false;
        }

        if (s.isDead()) 
            return false;

        int n1 = NULL_NODE, n2 = NULL_NODE;
        Pair<Integer,Integer> next = null;
        boolean found = false;
        while (!found && (next = s.nextPair(n1, n2)) != null) {
            n1 = next.getFirst();
            n2 = next.getSecond();
            
            if (s.isFeasiblePair(n1, n2,uniqueness)) {
                State copy = s.copy();
                copy.addPair(n1, n2);
                //found = match(copy, isoMap);
                match(copy, isoMap);
                // If we found a mapping, fill the vertex mapping state
               // if (found){ 
                    //isoMap.putAll(copy.getVertexMapping());
                copy.backTrack();
                    
               // }
                // Otherwise, back track and try again
              ///  else
               //     copy.backTrack();
            }
        }
        return found;
    }

    
}