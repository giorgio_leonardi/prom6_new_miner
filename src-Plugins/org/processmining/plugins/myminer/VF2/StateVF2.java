package org.processmining.plugins.myminer.VF2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.processmining.models.andOrGraph.DirectedGraphEdgeImpl;
import org.processmining.models.andOrGraph.EventsNode;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.models.andOrGraph.Pair;
import org.processmining.models.graphbased.NodeID;

public class StateVF2 implements State {
	 /**
     *il grafo da comparare
     */
    //private Node[] graphArray;

    private static GraphAndOr graph;
    
    private static Map<NodeID,Integer> graphMapping;
    
    /**
     * il sottografo da cercare
     */
   // private Node[] subgraphArray;
    
    private static GraphAndOr subgraph;
    
    private static Map<NodeID,Integer> subgraphMapping;
    
    /**
     *il numero di nodi matchati fra grafo e sottografo
     */
    
    
    int coreLen;

    /**
     *il numer di nodi matchati prima della aggiunta dell`ultimo mapping
     */
    int origCoreLen;

    /**
     *ultimo nodo del grafo aggiunto
     */
    int addedNodeGraph;
    
    // State information
    int t1bothLen, t2bothLen, t1inLen, t1outLen, 
        t2inLen, t2outLen; // Core nodes are also counted by these...
    
    private static int[] core1;
    private static int[] core2;
    private static int[] in1;
    private static int[] in2;
    private static int[] out1;
    private static int[] out2;
    
      
    int[] order;
    
    /**
     * The number of nodes in {@code g1}
     */
    private final int graphSize;

    /**
     * The number of nodes in {@code g2}
     */
    private final int subgraphSize;

	private static TreeMap<Integer, NodeID> graphReverseMapping;

	private static TreeMap<Integer, NodeID> subgraphReverseMapping;
    
   
    /**
     * Creates a new {@code VF2State} with an empty mapping between the two
     * graphs.
     */
    public StateVF2(GraphAndOr g1, GraphAndOr g2) {
    	
        graphReverseMapping=new TreeMap<Integer,NodeID>();
        graphMapping=new TreeMap<NodeID, Integer>();
        subgraphReverseMapping=new TreeMap<Integer,NodeID>();
        subgraphMapping=new TreeMap<NodeID, Integer>();
    	remap(g1,graphMapping,graphReverseMapping);
    	remap(g2,subgraphMapping,subgraphReverseMapping);
        graph=g1; 
        subgraph=g2;           
       
        graphSize = g1.nodeCount();
       
        subgraphSize = g2.nodeCount();   
        order = null;

        coreLen = 0;
        origCoreLen = 0;
        t1bothLen = 0;
        t1inLen = 0;
        t1outLen = 0;
        t2bothLen = 0; 
        t2inLen = 0;
        t2outLen = 0;
        
	addedNodeGraph = NULL_NODE;

        core1 = new int[graphSize];
        core2 = new int[subgraphSize];
        in1 = new int[graphSize];
        in2 = new int[subgraphSize];
        out1 = new int[graphSize];
        out2 = new int[subgraphSize];
        
        Arrays.fill(core1, NULL_NODE);
        Arrays.fill(core2, NULL_NODE);
        
    }

    protected StateVF2(StateVF2 copy) {
        
  //     graph = copy.graph;//.clone();
  //      subgraph = copy.subgraph;//.clone();
       
        coreLen = copy.coreLen;
      
        origCoreLen=copy.origCoreLen;
        t1bothLen = copy.t1bothLen;
        t2bothLen = copy.t2bothLen;
        t1inLen = copy.t1inLen;
        t2inLen = copy.t2inLen;
        t1outLen = copy.t1outLen;
        t2outLen = copy.t2outLen;
        graphSize = copy.graphSize;
        subgraphSize = copy.subgraphSize;

//        graphReverseMapping=new TreeMap<Integer, NodeID> (copy.graphReverseMapping);
//        subgraphReverseMapping=new TreeMap<Integer, NodeID>(copy.subgraphReverseMapping);
//        graphMapping=new TreeMap<NodeID, Integer>(copy.graphMapping);
//        subgraphMapping=copy.subgraphMapping;

        addedNodeGraph = NULL_NODE; 

        // NOTE: we don't need to copy these arrays because their state restored
        // via the backTrack() function after processing on the cloned state
        // finishes
//        core1 = copy.core1;//.clone();
//        core2 = copy.core2;//.clone();
//        in1 = copy.in1;//.clone();
//        in2 = copy.in2;//.clone();
//        out1 = copy.out1;//.clone();
//        out2 = copy.out2;//.clone();
//               
    }   

    /**
     * {@inheritDoc}
     */
    public GraphAndOr getGraph() {
        return graph;
    }

    /**
     * {@inheritDoc}
     */
    public GraphAndOr getSubgraph() {
        return subgraph;
    }

    /**
     * {@inheritDoc}
     */
    public Pair<Integer,Integer> nextPair(int prevN1, int prevN2) {
        if (prevN2 == NULL_NODE)
            prevN2 = 0;

        if (prevN1 == NULL_NODE)
            prevN1 = 0;
        else
            prevN1++;

	if (t1bothLen>coreLen && t2bothLen > coreLen) {
            while (prevN1 < graphSize && (core1[prevN1] != NULL_NODE 
                                   || out1[prevN1]==0
                                   || in1[prevN1]==0)) {
                prevN1++;    
                prevN2 = 0;
            }
        }
        else if (t1outLen>coreLen && t2outLen>coreLen) {
            while (prevN1<graphSize &&
                   (core1[prevN1]!=NULL_NODE || out1[prevN1]==0)) {
                prevN1++;    
                prevN2=0;
            }
        }
        else if (t1inLen>coreLen && t2inLen>coreLen) {
            while (prevN1<graphSize &&
                   (core1[prevN1]!=NULL_NODE || in1[prevN1]==0)) {
                 prevN1++;    
                 prevN2=0;
            }
        }
	else if (prevN1 == 0 && order != null) {
            int i=0;
	    while (i < graphSize && core1[prevN1=order[i]] != NULL_NODE)
                i++;
	    if (i == graphSize)
                prevN1=graphSize;
        }
	else {
            while (prevN1 < graphSize && core1[prevN1] != NULL_NODE ) {
                prevN1++;    
                prevN2=0;
            }
        }

	if (t1bothLen>coreLen && t2bothLen>coreLen) {
            while (prevN2<subgraphSize && (core2[prevN2]!=NULL_NODE 
                                 || out2[prevN2]==0
                                 || in2[prevN2]==0)) {
                prevN2++;    
            }
        }
	else if (t1outLen>coreLen && t2outLen>coreLen) {
            while (prevN2 < subgraphSize && (core2[prevN2] != NULL_NODE
                                   || out2[prevN2] == 0)) {
                prevN2++;    
            }
        }
        else if (t1inLen>coreLen && t2inLen>coreLen) {
             while (prevN2 < subgraphSize && (core2[prevN2] != NULL_NODE
                                    || in2[prevN2] == 0)) {
                  prevN2++;    
             }
        }
	else {
            while (prevN2 < subgraphSize && core2[prevN2] != NULL_NODE) {
                prevN2++; 
            }
        }

//         System.out.printf("prevN1: %d, prevN2: %d%n", prevN1, prevN2);
        if (prevN1 < graphSize && prevN2 < subgraphSize) 
            return new Pair<Integer,Integer>(prevN1, prevN2);
        else
            return null;

    }
    
    protected boolean areCompatibleVertices(Node v1, Node v2,boolean  uniqueness) {
       if (uniqueness)
       {
    	   String[] t=v1.getLabel().split("&&");
    	   String[] t2=v2.getLabel().split("&&");
    	   int i=0,j=0;
    	   while (i<t.length && j<t2.length){
    		   int c=t[i].compareTo(t2[j]);
    		   if (c==0)
    			   return true;
    		   else if(c>0) j++;
    		   		else i++;
    	   }
    	   return false;
       }
       else	return v1.getLabel().equals(v2.getLabel());
    }

    /**
     * {@inheritDoc}
     */
    public boolean isFeasiblePair(int node1, int node2,boolean uniqueness) {
        assert node1 < graphSize;
        assert node2 < subgraphSize;
        assert core1[node1] == NULL_NODE;
        assert core2[node2] == NULL_NODE;

       Node a= graph.getNodeByID(graphReverseMapping.get(node1));
       Node b= subgraph.getNodeByID(subgraphReverseMapping.get(node2));
       
       if (b.equals("em_ecgperformed")) 
    	   System.out.println("dsaghj");
       
       if (!areCompatibleVertices(a, b,uniqueness)) return false;
       
        // TODO: add checks for compatible nodes here

        // int i = 0;// , other1 = 0, other2 = 0;
        int termout1=0, termout2=0, termin1=0, termin2=0, new1=0, new2=0;

        // Check the 'out' edges of node1
        
//        for (int other1 : getSuccessors(graph, node1)) {
//        
//        if (core1[other1] != NULL_NODE) {
//                int other2 = core1[other1];
//                // If there's node edge to the other node, or if there is some
//                // edge incompatability, then the mapping is not feasible
//                if (!subgraph.contains(subgraphReverseMapping.get(node2),subgraphReverseMapping.get(other2)))
//                    return false;
//            }
//            else {
//                if (in1[other1] != 0)
//                    termin1++;
//                if (out1[other1] != 0)
//                    termout1++;
//               // if (in1[other1] == 0 && out1[other1] == 0)
//                //    new1++;
//            }
//        }
//            
//        // Check the 'in' edges of node1
//        for (int other1 : getPredecessors(graph, node1)) {           
//            if (core1[other1] != NULL_NODE) {
//                int other2 = core1[other1];
//                // If there's node edge to the other node, or if there is some
//                // edge incompatability, then the mapping is not feasible
//                if (!subgraph.contains(subgraphReverseMapping.get(other2), subgraphReverseMapping.get(node2)) )
//                    return false;
//            }
//            else {
//                if (in1[other1] != 0)
//                    termin1++;
//                if (out1[other1] != 0)
//                    termout1++;
//                //if (in1[other1] == 0 && out1[other1] == 0)
//                //   new1++;
//            }
//        }


        // Check the 'out' edges of node2
        for (int other2 : getSuccessors(subgraph, node2)) {
            if (core2[other2] != NULL_NODE) {
                int other1 = core2[other2];
                if (!graph.contains(graphReverseMapping.get(node1), graphReverseMapping.get(other1)))
                    return false;
            }
            else {
                if (in2[other2] != 0)
                    termin2++;
                if (out2[other2] != 0)
                    termout2++;
                //if (in2[other2] == 0 && out2[other2] == 0)
                //    new2++;
            }
        }

        // Check the 'in' edges of node2
        for (int other2 : getPredecessors(subgraph, node2)) {
            if (core2[other2] != NULL_NODE) {
                int other1 = core2[other2];               
                if (!graph.contains(graphReverseMapping.get(other1),graphReverseMapping.get(node1)))
                    return false;
            }

            else {
                if (in2[other2] != 0)
                    termin2++;
                if (out2[other2] != 0)
                    termout2++;
            //    if (in2[other2] == 0 && out2[other2] == 0)
            //        new2++;
            }
        }

        return termin1 == termin2 && termout1 == termout2 && new1 == new2;
    }

    /**
     * {@inheritDoc}
     */
    public void addPair(int node1, int node2) {
        assert node1 < graphSize;
        assert node2 < subgraphSize;
        assert coreLen < graphSize;
        assert coreLen < subgraphSize;

        origCoreLen=coreLen;
        coreLen++;
        addedNodeGraph = node1;

	if (in1[node1] == 0) {
            in1[node1] = coreLen;
	    t1inLen++;
            if (out1[node1] != 0)
                t1bothLen++;
        }
	if (out1[node1] == 0) {
            out1[node1]=coreLen;
	    t1outLen++;
            if (in1[node1] != 0)
                t1bothLen++;
        }

	if (in2[node2] == 0) {
            in2[node2]=coreLen;
	    t2inLen++;
            if (out2[node2] != 0)
                t2bothLen++;
        }
	if (out2[node2] == 0) {
            out2[node2]=coreLen;
	    t2outLen++;
            if (in2[node2] != 0)
                t2bothLen++;
        }

        core1[node1] = node2;
        core2[node2] = node1;

//        for (int other : getPredecessors(graph, node1)) {
//            if (in1[other] == 0) {
//                in1[other] = coreLen;
//                t1inLen++;
//                if (out1[other] != 0)
//                    t1bothLen++;
//            }
//        }
//
//        for (int other : getSuccessors(graph, node1)) {
//            if (out1[other] == 0) {
//                 out1[other]=coreLen;
//                 t1outLen++;
//                 if (in1[other] != 0)
//                     t1bothLen++;
//            }
//        }
//    
//        for (int other : getPredecessors(subgraph, node2)) {            
//            if (in2[other] == 0) {
//                in2[other]=coreLen;
//                t2inLen++;
//                if (out2[other] != 0)
//                    t2bothLen++;
//            }
//        }
//
//        for (int other : getSuccessors(subgraph, node2)) {            
//            if (out2[other] == 0) {
//                out2[other]=coreLen;
//                t2outLen++;
//                if (in2[other] != 0)
//                    t2bothLen++;
//            }
//        }        
    }

    /**
     * {@inheritDoc}
     */
    public boolean isGoal() {
        return coreLen == subgraphSize;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isDead() {
        return subgraphSize>graphSize
            || t1bothLen != t2bothLen
            || t1outLen != t2outLen
            || t1inLen != t2inLen;        
    }

    /**
     * {@inheritDoc}
     */
    public Map<NodeID,NodeID> getVertexMapping() {
        Map<NodeID,NodeID> vertexMapping = new HashMap<NodeID,NodeID>();
        for (int i = 0; i < graphSize; ++i) {
            if (core1[i] != NULL_NODE) {
                vertexMapping.put(subgraphReverseMapping.get(core1[i]),graphReverseMapping.get(i));
            }
        }
        return vertexMapping;
    }
    
    /**
     * {@inheritDoc}
     */
    public StateVF2 copy() {
        return new StateVF2(this);
    }

    /**
     * {@inheritDoc}
     */
    public void backTrack() {
        assert coreLen - origCoreLen <= 1;
        assert addedNodeGraph != NULL_NODE;
        
        if (origCoreLen < coreLen) {
            int i, node2;

            if (in1[addedNodeGraph] == coreLen)
                in1[addedNodeGraph] = 0;

            for (int other : getPredecessors(graph, addedNodeGraph)) {
                if (in1[other]==coreLen)
                    in1[other]=0;
            }
        
            if (out1[addedNodeGraph] == coreLen)
                out1[addedNodeGraph] = 0;

            for (int other : getSuccessors(graph, addedNodeGraph)) {
                if (out1[other]==coreLen)
                    out1[other]=0;
            }
	    
            node2 = core1[addedNodeGraph];

            if (in2[node2] == coreLen)
                in2[node2] = 0;
	    
            for (int other : getPredecessors(subgraph, node2)) {
                if (in2[other]==coreLen)
                    in2[other]=0;
            }
            
            if (out2[node2] == coreLen)
                out2[node2] = 0;
	    
            for (int other : getSuccessors(subgraph, node2)) {
                if (out2[other]==coreLen)
                    out2[other]=0;
            }
	    
	    core1[addedNodeGraph] = NULL_NODE;
            core2[node2] = NULL_NODE;
	    
	    coreLen = origCoreLen;
            addedNodeGraph = NULL_NODE;
        }
    }

    /**
     * Returns those vertices that can be reached from {@code vertex} or the empty
     * set if {@code g} is not a directed graph.
     */
    @SuppressWarnings("unchecked")
	private Set<Integer> getSuccessors(GraphAndOr graph2, Integer vertex) {
        Set<Integer> result=new TreeSet<Integer> ();
        boolean isGraph=false;
        NodeID id;
        if (graph2.equals(graph))
        	isGraph=true;
        if (isGraph)
        	id=graphReverseMapping.get(vertex);
        else id=subgraphReverseMapping.get(vertex);
        
    	for (DirectedGraphEdgeImpl<EventsNode, EventsNode> arc: graph2.getNodeByID(id).getOutEdge())
    	{
    		NodeID succ=arc.getTarget().getId();
    		int i;
    		if (isGraph)
    			i=graphMapping.get(succ);
    		else i=subgraphMapping.get(succ);
    		result.add(new Integer(i)); 
    	}
    	return result;
    }

    /**
     * Returns those vertices that point to from {@code vertex} or all the
     * adjacent vertices if {@code g} is not a directed graph.
     */
    @SuppressWarnings("unchecked")
    private Set<Integer> getPredecessors(GraphAndOr graph2, Integer vertex) {
    	Set<Integer> result=new TreeSet<Integer> ();
    	boolean isGraph=false;
        NodeID id;
        if (graph2.equals(graph))
        	isGraph=true;
        if (isGraph)
        	id=graphReverseMapping.get(vertex);
        else id=subgraphReverseMapping.get(vertex);
    	for (DirectedGraphEdgeImpl<EventsNode, EventsNode> arc: graph2.getNodeByID(id).getInEdge())
    	{
    		NodeID pred=arc.getSource().getId();
    		int i;
    		if (isGraph)
    			i=graphMapping.get(pred);
    		else i=subgraphMapping.get(pred);
    		result.add(i);
    	}
    	return result;
    }
    
    
    private void remap(GraphAndOr g, Map<NodeID,Integer> vMap,Map<Integer,NodeID> rvMap) {
          

        //mappa i vertici in un intervallo contiguo da 0 a #nodi-1
    	
        int j = 0;
     
        for (Node n:  g.getNodes()) {
        	rvMap.put(j, n.getId());
            vMap.put(n.getId(), j++);
        }
        
       // Node[] result=new Node[g.nodeCount()];
     
        //metto i nodi in un array  
       //result= Arrays.copyOf(g.getNodes().toArray(),g.nodeCount(),Node[].class);
        
       // return result;
    }   
    
    
}