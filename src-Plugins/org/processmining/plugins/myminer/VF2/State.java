package org.processmining.plugins.myminer.VF2;

import java.util.Map;

import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.models.andOrGraph.Pair;
import org.processmining.models.graphbased.NodeID;

public interface State {
	
	/**
     * identificatore usato quando il nodo non � stato ancora matchato 
     */
    public static final int NULL_NODE = -1;

    /**
     * ritorna il grafo  
     */
    GraphAndOr getGraph();

    /**
     * ritorna il sottografo 
     */
    GraphAndOr getSubgraph();

    /**
     * ritorna il prossimo candidato per il matching dell`isomorfismo a partire dai due nodi che sono stati matchati prima.
     * se uno dei due � null allora ritorno il candidato iniziale.
     */
    Pair<Integer,Integer> nextPair(int prevN1, int prevN2);

    /**
     * aggiunge i due vertici al map     */ 
    void addPair(int n1, int n2);

    /**
     *ritorna vero se il mapping fra node1 e node2 preserva l`isomorfismo    */
    boolean isFeasiblePair(int node1, int node2,boolean u);

    /**
     *ritorna vero se sono stati mappati tutti i vertici e i grafi sono isomorfi */
    boolean isGoal();

    /**
     * ritorna vero se allo stato corrente esistano dei mapping che invalidano l`isomorfismo
     */
    boolean isDead();

    /**
     *ritorna il mapping allo stato corrente      */
    Map<NodeID,NodeID> getVertexMapping();

    /**
     *copia
     */
    State copy();

    /**
     *elimina il mapping aggiunto nella chiamata precedente del metodo addPair
     */
    void backTrack();
	
}
