package org.processmining.plugins.myminer.VF2;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.graphbased.NodeID;




/**
 * An interface for algorithms that detect whether two graphs are <a
 * href="http://en.wikipedia.org/wiki/Graph_isomorphism">isomorphic</a>.
 * Implementations are open to define any further refinements beyond structural
 * isomorphism, which may include: <ul> <li> node types <li> edge types <li>
 * edge weights <li> edge directions </ul>.
 */
public interface IsomorphismTester {

    /**
     * Returns {@code true} if the graphs are isomorphism of each other.
     */
    boolean areIsomorphic(GraphAndOr graph, GraphAndOr subgraph);

    /**
     * Returns an isomorphic mapping from the vertices in {@code g1} to the
     * vertices in {@code g2}, or an empty {@code Map} if no such mapping
     * exists.
     */
    ArrayList<Map<NodeID,NodeID>> findIsomorphism(GraphAndOr graph, 
                                         GraphAndOr subgraph ,boolean u);
}
