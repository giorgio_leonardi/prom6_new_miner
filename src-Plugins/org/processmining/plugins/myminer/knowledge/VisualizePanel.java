package org.processmining.plugins.myminer.knowledge;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.plugins.myminer.abstraction.Abstraction;
import org.processmining.plugins.myminer.simOntology.SIMplugin;
import org.processmining.plugins.myminer.simOntology.UpPanel;

import owl2prefuse.graph.GraphDisplay;
import owl2prefuse.graph.GraphPanel;
import prefuse.data.Graph;
import java.awt.GridLayout;

@SuppressWarnings("serial")
public class VisualizePanel extends JPanel {

	public JTree alberoClassi;

	/**
	 * Create the panel.
	 */
	public VisualizePanel(OWLOntologia ontology, JTextField textfield) {
		
		//root albero
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Thing");
		//aggiunta del renderer per i nodi
		TreeCellRenderer renderer = new ClassCellRenderer();
		
		//recupero i nomi di tutte le classi
		ArrayList<String> nomiClassi = ontology.retrieve_ontology_classes();
		
		//creo i nodi diretti discendenti della root
		for(String nome : nomiClassi) {
			ArrayList<String> superClassi = ontology.superClasses(nome);
			if(superClassi == null) {
				
				DefaultMutableTreeNode nodo = new DefaultMutableTreeNode(nome);
				nodo = aggiungiLivello(nodo, nome, ontology);
				//aggiungo il nodo alla root
				root.add(nodo);
			}
		}
		setLayout(new GridLayout(0, 2, 0, 0));

		alberoClassi = new JTree(root);
		add(alberoClassi);
		alberoClassi.setCellRenderer(renderer); 
		//aggiungo il listener per l'azione di click
		alberoClassi.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
	        public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
	            onClick(evt, ontology, textfield); 
	            alberoClassi.clearSelection();
	        }
	    });
		alberoClassi.setOpaque(false);
		
		
		//crea il convertitore per il grafo
		OWLToGraphConverter graphConverter = new OWLToGraphConverter();
		//converte il grafo
		Graph m_graph = graphConverter.getGrafo(ontology);
		
		//crea il layout per il grafo
		GraphDisplay graphDisp = new GraphDisplay(m_graph, false);
		
		//crea il pannello di visualizzazione del grafo
		GraphPanel m_graphPanel = new GraphPanel(graphDisp, false, false);
		if(textfield == null)
			add(m_graphPanel);
	}
	
	/**
	 * Ricorsivamente inserisce i sottonodi
	 * 
	 * @param nodo nodo padre di cui si vogliono avere i sottonodi
	 * @param antenato nome del padre
	 * @param ontology Ontologia analizzata
	 * @return L'oggetto nodo da appendere all'albero
	 */
	private static DefaultMutableTreeNode aggiungiLivello(DefaultMutableTreeNode nodo, String antenato, OWLOntologia ontology) {
		//ricerco le sottoclassi
		ArrayList<String> sottoClassi = ontology.subClasses(antenato);
		
		if(sottoClassi == null) {//se non si hanno sottoclassi, viene restituito il nodo
			return nodo;
		}
		else {
			for(String sub : sottoClassi) { //altrimenti si inseriscono le sottoclassi
				
				DefaultMutableTreeNode subNodo;
				
				subNodo = new DefaultMutableTreeNode(sub);
				subNodo = aggiungiLivello(subNodo, sub, ontology); //ripeto l'operazioni in caso di livelli di dipendenza multipli
				nodo.add(subNodo);
			}
		}
		return nodo; //ritorno il nodo con il proprio sottoalbero
	}
	
	/**
	 * Effettua il display della query relativa alla classe selezionata nell'albero
	 * 
	 * @param tse Evento di selezione dell'albero
	 */
	private static void onClick( TreeSelectionEvent tse , OWLOntologia ontology, JTextField textfield ) { 
	     
		 if(tse.getNewLeadSelectionPath() == null)
			 return;
		
		 String node = tse.getNewLeadSelectionPath().getLastPathComponent().toString(); //recupero del nome del nodo
	     if(node.equals("Thing")) { //in caso di radice si evita di effettuare il display della query
	    	 return;
	     }
	     
	     String query = ontology.getQuery(node);
	     if(query == null) {
	    	 query = "Query non esistente";
	     }
	     // Pulisce il textfield
	     if(textfield != null) {
	    	textfield.setText("");
	    	 
	    	// Nodo premuto sull'ontologia
		    DefaultMutableTreeNode nn = (DefaultMutableTreeNode) tse.getNewLeadSelectionPath().getLastPathComponent();
		    
		    Object[] options = {"Automatic", "Interactive", "Cancel"};
		     
		    int scelta = JOptionPane.showOptionDialog(SIMplugin.frame, 
		    		 "Type of abstraction for node "+nn.toString()+":", "Choose the type of abstraction", 
		    		 JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
		    		 options, options[0]);
		     
		    // Nuovo grafo con nodo astratto	     
		    GraphAndOr gg = null;
		    //Scelta dell'utente
		    if(scelta==Abstraction.TYPE_AUTOMATIC) 
		    	gg = SIMplugin.ActAbstract(nn, Abstraction.TYPE_AUTOMATIC);
		    else
		     if(scelta==Abstraction.TYPE_INTERACTIVE) 
			    	gg = SIMplugin.ActAbstract(nn, Abstraction.TYPE_INTERACTIVE);
		    //Scelta Cancel	
		    if(scelta != Abstraction.TYPE_AUTOMATIC && scelta != Abstraction.TYPE_INTERACTIVE)
		    	return;
		    
		     // Se non � nullo, astrazione eseguita quindi nuovo grafo
			if(gg!=null) {
				SIMplugin.coordinate();
				if(scelta==Abstraction.TYPE_AUTOMATIC) 
					UpPanel.addRows(node,"Automatic abstraction of");	
				else
			     if(scelta==Abstraction.TYPE_INTERACTIVE)
						UpPanel.addRows(node,"Interactive abstraction of");	
				   	 
			} 
			else
				JOptionPane.showMessageDialog(SIMplugin.frame, 
											  "The node "+ nn.toString() + "cannot be abstract or has already been abstracted",
											  "Error in abstraction",
											  JOptionPane.ERROR_MESSAGE);
	     }
	     else{
	    	//Frame
	    	 JFrame frame = new JFrame("Query per "+ node); //JFrame che verr� visualizzato e settaggio impostazioni
	    	 Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	    	 frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2); //posizione
	    	 frame.setVisible(true); //visibilit�
	    	 frame.setMinimumSize(new Dimension(300, 200)); //dimensione minima
	     
	    	 JTextPane areaTesto = new JTextPane(); //area di testo interna per il display della Stringa
	    	 areaTesto.setContentType("text/html");
	    	 areaTesto.setText("<html><center>" + query + "</center></html>"); //stringa che verr� visualizzata
	    	 areaTesto.setEditable(false); //eliminazione possibilit� di editing del testo
	    	 frame.getContentPane().add(areaTesto); //aggiunge al frame
	     }    
	}
//	public static VisualizePanel getVisualizePanel() {
//		return ;
//	}
}