package org.processmining.plugins.myminer.knowledge;


import org.semanticweb.owlapi.model.OWLOntology;

import org.semanticweb.owlapi.model.OWLOntologyManager;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Stream;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

@SuppressWarnings("deprecation")
public class OWLOntologia {

	private OWLOntologyManager manager; 
	private OWLOntology ontology;
	private OWLReasoner hermit;
	
	public OWLOntology getOntologia() {
		return ontology;
	}
	protected void setOntologia(OWLOntology ontologia) {
		this.ontology = ontologia;
		//creo il reasoner per svolgere recupero informazioni
		Reasoner reasonerFactory = new Reasoner(ontology);
//		hermit = reasonerFactory.createReasoner(ontology);
		hermit = reasonerFactory;
	}
	public OWLOntologyManager getManager() {
		return manager;
	}
	protected void setManager(OWLOntologyManager manager) {
		this.manager = manager;
	}
	
	/**
	 * Stampa tutto il contenuto dell'ontologia, in particolare tutti gli assiomi logici
	 */
	public void stampa_ontologia() {
		ontology.logicalAxioms().forEach(System.out::println);
	}
	
	/**
	 * Stampa il nome di tutte le classi presente in una ontologia
	 * 
	 * @return ArrayList con i nomi delle classi
	 */
	public ArrayList<String> retrieve_ontology_classes () {
		//ArrayList che conterr� i nomi delle classi
		ArrayList<String> nomiClassi = new ArrayList<String>();
		//il Set conterr� tutte le classi presenti nella ontologia
		Set<OWLClass> classi = ontology.getClassesInSignature();
		
		//stampa del numero e delle classi
		//System.out.println("numero totale di classi: "+ classi.size());
		//System.out.println("elenco classi ontologia:");
		for (OWLClass classe : classi) {
			nomiClassi.add(classe.getIRI().getFragment());
			//System.out.println("\t"+classe.getIRI().getFragment());
		}
		return nomiClassi;
	}
	
	/**
	 * Recupera tutte le sottoclassi di una data classe
	 * 
	 * @param classe classe di cui si vogliono le sottoclassi
	 * @return ArrayList contenente i nomi delle sottoclassi
	 */
	public ArrayList<String> subClasses(String classe){
		ArrayList<String> risultato;
		OWLClass target = null;
		
		//check per creare l'istanza della classe
		Set<OWLClass> classi = ontology.getClassesInSignature();
		for (OWLClass e : classi) { //scorro le classe OWL del set comparandone l'IRI con il nome della classe desiderata
			if(e.getIRI().getFragment().equals(classe)) {
				 target = e;
			}
		}
		
		//display sottoclassi
		NodeSet<OWLClass> sottoClassi = hermit.getSubClasses(target, true);
		//System.out.println("Sottoclassi:");
		if(sottoClassi.isEmpty()) {
			//System.out.println("\tQuesta classe non ha sottoclassi");
			return null;
		}
		else {
			risultato = new ArrayList<String>();
			for(Node<OWLClass> sottoclasse : sottoClassi) {
				//System.out.println("\t"+ sottoclasse.getRepresentativeElement().getIRI().getFragment());
				String nome = sottoclasse.getRepresentativeElement().getIRI().getFragment();
				if(nome.equals("Nothing")) {
					continue;
				}
				else {
					risultato.add(nome);
				}
			}
		}
		if(risultato.isEmpty()) {
			return null;
		}
		return risultato;
	}
	
	/**
	 * Recupera tutte le superclassi di una data classe
	 * 
	 * @param classe classe di cui si vogliono le superclassi
	 * @return ArrayList contenente i nomi delle superclassi
	 */
	public ArrayList<String> superClasses(String classe) {
		ArrayList<String> risultato;
		OWLClass target = null;
		
		//check per creare l'istanza della classe
		Set<OWLClass> classi = ontology.getClassesInSignature();
		for (OWLClass e : classi) { //scorro le classe OWL del set comparandone l'IRI con il nome della classe desiderata
			if(e.getIRI().getFragment().equals(classe)) {
				 target = e;
			}
		}
		
		//display superclassi
		NodeSet<OWLClass> superClassi = hermit.getSuperClasses(target, true);
		//System.out.println("Superclassi:");
		risultato = new ArrayList<String>();
		for(Node<OWLClass> superclasse: superClassi) {
			//System.out.println("\t"+ superclasse.getRepresentativeElement().getIRI().getFragment());
			String nome = superclasse.getRepresentativeElement().getIRI().getFragment();
			if(nome.equals("Thing")) {
				continue;
			}
			else {
				risultato.add(nome);
			}
		}
		if(risultato.isEmpty()) {
			return null;
		}
		return risultato;
	}
	
	
	/**
	 * Recupera tutte le classi disgiunte di una certa classe
	 * 
	 * @param classe classe di cui si vogliono recuperare le classi disgiunte
	 * @return ArrayList contenente i nomi delle classi disgiunte
	 */
	public ArrayList<String> disjunctClasses(String classe){
		ArrayList<String> risultato;
		OWLClass target = null;
		
		//check per creare l'istanza della classe
		Set<OWLClass> classi = ontology.getClassesInSignature();
		for (OWLClass e : classi) { //scorro le classe OWL del set comparandone l'IRI con il nome della classe desiderata
			if(e.getIRI().getFragment().equals(classe)) {
				 target = e;
			}
		}
		
		//display classi disgiunte
		NodeSet<OWLClass> classiDisgiunte = hermit.getDisjointClasses(target);
		//System.out.println("Classi disgiunte:");
		if(classiDisgiunte.isEmpty()) {
			//System.out.println("\tQuesta classe non ha classi disgiunte");
			return null;
		}
		else {
			risultato = new ArrayList<String>();
			for(Node<OWLClass> disgiunta : classiDisgiunte) {
				//System.out.println("\t"+ disgiunta.getRepresentativeElement().getIRI().getFragment());
				risultato.add(disgiunta.getRepresentativeElement().getIRI().getFragment());
			}
		}
		return risultato;
	}
	
	/**
	 * Recupera le istanze di una data classe
	 * 
	 * @param classe Classe di cui si vogliono recuperare le istanze
	 * @return ArrayList contenente i nomi delle istanze
	 */
	public ArrayList<String> getInstances(String classe) {
		ArrayList<String> risultato;
		OWLClass target = null;
		
		//check per creare l'istanza della classe
		Set<OWLClass> classi = ontology.getClassesInSignature();
		for (OWLClass e : classi) { //scorro le classe OWL del set comparandone l'IRI con il nome della classe desiderata
			if(e.getIRI().getFragment().equals(classe)) {
				 target = e;
			}
		}
		
		//display istanze della classe
		NodeSet<OWLNamedIndividual> individui = hermit.getInstances(target);
		//System.out.println("Istanze:");
		if(individui.isEmpty()) {
			//System.out.println("\tQuesta classe non ha istanze");
			return null;
		}
		else {
			risultato = new ArrayList<String>();
			for(Node<OWLNamedIndividual> individuo : individui) {
				//System.out.println("\t"+ individuo);
				risultato.add(individuo.getRepresentativeElement().getIRI().getFragment());
			}
		}
		return risultato;
	}
	
	/**
	 * Recupera tutte le classi che fanno parte di una certa classe, res[0]<br>
	 * Recupera tutte le classi che hanno come parte quella attuale, res[1]
	 * 
	 * @param classe Classe di cui si vogliono recuperare le informazioni
	 * @return Object array che contiene due ArrayList con le informazioni
	 */
	public Object[] getPartOfClasses(String classe){
		ArrayList<String> risultato = new ArrayList<String>();
		ArrayList<String> risultato2 = new ArrayList<String>();
		Object[] res = new Object[2];
		OWLClass target = null;
		
		//check per creare l'istanza della classe
		Set<OWLClass> classi = ontology.getClassesInSignature();
		for (OWLClass e : classi) { //scorro le classe OWL del set comparandone l'IRI con il nome della classe desiderata
			if(e.getIRI().getFragment().equals(classe)) {
				 target = e;
			}
		}
		
		//ricerca delle classi che fanno parte dell'attuale
		//System.out.println("Questa classe ha come parti: ");
		OWLDataFactory df = manager.getOWLDataFactory();
		OWLObjectProperty partOf = df.getOWLObjectProperty("part-Of");
		OWLClassExpression c = df.getOWLObjectSomeValuesFrom(partOf, target);
		NodeSet<OWLClass> subClasses = hermit.getSubClasses(c, true);
		if(subClasses.isEmpty()) {
			//System.out.println("\tQuesta classe non ha parti");
			res[0] = null;
		}
		else {
			for(Node<OWLClass> parte : subClasses) {
				risultato.add(parte.getRepresentativeElement().getIRI().getFragment());
			}
			res[0] = risultato;
		}
		
		
		//ricerca delle classi che hanno come parti quella attuale
		//System.out.println("Questa classe � parte di: ");
		@SuppressWarnings("unchecked")
		OWLClassExpression domain = df.getOWLObjectIntersectionOf((Stream<? extends OWLClassExpression>) ontology.objectPropertyDomainAxioms(partOf));
		NodeSet<OWLClass> subClassesInDomain = hermit.getSubClasses(domain, false);
		if(subClassesInDomain.isEmpty()) {
			//System.out.println("\tQuesta classe non � parte di nessun'altra");
			res[1] = null;
		}
		else {
			for(Node<OWLClass> parteDi : subClassesInDomain) {
				risultato2.add(parteDi.getRepresentativeElement().getIRI().getFragment());
			}
			res[1] = risultato2;
		}
		
		return res;
	}
	
	public String getQuery(String classe) {
		String risultato = null;
        for (OWLClass cls : ontology.getClassesInSignature()) {
        	 for (OWLOntology o : ontology.getImportsClosure()) {
        		 for (OWLAnnotationAssertionAxiom annotation : o.getAnnotationAssertionAxioms(cls.getIRI())) {
        			 if (annotation.getValue() instanceof OWLLiteral) {
        				 OWLLiteral val = (OWLLiteral) annotation.getValue();
        				 if(cls.getIRI().getFragment().equals(classe))
        					 return val.getLiteral();
        			 }
        		 }
        	 }
        }
		return risultato;
	}

}