package org.processmining.plugins.myminer.knowledge;


import java.io.File;


import java.io.InputStream;
import org.processmining.contexts.uitopia.annotations.*;
import org.processmining.framework.abstractplugins.*;
import org.processmining.framework.plugin.*;
import org.processmining.framework.plugin.annotations.*;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

@Plugin (name = "Importa Ontologia",
		 parameterLabels = {"FileName"}, 
		 returnLabels = {"Ontologia"}, 
		 returnTypes = {OWLOntologia.class})
@UIImportPlugin (description = "Ontologia", 
				 extensions = {"owl"})

public class OWLOntologiaImportPlugin extends AbstractImportPlugin {
	
	@Override
	protected OWLOntologia importFromStream(final PluginContext context, final InputStream input, final String filename, final long fileSizeInBytes) {
		
		try {
			context.getFutureResult(0).setLabel(filename);
		}catch (final Throwable e) {
			
		}
		OWLOntologia result = new OWLOntologia();
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		
		File loader = getFile();
		
		try {
			OWLOntology ontologiaTmp = manager.loadOntologyFromOntologyDocument(loader);
			result.setOntologia(ontologiaTmp);
			result.setManager(manager);
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		return result;
	}
}
