package org.processmining.plugins.myminer.knowledge;

import java.util.ArrayList;
import java.util.Hashtable;

import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;

public class OWLToGraphConverter {
	
	private OWLOntologia ontologia;
	private Graph m_graph; //il grafo di prefuse creato
	private ArrayList<String[]> m_edges; //contiene tutti gli archi aggiunti
	private Hashtable<String, Node> m_nodes; //contiene tutti i nodi del grafo
	
	public Graph getGrafo(OWLOntologia ontology) {
		ontologia = ontology;
		m_edges = new ArrayList<String[]>();
		m_nodes = new Hashtable<String, Node>();
		
		//crea un nuovo grafo vuoto
		m_graph = new Graph(false); //true -> direct , false -> indirect
		
        m_graph.getNodeTable().addColumn("name", String.class);
        m_graph.getNodeTable().addColumn("type", String.class);
        m_graph.getEdgeTable().addColumn("label", String.class);
        
        //recupero tutte le classi dell'ontologia
        ArrayList<String> classi = ontology.retrieve_ontology_classes();
        
        String root = null;
        //trovo la root
        for(String classe : classi) {
        	if(ontology.superClasses(classe) == null) {
        		root = classe;
        	}
        }
		
        buildGraph(root);
        
		return m_graph;
	}
	
	private void buildGraph(String classeCorrente) {
		
		Node currNode = m_graph.addNode(); //crea il nodo
		currNode.setString("name", classeCorrente);
		currNode.setString("type", "class");
		
		//aggiunge ricorsivamente i sottonodi
		ArrayList<String> subClassi = ontologia.subClasses(classeCorrente);
		if(subClassi != null) {
			for(String classe : subClassi) {
				buildGraph(classe);
			}
		}
		
		//aggiungo il nodo all'ArrayList
		m_nodes.put(classeCorrente, currNode);
		
		//creo la lista degli archi da aggiungere
		if(subClassi != null) {
			for(String sub : subClassi) {
				String partOf = ontologia.getQuery(sub);
				String[] edge = new String[3]; //caratteristiche arco
				edge[0] = classeCorrente; //testa arco
				//tipo arco
				if(partOf == null) {
					edge[1] = "part_of";
				}
				else {
					edge[1] =  "IS_A";
				}
				//coda arco
				edge[2] = sub;
				m_edges.add(edge);
			}
			
			//creo gli archi
			for(int i = 0; i < m_edges.size(); i++) {
				String[] strEdge = m_edges.get(i);
				
				//creo i nodi sorgente e target
				Node source = m_nodes.get(strEdge[0]);
				Node target = m_nodes.get(strEdge[2]);
				
				//creo l'oggetto arco
				Edge arco = m_graph.addEdge(source, target);
				arco.setString("label", strEdge[1]);
			}
		}
	}
}
