package org.processmining.plugins.myminer.knowledge;


import java.io.File;

import org.processmining.contexts.uitopia.*;
import org.processmining.contexts.uitopia.annotations.*;
import org.processmining.framework.plugin.annotations.*;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

@Plugin(name = "Ontologia plugin",
parameterLabels = { "nomeFile" },
returnLabels = { "ontology" },
returnTypes = { OWLOntologia.class })
//userAccessible = true)

public class OWLOntologyPlugin {
	
	@UITopiaVariant(affiliation = "UniversitÓ del piemonte orientale",
					author = "Diego Spalla",
					email = "20010815@studenti.uniupo.it",
					uiLabel = UITopiaVariant.USEPLUGIN)
	
	@PluginVariant(requiredParameterLabels = {0})
	public static OWLOntologia ontology(final UIPluginContext context, final String nomeFile) {
		
		OWLOntologia ontology = new OWLOntologia();
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		File loader = new File(nomeFile);
		
		try {
			OWLOntology ontologiaTmp = manager.loadOntologyFromOntologyDocument(loader);
			ontology.setOntologia(ontologiaTmp);
			ontology.setManager(manager);
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		return ontology;
	}
	
}
