package org.processmining.plugins.myminer.knowledge;

import org.processmining.framework.plugin.annotations.*;

import javax.swing.*;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.*;

@Plugin(name = "Display Ontologia",
		parameterLabels = {"OWLOntologia"},
		returnLabels = {"Ontology viewer"},
		returnTypes = { JComponent.class },
		userAccessible = false)

@Visualizer
public class OntologyVisualizer {
    
	@PluginVariant(requiredParameterLabels = {0})
	public static JComponent visualize(final UIPluginContext context, final OWLOntologia ontology) {
	    
		VisualizePanel panel = new VisualizePanel(ontology,null);
		panel.setVisible(true);
		panel.setOpaque(false);
		
		return panel;
	}
}
