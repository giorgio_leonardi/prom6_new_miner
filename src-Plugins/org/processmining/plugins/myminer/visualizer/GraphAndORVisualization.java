package org.processmining.plugins.myminer.visualizer;

import javax.swing.JComponent;

import org.processmining.contexts.uitopia.annotations.Visualizer;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraphvisualize.JGraphAndOr;
import org.processmining.models.jgraph.ProMJGraphVisualizer;

@Plugin(name = "Visualize GraphAndOr", parameterLabels = { "Graph" }, returnLabels = { "GraphAndOr Visualization " }, returnTypes = { JComponent.class })
@Visualizer
public class GraphAndORVisualization {

@PluginVariant(requiredParameterLabels = { 0 })
public static JComponent visualize(PluginContext context, GraphAndOr graph) {
		
		return ProMJGraphVisualizer.instance().visualizeGraph(context,new JGraphAndOr(graph));
	
	}


}