package org.processmining.plugins.myminer.sim;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.processmining.plugins.myminer.index.IndexTable;

public class TracesPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private Set<Integer> traces;
	
	private JList<Trace> list;
	private DefaultListModel<Trace> model;
	
	public TracesPanel(Set<Integer> traces) {
		this.traces = traces;
		initList();
		setVisible(true);		
	}
	
	private void initList() {
		model = new DefaultListModel<Trace>();
		
		for(Integer trace: traces) 
			model.addElement(new Trace(trace));
			
		list = new JList<Trace>(model) {

			private static final long serialVersionUID = 1L;
			
			@Override
			public boolean getScrollableTracksViewportWidth() {
	            return true;
	        }
		};
		
		list.setCellRenderer(new TracesRenderer());
		
		ComponentListener listener = new ComponentAdapter() {
			
			@Override
			public void componentResized(ComponentEvent e) {
				list.setFixedCellHeight(10);
	            list.setFixedCellHeight(-1);
			}
			
		};
		
		list.addComponentListener(listener);		
		add(new JScrollPane(list));
	}
	
	public JList<Trace> getJList() {
		return list;
	}	

}
