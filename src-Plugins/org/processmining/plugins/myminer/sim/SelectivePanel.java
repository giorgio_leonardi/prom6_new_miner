package org.processmining.plugins.myminer.sim;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;

import org.processmining.plugins.myminer.index.IndexTable;

public class SelectivePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Vector<IndexTable> sol;

	private JList<Path> list;
	private DefaultListModel<Path> model;
	
	public SelectivePanel(Vector<IndexTable> sol) {
		this.sol = sol;		
        initList();        
        setVisible(true);
	}
	
	private void initList() {
		model = new DefaultListModel<Path>();
		
		for(IndexTable it: sol) 
			model.addElement(new Path(it.getMatches()));
			
		list = new JList<Path>(model) {

			private static final long serialVersionUID = 1L;
			
			@Override
			public boolean getScrollableTracksViewportWidth() {
	            return true;
	        }
		};
		
		list.setCellRenderer(new PathsRenderer());
		
		ComponentListener listener = new ComponentAdapter() {
			
			@Override
			public void componentResized(ComponentEvent e) {
				list.setFixedCellHeight(10);
	            list.setFixedCellHeight(-1);
			}
			
		};
		
		list.addComponentListener(listener);		
		add(new JScrollPane(list));
	}
	
	public JList<Path> getJList() {
		return list;
	}

}
