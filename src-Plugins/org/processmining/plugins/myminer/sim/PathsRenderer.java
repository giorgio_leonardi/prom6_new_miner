package org.processmining.plugins.myminer.sim;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;

public class PathsRenderer extends JLabel implements ListCellRenderer<Path> {

	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JTextArea textArea;
	
	public PathsRenderer() {
		panel = new JPanel();
		panel.setLayout(new BorderLayout());

		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setSize(1500, 1000);
		textArea.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		textArea.setFont(new Font("Arial", Font.PLAIN, 16));
		panel.add(textArea, BorderLayout.CENTER);
		
		setOpaque(true);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends Path> list, Path path, int index, boolean isSelected, boolean cellHasFocus) {
		
		setIcon(null);
		textArea.setText(path.toString());
		
		
		if(isSelected) {
			textArea.setBackground(list.getSelectionBackground());
			textArea.setForeground(list.getSelectionForeground());
		}
		
		else {
			textArea.setBackground(list.getBackground());
			textArea.setForeground(list.getForeground());
		}
		
		return panel;
	}

}
