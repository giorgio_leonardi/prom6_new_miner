package org.processmining.plugins.myminer.sim;

public class Tabella {
	
	private int id;
	private String[] stringa;

	public Tabella(int id, String[] stringa) {
		this.id = id;
		this.stringa = stringa;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String[] getStringa() {
		return stringa;
	}

	public void setStringa(String[] stringa) {
		this.stringa = stringa;
	}
}
