package org.processmining.plugins.myminer.sim;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Panel;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.geom.Ellipse2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.models.andOrGraph.DirectedGraphEdgeImpl;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.plugins.myminer.Merge.ChAPMaNUnicitaPlugin;
import org.processmining.plugins.myminer.Merge.RetrievalSolution;
import org.processmining.plugins.myminer.fsm.NonDeterministicFSM_Thomson;
import org.processmining.plugins.myminer.index.IndexTable;
import org.processmining.plugins.myminer.miner.ChAPMaNSettings;
import org.processmining.plugins.myminer.miner.MergingStep;
import org.processmining.plugins.myminer.miner.MyMiner;
import org.processmining.plugins.myminer.traceRetrieval.TraceRetrieval;
import org.processmining.plugins.myminer.visualizer.GraphAndORVisualization;

public class SIMplugin {
	private static LeftPanel leftPanel;
	private static DownPanel downPanel;
	private static UpPanel upPanel;
	public static GraphAndOr graph;
	public static JComponent frame;
	public static JComponent graphJcomponent;
	private static PluginContext c;
	private static RetrievalSolution sol;
	
	private static SelectivePanel selectivePanel;
	private static boolean selective;

	@Plugin(
             name = "Sim Plugin", 
             parameterLabels = {"GraphAndOr"}, 
             returnLabels = { "JComponent" }, 
             returnTypes = { JComponent.class }, 
             userAccessible = true
     )
     @UITopiaVariant(
             affiliation = "Universit� del Piemonte Orientale", 
             author = "Stefano Nera, Francesco Leonardi", 
             email = "20024645@studenti.uniupo.it, 20019152@studenti.uniupo.it"
     )
	public static JComponent main(PluginContext context, GraphAndOr g) {
		//Setta le variabili
		c = context;
		graph = g;
		
		selective = false;
		
		//Ritorna un JComponent contenente il grafo iniziale
		graphJcomponent =GraphAndORVisualization.visualize(context,graph);
		
		//Crea il frame 
		frame = new JPanel(new BorderLayout());
		
		//Crea i pannelli
		leftPanel = new LeftPanel();
		downPanel = new DownPanel();
		upPanel = new UpPanel();
		
		//Aggiungo le scroll al Left Panel
		JScrollPane jScrollPane = new JScrollPane(leftPanel);
		leftPanel.setPreferredSize(new Dimension (3000, 1000));
		jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	    jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	    jScrollPane.setPreferredSize(new Dimension (500, 500));
	    jScrollPane.setBounds(50, 30, 300, 50);
	    jScrollPane.getViewport().setViewPosition(new Point(1300, 0));
		
		//Aggiunge componenti al frame
		frame.add(downPanel, BorderLayout.SOUTH);
		frame.add(upPanel, BorderLayout.NORTH);
		frame.add(graphJcomponent,  BorderLayout.CENTER);
		frame.add(jScrollPane, BorderLayout.WEST);
		
		//Imposta come dimensione di default lo screen
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setSize(screenSize.width, screenSize.height);
		frame.setVisible(true);
		
		//Ritorna il frame(JComponent) che viene passato a PROM per essere visualizzato
		return frame;
}
	
	//Ritorna il grafo attualmente visualizzato
	public static GraphAndOr getGraph() {
		return graph;
	}
	
	public static boolean getSelective() {
		return selective;
	}
	
	public static void setSelective(boolean sel) {
		selective = sel;
	}
	
	//Setta il nuovo grafo da visualizzare 
	public static void setGraph(GraphAndOr newGraph) {
		
		//Rimuove il JComponet attualmente visualizzato
		frame.remove(graphJcomponent);
		
		//Setta la variabile globale graph con il nuovo input
		graph = newGraph;
		
		//Ritorna il nuovo JComponent contente il grafo da visualizzare
		graphJcomponent = GraphAndORVisualization.visualize(c,graph);
		
		//Aggiunge il JComponent nuovo al frame
		frame.add(graphJcomponent);
		frame.validate();
	}
	
	//Visualizza il grafo inerente alla query applicata
	public static void setGraphQuery(String query) throws UserCancelledException {
		
		//Applica la query, ritorna un oggetto contenente in [0] il grafo e in [1] la soluzione
		Object[] objRetrive = retrieve(query,graph);
		
		//Rimuove il JComponet attualmente visualizzato nel Left Panel
		frame.remove(graphJcomponent);
		
		//Setta la variabile globale graph con il nuovo input
		graph = (GraphAndOr) objRetrive[0];
		
		//Setta la variabile  sol
		sol = (RetrievalSolution) objRetrive[1];
		
		//Ritorna il nuovo JComponent contente il grafo da visualizzare
		graphJcomponent = GraphAndORVisualization.visualize(c,graph);
		
		//Aggiunge il JComponent nuovo al frame
		frame.add(graphJcomponent);
		frame.validate();
	}
	
	//Applica la query al grafo
	public static Object[] retrieve(String query, GraphAndOr graph) throws UserCancelledException{
	
		NonDeterministicFSM_Thomson nfa=null;
		
		Vector<IndexTable> res = null ;
		
		String regEx = query;
		
		IndexTable it = new IndexTable();
		nfa=new NonDeterministicFSM_Thomson(regEx,it);
		GraphAndOr g = graph.clone();
				
		g.reset();
		res = TraceRetrieval.search_process(g, nfa,it);
		for (IndexTable r:res){
			r.setResult();
		}
	
		Object[] ret=new Object[2];
		ret[0]=g;
		ret[1]=new RetrievalSolution();
		((RetrievalSolution)ret[1]).setSol(res);
				
		getTraces(g,regEx,it);
		
		return ret;
	}
	
	public static void getTraces(GraphAndOr g, String regEx, IndexTable it) {
		
		
		Set<Integer> traces = new HashSet<>();
		
		Set<Integer> white = new HashSet<>();
		Set<Integer> red = new HashSet<>();
		Set<Integer> yellow = new HashSet<>();
		
		Set<Integer> green = new HashSet<>();
		
		
		for(Node n: g.getNodes()) {
			switch (n.isRetrieve()) {
			case 0:
				insertTraces(white, n.getTraces());
				break;
			case 1:
				insertTraces(red, n.getTraces());
				break;
			case 2:
				insertTraces(yellow, n.getTraces());
				break;
			case 3:
				insertTraces(green, n.getTraces());
				break;
			}	
		}	
	
		// per ogni traccia rossa
		for(Integer trace_number:red) {
			
			// Prendo il grafo relativo alla traccia
			GraphAndOr g_trace = g.getTraceGraph(trace_number);		
			
			/*  Questo perch� nel caso in cui un grafo viene importato e quindi non viene creato perch� minato un Log
			 *  si perde il riferimento tra il grafo e il log, in quanto il Log non viene esportato insieme al modello del grafo.
			 *  Questo fa si che risulta impossibile ricostruire le tracce perch� non si ha pi� il riferimento
			 *  Il processo di esportazione e importazione del grafo � nel file /src-Plugins/ioUtility/FileManager.java
			 */			
			if(g_trace == null)
				break;
			
			//Creo l'automa che esegue la query
			NonDeterministicFSM_Thomson nfa_green =new NonDeterministicFSM_Thomson(regEx,it);			
			//esegue la query
			g_trace.reset();			
			Vector<IndexTable> res = TraceRetrieval.search_process(g_trace, nfa_green, it);
			for (IndexTable r:res){
				r.setResult();
			}
			//per ogni nodo della traccia guardo se esiste uno rosso, in quel caso la query � vera e quindi la traccia viene restituita			
			for(Node n: g_trace.getNodes()) {
				if (n.isRetrieve() == 2) {
					insertTraces(traces, n.getTraces());
					break;					
				}	
			}	
		}
		
		DownPanel.setTraces(traces);	
	}
	
	
	private static void insertTraces(Set<Integer> a, Set<Integer> b) {
		for(Integer trace: b)
			if(!a.contains(trace))
				a.add(trace);
	}
	
	public static GraphAndOr applyMerge(int merge) {
		GraphAndOr g = null;
		//Weak Or Strong
		if(merge==0 || merge==1) {
			g = WeakOrStrong(merge);
		}
		//Chapman Weak Or Strong
		else{
			try {
				g = merge(merge);
			} catch (IOException e) {				
				//e.printStackTrace();
				System.out.println("(SIMplugin) Exception: IOException by applyMerge");
			}
		}
		return g;
	}

	
	//Applica il merge Weak o Strong
	public static GraphAndOr WeakOrStrong(int merge) {
		upPanel.setSelective(false);
		
		RetrievalSolution curr=sol.clone();
		curr.removeDuplicates();
		
		// INIZIO SELECTIVE

		if(getSelective()) {
			// mostro il pannello per selezionare i percorsi
			selectivePanel = new SelectivePanel(curr.getSol());
			int option = JOptionPane.showConfirmDialog(frame, selectivePanel, "Seleziona i percorsi", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
			
			// se � stato cliccato "ok"
			if(option == 0) {
				upPanel.setSelective(true);
				// di supporto
				RetrievalSolution selected = new RetrievalSolution();
				
				// salvo gli indici dei percorsi selezionati
				ArrayList<Integer> selectedIndices = new ArrayList<>();
				for(int index: selectivePanel.getJList().getSelectedIndices())
					selectedIndices.add(index);
				
				// riempio il retrieval solution di supporto con tutti i percorsi selezionati
				for(int i = 0; i < curr.getSol().size(); i++)
					if(selectedIndices.contains(i))
						selected.getSol().add(curr.getSol().get(i));
				
				// curr viene impostato con i percorsi selezionati
				curr = selected.clone();
			}
	
		}
		
		// FINE SELECTIVE
		
		GraphAndOr g=graph.clone();
		//graph.printNodesID();
		//System.out.println("clone");
		//g.printNodesID();
		curr.reWrite(g);
		g.reset();
		
		if (merge == 0)
			TraceRetrieval.mergeStrong(curr.getSol(), g);
		else TraceRetrieval.mergeWeak(curr.getSol(), g);
		g.simplifies();
		
		frame.remove(graphJcomponent);
		graph = g;
		graphJcomponent = GraphAndORVisualization.visualize(c,graph);
		frame.add(graphJcomponent);
		frame.validate();
			
		return g;
	}
	
	//ChapMAn
	public static GraphAndOr merge(int merge)throws IOException {
		//ParametersUniqPanel parameters = new ParametersUniqPanel();
		boolean hard = false;
		if(merge == 3) {
			hard = true;
		}
		GraphAndOr graphChapman = graph.clone();
		MergingStep m = new MergingStep(graphChapman);
		// foglie 
		Set<Node> leafs = graphChapman.getLeafs();
		// insieme dei nodi da esaminare
		Set<String> s=new LinkedHashSet<String>();
		Set<String> examined=new TreeSet<String>();
		//lo riempo con le foglie
		for (Node n:leafs)
			s.add(n.getLabel());
		//se devo fare un merge soft costruisco i superset
		if (!hard)
			s=m.getSuperSets(leafs);
		Iterator<String> it=s.iterator();
		while (it.hasNext())
		{
			//estraggo un elemento di cui fare il merge
			String node=it.next();
			it.remove();
			
			//nodi da mergiare
			Set<Node> copy ;
			//HARD
			if (hard)
				copy = graphChapman.getNodesByLabel(node);	
			else copy=graphChapman.getNodesByEvents(node);
			
			if(copy == null)
				throw new IOException();
			
			Node newNode=m.mergeNode(copy,node,hard);

			for (DirectedGraphEdgeImpl<?, ?> edge : newNode.getInEdge()) {
				//Ho dovuto modificare is Already CHecked in public !
				if (!ChAPMaNUnicitaPlugin.isAlreadyChecked(s,examined,edge.getSource().getLabel(),hard)){
					Set<Node> temp=new HashSet<Node>();
					temp.add(edge.getSource());
					s.addAll(m.getSuperSets(temp));
				}
			}
			examined.add(node);
			it=s.iterator();
		}
		
		frame.remove(graphJcomponent);
		graph = graphChapman;
		graphJcomponent =GraphAndORVisualization.visualize(c,graph);
		frame.add(graphJcomponent);
		frame.validate();
		
		return graphChapman;
	}
	
	//Calcola le coordinate del nuovo nodo
	public static void coordinate() {
		int[] circleValues = LeftPanel.generateValues();
		int x = circleValues[0];
		int y = circleValues[1];
		int width = 50;
		int height = 50;
		if(x!=0 && y!=0 || x==0 && y!=0 || x!=0 && y==0) {
			LeftPanel.circles.add(new Ellipse2D.Double(x, y, width, height));
			leftPanel.setLastRed(circleValues);
			leftPanel.repaint();		
		}
		
	}
	
	

}
