package org.processmining.plugins.myminer.sim;

import java.util.ArrayList;

import org.processmining.plugins.myminer.miner.LogsMap;

public class Trace {
	
	private Integer code;
	private ArrayList<String> content;
	
	public Trace(Integer code) {
		this.code = code;
		content = LogsMap.get(code);		
	}
	
	@Override
	public String toString() {
		
		String output = "[ " + code + " ] ";
		
		for(String s:content) {
			output = output + extractConceptName(s);
		}
		//return content.toString();
		return output;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public ArrayList<String> getContent() {
		return content;
	}

	public void setContent(ArrayList<String> content) {
		this.content = content;
	}
	
	
	/*
	 * Estrae il concept name per rendere la visione delle tracce pi� comprensibile
	 * 
	 * @trace: Traccia dalla quale estrarre i concept name
	 * 
	 * return: Stringa in formato "  -> concept.name"
	 * 
	 */
	
	public static String extractConceptName(String trace) {
		
		String output = "";
						
		String[] logAttributes = trace.split(",");
			
		for(String attribute:logAttributes) {
				
			if(attribute.contains("concept:name")) {
				String[] value = attribute.split("=");
				output = output + " -> " + (value[1]);
			}
		}	
		
		return output;
	}

}
