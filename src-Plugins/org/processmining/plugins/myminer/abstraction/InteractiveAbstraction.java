package org.processmining.plugins.myminer.abstraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.plugins.myminer.knowledge.OWLOntologia;
import org.processmining.plugins.myminer.sim.SIMplugin;
import org.processmining.plugins.myminer.simOntology.DownPanel;

public class InteractiveAbstraction extends Abstraction {
	// Tabella dei livelli <Traccia, nodi a quel livello>
	private static HashMap<Integer, ArrayList<DefaultMutableTreeNode>> tableLevel;
	
	/**
	 * performAbstaction effettua l'astrazione automatica sui rami
	 * @param nodeToAbstract: nodo d'astrarre
	 * @param currentGraph: modello di processo
	 * @param ontologia: ontologia contenente il nodo d'astrarre
	 * @return il modello di processo con il nodo astratto
	 */
	@SuppressWarnings("unchecked")
	public static GraphAndOr performAbstraction(DefaultMutableTreeNode nodeToAbstract,
											   GraphAndOr currentGraph,			 
											   OWLOntologia ontologia)			  	  
	{
		// Livelli d'astrazione
		tableLevel = DownPanel.getLevelTable();
		// Mappa dei discendenti <Livello, Nodi>
		HashMap<Integer,ArrayList<DefaultMutableTreeNode>> discendenti = discendenti(nodeToAbstract);
		// Distanza dal ground
		int D = distanceGround(discendenti);
		// Si cicla dal livello pi� alto fino al livello del nodo d'astrarre
		for(int level=D; level>=getANLevel(nodeToAbstract); level--) {
			// getNodesLevel si prendono i discendenti del nodo d'astrarre al livello "level"
			ArrayList<DefaultMutableTreeNode> nodes = discendenti.get(level);
			// Per ogni discendente
			for(DefaultMutableTreeNode node:nodes) {
				if(isLegalAbstraction(node, currentGraph.getAbstractionTable())){
					// Mappa <Tracce, Set di nodi della traccia>
					HashMap<Set<Integer>,Set<Node>> selective = new HashMap<Set<Integer>,Set<Node>>();
					// Controllo ISA
					if( isISA(ontologia, node) ) {
						// Prendo i figli
						ArrayList<TreeNode> children = Collections.list(node.children());
						// Lista dei nodi che saranno fusi in IS_A
						ArrayList<String> ISA_node = new ArrayList<String>();
						// Per ogni figlio
						for(TreeNode child:children) {
							//Prendo il nome dei figli
							ISA_node.add(((DefaultMutableTreeNode)child).toString());
						}
						// Nodi del grafo
						Set<Node> graph_node =  Find(ISA_node, currentGraph);
						//Per ogni nodo ISA si astrae singolarmente
						for(Node node_ISA:graph_node) {
							Set<Node> set_node = new TreeSet<Node>();
							set_node.add(node_ISA);
							Substitute(currentGraph, set_node, node.toString());
						}
						// Si marca il nodo nella tabella delle astrazioni
						addMark(currentGraph, node);
						}
					else {
						// Recupero i figli dalla ontologia
						ArrayList<TreeNode> children = Collections.list(node.children());
						// Lista di nodi che corrispondo alla query
						ArrayList<Node> red = new ArrayList<Node>();
						// Per ogni figlio
						for(TreeNode child:children) {
							// query
							red.addAll(precomputationQuery((DefaultMutableTreeNode)child, currentGraph));
						}
						// Nodi recuperati dal grafo
						Set<Node> graph_node = FindPartOf(red, currentGraph);
									
						// Hashmap che permette di discriminare i nodi per percorso sul grafo
						// Set<Integer> sono le tracce, nodi che hanno la stessa traccia sono per forza
						// sullo stesso ramo del grafo
						selective = differenziaPercorsi(graph_node);
						// Se sono presenti dei percorsi
						if(!selective.isEmpty()) {
							// Gestione dei conflitti
							selective = gestioneConflitti(selective);
							System.out.println(selective);
							// Astrazioni che non si sovrappongono
							HashMap<Set<Integer>, ArrayList<Set<Node>>> notOverlap = notOverlapped(selective);
							// Si inizializzano la lista che conterr� l'insiemi delle astrazioni
							List<TraceAbstraction> abstractionSelect;
							// Tracce nel trace panel
							TracesPanel traceOfAbstraction = new TracesPanel(notOverlap);
							// Viene mostrato il frame nel quale l'utente sceglie quale astrazione effettuare
							JOptionPane.showConfirmDialog(SIMplugin.frame, traceOfAbstraction, "Elenco dei conflitti", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
							// Si prendono le astrazioni selezionate
							abstractionSelect = traceOfAbstraction.getJList().getSelectedValuesList();
							// Se non sono state selezionate le tracce d'astrazioni
							if(abstractionSelect == null || abstractionSelect.isEmpty() || abstractionSelect.size()>1)
								return currentGraph;
							

							// Questione rimozione delle tracce
							TracesPanelRemove traceToRemove = new TracesPanelRemove(selective);
							// Viene mostrato il frame delle tracce da escludere
							JOptionPane.showConfirmDialog(SIMplugin.frame, traceToRemove, "Tracce da escludere", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
							// Lista delle scelte dell'utente contenente le tracce da rimuovere
							List<TraceRemove> list_remove = traceToRemove.getJList().getSelectedValuesList();
							
							// Per ogni traccia selezionata
							for(TraceAbstraction entry:abstractionSelect)
								if(!entry.getContent().isEmpty())
									// Per ogni astrazione
									for(Set<Node> track:entry.getContent()) {
										// Inizializza la rimozione dei nodi
										ArrayList<Node> nodesToRemove = new ArrayList<Node>();
										// Per ogni nodo dell'astrazione
										for(Node nodeTrack:track) {
											// Recupera le tracce passanti del nodo
											Set<Integer> traccePassanti = nodeTrack.getTraces();
											// Se esiste un traccia da escludere setta true in modo da ridurre i cicli
											boolean isToRemove = false;
											// Per ogni traccia passante
											for(Integer trackInNode:traccePassanti) {
												// Ottimizzazione
												if(isToRemove)
													break;
												// Per ogni traccia da rimuovere
												for(TraceRemove trackToRemove:list_remove) {
													// Se le tracce da rimuovere contengono una traccia passante
													if(trackToRemove.getCode().contains(trackInNode)) {
														// Ottimizzazione
														isToRemove = true;
														// Aggiungi il nodo alla lista di nodi da rimuovere
														nodesToRemove.add(nodeTrack);
													}
												}
											}												
										}
										//Rimozione dei nodi
										for(Node n:nodesToRemove)
											track.remove(n);
										// Applica la sostituzione
										Substitute(currentGraph, track, node.toString());
									}
							// Si pulisce la selezione
							selective.clear();
						}
						else {
							selective.clear();
							return currentGraph;
							
						}
						// Si marca il nodo nella tabella
						addMark(currentGraph, node);
					}
				}//end if legal
			}//end for nodes
		}//end for level
		return currentGraph;
	}
	
	/**
	 * Estrapola il livello nodo da astrarre
	 * @param nodeToAbstract: livello del nodo d'astrarre
	 * @return ritorna il livello del nodo
	 */
	private static int getANLevel(DefaultMutableTreeNode nodeToAbstract) {
		// Per ogni riga della tabella dei livelli
		for(Entry<Integer,ArrayList<DefaultMutableTreeNode>> entry:tableLevel.entrySet())
			// Verifica se al quel livello c�� il nodo d'astrarre
			if(entry.getValue().contains(nodeToAbstract))
				// Se viene verificato, ritorna il livello 
				return entry.getKey();
		// Altrimenti ritorna la radice (Livello 0)
		return 0;
	}
	
	/**
	 * Distanza dal nodo ground 
	 * @param discendenti: discendeti del nodo d'astrarre
	 * @return Ritorna il livello massimo
	 */
	private static int distanceGround(HashMap<Integer,ArrayList<DefaultMutableTreeNode>> discendenti) {
		int max_index = 0;
		// Si cicla sulla Mappa
		for(Entry<Integer,ArrayList<DefaultMutableTreeNode>> entry:tableLevel.entrySet())
			// Si aggiorna
			if(max_index<entry.getKey())
				max_index=entry.getKey();
		// Si ritorna l'indice massimo
		return max_index;
	}
	
	/**
	 * La funzione serve per prendere i discendenti di un nodo
	 * @param nodeToAbstract: Nodo d'astrarre del quale si vogliono recuperare i discendenti
	 * @return Ritorna una mappa <Livello, Set di nodi discendenti>
	 */
	private static HashMap<Integer,ArrayList<DefaultMutableTreeNode>> discendenti(DefaultMutableTreeNode nodeToAbstract) {
		// Si crea la mappa dei discendenti
		HashMap<Integer,ArrayList<DefaultMutableTreeNode>> nodiDiscendenti = new HashMap<Integer,ArrayList<DefaultMutableTreeNode>>();
		// Si effettua una discesa ricorsiva dal nodo d'astrarre ai nodi ground	
		performDiscendenti(nodeToAbstract, nodiDiscendenti, getANLevel(nodeToAbstract));
		// Si ritorna la mappa dei discendenti
		return nodiDiscendenti;
	}
	
	/**
	 * Effettua una discesa ricorsiva per recuperare i discendenti
	 * @param nodeToAbstract: Nodo d'astrarre
	 * @param nodiDiscendenti: Lista dei nodi discendenti per livello [da decorare]
	 * @param level: il livello di profondit�
	 */
	@SuppressWarnings("unchecked")
	private static void performDiscendenti(DefaultMutableTreeNode nodeToAbstract, HashMap<Integer,
						                      ArrayList<DefaultMutableTreeNode>> nodiDiscendenti,
											  int level) {
	    // Se non � ancora presente il livello nella mappa
		if(!nodiDiscendenti.containsKey(level)) {
			// Si crea la lista
			ArrayList<DefaultMutableTreeNode> nodi = new ArrayList<DefaultMutableTreeNode>();
			// Si aggiunge il nodo d'astrarre
			nodi.add(nodeToAbstract);
			// Si inserisce nella mappa
			nodiDiscendenti.put(level, nodi);
		}
		else {
			// Si prende la lista dei nodi del livello
			ArrayList<DefaultMutableTreeNode> nodi = nodiDiscendenti.get(level);
			// Si aggiunge il nodo d'astrarre
			nodi.add(nodeToAbstract);
			// Si inserisce nella mappa
			nodiDiscendenti.put(level, nodi);
		}
		// Se il nodo � una foglia � il caso base
		if(nodeToAbstract.isLeaf())
			return;
		else {
			// Per ogni figlio
			ArrayList<TreeNode> children = Collections.list(nodeToAbstract.children());
			for(TreeNode child:children)
				// Effettua una discesa sui figli e aggiorna il livello
				performDiscendenti((DefaultMutableTreeNode)child, nodiDiscendenti, level+1);
		}
	}
		
}
