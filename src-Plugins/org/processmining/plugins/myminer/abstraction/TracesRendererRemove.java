package org.processmining.plugins.myminer.abstraction;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;

public class TracesRendererRemove extends JLabel implements ListCellRenderer<TraceRemove> {

	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JTextArea textArea;
	
	public TracesRendererRemove() {
		panel = new JPanel();
		panel.setLayout(new BorderLayout());

		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setSize(1500, 1200);
		textArea.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		textArea.setFont(new Font("Arial", Font.PLAIN, 13));
		panel.add(textArea, BorderLayout.CENTER);		
		
		setOpaque(true);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends TraceRemove> list, TraceRemove trace, int index, boolean isSelected, boolean cellHasFocus) {
		
		setIcon(null);
		textArea.setText(trace.toString());
		
		
		if(isSelected) {
			textArea.setBackground(list.getSelectionBackground());
			textArea.setForeground(list.getSelectionForeground());
		}
		
		else {
			textArea.setBackground(list.getBackground());
			textArea.setForeground(list.getForeground());
		}
		
		return panel;
	}

}
