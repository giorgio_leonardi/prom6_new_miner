package org.processmining.plugins.myminer.abstraction;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.processmining.models.andOrGraph.DirectedGraphEdgeImpl;
import org.processmining.models.andOrGraph.Node;
import org.processmining.plugins.myminer.miner.LogsMap;

public class TraceAbstraction {
	
	private Set<Integer> code;
	private ArrayList<Set<Node>> content;
	private String output_string = "";
	
	public TraceAbstraction(Set<Integer> code, ArrayList<Set<Node>>  content, String output) {
		this.code = code;
		this.content = content;
		output_string = output;
	}
	
		@Override
	public String toString() {
		return output_string;
	}

	public Set<Integer> getCode() {
		return code;
	}
	
	public void setCode(Set<Integer> code) {
		this.code = code;
	}

	public ArrayList<Set<Node>> getContent() {
		return content;
	}

	public void setContent(ArrayList<Set<Node>> content) {
		this.content = content;
	}

}
