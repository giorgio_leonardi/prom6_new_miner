package org.processmining.plugins.myminer.abstraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.plugins.myminer.knowledge.OWLOntologia;

	public class AutomaticAbstraction extends Abstraction{

		/**
		 * performAbstaction effettua l'astrazione automatica sui rami
		 * @param nodeToAbstract: nodo d'astrarre
		 * @param currentGraph: modello di processo
		 * @param ontologia: ontologia contenente il nodo d'astrarre
		 * @return il modello di processo con il nodo astratto
		 */
		@SuppressWarnings("unchecked")
		public static GraphAndOr performAbstraction(DefaultMutableTreeNode nodeToAbstract,
												   GraphAndOr currentGraph,			  
												   OWLOntologia ontologia)			  
		{
			// Se � un astrazione legale
			if(isLegalAbstraction(nodeToAbstract, currentGraph.getAbstractionTable())) {
				// Trovato su OWLToGraphConverter se null � partOf		
				if( isISA(ontologia, nodeToAbstract) ) {
					// Recupero i figli dell'ontologia
					ArrayList<TreeNode> children = Collections.list(nodeToAbstract.children());
					// Lista dei nodi che saranno fusi in IS_A
					ArrayList<String> ISA_node = new ArrayList<String>();
					// Per ogni figlio
					for(TreeNode child:children) {
						// Chiamata ricorsiva
						performAbstraction((DefaultMutableTreeNode)child, currentGraph, ontologia);
						// Aggiungo il figlio alla lista IS_A
						ISA_node.add(child.toString());
					}
					//Nodi recuparati dal grafo
					Set<Node> graph_node = Find(ISA_node, currentGraph);
					//Viene creato un set di nodi per ogni nodo, serve per il metodo substitute
					for(Node node:graph_node){
						Set<Node> set_node = new TreeSet<Node>();
						set_node.add(node);
						//Sostituisco il nodo nel grafo
						Substitute(currentGraph, set_node, nodeToAbstract.toString());
					}
					//Marco il nodo nella tabella
					addMark(currentGraph, nodeToAbstract);
				}
				else {
					// Recupero i figli dalla ontologia
					ArrayList<TreeNode> children = Collections.list(nodeToAbstract.children());
					// Lista di nodi che corrispondo alla query
					ArrayList<Node> red = new ArrayList<Node>();
					// Per ogni figlio
					for(TreeNode child:children) {
						// Chiamata ricorsiva
						performAbstraction((DefaultMutableTreeNode)child, currentGraph, ontologia);
						red.addAll(precomputationQuery((DefaultMutableTreeNode)child, currentGraph));
					}
					//Marco il nodo nella tabella
					addMark(currentGraph, nodeToAbstract);
					//Nodi recuparati dal grafo
					Set<Node> graph_node = FindPartOf(red, currentGraph);
					// Hashmap che permette di discriminare i nodi per percorso sul grafo Set<Integer> sono le tracce, 
					// nodi che hanno la stessa traccia sono per forza sullo stesso ramo del grafo
					HashMap<Set<Integer>,Set<Node>> selective = differenziaPercorsi(graph_node);
					// Gestione dei conflitti
					selective = gestioneConflitti(selective);
					// Percorsi non sovrapponibili
					HashMap<Set<Integer>, ArrayList<Set<Node>>> overlap = notOverlapped(selective);
					// Politica FIFO
					overlap = FIFO(overlap);
					// Per ogni riga della HashMap
					for(Map.Entry<Set<Integer>,ArrayList<Set<Node>>> e:overlap.entrySet()) {
						// Prendo ogni astrazione
						for(Set<Node> nodes:e.getValue()) 
							Substitute(currentGraph, nodes, nodeToAbstract.toString());
					}//fine for each HashMap
				}//End if Part of
			}//End if isLegal
			return currentGraph;
		}

		/**
		 * Ritorna una HashMap che contiene solo la prima astrazione possibile secondo la politica FIFO
		 * @param overlap: HashMap che contiene i conflitti gi� gestiti
		 * @return Ritorna l'HashMap con l'unica astrazione possibile secondo la politica FIFO
		 */
		private static HashMap<Set<Integer>, ArrayList<Set<Node>>> FIFO(HashMap<Set<Integer>, ArrayList<Set<Node>>> overlap) {
			HashMap<Set<Integer>, ArrayList<Set<Node>>> fifo = new HashMap<Set<Integer>, ArrayList<Set<Node>>>();
			// Per ogni riga della mappa
			for(Entry<Set<Integer>, ArrayList<Set<Node>>> entry:overlap.entrySet()) {
				// Prendo la prima e poi esco dal ciclo
				fifo.put(entry.getKey(), entry.getValue());
				break;
			}
			return fifo;
		}
}

