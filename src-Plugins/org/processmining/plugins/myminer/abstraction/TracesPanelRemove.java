package org.processmining.plugins.myminer.abstraction;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.processmining.models.andOrGraph.DirectedGraphEdgeImpl;
import org.processmining.models.andOrGraph.Node;
import org.processmining.plugins.myminer.index.IndexTable;

public class TracesPanelRemove extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private HashMap<Set<Integer>, String> sortAbstraction;
	private HashMap<Set<Integer>, Set<Node>> nodeToRemove;
	
	private JList<TraceRemove> list;
	private DefaultListModel<TraceRemove> model;
	
	public TracesPanelRemove(HashMap<Set<Integer>, Set<Node>> nodeToRemove){
		this.nodeToRemove = nodeToRemove;
		//sortAbstractionTrace();
		initList();
		setVisible(true);		
	}
	
	private void initList() {
		model = new DefaultListModel<TraceRemove>();
		
		for(Entry<Set<Integer>, Set<Node>> e:nodeToRemove.entrySet())
			model.addElement(new TraceRemove(e.getKey(),e.getValue(), e.getKey().toString()));
			
		list = new JList<TraceRemove>(model) {

			private static final long serialVersionUID = 1L;
			
			@Override
			public boolean getScrollableTracksViewportWidth() {
	            return true;
	        }
		};
		
		list.setCellRenderer(new TracesRendererRemove());
		
		ComponentListener listener = new ComponentAdapter() {
			
			@Override
			public void componentResized(ComponentEvent e) {
				list.setFixedCellHeight(10);
	            list.setFixedCellHeight(-1);
			}
			
		};
		
		list.addComponentListener(listener);		
		add(new JScrollPane(list));
	}
	
	public JList<TraceRemove> getJList() {
		return list;
	}	
	
	/*
	private void sortAbstractionTrace() {
		
		ArrayList<Node> root = new ArrayList<Node>();
		sortAbstraction = new HashMap<Set<Integer>, String>();
		
		for(Entry<Set<Integer>, ArrayList<Set<Node>>> entry:abstracionNotOverlap.entrySet()) {
	
			for(Set<Node> nodes:entry.getValue()) {
				for(Node node:nodes) {

					boolean b_first = true;
					
					ArrayList<Node> nodi_in = new ArrayList<Node>();
					for(DirectedGraphEdgeImpl in:node.getInEdge()) {
						nodi_in.add(in.getSource());
					}
					
					for(Node in:nodi_in)
						//se contiene un nodo dell'astrazione, allora non � il primo
						if(nodes.contains(in))
							b_first = false;
					//se non contiene nodi dell'astrazione allora � il primo
					if(b_first) {
						//first = (Node) node;
						root.add(node);
						break;
					}
				}
			}
		}
		
		String output = "";
		int index = 0;	
			
		if(!root.isEmpty()){
			for(Entry<Set<Integer>, ArrayList<Set<Node>>> track:abstracionNotOverlap.entrySet()) {
					
				output = "";
					
				for(Set<Node> nodes:track.getValue()) {
					output += "..  -> [";
						
					output = output + sortPerform(nodes, root.get(index));

					output += "] <-  ..";
						
					index++;
				}

				sortAbstraction.put(track.getKey(), output);
			}
				
		}
	
		
	}
	
	
	private String sortPerform(Set<Node> set_nodi, Node node) {
		
		for(DirectedGraphEdgeImpl out:node.getOutEdge()) {
			
			if(set_nodi.contains(out.getTarget())) {
				 return node.toString() + " -> " + sortPerform(set_nodi, out.getTarget());
			}
		}
		
		return node.toString();
	}
	*/

}
