package org.processmining.plugins.myminer.abstraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.models.andOrGraph.DirectedGraphEdgeImpl;
import org.processmining.models.andOrGraph.Edge;
import org.processmining.models.andOrGraph.Event;
import org.processmining.models.andOrGraph.EventsNode;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.Node;
import org.processmining.models.andOrGraph.Pair;
import org.processmining.plugins.myminer.knowledge.OWLOntologia;
import org.processmining.plugins.myminer.sim.SIMplugin;

public class Abstraction {

	/**
	 * Costanti per indicare il tipo di astrazione
	 */
	public final static int TYPE_AUTOMATIC = 0;
	public final static int TYPE_INTERACTIVE = 1;
	
	/**
	 * performAbstaction effettua l'astrazione automatica sui rami
	 * @param nodeToAbstract
	 * @param currentGraph
	 * @param ontologia
	 * @return
	 */
	public static GraphAndOr performAbstraction(DefaultMutableTreeNode nodeToAbstract, GraphAndOr currentGraph, OWLOntologia ontologia) {return null;};
	
    /**
     * Verifica se l'astrazione � legale
     * @param nodeToAbstract: Nodo d'astrarre
     * @param tableAbstraction: Tabella delle astrazioni
     * @return True se l'astrazione � legale
     */
	public static boolean isLegalAbstraction(DefaultMutableTreeNode nodeToAbstract,
									   		 ArrayList<String> tableAbstraction) {
		// Verifica nodo Ground
		if(nodeToAbstract.children().hasMoreElements()==false)
			return false;
		// Verifico se non � stato aggiunto
		if(tableAbstraction.contains(nodeToAbstract.getUserObject().toString()))
			return false;
		// Se � valido
		return true;
	}
	
	/**
	 * Metodo Find, trova i nodi del grafo dati i nodi dell'ontologia
	 * @param nodes: Nodi dell'ontologia
	 * @param currentGraph: Modello di processo corrente
	 * @return Nodi del grafo restituiti
	 */
	protected static Set<Node> Find(ArrayList<String> nodes, GraphAndOr currentGraph) {
		//Nodi del grafo da ritornare 
		Set<Node> output = new TreeSet<Node>();
		// Per ogni nodo "rosso" perch� figlio del nodo IS_A
		for(String node:nodes)
		{
			//nodi da mergiare
			Set<Node> n = currentGraph.getNodesByLabel(node); 
			if(n!=null) {
				output.addAll(n);
			}
		}
		return output;
	}
	

	/**
	 * Metodo FindPartOf, trova i nodi del grafo dati i nodi dell'ontologia
	 * @param nodes: Nodi dell'ontologia
	 * @param currentGraph: Modello di processo corrente
	 * @return Nodi del grafo restituiti
	 */
	protected static Set<Node>FindPartOf(ArrayList<Node> nodes, GraphAndOr currentGraph) {
		//Nodi del grafo da ritornare 
		Set<Node> output = new TreeSet<Node>();
		// Per ogni nodo "rosso" perch� figlio del nodo IS_A
		for(Node node:nodes)
		{
			Set<Node> n = currentGraph.getNodesByLabel(node.getLabel());
			if(n!=null) {
				// Aggiungo tutti i nodi identificati con il Label dei nodi rossi/verdi
				output.addAll(n);	
			}
		}
		return output;
	}
	
	/**
	 * Metodo che verifica se il nodo ha una relazione di tipo ISA
	 * @param ontologia: L'ontologia 
	 * @param nodeToAbstract: Nodo da astrarre
	 * @return Ritorna True se il nodo ha una relazione di tipo ISA
	 */
	protected static boolean isISA(OWLOntologia ontologia, DefaultMutableTreeNode nodeToAbstract) {
		
		@SuppressWarnings("unchecked")
		ArrayList<TreeNode> children = Collections.list(nodeToAbstract.children());
		
		for(TreeNode child:children) {
			
			if(ontologia.getQuery(((DefaultMutableTreeNode)child).toString()) != null)
				return true;
			else 
				return false;	
		}
		return true;
	}
	
	/**
	 * Effettua una precomputazione della query
	 * @param child: Nodo della query d'astrarre
	 * @param currentGraph: Modello di processo
	 * @return Nodi rossi e verdi che corrispondono alla query
	 */
	protected static ArrayList<Node> precomputationQuery(DefaultMutableTreeNode child, 
											  			 GraphAndOr currentGraph) {
		//Lista di nodi rossi ritornati
		ArrayList<Node> red = new ArrayList<Node>();
		/*	PRE-COMPUTO LA QUERY */
		// Gestione dello spazio 
		String query_name = child.toString().replace("_", " ");
		// Intervallo JOLLY
		String intervallo = "(0,20)";
		String q = intervallo+query_name+intervallo;
		
		// recupero i nodi rossi e verde della query
		try {
			Object[] oo = SIMplugin.retrieve(q,currentGraph);
			GraphAndOr gg = (GraphAndOr) oo[0];
			for(Node n: gg.getNodes()) {
				switch (n.isRetrieve()) {
				case 1:
					if(!red.contains(n))
						red.add(n);
					break;
				case 3:
					if(!red.contains(n))
						red.add(n);
					break;
				}	
			}
									
		} catch (UserCancelledException e) {
			e.printStackTrace();
		}
		return red;
	}
	

	/** 
	 * Substitute per l'astrazione
	 * @param nodeMerge: Set di nodi da astrarre
	 * @param label: Nome del nuovo nodo
	 * @return Nuovo nodo astratto
	 */
	public static Node Substitute(GraphAndOr net, Set<Node> nodeMerge, String label) {
		
		Iterator<Node> it = nodeMerge.iterator();

		if(nodeMerge.isEmpty())
			return null;
		// Nodo sopravvissuto
		Node newNode=null;
		
		newNode= new EventsNode(new Event(label), false, 0);
		net.addNode(newNode);

		Set<Edge> remove = new HashSet<Edge>();
		while (it.hasNext()) {
			Node n = it.next();
			it.remove();
			for (DirectedGraphEdgeImpl<?, ?> edge : n.getOutEdge()) {
				// inserisco un arco in cui il nuovo nodo � la sorgente
				remove.add((Edge) edge);
				if(edge.getTarget().compareTo(newNode)!=0) {
				Edge newEdge = new Edge((EventsNode) newNode, edge.getTarget(),
						Double.parseDouble(edge.getLabel()));
				edge.getTarget().removeInEdge(edge);
				edge.getTarget().addInEdge(newEdge);
				newNode.addOutEdge(newEdge);
				net.addEdge(newEdge);
				}
			}
			// inserisco un arco in cui il nuovo nodo � il destinatario
			for (DirectedGraphEdgeImpl<?, ?> edge : n.getInEdge()) {

				remove.add((Edge) edge);
				if(edge.getSource().compareTo(newNode)!=0) {
				Edge newEdge = new Edge(edge.getSource(), (EventsNode) newNode,
						Double.parseDouble(edge.getLabel()));
				edge.getSource().addOutEdge(newEdge);
				edge.getSource().removeOutEdge(edge);
				newNode.addInEdge(newEdge);
				net.addEdge(newEdge);
				}
			}
			
			ArrayList<Pair<Integer, Integer>> a = n.getEvent().getOccurs();
			
			newNode.getEvent().addOccurs(a, label);
			
			net.removeNode(n);
		}
		// rimuovo i vecchi archi
		for (Edge edge : remove) {
			net.removeEdge(edge);
		}

		return newNode;
	}

	
	/**
	 * Marca il nodo nella tabella delle astrazioni del modello di processo in input
	 * @param currentGraph: Modello di processo
	 * @param nodeToAbstract: Nodo d'astrarre
	 */
	protected static void addMark(GraphAndOr currentGraph, DefaultMutableTreeNode nodeToAbstract) {
		currentGraph.markedAbstraction(nodeToAbstract.toString());
	}
	

	/**
	 * Restituisce una HashMap contenente <Tracce, Lista di lista di nodi> che non si sovrappongono
	 * @param selective: HashMap contenente le differenti tracce 
	 * @return Restituisce una HashMap contenente <Tracce, Lista di lista di nodi> che non si sovrappongono
	 */
	protected static HashMap<Set<Integer>, ArrayList<Set<Node>>> notOverlapped(HashMap<Set<Integer>, Set<Node>> selective) {
		
		selective = sortByValue(selective);
		HashMap<Set<Integer>, ArrayList<Set<Node>>> notOver = new HashMap<Set<Integer>, ArrayList<Set<Node>>>();
		Set<Node> tmp_node;
		Set<Integer> tmp_index;
		ArrayList<Set<Node>> tmp_array;
		int index = 0;
		
		//Ciclo su tutte le selective
		for(Entry<Set<Integer>, Set<Node>> entry_selective:selective.entrySet()) {
			
			tmp_node = new TreeSet<Node>();
			tmp_index = new TreeSet<Integer>();
			tmp_array = new ArrayList<Set<Node>>();
			
			tmp_node.addAll(entry_selective.getValue());
			tmp_array.add(entry_selective.getValue());
			tmp_index.add(index);

			int index_inside = 0;
			
			//Per ogni selective
			for(Entry<Set<Integer>, Set<Node>> entry:selective.entrySet()) {
				
				boolean overlap = false;
				
				for(Node node:entry.getValue()) {
					// se contiene gi� il nodo lascia stare
					if(tmp_node.contains(node))
						overlap = true;
				}
				// Se non � presente � buono per le astrazioni
				if(!overlap) {
					tmp_node.addAll(entry.getValue());
					tmp_index.add(index_inside);
					tmp_array.add(entry.getValue());
				}

				index_inside += 1;
			}

			notOver.putIfAbsent(tmp_index, tmp_array);
			index += 1;
		}
		
		return notOver;
	}
	

	protected static HashMap<Set<Integer>, Set<Node>> gestioneConflitti(HashMap<Set<Integer>, Set<Node>> selective) {
		
		//Viene effettuato un doppio ciclo su HashMap selective
		HashMap<Set<Integer>,Set<Node>> conflitti = new HashMap<Set<Integer>,Set<Node>>();
		Iterator<Entry<Set<Integer>, Set<Node>>> it = selective.entrySet().iterator();
		HashSet<Set<Integer>> abst_parz = new HashSet<Set<Integer>>();
		//Per ogni coppia <Tracce, Nodi>
		while(it.hasNext()) {				
				Map.Entry<Set<Integer>,Set<Node>> entry = it.next();
				//Recupera i nodi
				Set<Node> path = entry.getValue();
				//Evita i duplicati
				Set<Node> path_new = new TreeSet<Node>();
				//Astrazioni parziali
				//Itera ed effettua un merge per discretizzare i percorsi da astrarre
				Iterator<Entry<Set<Integer>, Set<Node>>> it_interno = selective.entrySet().iterator();
				//Per ogni nodo della HashMap in input
				while(it_interno.hasNext()) {			
					//Recupera l'entry
					Map.Entry<Set<Integer>,Set<Node>> altra_entry = it_interno.next();
					//Se la chiave contiene la chiave del ciclo esterno
					if(altra_entry.getKey().containsAll(entry.getKey()) && !altra_entry.getKey().equals(entry.getKey())) {
						//Allora aggiungi al percorso attuale i nodi della entry di questo ciclo
						//path.addAll(altra_entry.getValue());
						abst_parz.add(altra_entry.getKey());

						/* Nuova parte */
						for(Node node_out: altra_entry.getValue()) {
							boolean exits = false;
							for(Node node_in: entry.getValue()) 
								if(node_out.getLabel().equals(node_in.getLabel())) 
										exits = true;
							if(!exits)
								path_new.add(node_out);
						}
						
						
					}	
				}
				/* Nuova parte */
				path.addAll(path_new);
				
				if(!abst_parz.contains(entry.getKey()))
					// Aggiungi <Tracce, nodi>
					conflitti.put(entry.getKey(), path);
			}

		return conflitti;
	}


	/**
	 * Differenzia i percorsi dato un set di nodi in base alle tracce
	 * @param nodes: Set di nodi sul quale differenziare le tracce
	 * @return Restituisce una HashMap contenente <Tracce, Set di nodi>
	 */
	protected static HashMap<Set<Integer>,Set<Node>> differenziaPercorsi(Set<Node> nodes) {

		HashMap<Set<Integer>,Set<Node>> selective = new HashMap<Set<Integer>,Set<Node>>();
		for(Node node:nodes)
		{
			// prendo le tracce
			Set<Integer> tt = node.getTraces();
			// controllo che la traccia sia gi� inserita nel HashMap 
			if(selective.containsKey(tt)){
				// Se inserita prendo il set di nodi gi� inseriti
				Set<Node> tmp = selective.get(tt);
				// Aggiungo il nodo analizzato
				tmp.add(node);
				// aggiorno la Hashmap
				selective.put(tt, tmp);
			}
			else {
				// Creo un set di nodi
				Set<Node> tmp = new HashSet<Node>();
				// Aggiungo l'attuale nodo 
				tmp.add(node);
				// Inserisco la nuova entry nel HashMap
				selective.put(tt, tmp);
			}
		}
		return selective;
	}
	
	
	private static <K, V extends Comparable<? super V>> HashMap<Set<Integer>, Set<Node>> sortByValue(HashMap<Set<Integer>, Set<Node>> selective){
		
		List<Entry<Set<Integer>, Set<Node>>> list = new ArrayList<>(selective.entrySet());
		list.sort(Entry.comparingByValue(new NodeComparator()));
		
		LinkedHashMap<Set<Integer>, Set<Node>> result = new LinkedHashMap<>();
		for(Entry<Set<Integer>, Set<Node>> entry:list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
		
	}
	
	static class NodeComparator implements Comparator<Set<Node>>{

		@Override
		public int compare(Set<Node> s1, Set<Node> s2) {

			if (s1.size()<s2.size())
				return 1;
			else{
				if (s1.size()>s2.size())
					return -1;
			return 0;
			}
		}
		
	}
	
	
}
