package org.processmining.plugins.myminer.traceRetrieval;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import org.processmining.models.andOrGraph.DirectedGraphEdgeImpl;
import org.processmining.models.andOrGraph.Edge;
import org.processmining.models.andOrGraph.Event;
import org.processmining.models.andOrGraph.EventsNode;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.andOrGraph.GraphAndOrInterface;
import org.processmining.models.andOrGraph.Node;
import org.processmining.models.andOrGraph.Pair;
import org.processmining.models.graphbased.NodeID;
import org.processmining.plugins.myminer.fsm.NonDeterministicFSM_Thomson;
import org.processmining.plugins.myminer.index.IndexEntry;
import org.processmining.plugins.myminer.index.IndexTable;
import org.processmining.plugins.myminer.index.Match;


public class TraceRetrieval {
	
	
	// metodo di testing 1
	private static void testing(GraphAndOrInterface graph, NonDeterministicFSM_Thomson fsm, IndexTable it) {
		System.out.println("Graph: \n\t" + graph.toString() + "\n\n");
		System.out.println("FSM: \n\t" + fsm.toString() + "\n\n");
		System.out.println("IT: " + 
				"\n\tquery matches: " + it.getMatches().toString() + 
				"\n\ttable: " + it.getTable().toString() +
				"\n\n");
	}
	
	// metodo di testing 2
	private static void printIndexTableTesting(Vector<IndexTable> vector) {
		for(int i = 0; i < vector.size(); i++)
			System.out.println(i + ": " + vector.get(i));
	}

	public static Vector<IndexTable> search_process(GraphAndOrInterface graph,
		NonDeterministicFSM_Thomson fsm,IndexTable it) {
		
		//testing
		//testing(graph, fsm, it);
	
		// insieme dei nodi risultato
		Vector<IndexTable> result=new Vector<IndexTable>();
		// coppia nodo-stato dell`automa
		Vector<Pair<Pair<Node, IndexTable>, String>> searching = new Vector<Pair<Pair<Node, IndexTable>, String>>();
		// prendo la radice del grafo
		Node root = graph.getRoot();
		// estraggo tutti i nodi figli della radice
		String start_state = fsm.getStartState();
	
		Pair<Node, IndexTable> pair = new Pair<Node, IndexTable>(
					root, it);
		searching.add(new Pair<Pair<Node, IndexTable>, String>(pair,
					start_state));
		
		int cicli=0;
		int cicli_tmp = 0;
		
		// finch� ci sono ancora nodi da visitare continuo la ricerca
		while (!searching.isEmpty()) {
			// insieme temporaneo delle coppie nodo-stato automa
			Vector<Pair<Pair<Node, IndexTable>, String>> temp = new Vector<Pair<Pair<Node, IndexTable>, String>>();
			
			cicli += cicli_tmp;
			cicli_tmp = 0;
			
			// per ogni nodo-stato ancora non analizzato lo simulo
			for (Pair<Pair<Node, IndexTable>, String> p : searching) {
				Node node = p.getFirst().getFirst();
				
				cicli_tmp += 1;
				//System.out.print(" "+aaa);
				
				
				for (String perm : node.getEvent().getPermutations()) {
					Vector<String> v=new Vector<String>(Arrays.asList(perm.split("&&")));
				
					Set<Pair<String,IndexTable>> new_states = fsm.simulate(node, v, p.getSecond(),p.getFirst().getSecond());
					// se ho nuovi stati
					if (new_states != null) {
						// se � una foglia e lo stato � finale allora il nodo lo
						// inserisco nel risultato finale
						if (node.isLeaf()) {
							IndexTable newResult;
							if ((newResult=fsm.getResult(new_states))!=null) 
								result.add(newResult);
								
							// se non � una foglia allora inserisco questa
							// coppia nella lista delle coppie da analizzare
						} else {
							for ( DirectedGraphEdgeImpl e : node.getOutEdge()) {
								for (Pair<String,IndexTable> state : new_states) {
									Pair<Node, IndexTable> newPair = new Pair<Node, IndexTable>(
										e.getTarget(), state.getSecond());
									temp.add(new Pair<Pair<Node, IndexTable>, String>(
											newPair, state.getFirst()));
								}
							}
						}
					}
				}
			}

			// preparo la lista per il prossimo ciclo
			searching = temp;
		}
		
		//testing
//		System.out.println("\n\n Struttura dati risultato search: \n\n");		
//		printIndexTableTesting(result);
		
//		System.out.println("Cicli "+cicli);
		
		return result;
	}

	public static void mergeStrong(Vector<IndexTable> sol, GraphAndOr g){
		
		//testing
//		System.out.println("\n\n Struttura dati risultato merge strong: \n\n");
//		printIndexTableTesting(sol);
		
		//se ho meno di 2 soluzioni non devo fondere niente
		if (sol.size()<2) return;
		Set<Node> old_nodes=new HashSet<Node>();
		Set<DirectedGraphEdgeImpl> edgesToDelete = new HashSet<DirectedGraphEdgeImpl>();
		//scorro tutti i pattern della query
		for (int i=0;i<sol.get(0).size();i++){
			IndexEntry ie=sol.get(0).getQueryPattern(i);
			//se � un parte di query specificata devo fonderla, altrimenti lo salto
			if (!ie.isDelay()){
				Set<Node> new_nodes=new HashSet<Node>();
				Node n=null;
				//per ogni soluzione devo aggiungere gli archi al nuovo nodo
				//aggiungere all'insieme quelli da eliminare 
				//trovare gli scarti che verranno aggiunti come nuovi nodi
				for (int j=0;j<sol.size();j++){
					
					Collection<Match> matches=sol.get(j).getQueryMatches(i);
					//nodi coinvolti nel match del pattern
					Collection<Node> nodeSol=getNodes(matches);
					//insieme delle attivit� coinvolte nel match del pattern
					Collection<String> actSet=getActivity(nodeSol);
					
					Set<String> elemPattern = ie.getElementQuery();
					if (elemPattern.containsAll(actSet))
						n=getNode(new_nodes,elemPattern);
					else if (actSet.containsAll(elemPattern))
						n=getNode(new_nodes,actSet);
					else if (isDisjoint(actSet,elemPattern))
						n=getNode(new_nodes,union(actSet,elemPattern));
					g.addNode(n);
					//ridireziono gli archi
					for (Node ns:nodeSol){
						//controllo tutti gli archi uscenti
						for (DirectedGraphEdgeImpl e:ns.getOutEdge()){
							//se il nodo target � in questo match allora l'arco lo devo eliminare
							if (nodeSol.contains(e.getTarget()))
								edgesToDelete.add(e);
							//altrimenti connetto il nuovo nodo al target
							else
							{
								Edge edge= new Edge((EventsNode) n,e.getTarget(),0,true);
								if (g.addEdge(edge)){
									n.addOutEdge(edge);
									e.getTarget().addInEdge(edge);
								}
								e.getTarget().removeInEdge(e);
							}
						}
						//controllo gli archi entranti
						for (DirectedGraphEdgeImpl e:ns.getInEdge()){
						//se il nodo sorgente � in questo match allora devo eliminare l'arco
							if (nodeSol.contains(e.getSource()))
								edgesToDelete.add(e);
							//altrimenti connetto la sorgente al nuovo nodo creato
							else
							{
								Edge edge= new Edge(e.getSource(),(EventsNode) n,0,true);
								if (g.addEdge((Edge) edge)){
									e.getSource().addOutEdge(edge);
									n.addInEdge(edge);
								}
								e.getSource().removeOutEdge(e);
							}
						}
						new_nodes.add(n);
						old_nodes.addAll(nodeSol);
					}
			}
		}
			}
		g.removeEdges(edgesToDelete);
		//elimino tutti i nodi che facevano parte delle soluzioni
		for (Node node:old_nodes){
			Set<DirectedGraphEdgeImpl> e = node.getInEdge();
			g.removeEdges(e);
			e=node.getOutEdge();
			g.removeEdges(e);
			g.removeNode(node);
		}
	}
	
	public static void mergeWeak(Vector<IndexTable> sol, GraphAndOr g){	
		
		//testing
//		System.out.println("\n\n Struttura soluzioni input merge weak: \n\n");
//		printIndexTableTesting(sol);
		
		//se ho meno di 2 soluzioni non devo fondere niente
		if (sol.size()<2) return;
		Set<Node> old_nodes=new HashSet<Node>();
		Set<DirectedGraphEdgeImpl> edgesToDelete = new HashSet<DirectedGraphEdgeImpl>();
		//scorro tutti i pattern della query
		for (int i=0;i<sol.get(0).size();i++){
			IndexEntry ie=sol.get(0).getQueryPattern(i);
			//se � un parte di query specificata devo fonderla, altrimenti lo salto
			if (!ie.isDelay()){
				Set<ArrayList<Node>> new_nodes=new HashSet<ArrayList<Node>>();
				Node n=null;
				//per ogni soluzione devo aggiungere gli archi al nuovo nodo
				//aggiungere all'insieme quelli da eliminare 
				//trovare gli scarti che verranno aggiunti come nuovi nodi
				for (int j=0;j<sol.size();j++){
					
					Collection<Match> matches=sol.get(j).getQueryMatches(i);
					ArrayList<Node> path=getPath(matches);
					//nodi coinvolti nel match del pattern
					ArrayList<Node> p=getPaths(new_nodes,path,g);
					edgesToDelete.addAll(redirect(path,p,g));
					new_nodes.add(p);
					old_nodes.addAll(path);
				}
			}
		}
		
		
		g.removeEdges(edgesToDelete);
		//elimino tutti i nodi che facevano parte delle soluzioni
		for (Node node:old_nodes){
			Set<DirectedGraphEdgeImpl> e = node.getInEdge();
			g.removeEdges(e);
			e=node.getOutEdge();
			g.removeEdges(e);
			g.removeNode(node);
		}
	}
	
	private static ArrayList<Node> getPath(Collection<Match> matches) {
		ArrayList<Node> result=new ArrayList<Node>();
		for (Match m:matches)
			result.add(m.getNode());
		return result;
	}

	private static Collection<? extends DirectedGraphEdgeImpl> redirect(
		ArrayList<Node> path, ArrayList<Node> p,GraphAndOr g) {

		Set<DirectedGraphEdgeImpl> result= new HashSet<DirectedGraphEdgeImpl>();
		
		Iterator<Node> it = p.iterator();
	
		for (Node n:path){
			Node newN=it.next();
			//controllo tutti gli archi uscenti
			for (DirectedGraphEdgeImpl e:n.getOutEdge()){
				//se il nodo target � in questo match allora l'arco lo devo eliminare
				if (path.contains(e.getTarget())){
					result.add(e);
				}
					
					
				//altrimenti connetto il nuovo nodo al target
				else
				{
					Edge edge= new Edge((EventsNode) newN,e.getTarget(),0,true);
					if (g.addEdge(edge)){
						newN.addOutEdge(edge);
						e.getTarget().addInEdge(edge);
					}
					e.getTarget().removeInEdge(e);
				}
			}
			//controllo gli archi entranti
			for (DirectedGraphEdgeImpl e:n.getInEdge()){
			//se il nodo sorgente � in questo match allora devo eliminare l'arco
				if (path.contains(e.getSource()))
					result.add(e);
				//altrimenti connetto la sorgente al nuovo nodo creato
				else
				{
					Edge edge= new Edge(e.getSource(),(EventsNode) newN,0,true);
					if (g.addEdge((Edge) edge)){
						e.getSource().addOutEdge(edge);
						newN.addInEdge(edge);
					}
					e.getSource().removeOutEdge(e);
				}
			}
		}
		
		return result;
	}

	private static ArrayList<Node> getPaths(Set<ArrayList<Node>> new_nodes,
		ArrayList<Node> path,GraphAndOr g) {
	
		for (ArrayList<Node> n:new_nodes){
			int i=0;
			while (i<n.size()){
				if (n.get(i).getLabel().equals(path.get(i).getLabel()))
					i++;
				else break;
			}
			if (i==n.size())
				return n;
		}
		//costruisco il path se non c'era nell'arraylist
		ArrayList<Node> newPath=new ArrayList<Node>();
		for (Node n:path){
			Node newNode=makeNode(n.getEvent().getEvents());
			newPath.add(newNode);
			g.addNode(newNode);
		}
		for (int i=0;i<newPath.size()-1;i++){
			Edge edge= new Edge((EventsNode) newPath.get(i),(EventsNode) newPath.get(i+1),0,true);
			if (g.addEdge(edge)){
				newPath.get(i).addOutEdge(edge);
				newPath.get(i+1).addInEdge(edge);
			}
		}
		
		return newPath;
	}

	private static Collection<String> getActivity(Collection<Node> nodeSol) {
		Set<String> result=new TreeSet<String>();
		for (Node n:nodeSol)
			result.addAll(n.getEvent().getEvents());
		return result;
	}

	private static Collection<Node> getNodes(Collection<Match> matches) {
		Set<Node> result=new HashSet<Node>();
		for (Match m:matches)
			result.add(m.getNode());
		return result;
	}

	private static Collection<String> union(Collection<String> actSet,
		Set<String> elemPattern) {
		Set<String> result=new HashSet<String>();
		
		result.addAll(actSet);
		result.addAll(elemPattern);
		return result;
	}

	private static boolean isDisjoint(Collection<String> actSet,
			Set<String> elemPattern) {
		for (String s:actSet){
			if (elemPattern.contains(s))
				return true;
		}
		return false;
	}

	private static Node getNode(Set<Node> new_nodes, Collection<String> actSet) {
		
		for (Node n:new_nodes){
			if (n.getEvent().getEvents().size()==actSet.size() && 
					n.getEvent().getEvents().containsAll(actSet))
				return n;
		}
		
		return makeNode(actSet);
	}

	private static void updateReference(Map<NodeID, Set<Match>> mapMatch,Node d, Node matchNode, Node n) {
		Set<Match> set = mapMatch.get(matchNode.getId());
		Collection<String> evDiff = d.getEvent().getEvents();
		for (Match m:set){
			Collection<String> ev1 = m.getElement();
			if (ev1.containsAll(evDiff))
				m.setNode(d);
			else m.setNode(n);
		}
	}

	private static Map<NodeID, Set<Match>> createMapMatch(Vector<IndexTable> sol) {
		Map<NodeID,Set<Match>> mapMatch=new HashMap<NodeID, Set<Match>>();
		for (IndexTable it:sol){
			for (Match m:it.getMatches()){
				Node n=m.getNode();
				Set<Match> set = mapMatch.get(n.getId());
				if (set==null)
					set=new HashSet<Match>();
				set.add(m);
				mapMatch.put(n.getId(),set);
			}
		}
		return mapMatch;
	}

	private static Node retrieveNode(Set<String> elementQuery,Vector<IndexTable> sol, int i) {		
		for (int j=0;j<sol.size();j++){
			Vector<Match> matches=sol.get(j).getQueryMatches(i);
			Node n= matches.firstElement().getNode();
			if (n.getEvent().getEvents().equals(elementQuery)){
				return n;
			}
		}
		return null;
	}

	private static Node makeNode(Collection<String> elements) {
		String name="";
		for(String s: elements)
			name+=s+"&&";
		Node n=new EventsNode(new Event(name.substring(0,name.length()-2)), false, 0);
		return n;
	}

}
