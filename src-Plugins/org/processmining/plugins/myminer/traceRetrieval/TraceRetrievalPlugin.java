package org.processmining.plugins.myminer.traceRetrieval;

import java.util.Vector;

import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.util.ui.widgets.helper.ProMUIHelper;
import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.models.andOrGraph.GraphAndOr;


import org.processmining.plugins.myminer.Merge.RetrievalSolution;
import org.processmining.plugins.myminer.fsm.NonDeterministicFSM_Thomson;
import org.processmining.plugins.myminer.index.IndexTable;



public class TraceRetrievalPlugin {
	
@Plugin(name = "Trace Retrieval",
		parameterLabels = {"Model"},
		returnLabels = {"Model","Path Retrieved"},
		returnTypes = {GraphAndOr.class,RetrievalSolution.class},
		userAccessible = true,
		help = "trace retrieval")
				
@UITopiaVariant(affiliation = "UniversitÓ di Torino", author = "Luca Canensi", email = "canensi@di.unito.it")
			
	public static Object[] retrieve(UIPluginContext context ,GraphAndOr graph) throws UserCancelledException{

	String query = ProMUIHelper.queryForString(context,"Query","(min,max)Eventi divisi da virgola o &(min,max)....(min,max)","(2,5)C(5,10)");
	
	NonDeterministicFSM_Thomson nfa=null;
	
	Vector<IndexTable> res = null ;
	
	String regEx = query;
	
	IndexTable it =new IndexTable();
	nfa=new NonDeterministicFSM_Thomson(regEx,it);
//	System.out.println(nfa.toString());
	GraphAndOr g=graph.clone();
	g.reset();
	//graph.reset();
	res = TraceRetrieval.search_process(g, nfa,it);
	for (IndexTable r:res){
		r.setResult();
	}
	//System.out.println(res);
//	nfa.reset();
//	
//	TraceRetrieval.merge(res, g);
//	g.stampaDettagliato();
//	g.simplifies();

	Object[] ret=new Object[2];
	ret[0]=g;
	ret[1]=new RetrievalSolution();
	((RetrievalSolution)ret[1]).setSol(res);
	return ret;
	//return g;
	}
}