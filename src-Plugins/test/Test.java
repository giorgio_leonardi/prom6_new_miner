package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XLog;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.plugins.heuristicsnet.miner.genetic.miner.GeneticMiner;
import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.HeuristicsMiner;
import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.settings.HeuristicsMinerSettings;
import org.processmining.plugins.myminer.miner.ChAPMaNSettings;
import org.processmining.plugins.myminer.miner.MyMiner;

public class Test {

	private static String path = ".\\log\\";

//	private static String[] filenames = {"logSample.mxml","logSample2k.mxml","logSample4k.mxml","logSample8k.mxml","logSample16k.mxml"};
	private static String[] filenames = {"ch2012A.mxml"};

	static int nIter = 1000;
	
	static double[] time = new double[nIter];
	static int daElim = 100;
	
	public static XLog readMXMLLog(String fileName) throws Exception {
		XLog result = null;

		System.out.println(fileName);
		XMxmlParser parser = new XMxmlParser();
		File fl = new File(fileName);
		List<XLog> logs = parser.parse(fl);
		if (logs != null) {
			if (!logs.isEmpty())
				result = logs.get(0);
		}

		return result;
	}

	public static void main(String[] args) throws Exception {
		XLog log = null;
		

		FileWriter outFile;
		outFile = new FileWriter(path+"Risultati.csv");

		BufferedWriter outBuffer;
		outBuffer = new BufferedWriter(outFile);
		// intestazione csv
		outBuffer.write("RISULTATI SPERIMENTAZIONE in ms;;\n\n");

		//intestazione colonne
		outBuffer.write("DIMENSIONE;ChAPMaN;Heuristic;Genetic\n");
		
			
		for (int j=0;j<filenames.length;j++){
			/*chapman*/
			log=readMXMLLog(path+filenames[j]);
			outBuffer.write(filenames[j] + ";");
			//System.out.println("Chapman");
			double ms;
			ms=ChAPMaNtest(log);
			System.out.println("Tempo per il mining ChAPMaN:"+filenames[j]+" " +ms+" ms");
			outBuffer.write(" "+Double.toString(ms)+";");
			/*heuristic miner*/
			log=readMXMLLog(path+filenames[j]);
			ms=HMtest(log);
			System.out.println("Tempo per il mining HM:"+filenames[j]+" " +ms+" ms");
			outBuffer.write(" "+Double.toString(ms)+";");
			/*genetic miner*/
//			log=readMXMLLog(path+filenames[j]);
//			ms=Genetictest(log);
//			System.out.println("Tempo per il mining Genetic:"+filenames[j]+" " +ms+" ms");
			//outBuffer.write(" "+Double.toString(ms)+"\n");
		}
		outBuffer.flush();
		outBuffer.close();
	
	}
	/*chapman*/
	public static double ChAPMaNtest(XLog log){
		ChAPMaNSettings ChAPMaNSettings = new ChAPMaNSettings();
		for (int i=0; i<nIter;i++){
			MyMiner m = new MyMiner(log, ChAPMaNSettings);
			long start = System.nanoTime();
			GraphAndOr g = m.mine();

			//MergingStep ms = new MergingStep(g);

			//g = ms.merge();
			long end=System.nanoTime();
			time[i]=end-start;
			//System.out.println("Iterazione: "+i);
		}
		double sum = 0;
		for (int i=daElim;i<nIter;i++){
			sum+=time[i];
		}
		double ms=sum/(nIter-daElim)/1000000;
		return ms;
	}
	
	public static double HMtest(XLog log){
		XLogInfo loginfo = XLogInfoFactory.createLogInfo(log);
		
		HeuristicsMinerSettings hmset=new HeuristicsMinerSettings();
				
		hmset.setClassifier(loginfo.getEventClasses().getClassifier());
		for (int i=0; i<nIter;i++){
			
			HeuristicsMiner hm=new HeuristicsMiner(null,log,loginfo,hmset);
			long start = System.nanoTime();
			HeuristicsNet hnet = hm.mine();
			long end=System.nanoTime();
			time[i]=end-start;
			//System.out.println("Iterazione: "+i);
		}
		double sum = 0;
		for (int i=daElim;i<nIter;i++){
			sum+=time[i];
		}
		double ms = sum/(nIter-daElim)/1000000;
		return ms;
	}
	
	public static double Genetictest(XLog log){
		
		XLogInfo logInfo=XLogInfoFactory.createLogInfo(log);
	
		for (int i=0; i<1;i++){
			
			GeneticMiner gm=new GeneticMiner(null, logInfo);
			long start = System.nanoTime();
			HeuristicsNet[] hn = gm.mine();
			long end=System.nanoTime();
			time[i]=end-start;
			//System.out.println("Iterazione: "+i);
		}
		double sum = 0;
		for (int i=0;i<1;i++){
			sum+=time[i];
		}
		double ms = sum/1000000;
		return ms;
	}
}
