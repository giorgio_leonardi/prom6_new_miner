package test;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.models.andOrGraph.Edge;
import org.processmining.models.andOrGraph.Event;
import org.processmining.models.andOrGraph.EventsNode;
import org.processmining.models.andOrGraph.GraphAndOr;
import org.processmining.models.graphbased.NodeID;
import org.processmining.models.andOrGraph.Node;
import org.processmining.plugins.myminer.VF2.VF2IsomorphismTester;

public class Isomorphism {

	@Plugin(name = "VF2", parameterLabels = { }, returnLabels = { "stringa" }, returnTypes = { String.class }, userAccessible = true)
	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "", email = "")
	
	public static String test(PluginContext context) {
		
		VF2IsomorphismTester test=new VF2IsomorphismTester();
		
		
		GraphAndOr graph=new GraphAndOr("Grafo",false);
		
		Event a=new Event("A");
		Event b=new Event("B");
		Event b2=new Event("B");
		Event c=new Event("C");
		Event d=new Event("D");
		Event e=new Event("E");
		Event c2=new Event("C");
		Event c3=new Event("C");
		
		EventsNode nodeA=new EventsNode(a,false,0);
		
		EventsNode nodeB=new EventsNode(b,false,0);
		
		EventsNode nodeC=new EventsNode(c,false,0);
		
		EventsNode nodeC2=new EventsNode(c2,false,0);
		
		EventsNode nodeB2=new EventsNode(b2,false,0);
		
		EventsNode nodeC3=new EventsNode(c3,false,0);
		
		graph.addNode(nodeA);
		graph.addNode(nodeB);
		graph.addNode(nodeC);
		graph.addNode(nodeC2);
		graph.addNode(nodeB2);
		//graph.addNode(nodeC3);
		
		Edge edge1 = new Edge(nodeA, nodeB);
		nodeA.addOutEdge(edge1);
		nodeB.addInEdge(edge1);
		graph.addEdge(edge1);
		Edge edge2=new Edge(nodeA, nodeC);
		nodeA.addOutEdge(edge2);
		nodeB.addInEdge(edge2);
		graph.addEdge(edge2);
		Edge edge3=new Edge(nodeB, nodeC);
		nodeB.addOutEdge(edge3);
		nodeC.addInEdge(edge3);
		graph.addEdge(edge3);
		Edge edge4=new Edge(nodeC, nodeB2);
		nodeC.addOutEdge(edge4);
		nodeB2.addInEdge(edge4);
		graph.addEdge(edge4);
		
		Edge edge6=new Edge(nodeB2, nodeC);
		nodeB2.addOutEdge(edge6);
		nodeC.addInEdge(edge6);
		graph.addEdge(edge6);
		
		GraphAndOr subgraph=new GraphAndOr("Sottografo",false);
	
		EventsNode nodeCsub=new EventsNode(c,false,0);
	
		EventsNode nodeBsub=new EventsNode(b,false,0);
		
		EventsNode nodeAsub=new EventsNode(a,false,0);
		
		subgraph.addNode(nodeCsub);
		subgraph.addNode(nodeBsub);
		//subgraph.addNode(node3sub);
		Edge edge1sub=new Edge(nodeBsub, nodeCsub);
		nodeBsub.addOutEdge(edge1sub);
		nodeCsub.addInEdge(edge1sub);
		subgraph.addEdge(edge1sub);
//		
		//subgraph.addEdge(new Edge(node1sub, node3sub));
		//subgraph.addEdge(new Edge(node3sub, node2sub));
		
		ArrayList<Map<NodeID, NodeID>> result = test.findIsomorphism(graph, subgraph,false);
		String risultato="";
		for (Map<NodeID, NodeID> elem:result){
			System.out.println("Isomorfismo trovato:");
			for (Entry<NodeID, NodeID> elemMap:elem.entrySet()){
				Node n = graph.getNodeByID(elemMap.getValue());
				risultato+=n.getLabel()+"["+elemMap.getValue()+"]";
				risultato+="--";
				System.out.print("Nodo del sottografo "+n.getLabel()+" ["+n.getId() +"]->");
				n=subgraph.getNodeByID(elemMap.getKey());
				risultato+=n.getLabel()+"["+elemMap.getKey()+"]";
				risultato+=" ";		
				System.out.println("Nodo del grafo "+n.getLabel()+" ["+n.getId() +"]");
			}
			risultato+="\n";
		}
		return risultato;
	}

}
