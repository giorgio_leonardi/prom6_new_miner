package logUtility;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XEvent;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.util.ui.widgets.helper.ProMUIHelper;
import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.plugins.myminer.miner.ParametersPanel;

public class extractNihss {

	@Plugin(name = "Estrai differenza HIHSS", parameterLabels = { "Log" }, returnLabels = { "Log" }, returnTypes = { XLog.class }, userAccessible = true, help = "Estrae un file contenente la classe :'Migliorati rispetto a nihh scale'")
	@UITopiaVariant(affiliation = "UPO", author = "Giorgio Leonardi", email = "giorgio.leonardi@uniupo.it")
	public static XLog duplica(UIPluginContext context, XLog log) {

		XLog result = XFactoryRegistry.instance().currentDefault().createLog(log.getAttributes());
		result.add(log.get(0));

		int i= 0;
		String path = "nihss_tutti.txt";
		try {
			File file = new File(path);
			FileWriter fw = new FileWriter(file);

			for (XTrace t : log) {
				if (t != null) {
					String traceName = t.getAttributes().get("concept:name").toString();

					System.out.println ("Codice della traccia: " + traceName);
					XEvent ev= t.get(0);

					String ospedale = ev.getAttributes().get("l_shortname").toString();
					XAttribute e_nihss_attr= ev.getAttributes().get("e_nihss");
					XAttribute di_nihss_attr= ev.getAttributes().get("di_nihss");

					if ((getLevel(ospedale) == 1) && (e_nihss_attr != null) && (di_nihss_attr != null)) {
						i++;
						String e_nihss_str = e_nihss_attr.toString();
						int e_nihss = Integer.parseInt(e_nihss_str);
						System.out.println ("Codice nihss in emergenza: " + e_nihss);
						
						String di_nihss_str = di_nihss_attr.toString();
						int di_nihss = Integer.parseInt(di_nihss_str);
						System.out.println ("Codice nihss a dimissione: " + di_nihss + "\n");

						int classe= di_nihss - e_nihss;
						if (classe > 0) classe= 1;
						else classe= 0;
						
						fw.write(traceName + "," + classe + "\n");
					}
					else if ((e_nihss_attr == null) | (di_nihss_attr == null)) System.out.println ("Codice nihss mancante!\n");
					else  System.out.println ("Ospedale di livello SC\n");
				}
			}
			
			fw.flush();
			fw.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		} catch (java.lang.IllegalArgumentException ex) {
			System.out.println("\n\n Numero di istanze: " + i);
		}
		
		System.out.println("\n\n Numero di istanze: " + i);
		return result;
	}
	
	public static int getLevel (String ospedale) {
		int livello= 0;
		
		if (ospedale.compareTo("Santa Rita") == 0) livello= 1;
		if (ospedale.compareTo("Ist. Auxologico") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Gallarate") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Lodi") == 0) livello= 1;
		if (ospedale.compareTo("Ist. Mondino") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Merate") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Saronno") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Desio") == 0) livello= 1;
		if (ospedale.compareTo("A.O. S.Anna") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Vimercate") == 0) livello= 1;
		if (ospedale.compareTo("Osp. San Carlo") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Busto Arsizio") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Besta") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Voghera") == 0) livello= 1;
		if (ospedale.compareTo("Riuniti di Bergamo") == 0) livello= 1;
		if (ospedale.compareTo("Fond. Macchi") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Monza") == 0) livello= 1;
		if (ospedale.compareTo("Osp. San Raffaele") == 0) livello= 1;
		if (ospedale.compareTo("Spedali Civili BS") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Manzoni Lecco") == 0) livello= 1;
		if (ospedale.compareTo("Ist. Humanitas") == 0) livello= 1;
		if (ospedale.compareTo("Osp. Niguarda") == 0) livello= 1;
		if (ospedale.compareTo("Policlinico Milano") == 0) livello= 1;
		
		return livello;
	}
}
