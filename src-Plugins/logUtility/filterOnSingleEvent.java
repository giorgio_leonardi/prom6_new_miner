package logUtility;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.XEvent;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.util.ui.widgets.helper.ProMUIHelper;
import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.plugins.myminer.miner.ParametersPanel;

public class filterOnSingleEvent {

	@Plugin(name = "Filter on single event", parameterLabels = { "Log",}, returnLabels = { "Log" }, returnTypes = { XLog.class }, userAccessible = true, help = "")
	@UITopiaVariant(affiliation = "UNIUPO", author = "Giorgio Leonardi", email = "")
	public static XLog filtra(UIPluginContext context, XLog log) {

		String eventName;
		try {
			eventName = ProMUIHelper.queryForString(context,
					"Specify the name of the activity in the traces to be retained");
			XLogInfo info = XLogInfoFactory.createLogInfo(log);
			XLog result = (XLog) log.clone();
			for (int i = 0; i < info.getNumberOfTraces(); i++) {
				result.remove(0);
			}
			XTrace tr;
			for (int i = 0; i < info.getNumberOfTraces(); i++) {
				tr = log.get(i);
				if (toBeRetained (tr, eventName)) {
					result.add(tr);
				}
			}
			return result;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UserCancelledException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	static boolean toBeRetained (XTrace tr, String eventName) {
		boolean result = false;
		
		for (XEvent ev: tr) {
			if (eventName.compareTo(ev.getAttributes().get("concept:name").toString()) == 0) result= true;
		}
		
		return result;
	}
}
