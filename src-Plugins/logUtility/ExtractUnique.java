package logUtility;

import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;

public class ExtractUnique {

	@Plugin(name = "Estrai le tracce uniche", parameterLabels = { "Log",}, returnLabels = { "Log" }, returnTypes = { XLog.class }, userAccessible = true, help = "")
	@UITopiaVariant(affiliation = "UniversitÓ di Torino", author = "Luca Canensi", email = "canensi@di.unito.it")
	public static XLog estrai(UIPluginContext context, XLog log) {

		try {
			XLogInfo info = XLogInfoFactory.createLogInfo(log);
			XLog result = (XLog) log.clone();
			result.clear();
			Set<String> maps=new TreeSet<String>();
			XTrace tr;
			for (int i = 0; i < info.getNumberOfTraces(); i++) {
				
				tr = log.get(i);
				String trace="";
				for (int j=0;j<tr.size();j++){
					 String event=XLogInfoImpl.NAME_CLASSIFIER.getClassIdentity(tr.get(j));
					 trace+=event+"-";
				}
				if (!maps.contains(trace)){
					maps.add(trace);
					result.add(tr);
				}
			}
			return result;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} 
	}
}
