package logUtility;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.util.ui.widgets.helper.ProMUIHelper;
import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.plugins.myminer.miner.ParametersPanel;

public class DuplicateLog {

	@Plugin(name = "Duplica Log", parameterLabels = { "Log",}, returnLabels = { "Log" }, returnTypes = { XLog.class }, userAccessible = true, help = "")
	@UITopiaVariant(affiliation = "UniversitÓ di Torino", author = "Luca Canensi", email = "canensi@di.unito.it")
	public static XLog duplica(UIPluginContext context, XLog log) {

		int nTimes;
		try {
			nTimes = ProMUIHelper.queryForInteger(context,
					"Specify number of repetitions");
			XLogInfo info = XLogInfoFactory.createLogInfo(log);
			XLog result = (XLog) log.clone();
			XTrace tr;
			for (int i = 0; i < info.getNumberOfTraces(); i++) {
				tr = log.get(i);
				for (int j = 0; j < nTimes; j++)
					result.add(tr);
			}
			return result;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UserCancelledException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
